const { writeFileSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '../'));
const version = require(join(ROOT, 'package.json')).version;

const JS_VERSION_PATH = resolve(join(ROOT, 'src', 'package.json'));
content = require(JS_VERSION_PATH);
content.version = version;
writeFileSync(JS_VERSION_PATH, JSON.stringify(content, null, 2), 'UTF-8');
