require('dotenv').config();

const { readdirSync, copyFileSync, lstatSync } = require('fs');
const { join } = require('path');

const { ROOT, fixturesFolder, downloadNode } = require('./utils');

const moduleFolder = join(ROOT, 'node_modules', '@hestia-earth');

const listIds = folder =>
  readdirSync(join(fixturesFolder, folder)).filter(f => lstatSync(join(fixturesFolder, folder, f)).isDirectory());

const updateFixtures = nodeType => Promise.all(listIds(nodeType).map(id => downloadNode(nodeType, id)));

const nodeTypes = readdirSync(fixturesFolder).filter(f => lstatSync(join(fixturesFolder, f)).isDirectory());

const updateConfigs = () => {
  const types = ['Cycle', 'Site', 'ImpactAssessment'];
  for (const type of types) {
    copyFileSync(
      join(moduleFolder, 'engine-config', 'config', `${type}.json`),
      join(fixturesFolder, type.toLowerCase(), 'config.json')
    );
  }
};

const run = async () => {
  // copy files as not dynamically available
  updateConfigs();
  return await Promise.all(nodeTypes.map(updateFixtures));
};

run()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
