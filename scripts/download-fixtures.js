const { downloadNode } = require('./utils');

const [nodeType, nodeId] = process.argv.slice(2);

const run = async () => downloadNode(nodeType, nodeId);

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
