require('dotenv').config();

const { writeFileSync, mkdirSync } = require('fs');
const { resolve, join } = require('path');
const axios = require('axios');

const API_URL = process.env.API_URL || 'https://api.hestia.earth';
const ROOT = resolve(join(__dirname, '../'));
const fixturesFolder = join(ROOT, 'src', 'fixtures');

const getJsonContent = async url => {
  const { data } = await axios.get(
    url,
    process.env.API_TOKEN
      ? {
          headers: {
            'X-ACCESS-TOKEN': process.env.API_TOKEN
          }
        }
      : {}
  );
  return JSON.stringify(data, null, 2);
};

const parseLogLine = line => {
  try {
    const { timestamp, filename, ...v } = JSON.parse(line);
    return JSON.stringify(v);
  } catch (err) {
    console.error('Error parsing line:', err);
    console.error(line);
    return '';
  }
};

const getLogContent = async url => {
  const { data } = await axios.get(url);
  return data.trim().split('\n').map(parseLogLine).join('\n');
};

const updateFilename = {
  'original.jsonld': (type, id) => getJsonContent(`${API_URL}/${type.toLowerCase()}s/${id}?dataState=original`),
  'recalculated.jsonld': (type, id) => getJsonContent(`${API_URL}/${type.toLowerCase()}s/${id}?dataState=recalculated`),
  'run.log': (type, id) => getLogContent(`${API_URL}/${type.toLowerCase()}s/${id}/log?dataState=recalculated`)
};

const updateFile = async (nodeType, id, filename) => {
  const folder = join(fixturesFolder, nodeType, id);
  mkdirSync(folder, { recursive: true });
  const filepath = join(folder, filename);
  try {
    const content = await updateFilename[filename](nodeType, id);
    writeFileSync(filepath.replace('.jsonld', '.json').replace('.log', '.txt'), content, 'utf-8');
  } catch (err) {
    console.error('An error occured while loading', filepath, err.message);
  }
};

const downloadNode = (nodeType, id) => Promise.all(Object.keys(updateFilename).map(f => updateFile(nodeType, id, f)));

module.exports = {
  API_URL,
  ROOT,
  fixturesFolder,
  downloadNode
};
