const { readdirSync, writeFileSync } = require('fs');
const { join } = require('path');

const ROOT = join(__dirname, '..', 'src', 'svg-icons');
const encoding = 'utf-8';

const icons = readdirSync(ROOT).filter(f => f.endsWith('.svg'));

const content = {
  iconImageFiles: icons.map(icon => ({
    iconName: icon.split('.')[0],
    iconPath: `./assets/svg-icons/${icon}`
  })),
  customIcons: []
};

writeFileSync(join(ROOT, 'icons.json'), JSON.stringify(content, null, 2), encoding);
