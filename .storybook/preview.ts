import '@angular/localize/init';
import { ActivatedRoute } from '@angular/router';
import type { Preview } from '@storybook/angular';
import { componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { of } from 'rxjs';

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/
      }
    }
  },
  decorators: [
    componentWrapperDecorator(story => `<div style="padding: 1.5rem" >${story}</div>`),
    moduleMetadata({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of({})
          }
        }
      ]
    })
  ]
};

export default preview;
