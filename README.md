# HESTIA UI Components

[![Pipeline Status](https://gitlab.com/hestia-earth/hestia-ui-components/badges/master/pipeline.svg)](https://gitlab.com/hestia-earth/hestia-ui-components/commits/master)
<a href="https://npmjs.org/package/@hestia-earth/ui-components" title="View this project on NPM"><img src="https://img.shields.io/npm/v/@hestia-earth/ui-components.svg" alt="NPM version" /></a>

## Demo

You can see the components live here: [demo](https://hestia-earth.gitlab.io/hestia-ui-components).

## Dependencies

The supported versions are:

| @hestia-earth/ui-components | @hestia-earth/schema | Angular |
| --------------------------- | -------------------- | ------- |
| 0.8.x                       | == 13.0.0            | 12.x.x  |
| 0.19.x                      | >= 21.0.0 < 22.0.0   | 14.x.x  |
| 0.20.x                      | >= 22.0.0 < 24.3.0   | 15.x.x  |
| 0.21.x                      | >= 22.0.0 < 24.3.0   | 16.x.x  |
| 0.22.x                      | >= 24.3.0            | 16.x.x  |
| 0.23.x                      | >= 25.0.0            | 16.x.x  |
| 0.24.x                      | >= 26.0.0            | 16.x.x  |
| 0.25.x                      | >= 26.0.0            | 16.x.x  |
| 0.26.x                      | >= 26.0.0            | 17.x.x  |
| 0.27.x                      | >= 26.0.0            | 17.x.x  |
| 0.28.x                      | >= 29.0.0            | 18.x.x  |
| 0.30.x                      | >= 30.0.0            | 19.x.x  |

**Note**: since version `0.27`, all components are `standalone` components, so they need to be imported individually.

## Usage

```
npm install @hestia-earth/ui-components
```

```typescript
import { FilesFormComponent } from '@hestia-earth/ui-components';

@Component({
  imports: [FilesFormComponent],
  template: `<he-files-form [node]="node"></he-files-form>`
})
```

## Using locally on the Community Edition

If you are working on the [Community Edition](https://gitlab.com/hestia-earth/hestia-community-edition/ui), we recommend using `npm link` to be able to test the components:

1. In this repository, run `./build.sh` when you make changes to the components
2. In the community repository, in the folder **ui**, run `npm link @hestia-earth/ui-components`

## Storybook

Run storybook with command `npx ng run components:serve-storybook`
