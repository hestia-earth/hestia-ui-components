/* eslint-disable */
/**
 * @class EventEmitter
 *
 * @property {Array} _listeners
 */
export default class EventEmitter {
  private _listeners;
  /**
   * Construct EventEmitter
   *
   * @param listeners
   */
  constructor(listeners = []) {
    this._listeners = new Map(listeners);
  }

  /**
   * Destroys EventEmitter
   */
  destroy() {
    this._listeners = {};
  }

  /**
   * Count listeners registered for the provided eventName
   *
   * @param eventName
   */
  listenerCount(eventName) {
    if (!this._listeners.has(eventName)) {
      return 0;
    }

    return this._listeners.get(eventName).length;
  }

  /**
   * Subscribes on event eventName specified function
   *
   * @param eventName
   * @param listener
   */
  on(eventName, listener) {
    this._addListener(eventName, listener, false);
  }

  /**
   * Subscribes on event name specified function to fire only once
   *
   * @param eventName
   * @param listener
   */
  once(eventName, listener) {
    this._addListener(eventName, listener, true);
  }

  /**
   * Removes event with specified eventName.
   *
   * @param eventName
   */
  off(eventName) {
    this._removeListeners(eventName);
  }

  /**
   * Emits event with specified name and params.
   *
   * @param eventName
   * @param eventArgs
   */
  emit(eventName, ...eventArgs) {
    return this._applyEvents(eventName, eventArgs);
  }

  /**
   * Register a new listener
   *
   * @param eventName
   * @param listener
   * @param once
   */
  _addListener(eventName, listener, once = false) {
    if (Array.isArray(eventName)) {
      eventName.forEach(e => this._addListener(e, listener, once));
    } else {
      eventName = eventName.toString();
      const split = eventName.split(/,|, | /);

      if (split.length > 1) {
        split.forEach(e => this._addListener(e, listener, once));
      } else {
        if (!Array.isArray(this._listeners.get(eventName))) {
          this._listeners.set(eventName, []);
        }

        this._listeners.get(eventName).push({
          once,
          fn: listener
        });
      }
    }
  }

  /**
   *
   * @param eventName
   */
  _removeListeners(eventName: any = null) {
    if (eventName !== null) {
      if (Array.isArray(eventName)) {
        eventName.forEach(e => this._removeListeners(e));
      } else {
        eventName = eventName.toString();
        const split = eventName.split(/,|, | /);

        if (split.length > 1) {
          split.forEach(e => this._removeListeners(e));
        } else {
          this._listeners.delete(eventName);
        }
      }
    } else {
      this._listeners = new Map();
    }
  }

  /**
   * Applies arguments to specified event
   *
   * @param eventName
   * @param eventArguments
   * @protected
   */
  _applyEvents(eventName, eventArguments) {
    let result = eventArguments;

    if (this._listeners.has(eventName)) {
      const listeners = this._listeners.get(eventName);
      const removableListeners = [];

      listeners.forEach((listener, index) => {
        if ((result = listener.fn.apply(null, eventArguments))) {
          if (listener.once) {
            (removableListeners as any).unshift(index);
          }
        }
      });

      removableListeners.forEach(index => {
        listeners.splice(index, 1);
      });

      return result;
    }

    return result[0];
  }
}
