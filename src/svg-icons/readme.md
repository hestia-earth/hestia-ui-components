# Custom icons usage

## How to use custom icons

- Add svg icon to `src/svg-icons` directory (note: fill/stroke color should be currentColor)
- run `npm run generate:icons`
- Use the icon in the component

```html
<svg-icon name="icon-name" />
```
