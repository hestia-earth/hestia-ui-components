import { Component, ChangeDetectionStrategy, inject, model, effect, signal, computed, output } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { UntypedFormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgTemplateOutlet } from '@angular/common';
import { NgbActiveModal, NgbHighlight } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt, faSpinner, faTimes } from '@fortawesome/free-solid-svg-icons';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import { of, zip } from 'rxjs';
import { Bibliography, Source } from '@hestia-earth/schema';

import { HeMendeleyService } from '../../mendeley/mendeley.service';
import { HeSearchService } from '../../search/search.service';

type searchByFields = 'title' | 'documentDOI' | 'scopus';
type Result = Partial<Source> | Partial<Bibliography>;
const limit = 10;

@Component({
  selector: 'he-bibliographies-search-confirm',
  templateUrl: './bibliographies-search-confirm.component.html',
  styleUrls: ['./bibliographies-search-confirm.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FormsModule, ReactiveFormsModule, FaIconComponent, NgbHighlight, NgTemplateOutlet]
})
export class BibliographiesSearchConfirmComponent {
  private readonly formBuilder = inject(UntypedFormBuilder);
  private readonly mendeleyService = inject(HeMendeleyService);
  private readonly searchService = inject(HeSearchService);
  private readonly activeModal = inject(NgbActiveModal, { optional: true });

  protected readonly faTimes = faTimes;
  protected readonly faSpinner = faSpinner;
  protected readonly faExternalLinkAlt = faExternalLinkAlt;

  public readonly search = model('');
  public readonly searchSources = model(true);
  public readonly searchBibliographies = model(true);
  public readonly searchBy = model<searchByFields>('title');

  private readonly searchLimit = computed(
    () => limit / ([this.searchSources(), this.searchBibliographies()].filter(Boolean).length || 1)
  );

  protected readonly closed = output<Result>();

  protected readonly loading = signal(true);
  protected readonly formGroup = this.formBuilder.group({
    search: ['', Validators.required]
  });
  protected readonly searchControl = this.formGroup.get('search');

  private readonly _results = this.searchControl!.valueChanges.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    tap(() => this.loading.set(true)),
    tap(() => this.selectedResult.set(undefined)),
    switchMap(term => (term.length > 1 ? this.runSearch(term) : of([] as any))),
    tap(() => this.loading.set(false))
  );
  protected readonly results = toSignal(this._results);
  protected readonly selectedResult = signal(undefined as Result);
  protected readonly hasResults = computed(() => this.results()?.length > 0);

  constructor() {
    effect(() => {
      const search = this.search();
      this.searchControl!.setValue(search || '');
    });
  }

  private runSearchSources(term: string) {
    const fields = [`bibliography.${this.searchBy}`];
    const includes = ['bibliography.title', 'bibliography.documentDOI', 'bibliography.scopus'];
    return this.searchSources
      ? this.searchService.suggestSource$(term, this.searchLimit(), fields, includes)
      : of([] as Partial<Source>[]);
  }

  private runSearchBibliographies(term: string) {
    // scopus
    const params =
      this.searchBy() === 'title'
        ? { title: term }
        : this.searchBy() === 'documentDOI'
          ? { doi: term }
          : { scopus: term };
    return this.searchBibliographies
      ? this.mendeleyService.find({ ...params, limit: this.searchLimit() }).pipe(map(({ results }) => results))
      : of([] as Partial<Bibliography>[]);
  }

  private runSearch(term: string) {
    return zip(this.runSearchSources(term), this.runSearchBibliographies(term)).pipe(
      catchError(() => of([])),
      map(res => res.flat())
    );
  }

  protected searchFocus(e: Event) {
    e.stopPropagation();
    setTimeout(() => {
      const inputEvent = new Event('input');
      e.target?.dispatchEvent(inputEvent);
    }, 0);
  }

  protected resetSearch() {
    this.searchControl!.setValue('');
  }

  protected confirm() {
    return this.close(this.selectedResult());
  }

  protected cancel() {
    return this.close(null);
  }

  protected close(value) {
    this.closed.emit(value);
    this.activeModal?.close(value);
  }
}
