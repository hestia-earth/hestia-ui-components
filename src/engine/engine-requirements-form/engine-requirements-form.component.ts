import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { KeyValuePipe } from '@angular/common';
import { UntypedFormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { productTermTermType, SiteSiteType, EmissionMethodTier, NodeType, ITermJSONLD } from '@hestia-earth/schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbTypeahead, NgbHighlight } from '@ng-bootstrap/ng-bootstrap';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

import { ICalculationsModelsParams } from '../engine.service';
import { matchType, matchTermType } from '../../search/search.model';
import { HeSearchService, suggestMatchQuery } from '../../search/search.service';
import { KeyToLabelPipe } from '../../common/key-to-label.pipe';

@Component({
  selector: 'he-engine-requirements-form',
  templateUrl: './engine-requirements-form.component.html',
  styleUrls: ['./engine-requirements-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FormsModule, ReactiveFormsModule, KeyValuePipe, NgbTypeahead, FaIconComponent, NgbHighlight, KeyToLabelPipe]
})
export class EngineRequirementsFormComponent {
  private readonly formBuilder = inject(UntypedFormBuilder);
  private readonly searchService = inject(HeSearchService);

  protected readonly faSpinner = faSpinner;

  protected readonly form = this.formBuilder.group({
    productTermId: [undefined],
    productTermType: [undefined],
    tier: [undefined],
    siteType: [undefined]
  });

  protected readonly productTermTermType = productTermTermType.term;
  protected readonly EmissionMethodTier = EmissionMethodTier;
  protected readonly SiteSiteType = SiteSiteType;

  protected readonly suggestingProductTermId = signal(false);
  protected readonly suggestProductTermId = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.suggestingProductTermId.set(true)),
      switchMap(term => (term.length < 1 ? [] : this.suggestTerm(term))),
      tap(() => this.suggestingProductTermId.set(false))
    );
  protected readonly inputFormatterProductTermId = ({ name }: ITermJSONLD) => name;

  private suggestTerm(term: string) {
    return this.searchService.suggest$(term, NodeType.Term, null, {
      bool: {
        must: [
          matchType(NodeType.Term),
          {
            bool: {
              should: suggestMatchQuery(term),
              minimum_should_match: 1
            }
          },
          {
            bool: {
              should: productTermTermType.term.map(matchTermType),
              minimum_should_match: 1
            }
          }
        ]
      }
    });
  }

  /**
   * Return if form is valid.
   */
  public get valid() {
    return this.form ? !this.form.invalid : true;
  }

  public formValue(): ICalculationsModelsParams {
    const { productTermId, ...value } = this.form ? this.form.getRawValue() : { productTermId: undefined };
    return {
      ...value,
      ...(productTermId ? { productTermId: productTermId['@id'] } : {}) // selected as a JSON-LD Object
    };
  }
}
