import { Component, computed, effect, inject, input, model, signal } from '@angular/core';
import { DatePipe } from '@angular/common';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { ICycleJSONLD, IImpactAssessmentJSONLD, JSONLD, jsonldPath, NodeType } from '@hestia-earth/schema';
import { DataState } from '@hestia-earth/api';
import { catchError, distinct, filter, map, mergeMap, tap, toArray } from 'rxjs/operators';
import { combineLatest, from, of } from 'rxjs';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faAngleDown, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { unique } from '@hestia-earth/utils';

import { distinctUntilChangedDeep } from '../../common/rxjs-utils';
import { TimesPipe } from '../../common/times.pipe';
import { SkeletonTextComponent } from '../../common/skeleton-text/skeleton-text.component';
import { NODE_MAX_STAGE, HeNodeService, IRelatedNode, nodeColours } from '../../node/node.service';
import { nodes, HeNodeStoreService, mergeDataWithHeaders } from '../../node/node-store.service';
import { EngineModelsVersionLinkComponent } from '../engine-models-version-link/engine-models-version-link.component';
import { NodeLinkComponent } from '../../node/node-link/node-link.component';

interface IJSONLDExtended extends JSONLD<NodeType> {
  stage?: number;
  maxstage?: number;
}

const nestedNodesByType: {
  [type in NodeType]?: (node: IJSONLDExtended) => (JSONLD<NodeType> | JSONLD<NodeType>[])[];
} = {
  [NodeType.Cycle]: (node: ICycleJSONLD) => [
    node.site,
    node.otherSites,
    node.inputs?.map(input => input.impactAssessment as any)?.flat(),
    node.animals?.flatMap(animal => animal.inputs?.map(input => input.impactAssessment as any)?.flat())
  ],
  [NodeType.ImpactAssessment]: (node: IImpactAssessmentJSONLD) => [node.cycle]
};

const relatedNodesByType: {
  [type in NodeType]?: NodeType[];
} = {
  [NodeType.Site]: [NodeType.Cycle]
};

@Component({
  selector: 'he-engine-models-stage-deep',
  imports: [
    DatePipe,
    FaIconComponent,
    NgbPopoverModule,
    EngineModelsVersionLinkComponent,
    SkeletonTextComponent,
    TimesPipe,
    NodeLinkComponent
  ],
  templateUrl: './engine-models-stage-deep.component.html',
  styleUrl: './engine-models-stage-deep.component.scss'
})
export class EngineModelsStageDeepComponent<T extends NodeType> {
  private readonly nodeService = inject(HeNodeService);
  private readonly nodeStoreService = inject(HeNodeStoreService);

  protected readonly faAngleRight = faAngleRight;
  protected readonly faAngleDown = faAngleDown;

  protected readonly node = input.required<IJSONLDExtended>();

  protected readonly expanded = model(false);
  /**
   * When nesting the components, avoid allowing expansion on the top-level node.
   */
  protected readonly expandedNode = input<IJSONLDExtended>();

  protected readonly id = computed(() => this.node()?.['@id']);
  protected readonly type = computed(() => this.node()?.['@type']);

  protected readonly nodeColour = computed(() => nodeColours[this.type()]);
  protected readonly recalculatedAt = computed(
    () => (this.node() as any)?.updatedAt || (this.node() as any)?.createdAt
  );

  protected readonly loading = signal(true);

  private readonly relatedNodes = signal<(JSONLD<NodeType> | IRelatedNode)[]>([]);
  private readonly relatedNodes$ = toObservable(this.relatedNodes);
  protected readonly nodesLength = toSignal(this.relatedNodes$.pipe(map(nodes => nodes?.length ?? 0)));

  private readonly nodes = toSignal(
    combineLatest([this.relatedNodes$, this.nodeStoreService.nodes$()]).pipe(
      filter(([nodes]) => nodes?.length > 0),
      distinctUntilChangedDeep(),
      tap(() => this.loading.set(true)),
      mergeMap(([nodes, allNodes]) =>
        from(nodes).pipe(
          mergeMap(node => this.findNode(allNodes, node)),
          filter(v => !!v),
          distinct(node => jsonldPath(node['@type'], node['@id'])),
          toArray()
        )
      ),
      tap(() => this.loading.set(false))
    )
  );
  protected readonly expandableNodes = computed(
    () =>
      this.nodes()?.filter(
        n => this.expandedNode()?.['@type'] !== n['@type'] || this.expandedNode()?.['@id'] !== n['@id']
      ) ?? []
  );

  private readonly defaultMaxStage = computed(() => NODE_MAX_STAGE[this.type()]);
  protected readonly stage = computed(() => +this.node().stage || 0);
  protected readonly maxStage = computed(() => +this.node().maxstage || this.defaultMaxStage());
  protected readonly inProgress = computed(() => this.stage() < this.maxStage());

  protected readonly canExpand = computed(() => this.expandableNodes()?.length);

  constructor() {
    effect(() => {
      const node = this.node();
      const nestedNodes = nestedNodesByType[node['@type']]?.(node)?.flat()?.filter(Boolean) ?? [];
      const relatedNodes = relatedNodesByType[node['@type']] ?? [];

      from(relatedNodes)
        .pipe(
          mergeMap(nodeType => this.nodeService.getRelated$(node, nodeType).pipe(mergeMap(({ results }) => results))),
          toArray()
        )
        .subscribe(values => this.relatedNodes.set(unique([...values, ...nestedNodes])));
    });
  }

  private loadNode$(node: JSONLD<NodeType> | IRelatedNode, dataState: DataState) {
    return this.nodeService
      .getWithHeaders$<JSONLD<T>>({
        ...node,
        dataState
      })
      .pipe(map(({ data, headers }) => mergeDataWithHeaders<IJSONLDExtended>(data, headers)));
  }

  private findNode(allNodes: nodes, node: JSONLD<NodeType> | IRelatedNode) {
    const matchedNode = allNodes?.[node['@type']]?.find(v => v?.id === node['@id']);
    return matchedNode
      ? of((matchedNode.recalculated || matchedNode.original) as IJSONLDExtended)
      : this.loadNode$(node, DataState.recalculated).pipe(catchError(() => this.loadNode$(node, DataState.original)));
  }

  protected trackNode(node: IJSONLDExtended) {
    return jsonldPath(node['@type'], node['@id']);
  }
}
