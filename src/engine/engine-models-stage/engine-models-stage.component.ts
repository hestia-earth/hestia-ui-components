import { ChangeDetectionStrategy, Component, computed, inject, input } from '@angular/core';
import { JSONLD, NodeType } from '@hestia-earth/schema';
import { DataState } from '@hestia-earth/api';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { filter, map, mergeMap } from 'rxjs/operators';

import { HeNodeService, NODE_MAX_STAGE } from '../../node';

@Component({
  selector: 'he-engine-models-stage',
  templateUrl: './engine-models-stage.component.html',
  styleUrls: ['./engine-models-stage.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true
})
export class EngineModelsStageComponent {
  private readonly nodeService = inject(HeNodeService);

  protected readonly node = input.required<JSONLD<NodeType>>();

  private readonly defaultMaxStage = computed(() => NODE_MAX_STAGE[this.node()['@type']]);
  private readonly node$ = toObservable(this.node);
  private readonly headers = toSignal(
    this.node$.pipe(
      filter(node => !!node),
      mergeMap(node =>
        this.nodeService.getWithHeaders$({
          ...node,
          dataState: DataState.recalculated
        })
      ),
      map(({ headers }) => headers)
    )
  );

  protected readonly stage = computed(() => +this.headers()?.get('stage'));
  protected readonly maxStage = computed(() => +this.headers()?.get('maxstage') || this.defaultMaxStage());
  protected readonly inProgress = computed(() => this.stage() && this.stage() !== this.maxStage());
}
