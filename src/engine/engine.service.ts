import { Injectable, InjectionToken, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, shareReplay } from 'rxjs/operators';
import {
  EmissionMethodTier,
  JSONLD,
  NodeType,
  SchemaType,
  SiteSiteType,
  TermTermType,
  blankNodesType
} from '@hestia-earth/schema';
import {
  IModel,
  models as engineModels,
  allowedType,
  IOrchestratorConfig,
  IOrchestratorModelConfig,
  loadConfig
} from '@hestia-earth/engine-models';
import { toDashCase } from '@hestia-earth/utils';

import { baseUrl, gitHome, gitBranch, filterParams, Repository } from '../common/utils';

export const HE_CALCULATIONS_BASE_URL = new InjectionToken<string>('HE_CALCULATIONS_BASE_URL');

export const engineGitBaseUrl = () => [gitHome, Repository.models].join('/');
export const engineGitUrl = () => `${engineGitBaseUrl()}/-/blob/${gitBranch()}`;

const customTermApiDocsPath = {
  'excreta-kg-mass': 'excreta-kg'
};

export const pathToApiDocsPath = (model: string, term?: string) =>
  [
    baseUrl(false),
    'docs',
    ['#hestia-calculation-models', toDashCase(model), customTermApiDocsPath[toDashCase(term)] || toDashCase(term)]
      .filter(Boolean)
      .join('-')
  ].join('/');

export interface IModelExtended extends IModel {
  /**
   * Is model from EcoinventV3.
   */
  ecoinvent: boolean;
  /**
   * Path to the API Documentation.
   */
  apiDocsPath?: string;
}

const mapModelLink =
  (ecoinvent = false) =>
  ({ path, docPath, ...link }: IModelExtended) => ({
    ...link,
    ecoinvent,
    path: `${engineGitUrl()}/${path}`,
    docPath: `${engineGitUrl()}/${docPath}`,
    apiDocsPath: pathToApiDocsPath(link.model, link.term || link.modelKey)
  });

const allModels = () => {
  const { links, ecoinventLinks, ecoinventV3AndEmberClimateLinks } = engineModels;
  return [
    ...links.map(mapModelLink(false)),
    ...(ecoinventLinks || []).map(mapModelLink(true)),
    ...(ecoinventV3AndEmberClimateLinks || []).map(mapModelLink(true))
  ];
};

export const models = allModels();

export const findModels = (termId: string) => models.filter(({ term }) => term === termId);

export const findMatchingModel = (model: Partial<IModel>): IModelExtended =>
  Object.keys(model).length > 0
    ? models.flat().find(m => Object.entries(model).every(([key, value]) => value === m[key]))
    : null;

export const modelParams = (node: Partial<blankNodesType>, includeTerm = true, key?: string) =>
  filterParams({
    model: 'methodModel' in node ? node?.methodModel?.['@id'] : undefined,
    modelKey: key,
    ...(includeTerm ? { term: 'term' in node ? node?.term?.['@id'] : undefined } : {})
  });

export const modelKeyParams = (node: Partial<blankNodesType>, key: string) =>
  filterParams({
    model: (node?.['@type'] || node?.type || '').toLowerCase(),
    modelKey: key
  });

export const findNodeModel = (node: Partial<blankNodesType>, key?: string) => {
  const value = findMatchingModel(modelParams(node));
  return !key || value ? value : findMatchingModel(modelKeyParams(node, key));
};

/**
 * Find models from the orchestrator configuration.
 *
 * @param config The orchestrator configuration content.
 * @param termId The `@id` of the Term.
 * @param modelKey The key of the configuration (e.g. "products", "inputs")
 * @param models Optional - list of models from `model-links.json` to default when orchestrator does not contain config.
 * @returns List of models from orchestrator or models if set.
 */
export const findConfigModels = (config: IOrchestratorConfig, termId: string, modelKey: string) => {
  const configModels = config.models
    .flat()
    .filter(({ value, key }) => termId === value && (!modelKey || key === modelKey));
  return configModels.length ? configModels : findModels(termId);
};

export interface ICalculationsModelsParams {
  termType?: SchemaType;
  productTermId?: string;
  productTermType?: TermTermType;
  tier?: EmissionMethodTier;
  siteType?: SiteSiteType;
}

export interface ICalculationsRequirementsParams extends ICalculationsModelsParams {
  format?: 'csv';
}

export interface ICalculationsModel {
  model: string;
  key: string;
}

export const findOrchestratorModel = ({ models }: IOrchestratorConfig, model: Partial<IOrchestratorModelConfig>) =>
  Object.keys(model).length > 0
    ? models.flat().find(m => Object.entries(model).every(([key, value]) => value === m[key]))
    : null;

@Injectable({
  providedIn: 'root'
})
export class HeEngineService {
  private readonly _calculationsBaseUrl = inject(HE_CALCULATIONS_BASE_URL);
  protected readonly http = inject(HttpClient);

  private _recommendations: { [key: string]: Observable<string[]> } = {};
  private _requirements: { [key: string]: Observable<JSONLD<NodeType>> } = {};

  public listModels(params: ICalculationsModelsParams) {
    return this.http
      .get<{ models: ICalculationsModel[] }>(`${this._calculationsBaseUrl}/models`, {
        params: filterParams(params)
      })
      .toPromise();
  }

  public getRequirements$(params: ICalculationsRequirementsParams) {
    const key = JSON.stringify(filterParams(params));
    this._requirements[key] =
      this._requirements[key] ||
      this.http
        .get<JSONLD<NodeType>>(`${this._calculationsBaseUrl}/models/requirements`, {
          params: filterParams(params)
        })
        .pipe(shareReplay(1));
    return this._requirements[key];
  }

  public getRequirements(params: ICalculationsRequirementsParams) {
    return this.getRequirements$(params).toPromise();
  }

  public getTermIds$(params: ICalculationsRequirementsParams) {
    const key = JSON.stringify(filterParams(params));
    this._recommendations[key] =
      this._recommendations[key] ||
      this.http
        .get<string[]>(`${this._calculationsBaseUrl}/models/term-ids`, {
          params: filterParams(params)
        })
        .pipe(shareReplay(1));
    return this._recommendations[key];
  }

  public ochestratorConfig$(type: allowedType, id?: string) {
    return this.http
      .get<IOrchestratorConfig>(`${this._calculationsBaseUrl}/recalculate/config`, {
        params: filterParams({ type, '@type': type, id, '@id': id })
      })
      .pipe(catchError(() => of(loadConfig(type))));
  }
}
