import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { ENGINE_VERSION } from '@hestia-earth/engine-models';

import { nodeVersion } from '../../node/node-model-version';
import { engineGitBaseUrl } from '../engine.service';

@Component({
  selector: 'he-engine-models-version-link',
  templateUrl: './engine-models-version-link.component.html',
  styleUrls: ['./engine-models-version-link.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FaIconComponent]
})
export class EngineModelsVersionLinkComponent {
  protected readonly faExternalLinkAlt = faExternalLinkAlt;

  protected readonly node = input.required<any>();
  protected readonly showDetails = input(false);

  protected readonly ENGINE_VERSION = ENGINE_VERSION;
  protected readonly version = computed(() => nodeVersion(this.node()));
  protected readonly url = computed(() =>
    [engineGitBaseUrl(), '-', 'tree', `v${this.version()}`].filter(Boolean).join('/')
  );
}
