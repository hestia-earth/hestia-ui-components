import { ChangeDetectionStrategy, Component, computed, effect, inject, input, signal } from '@angular/core';
import { NgTemplateOutlet, JsonPipe, KeyValuePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { ITermJSONLD, NodeType, SchemaType } from '@hestia-earth/schema';
import { allowedType, IOrchestratorConfig, IOrchestratorModelConfig, loadConfig } from '@hestia-earth/engine-models';
import { keyToLabel, unique } from '@hestia-earth/utils';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbDropdownModule, NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { faCog, faExternalLinkAlt, faList, faTable, faTimes } from '@fortawesome/free-solid-svg-icons';
import { combineLatest, from } from 'rxjs';
import { filter, groupBy, map, mergeMap, reduce, startWith, toArray } from 'rxjs/operators';

import { IModelExtended, engineGitUrl, findMatchingModel } from '../engine.service';
import { Repository, schemaBaseUrl, gitBranch, gitHome } from '../../common/utils';
import { distinctUntilChangedDeep } from '../../common/rxjs-utils';
import { pluralize } from '../../common/pluralize';
import { KeyToLabelPipe } from '../../common/key-to-label.pipe';
import { HeSearchService } from '../../search/search.service';
import { matchId, matchType } from '../../search/search.model';
import { NodeLinkComponent } from '../../node/node-link/node-link.component';

type configs = (IOrchestratorModelConfig | IOrchestratorModelConfig[])[];

const gitUrl = `${gitHome}/${Repository.orchestrator}/-/blob/${gitBranch()}`;
const generalDocsUrl = `${gitUrl}/hestia_earth/orchestrator/config/README.md`;
const strategiesDocs = `${gitUrl}/hestia_earth/orchestrator/strategies`;

const schemaTypeKeys = Object.keys(SchemaType);

const toSchemaType = (value: string) => keyToLabel(value).replace(/\s/g, '');

const isSchemaType = (value: string) =>
  [
    schemaTypeKeys.includes(value),
    schemaTypeKeys.includes(pluralize(value, 1)),
    schemaTypeKeys.includes(pluralize(toSchemaType(value), 1)),
    ['emissionsResourceUse', 'impacts', 'endpoints'].includes(value)
  ].some(Boolean);

const isGapFilled = ({ runStrategy, mergeStrategy, mergeArgs }: IOrchestratorModelConfig) =>
  ['add_blank_node_if_missing', 'add_key_if_missing'].includes(runStrategy) &&
  (mergeStrategy !== 'list' || !mergeArgs?.replaceThreshold);

const isRecalculated = ({ runStrategy, mergeStrategy, mergeArgs }: IOrchestratorModelConfig) =>
  ['add_blank_node_if_missing', 'add_key_if_missing'].includes(runStrategy) &&
  mergeStrategy === 'list' &&
  mergeArgs?.replaceThreshold;

const filterGapFilled = (configs: configs, onlyGapFilled: boolean) =>
  configs
    .map(model => (Array.isArray(model) && onlyGapFilled ? model.filter(isGapFilled) : model))
    .filter(model => !onlyGapFilled || Array.isArray(model) || isGapFilled(model));

const modelMatch =
  (term: string) =>
  ({ key, model, value }: IOrchestratorModelConfig) =>
    [key, model, value].filter(Boolean).some(v => v.toLowerCase().includes(term.toLowerCase()));

const filterModels = (configs: configs, term = '') =>
  configs
    .map(model => (Array.isArray(model) && !!term ? model.filter(modelMatch(term)) : model))
    .filter(model => !term || Array.isArray(model) || modelMatch(term)(model));

const modelKeyName = (modelKey: string) => (modelKey.includes('.') ? modelKey.split('.')[1] : modelKey);

const groupConfig$ = (configs: configs) =>
  from(configs.flat()).pipe(
    filter(({ key }) => isSchemaType(key)),
    groupBy(({ key }) => key),
    mergeMap(group => group.pipe(toArray())),
    reduce(
      (prev, curr) => {
        const key = curr[0].key;
        prev[toSchemaType(key)] = curr;
        return prev;
      },
      {} as { [key: string]: IOrchestratorModelConfig[] }
    )
  );

const modelKeyUrl = ({ modelKey }: IModelExtended, model: string) => {
  const parts = modelKey.split('.');
  const schemaType = parts.length === 2 ? parts[0] : model;
  const url = [schemaBaseUrl(), toSchemaType(schemaType)].filter(Boolean).join('/');
  return isSchemaType(schemaType) ? `${url}#${parts.pop()}` : null;
};

@Component({
  selector: 'he-engine-orchestrator-edit',
  templateUrl: './engine-orchestrator-edit.component.html',
  styleUrls: ['./engine-orchestrator-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    KeyValuePipe,
    JsonPipe,
    NgbTooltip,
    NgbDropdownModule,
    FaIconComponent,
    FormsModule,
    NgTemplateOutlet,
    NodeLinkComponent,
    KeyToLabelPipe
  ]
})
export class EngineOrchestratorEditComponent {
  private readonly searchService = inject(HeSearchService);

  protected readonly faExternalLinkAlt = faExternalLinkAlt;
  protected readonly faTimes = faTimes;
  protected readonly faCog = faCog;
  protected readonly faList = faList;
  protected readonly faTable = faTable;

  protected readonly config = input<IOrchestratorConfig>();
  protected readonly nodeType = input<allowedType>();

  private readonly allModels = toSignal(
    combineLatest([toObservable(this.config), toObservable(this.nodeType)]).pipe(
      distinctUntilChangedDeep(),
      map(([config, nodeType]) => (config ? config : loadConfig(nodeType))),
      map(({ models }) => models),
      startWith([] as configs)
    )
  );
  private readonly termIds = computed(() =>
    unique(
      this.allModels()
        .flat()
        .flatMap(model => [model.model, model.value])
        .filter(Boolean)
    )
  );
  private readonly terms = toSignal(
    toObservable(this.termIds).pipe(
      mergeMap(ids =>
        this.searchService.search$<ITermJSONLD, NodeType.Term>({
          fields: ['@type', '@id', 'name', 'units'],
          limit: ids.length,
          query: {
            bool: {
              must: [matchType(NodeType.Term)],
              should: ids.map(matchId),
              minimum_should_match: 1
            }
          }
        })
      ),
      map(({ results }) => results)
    )
  );
  protected readonly termsById = computed(() =>
    (this.terms() ?? []).reduce((p, c) => ({ ...p, [c['@id']]: c }), {} as { [id: string]: ITermJSONLD })
  );

  protected readonly configUrl = [gitHome, 'hestia-engine-config'].join('/');

  protected readonly modelKeyName = modelKeyName;
  protected readonly modelKeyUrl = modelKeyUrl;
  protected readonly isArray = Array.isArray;
  protected readonly generalDocsUrl = generalDocsUrl;
  protected readonly strategiesDocs = strategiesDocs;

  protected readonly search = signal('');
  protected readonly onlyGapFilled = signal(false);
  protected readonly showAdvanced = signal(false);
  protected readonly displayBy = signal<'list' | 'type'>('list');

  protected readonly models = computed(() =>
    filterModels(filterGapFilled(this.allModels(), this.onlyGapFilled()), this.search())
  );
  protected readonly groupedModels = toSignal(toObservable(this.models).pipe(mergeMap(groupConfig$)));
  protected readonly selectedGroupedKey = signal<string>(undefined);

  constructor() {
    effect(() => {
      const key = Object.keys(this.groupedModels())?.[0];
      if (!this.selectedGroupedKey() && key) {
        this.selectedGroupedKey.set(key);
      }
    });
  }

  protected findModelLink({ model, value }: IOrchestratorModelConfig) {
    return findMatchingModel({ model, term: value });
  }

  protected findModelPathLink({ model, value }: IOrchestratorModelConfig) {
    return findMatchingModel({
      path: `${engineGitUrl()}/hestia_earth/models/${model}/${(value || '').replace('.', '/')}.py`
    });
  }
}
