import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {
  JSONLD,
  NodeType,
  ICycleJSONLD,
  ISourceJSONLD,
  ITermJSONLD,
  Source,
  SchemaType,
  IImpactAssessmentJSONLD
} from '@hestia-earth/schema';

import { HeCommonService } from '../common/common.service';
import {
  matchType,
  matchPhraseQuery,
  matchPhrasePrefixQuery,
  searchableType,
  searchableTypes,
  matchBoolPrefixQuery,
  matchExactQuery,
  matchNameNormalized
} from './search.model';

export type searchResult = ICycleJSONLD | ISourceJSONLD | IImpactAssessmentJSONLD | ITermJSONLD;

export type sortOrders = 'asc' | 'desc';

interface ISearchParamSort {
  [key: string]: sortOrders;
}

interface ISearchScript {
  _script: {
    type?: string;
    order?: sortOrders;
    script: string;
  };
}

export interface ISearchParams {
  limit?: number;
  offset?: number;
  fields?: string[];
  sort?: ISearchParamSort[] | ISearchScript;
  query?: any;
}

export interface ISearchResultExtended {
  _score: number;
  // geospacial search
  location?: {
    lat: number;
    lon: number;
  };
  // used in search
  properties?: string[];
  expanded?: boolean;
  extended?: boolean;
  pinned?: boolean;
  loading?: boolean;
  selected?: boolean;
  // count for lists
  inputsCount?: number;
  emissionsCount?: number;
  productsCount?: number;
}

export interface ISearchResults<C extends JSONLD<T>, T extends NodeType = searchableType> {
  /**
   * Amount of time the query took
   */
  time: number;
  /**
   * Numnber of results
   */
  count: number;
  results: (C & ISearchResultExtended)[];
}

const emptySearchResult = <C extends JSONLD<T>, T extends NodeType>() =>
  ({
    time: 0,
    count: 0,
    results: []
  }) as ISearchResults<C, T>;

export const MAX_RESULTS = 10000;

export const suggestMatchQuery = (query: string) => [
  matchPhraseQuery(query, 10),
  matchNameNormalized(query, 8),
  matchPhrasePrefixQuery(query, 2)
];

export const suggestQuery = <T extends NodeType = searchableType>(
  query: string,
  type?: T,
  extraQueries: any[] = []
) => ({
  bool: {
    must: extraQueries,
    should: (type ? [type] : searchableTypes).map(v => ({
      bool: {
        must: [matchType(v)],
        should: suggestMatchQuery(query),
        minimum_should_match: 1
      }
    })),
    minimum_should_match: 1
  }
});

const suggestSourceQuery = (query: string, fields: string[]) => ({
  bool: {
    must: [matchType(NodeType.Source)],
    should: [
      matchPhraseQuery(query, 100, ...fields),
      matchPhrasePrefixQuery(query, 20, ...fields),
      matchBoolPrefixQuery(query, 10, ...fields)
    ],
    minimum_should_match: 1
  }
});

@Injectable({
  providedIn: 'root'
})
export class HeSearchService {
  protected http = inject(HttpClient);
  protected commonService = inject(HeCommonService);

  public search$<C extends JSONLD<T>, T extends NodeType = searchableType>(params: ISearchParams) {
    return this.http
      .post<ISearchResults<C, T>>(`${this.commonService.apiBaseUrl}/search`, params)
      .pipe(catchError(() => of(emptySearchResult<C, T>())));
  }

  public async search<C extends JSONLD<T>, T extends NodeType = searchableType>(params: ISearchParams) {
    return await this.search$<C, T>(params).toPromise();
  }

  public count$(params = {}) {
    return this.http.post<number>(`${this.commonService.apiBaseUrl}/count`, params).pipe(catchError(() => of(0)));
  }

  public async count(params = {}) {
    return await this.count$(params).toPromise();
  }

  public get$<C extends JSONLD<T>, T extends NodeType>(type: T, id: string) {
    return this.http
      .post<ISearchResults<C, T>>(`${this.commonService.apiBaseUrl}/search`, {
        limit: 1,
        query: {
          bool: {
            must: [matchType(type), matchExactQuery('@id', id)]
          }
        }
      })
      .pipe(
        catchError(() => of(emptySearchResult<C, T>())),
        map(({ results }) => results[0] || null)
      );
  }

  public async get<C extends JSONLD<T>, T extends NodeType>(type: T, id: string) {
    return await this.get$<C, T>(type, id).toPromise();
  }

  public suggest$<C extends JSONLD<T>, T extends NodeType = searchableType>(
    term: string,
    type?: T,
    extraQueries: any[] = [],
    fullQuery?: any,
    limit = 10,
    includes: string[] = []
  ) {
    const query = fullQuery || suggestQuery(term, type, extraQueries);
    return this.http
      .post<ISearchResults<C, T>>(`${this.commonService.apiBaseUrl}/search`, {
        limit,
        fields: ['name', '@id', type ? '' : '@type', 'termType', 'aggregated', ...includes].filter(val => !!val),
        query
      })
      .pipe(
        catchError(() => of(emptySearchResult<C, T>())),
        map(({ results }) => results.map(({ _score, ...res }) => res))
      );
  }

  public suggest<C extends JSONLD<T>, T extends NodeType = searchableType>(
    term: string,
    type?: T,
    extraQueries: any[] = [],
    fullQuery?: any,
    limit = 10,
    includes: string[] = []
  ) {
    return this.suggest$<C, T>(term, type, extraQueries, fullQuery, limit, includes).toPromise();
  }

  public suggestSource$(term: string, limit = 5, fields = ['bibliography.title'], includes = ['bibliography.title']) {
    return this.suggest$(term, NodeType.Source, [], suggestSourceQuery(term, fields), limit, includes).pipe(
      map((sources: Partial<Source>[]) =>
        sources.map(
          source =>
            ({
              ...source,
              bibliography: {
                // needed to create a nested property
                type: SchemaType.Bibliography,
                ...source.bibliography
              }
            }) as Partial<Source>
        )
      )
    );
  }

  public suggestSource(term: string, limit = 5, fields = ['bibliography.title'], includes = ['bibliography.title']) {
    return this.suggestSource$(term, limit, fields, includes).toPromise();
  }
}
