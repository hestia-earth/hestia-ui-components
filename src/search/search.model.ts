import { NodeType, TermTermType, nestedSearchableKeys, productTermTermType } from '@hestia-earth/schema';

export type searchableType = NodeType.Cycle | NodeType.Source;

export const searchableTypes: searchableType[] = [NodeType.Cycle, NodeType.Source];

type multiMatchType = 'best_fields' | 'most_fields' | 'cross_fields' | 'phrase' | 'phrase_prefix' | 'bool_prefix';
type multiMatchAnalyzer = 'standard';

export const matchType = (type: NodeType | string) =>
  Object.freeze({
    match: { '@type.keyword': type }
  });

export const matchId = (id: string) =>
  Object.freeze({
    match: { '@id.keyword': id }
  });

export const matchTermType = (termType: TermTermType | string) => Object.freeze(matchExactQuery('termType', termType));

export const matchRegex = (key: string, value: string | RegExp) =>
  Object.freeze({
    regexp: { [key]: { value: value.toString() } }
  });

export const matchQuery = (key: string, value: any, boost?: number) =>
  Object.freeze({
    match: {
      [key]: {
        query: value,
        ...(boost ? { boost } : {})
      }
    }
  });

export const matchExactQuery = (key: string, value: any, boost?: number) => matchQuery(`${key}.keyword`, value, boost);

export const matchNameNormalized = (query: string, boost = 20) =>
  Object.freeze({
    match: { nameNormalized: { query, boost } }
  });

export const numberGte = (key: string, value: number) =>
  Object.freeze({
    range: { [key]: { gte: value } }
  });

export const multiMatchQuery = (
  query: string,
  fields: string[],
  type: multiMatchType = 'best_fields',
  boost?: number,
  analyzer?: multiMatchAnalyzer
) =>
  Object.freeze({
    multi_match: {
      query,
      fields,
      type,
      ...(boost ? { boost } : {}),
      ...(analyzer ? { analyzer } : {})
    }
  });

export const matchPhraseQuery = (query: string, boost?: number, ...fields: string[]) =>
  fields.length ? multiMatchQuery(query, fields, 'phrase', boost) : matchExactQuery('name', query, boost);

export const matchPhrasePrefixQuery = (query: string, boost = 2, ...fields: string[]) =>
  fields.length
    ? multiMatchQuery(query, fields, 'phrase_prefix', boost)
    : Object.freeze({
        match_phrase_prefix: {
          nameSearchAsYouType: {
            query,
            boost
          }
        }
      });

export const matchBoolPrefixQuery = (query: string, boost = 1, ...fields: string[]) =>
  fields.length
    ? multiMatchQuery(query, fields, 'bool_prefix', boost)
    : Object.freeze({
        match_bool_prefix: {
          name: {
            query,
            boost
          }
        }
      });

export const wildcardQuery = (query: string, boost = 3, ...fields: string[]) =>
  Object.freeze({
    simple_query_string: {
      query: `*${query}*`,
      fields,
      analyze_wildcard: true,
      boost
    }
  });

const matchCountryLevel = { match: { gadmLevel: 0 } };

export const matchGlobalRegion = { regexp: { '@id': 'region-*' } };

export const matchCountry = Object.freeze({
  bool: {
    should: [matchCountryLevel, matchGlobalRegion],
    minimum_should_match: 1
  }
});

export const countriesQuery = Object.freeze({
  bool: {
    must: [matchType(NodeType.Term), matchTermType(TermTermType.region), matchCountryLevel]
  }
});

export const allCountriesQuery = Object.freeze({
  bool: {
    must: [matchType(NodeType.Term), matchTermType(TermTermType.region)],
    ...matchCountry.bool
  }
});

export const matchRegion = numberGte('gadmLevel', 1);

export const regionsQuery = Object.freeze({
  bool: {
    must: [matchType(NodeType.Term), matchTermType(TermTermType.region), matchRegion]
  }
});

export const cropsQuery = Object.freeze({
  bool: {
    must: [matchType(NodeType.Term), matchTermType(TermTermType.crop)]
  }
});

export const productsQuery = Object.freeze({
  bool: {
    must: [matchType(NodeType.Term)],
    should: productTermTermType.term.map(matchTermType),
    minimum_should_match: 1
  }
});

export const matchAggregatedQuery = matchQuery('aggregated', true);

export const matchAggregatedValidatedQuery = matchQuery('aggregatedDataValidated', true);

const isNestedKey = (key: string) => nestedSearchableKeys.includes(key.split('.')[0]);

export const matchNestedKey = (key: string, query: any) =>
  isNestedKey(key)
    ? {
        nested: { path: key.split('.')[0], query }
      }
    : query;

export const matchPrimaryProductQuery = (product: string) =>
  matchNestedKey('products', {
    bool: {
      must: [matchExactQuery('products.term.name', product), matchQuery('products.primary', true)]
    }
  });
