import type { Meta, StoryObj } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';

import { SELECT_TOKEN, SelectOptionComponent } from '../../select';

const meta: Meta<SelectOptionComponent<string | string[]>> = {
  title: 'Common / Select / Option',
  component: SelectOptionComponent,
  decorators: [
    moduleMetadata({
      providers: [
        {
          provide: SELECT_TOKEN,
          useValue: {
            isMultiple: () => false
          }
        }
      ]
    })
  ],
  argTypes: {},
  args: {},
  render: args => ({
    props: {
      ...args
    },
    template: `
     <he-select-option [class.active]="active" [class.selected]="selected" [value]="1" [staticChecked]="staticChecked" [disabled]="disabled">Option 1</he-select-option>
    `
  })
};

export default meta;
type Story = StoryObj<SelectOptionComponent<string | string[]>>;

export const Single: Story = {
  args: {}
};

export const SingleChecked: Story = {
  args: {
    selected: true
  } as any
};

export const SingleDisabled: Story = {
  args: {
    disabled: true
  }
};

export const SingleActive: Story = {
  args: {
    active: true
  } as any
};

export const Multiple: Story = {
  args: {},
  decorators: [
    moduleMetadata({
      providers: [
        {
          provide: SELECT_TOKEN,
          useValue: {
            isMultiple: () => true
          }
        }
      ]
    })
  ]
};

export const MultipleChecked: Story = {
  ...Multiple,
  args: {
    ...SingleChecked.args,
    staticChecked: true
  }
};

export const MultipleDisabled: Story = {
  ...Multiple,
  args: {
    ...SingleDisabled.args
  }
};

export const MultipleActive: Story = {
  ...Multiple,
  args: {
    ...SingleActive.args
  }
};
