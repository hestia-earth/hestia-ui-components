import { type Meta, type StoryObj } from '@storybook/angular';
import { range } from 'lodash';

import { FilterComponent } from '../../select';

const meta: Meta<FilterComponent<string>> = {
  title: 'Common / Filter',
  component: FilterComponent,

  render: args => ({
    props: {
      ...args
    }
  })
};

export default meta;
type Story = StoryObj<FilterComponent<string>>;

export const Primary: Story = {
  args: {
    showSelectedOnTop: true,
    data: [
      {
        type: 'group',
        label: 'Group 1',
        trackId: 'group-1',
        options: [
          {
            type: 'group',
            label: 'Group 2',
            options: [
              {
                type: 'group',
                label: 'Group 3',
                options: [
                  {
                    type: 'option',
                    label: 'Option 8',
                    value: '8'
                  },
                  {
                    type: 'option',
                    label: 'Option 9',
                    value: '9'
                  }
                ]
              },
              {
                type: 'option',
                label: 'Option 6',
                value: '6'
              },
              {
                type: 'option',
                label: 'Option 7',
                value: '7'
              }
            ]
          },
          {
            type: 'option',
            label: 'Option 4',
            value: '4'
          },
          {
            type: 'option',
            label: 'Option 5',
            value: '5'
          }
        ]
      },
      {
        type: 'group',
        label: 'Group 5',
        options: [
          {
            type: 'option',
            label: 'Option 10',
            value: '10'
          },
          {
            type: 'option',
            label: 'Option 11',
            value: '11'
          }
        ]
      }
    ]
  }
};

export const Flat: Story = {
  args: {
    ...Primary.args,
    data: range(1, 100).map(i => ({
      type: 'option',
      label: `Option ${i}`,
      value: `${i}`
    }))
  }
};

export const Disabled: Story = {
  args: {
    ...Primary.args,
    disabled: true
  }
};

export const WithoutResultsOnTop: Story = {
  args: {
    ...Primary.args,
    showSelectedOnTop: false
  }
};

export const SingleSelect: Story = {
  args: {
    ...Primary.args,
    multiple: false,
    data: [
      {
        type: 'option',
        value: 'products',
        label: 'Product'
      },
      {
        type: 'option',
        value: 'periods',
        label: 'Period'
      },
      {
        type: 'option',
        value: 'minAggregatedQualityScore',
        label: 'Min Aggregated Quality Score'
      },
      {
        type: 'option',
        value: 'regions',
        label: 'Region'
      },
      {
        type: 'option',
        value: 'defaultMethodClassifications',
        label: 'Default Method Classification'
      }
    ]
  }
};

export const GroupedSingleSelect: Story = {
  args: {
    multiple: false,
    ...Primary.args
  }
};

export const LargeData: Story = {
  args: {
    data: [
      {
        type: 'group',
        label: 'Group 1',
        trackId: 'group-1',
        toggleButton: true,
        options: range(100).map(i => ({
          type: 'option',
          label: `Option ${i}`,
          value: `${i} 1`
        }))
      },
      {
        type: 'group',
        label: 'Group 2',
        trackId: 'group-2',
        toggleButton: true,
        options: range(1000).map(i => ({
          type: 'option',
          label: `Option ${i} 2`,
          value: `${i} 2`
        }))
      },
      {
        type: 'group',
        label: 'Group 3',
        trackId: 'group-3',
        toggleButton: true,
        options: range(10000).map(i => ({
          type: 'option',
          label: `Option ${i} 3`,
          value: `${i} 3`
        }))
      },

      {
        type: 'group',
        label: 'Group 4',
        trackId: 'group-4',
        options: range(10000).map(i => ({
          type: 'option',
          label: `Option ${i} 4`,
          value: `${i} 4`
        }))
      }
    ]
  }
};

export const SelectAll: Story = {
  args: {
    data: [
      {
        type: 'group',
        label: 'Group 1',
        trackId: 'group-1',
        toggleButton: true,
        options: [
          {
            type: 'group',
            label: 'Group 2',
            toggleButton: true,
            options: [
              {
                type: 'option',
                label: 'Option 6',
                value: '6'
              },
              {
                type: 'option',
                label: 'Option 7',
                value: '7'
              }
            ]
          },
          {
            type: 'option',
            label: 'Option 4',
            value: '4'
          },
          {
            type: 'option',
            label: 'Option 5',
            value: '5'
          }
        ]
      },
      {
        type: 'group',
        label: 'Group 5',
        options: [
          {
            type: 'option',
            label: 'Option 10',
            value: '10'
          },
          {
            type: 'option',
            label: 'Option 11',
            value: '11'
          }
        ]
      }
    ]
  }
};
