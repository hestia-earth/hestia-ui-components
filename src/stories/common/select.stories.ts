import type { Meta, StoryObj } from '@storybook/angular';

import { SelectComponent } from '../../select';

const _templateWrapper = (content: string) =>
  `
    <he-select [searchable]="searchable" [placeholder]="placeholder" [disabled]="disabled"
                [displayWith]="displayWith" [multiple]="multiple" [value]="value" >
                     ${content}
    </he-select>
`;

const meta: Meta<SelectComponent<string | string[]>> = {
  title: 'Common / Select',
  component: SelectComponent,
  argTypes: {
    searchChanged: { action: 'searchChanged' },
    closed: { action: 'closed' },
    selectionChanged: { action: 'selectionChanged' },
    opened: { action: 'opened' }
  },
  args: {
    placeholder: 'Nothing is selected...'
  },
  render: args => ({
    props: {
      ...args
    },
    template: `


    ${_templateWrapper(`
        <he-select-option [value]="1">Option 1</he-select-option>
        <he-select-option [value]="2">Option 2</he-select-option>
        <he-select-option [value]="3">Option 3</he-select-option>
        <he-select-option [value]="4">Option 4</he-select-option>
        <he-select-option [value]="5">Option 5</he-select-option>
        <he-select-option [value]="6">Option 6</he-select-option>
        <he-select-option [value]="7">Option 7</he-select-option>
        <he-select-option [value]="8">Option 8</he-select-option>
        <he-select-option [value]="9">Option 9</he-select-option>
        <he-select-option [value]="10">Option 10</he-select-option>
        <he-select-option [value]="11">Option 11</he-select-option>
      `)}`
  })
};

export default meta;
type Story = StoryObj<SelectComponent<string | string[]>>;

export const SingleSelect: Story = {
  args: {
    multiple: true
  }
};

export const Searchable: Story = {
  args: {
    searchable: true
  }
};

export const Disabled: Story = {
  args: {
    disabled: true,
    placeholder: 'disabled filter'
  }
};

export const WithSelectedValue: Story = {
  args: {
    multiple: true,
    value: ['1', '2']
  }
};

export const WithDisableOptions: Story = {
  args: {
    searchable: true,
    placeholder: 'Searchable placeholder example'
  },
  render: args => ({
    props: {
      ...args
    },
    template: _templateWrapper(`
            <he-select-option [value]="1">Option 1</he-select-option>
            <he-select-option [value]="2">Option 2</he-select-option>
            <he-select-option [disabled]="true" [value]="3">Option 3</he-select-option>
            <he-select-option [value]="4">Option 4</he-select-option>
            <he-select-option [value]="5">Option 5</he-select-option>
    `)
  })
};

export const WithGroups: Story = {
  args: {
    searchable: true,
    placeholder: 'Searchable placeholder example'
  },
  render: args => ({
    props: {
      ...args,
      multiple: true
    },
    template: _templateWrapper(`
         <he-select-option-group label="Global Comparisons" >
            <he-select-option [value]="1">Option 1</he-select-option>
            <he-select-option [value]="2">Option 2</he-select-option>
            <he-select-option [value]="3">Option 3</he-select-option>
            <he-select-option [value]="4">Option 4</he-select-option>
            <he-select-option [value]="5">Option 5</he-select-option>
        </he-select-option-group>

        <he-select-option-group label="Group 2" >
            <he-select-option [value]="6">Option 6</he-select-option>
            <he-select-option [value]="7">Option 7</he-select-option>
            <he-select-option [value]="8">Option 8</he-select-option>
        </he-select-option-group>

        <he-select-option-group label="Group 3" >
            <he-select-option [value]="9">Option 9</he-select-option>
            <he-select-option [value]="10">Option 10</he-select-option>
            <he-select-option [value]="11">Option 11</he-select-option>
       </he-select-option-group>
    `)
  })
};

export const WithSelectAllButton: Story = {
  args: {
    searchable: true,
    placeholder: 'Searchable placeholder example'
  },
  render: args => ({
    props: {
      ...args,
      multiple: true
    },
    template: _templateWrapper(`
        <he-select-option-group label="Group 1" >
            <he-select-option [toggleGroup] [value]="-1">All</he-select-option>
            <he-select-option [value]="9">Option 9</he-select-option>
            <he-select-option [value]="10">Option 10</he-select-option>
            <he-select-option [value]="11">Option 11</he-select-option>
       </he-select-option-group>
        <he-select-option-group label="Group 2" >
            <he-select-option [toggleGroup] [value]="-2">All</he-select-option>
            <he-select-option [value]="120">Option 10</he-select-option>
            <he-select-option [value]="131">Option 11</he-select-option>
       </he-select-option-group>
    `)
  })
};

export const NesteddGroups2Levels: Story = {
  args: {
    searchable: true,
    placeholder: 'Searchable placeholder example'
  },
  render: args => ({
    props: {
      ...args,
      multiple: true
    },
    template: _templateWrapper(`
        <he-select-option-group label="Group 1" >
            <he-select-option-group label="Group 2" >
                <he-select-option [value]="6">Option 6</he-select-option>
                <he-select-option [value]="7">Option 7</he-select-option>
                <he-select-option [value]="8">Option 8</he-select-option>
            </he-select-option-group>

            <he-select-option-group label="Group 3" >
                <he-select-option [value]="9">Option 9</he-select-option>
                <he-select-option [value]="10">Option 10</he-select-option>
                <he-select-option [value]="11">Option 11</he-select-option>
            </he-select-option-group>
        </he-select-option-group>

        <he-select-option-group label="Group 5" >
            <he-select-option [value]="19">Option 19</he-select-option>
            <he-select-option [value]="102">Option 1 20</he-select-option>
            <he-select-option [value]="131">Option 121</he-select-option>
        </he-select-option-group>
    `)
  })
};

export const NesteddGroups: Story = {
  args: {
    searchable: true,
    placeholder: 'Searchable placeholder example'
  },
  render: args => ({
    props: {
      ...args,
      multiple: true
    },
    template: _templateWrapper(`
        <he-select-option-group label="Group 1" >
           <he-select-option-group label="Group 3" >
              <he-select-option [value]="1">Option 1</he-select-option>
              <he-select-option [value]="2">Option 2</he-select-option>
              <he-select-option [value]="3">Option 3</he-select-option>
           </he-select-option-group>
           <he-select-option-group label="Group 4" >
              <he-select-option [value]="4">Option 4</he-select-option>
              <he-select-option [value]="5">Option 5</he-select-option>
              <he-select-option [value]="6">Option 6</he-select-option>
           </he-select-option-group>
           <he-select-option [value]="7">Option 7</he-select-option>
           <he-select-option [value]="8">Option8</he-select-option>
           <he-select-option-group label="Group 5" >
              <he-select-option [value]="9">Option 9</he-select-option>
              <he-select-option-group label="Group 6" >
                 <he-select-option [value]="10">Option 10</he-select-option>
                 <he-select-option [value]="11">Option 11</he-select-option>
                 <he-select-option [value]="12">Option 12</he-select-option>
              </he-select-option-group>
              <he-select-option-group label="Group 7" >
                 <he-select-option [value]="13">Option 13</he-select-option>
                 <he-select-option [value]="14">Option 14</he-select-option>
                 <he-select-option [value]="15">Option 15</he-select-option>
              </he-select-option-group>
           </he-select-option-group>
        </he-select-option-group>

        <he-select-option-group label="Group 2" >
           <he-select-option [value]="16">Option 16</he-select-option>
           <he-select-option [value]="17">Option 17</he-select-option>
           <he-select-option [value]="18">Option 18</he-select-option>
       </he-select-option-group>
    `)
  })
};

export const WithCustomInputTemplate: Story = {
  args: {
    searchable: false,
    overlayOptions: {
      offsetX: 0,
      offsetY: 0
    },
    menuWidth: '300px'
  },
  render: args => ({
    props: {
      ...args
    },
    template: `

    <he-select [searchable]="searchable" [placeholder]="placeholder" [disabled]="disabled"
                [displayWith]="displayWith" [multiple]="multiple" [value]="value" [selectTemplate]="inputTemplate" [overlayOptions]="overlayOptions" [menuWidth]="menuWidth" >
                  <he-select-option [value]="1">Option 1</he-select-option>
                  <he-select-option [value]="2">Option 2</he-select-option>
                  <he-select-option [disabled]="true" [value]="3">Option 3</he-select-option>
                  <he-select-option [value]="4">Option 4</he-select-option>
                  <he-select-option [value]="5">Option 5</he-select-option>
    </he-select>
     <ng-template #inputTemplate >
                <button class="button is-ghost p-0">X-ICONss</button>
     </ng-template>

    `
  })
};
