import type { Meta, StoryObj } from '@storybook/angular';
import { componentWrapperDecorator } from '@storybook/angular';

import { SortSelectComponent } from '../../common';

const meta: Meta<SortSelectComponent> = {
  title: 'Common / Sort Select',
  component: SortSelectComponent,
  decorators: [componentWrapperDecorator(story => `<div style="padding: 1.5rem;" >${story}</div>`)],
  argTypes: {},
  args: {}
};

export default meta;
type Story = StoryObj<SortSelectComponent>;

export const Empty: Story = {
  args: {}
};

export const MainPageSort: Story = {
  args: {
    sortOptions: [
      {
        id: 'relevance',
        label: 'Relevance'
      },
      {
        id: 'date',
        label: 'Date Quality'
      },
      {
        id: 'name',
        label: 'Name'
      },
      {
        id: 'inputs',
        label: 'Inputs',
        color: '#5C6BC0'
      },
      {
        id: 'emisions',
        label: 'Emissions',
        color: '#673AB7'
      },
      {
        id: 'products',
        label: 'Products',
        color: '#EF5350'
      }
    ]
  }
};
