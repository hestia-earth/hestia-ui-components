import type { Meta, StoryObj } from '@storybook/angular';
import { componentWrapperDecorator, moduleMetadata } from '@storybook/angular';

import { ShelfDialogComponent } from '../../common';
import {} from '@angular/common/http';

const meta: Meta<ShelfDialogComponent> = {
  title: 'Common / Shelf',
  component: ShelfDialogComponent,
  decorators: [
    componentWrapperDecorator(story => `<div style="height: 700px; background-color: RGBA(0,0,0,0.1)" >${story}</div>`),
    moduleMetadata({
      imports: [HttpClientModule]
    })
  ],
  argTypes: {},
  args: {},
  render: args => ({
    props: {
      ...args
    },
    template: `
      <he-shelf-dialog [title]="title">
        <div>
          <h1>Download in Progress</h1>
          <p>2 of 3 Downloaded</p>
          <div></div>
        </div>
      </he-shelf-dialog>
    `
  })
};

export default meta;
type Story = StoryObj<ShelfDialogComponent>;

export const Primary: Story = {
  args: {
    title: 'Lorem ipsum dolor sit amet'
  } as any
};

export const CustomHeader: Story = {
  args: {},
  render: args => ({
    props: {
      ...args
    },
    template: `
           <ng-template #headerTemplate>
                <div>
              <h1>Download in Progress</h1>
              <p>2 of 3 Downloaded</p>
                    </div>
          </ng-template>

      <he-shelf-dialog [headerTemplate]="headerTemplate">
        <div>
          <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit
          </h1>
        </div>
      </he-shelf-dialog>`
  })
};
