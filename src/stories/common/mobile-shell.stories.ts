import type { Meta, StoryObj } from '@storybook/angular';
import { componentWrapperDecorator } from '@storybook/angular';

import { MobileShellComponent } from '../../common';

const meta: Meta<MobileShellComponent> = {
  title: 'Common / Mobile Shell',
  component: MobileShellComponent,
  decorators: [
    componentWrapperDecorator(
      story => `<div style="height: 700px; max-width: 400px; background-color: RGBA(0,0,0,0.1)" >${story}</div>`
    )
  ],
  argTypes: {},
  args: {},
  render: args => ({
    props: {
      ...args
    },
    template: `
      <he-mobile-shell [defaultActiveButtonId]="defaultActiveButtonId" [menuButtons]=" [
        { icon: 'check', label:'test' },
        { icon: 'circle', label:'test'  },
        {icon: 'download' , label:'test' , menuRef: step1Template },
        { icon: 'download' , label:'test', activeDisabled: true },
        {
           icon: 'list', label:'test',
          activeDisabled: true
        },
    ]">
    <h3>CONTENT</h3>
      </he-mobile-shell>
      <ng-template #step1Template>
        <h3>MENU</h3>
        <button>test button</button>
      </ng-template>
    `
  })
};

export default meta;
type Story = StoryObj<MobileShellComponent>;

export const Primary: Story = {
  args: {}
};

export const WithDefaultActiveMenu: Story = {
  args: {
    defaultActiveButtonId: '0'
  }
};
