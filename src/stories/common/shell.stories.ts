import type { Meta, StoryObj } from '@storybook/angular';
import { componentWrapperDecorator } from '@storybook/angular';

import { ShellComponent } from '../../common';

const meta: Meta<ShellComponent> = {
  title: 'Common / Shell',
  component: ShellComponent,
  decorators: [
    componentWrapperDecorator(story => `<div style="height: 700px; background-color: RGBA(0,0,0,0.1)" >${story}</div>`)
  ],
  argTypes: {},
  args: {},
  render: args => ({
    props: {
      ...args
    },
    template: `
      <he-shell [shelfButtons]=" [
        { icon: 'check', activeDisabled: true  },
        { icon: 'circle', menuRef: step1Template },
        { icon: 'download', position: 'bottom', activeDisabled: true },
        {
          icon: 'list',
          activeDisabled: true,
          position: 'bottom',
        },
    ]" [defaultActiveButtonId]="defaultActiveButtonId" >
    <h3>CONTENT</h3>
      </he-shell>
      <ng-template #step1Template>
        <h3>MENU</h3>
        <button>test button</button>
      </ng-template>
    `
  })
};

export default meta;
type Story = StoryObj<ShellComponent>;

export const Primary: Story = {
  args: {}
};

export const WithDefaultActiveMenu: Story = {
  args: {
    defaultActiveButtonId: '0'
  }
};

export const WithCustomActiveClass: Story = {
  args: {},
  render: args => ({
    props: {
      ...args
    },
    template: `
      <he-shell [shelfButtons]=" [
        { icon: 'check', activeDisabled: true  },
        { icon: 'circle', menuRef: step1Template, activeClassName: 'has-text-danger has-background-success' },
        { icon: 'download', position: 'bottom', activeDisabled: true },
        {
          icon: 'list',
          activeDisabled: true,
          position: 'bottom',
        },
    ]" [defaultActiveButtonId]="defaultActiveButtonId" >
    <h3>CONTENT</h3>
      </he-shell>
      <ng-template #step1Template>
        <h3>MENU</h3>
        <button>test button</button>
      </ng-template>
    `
  })
};
