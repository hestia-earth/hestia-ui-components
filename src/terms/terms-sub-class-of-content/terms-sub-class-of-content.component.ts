import { Component, OnChanges, inject, input } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faMinus, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ITermJSONLD, NodeType } from '@hestia-earth/schema';

import { matchExactQuery, matchType } from '../../search/search.model';
import { ISearchParams, ISearchResults, HeSearchService } from '../../search/search.service';
import { NodeLinkComponent } from '../../node/node-link/node-link.component';

@Component({
  selector: 'he-terms-sub-class-of-content',
  templateUrl: './terms-sub-class-of-content.component.html',
  styleUrls: ['./terms-sub-class-of-content.component.scss'],
  imports: [FaIconComponent, NodeLinkComponent]
})
export class TermsSubClassOfContentComponent implements OnChanges {
  protected readonly searchService = inject(HeSearchService);

  protected readonly faMinus = faMinus;
  protected readonly faPlus = faPlus;
  protected readonly faSpinner = faSpinner;

  protected readonly term = input.required<ITermJSONLD>();

  protected loading = false;
  protected results: ISearchResults<ITermJSONLD, NodeType.Term>;
  protected open = false;

  ngOnChanges() {
    this.results = null;
    this.open = false;
  }

  private get searchParams(): ISearchParams {
    return {
      fields: ['@type', '@id', 'name'],
      limit: 100,
      query: {
        bool: {
          must: [matchType(NodeType.Term), matchExactQuery('subClassOf.name', this.term().name)]
        }
      }
    };
  }

  private async search() {
    this.loading = true;
    const results = await this.searchService.search<ITermJSONLD, NodeType.Term>(this.searchParams);
    this.loading = false;
    return results;
  }

  protected async toggle() {
    this.results = this.results || (await this.search());
    this.open = !this.open;
  }
}
