import { ChangeDetectionStrategy, Component, TemplateRef, computed, input } from '@angular/core';
import { ITermJSONLD, Term } from '@hestia-earth/schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgTemplateOutlet } from '@angular/common';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'he-terms-units-description',
  templateUrl: './terms-units-description.component.html',
  styleUrls: ['./terms-units-description.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NgbTooltip, NgTemplateOutlet, FaIconComponent]
})
export class TermsUnitsDescriptionComponent {
  protected readonly faQuestionCircle = faQuestionCircle;

  protected readonly term = input.required<ITermJSONLD | Term>();
  protected readonly iconTemplate = input<TemplateRef<any>>();

  public readonly content = computed(() => this.term()?.unitsDescription);
}
