export * from './terms.model';

export { TermsPropertyContentComponent } from './terms-property-content/terms-property-content.component';
export { TermsSubClassOfContentComponent } from './terms-sub-class-of-content/terms-sub-class-of-content.component';
export { TermsUnitsDescriptionComponent } from './terms-units-description/terms-units-description.component';
