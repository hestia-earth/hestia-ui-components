import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { RouterLink } from '@angular/router';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { Property } from '@hestia-earth/schema';
import { isEmpty, isNumber } from '@hestia-earth/utils';

import { PrecisionPipe } from '../../common/precision.pipe';
import { CompoundPipe } from '../../common/compound.pipe';
import { TermsUnitsDescriptionComponent } from '../terms-units-description/terms-units-description.component';

@Component({
  selector: 'he-terms-property-content',
  templateUrl: './terms-property-content.component.html',
  styleUrls: ['./terms-property-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [RouterLink, NgTemplateOutlet, TermsUnitsDescriptionComponent, FaIconComponent, CompoundPipe, PrecisionPipe]
})
export class TermsPropertyContentComponent {
  protected readonly faExternalLinkAlt = faExternalLinkAlt;

  protected readonly property = input.required<Property>();
  protected readonly showTermName = input(true);

  protected readonly isNumber = isNumber;
  protected readonly isEmpty = isEmpty;

  protected hasKey(key: string) {
    const keys = Object.keys(this.property()).map(v => v.toLowerCase());
    return keys.includes(key);
  }
}
