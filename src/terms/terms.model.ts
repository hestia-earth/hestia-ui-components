import { Term, ITermJSONLD, TermTermType, isExpandable } from '@hestia-earth/schema';
import { keyToLabel } from '@hestia-earth/utils';

export const termProperties = (term: Term | ITermJSONLD) =>
  Object.keys(term).filter(
    key =>
      !isExpandable(term[key]) &&
      ![
        'pinned',
        'expanded',
        'extended',
        'selected',
        'loading',
        '_score',
        '@type',
        '@id',
        '@context',
        'createdAt',
        'updatedAt',
        'name',
        'synonyms',
        'termType',
        'location',
        'properties',
        'schemaVersion'
      ].includes(key)
  );

export const findPropertyById = ({ defaultProperties }: Term | ITermJSONLD, key: keyof ITermJSONLD) =>
  (defaultProperties || []).find(({ term }) => term!['@id'] === key);

export const termLocationName = ({ gadmFullName }: Term | ITermJSONLD) => ({
  name: gadmFullName || ''
});

export const termLocation = ({ latitude, longitude }: Term | ITermJSONLD) => ({
  lat: latitude!,
  lng: longitude!
});

const termTypeToLabel: {
  [type in TermTermType]?: string;
} = {
  [TermTermType.methodEmissionResourceUse]: 'Method (Emissions)',
  [TermTermType.methodMeasurement]: 'Method (Measurement)',
  [TermTermType.pesticideAI]: 'Pesticide Active Ingredient',
  [TermTermType.standardsLabels]: 'Standards & Labels',
  [TermTermType.usdaSoilType]: 'USDA Soil Type'
};

const termKeyToLabel: {
  [property in keyof Term]?: string;
} = {
  pubchem: 'PubChem',
  casNumber: 'CAS Number',
  agrovoc: 'AGROVOC'
};

export const termTypeLabel = (value: TermTermType | keyof Term | string = 'N/A'): string =>
  termTypeToLabel[value] || termKeyToLabel[value] || keyToLabel(value);
