import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';

import { glossaryBaseUrl } from '../common';

export enum GlossaryMigrationFormat {
  csv = 'csv',
  json = 'json'
}

export const migrationsUrl = (format = GlossaryMigrationFormat.csv) => `${glossaryBaseUrl(false)}/migrations.${format}`;

export interface IGlossaryMigration {
  migratedOn: string;
  newValue?: string;
  insctructions?: string;
}

export interface IGlossaryMigrations {
  [oldValue: string]: IGlossaryMigration;
}

@Injectable({
  providedIn: 'root'
})
export class HeGlossaryService {
  protected http = inject(HttpClient);

  public migrations$(format = GlossaryMigrationFormat.json) {
    return this.http.get<IGlossaryMigrations>(migrationsUrl(format));
  }

  public migrations(format = GlossaryMigrationFormat.json) {
    return this.migrations$(format).toPromise();
  }
}
