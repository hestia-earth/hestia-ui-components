import { enableProdMode, importProvidersFrom, SecurityContext } from '@angular/core';
import { BrowserModule, bootstrapApplication } from '@angular/platform-browser';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { HttpClient, withInterceptorsFromDi, provideHttpClient, withJsonpSupport } from '@angular/common/http';
import { provideNgxWebstorage, withNgxWebstorageConfig, withLocalStorage } from 'ngx-webstorage';
import { provideMarkdown } from 'ngx-markdown';

import { HE_CALCULATIONS_BASE_URL } from '../engine';
import { HE_API_BASE_URL, HE_MAP_LOADED, loadMapApi } from '../common';
import { HeSvgIconsModule } from '../svg-icons';

import { environment } from './environments/environment';

import { AppComponent } from './app/app.component';
import { AppRoutingModule } from './app/app-routing.module';

if (environment.production) {
  enableProdMode();
}

const bootstrap = () =>
  bootstrapApplication(AppComponent, {
    providers: [
      provideNgxWebstorage(withNgxWebstorageConfig({ prefix: 'he-ui-c', separator: '-' }), withLocalStorage()),
      importProvidersFrom(BrowserModule, HeSvgIconsModule, AppRoutingModule),
      provideMarkdown({
        loader: HttpClient,
        sanitize: SecurityContext.NONE
      }),
      {
        provide: HE_API_BASE_URL,
        useValue: environment.apiBaseUrl
      },
      {
        provide: HE_CALCULATIONS_BASE_URL,
        useValue: environment.apiBaseUrl
      },
      {
        provide: HE_MAP_LOADED,
        useFactory: loadMapApi(environment.mapsApiKey),
        deps: [HttpClient]
      },
      provideHttpClient(withInterceptorsFromDi(), withJsonpSupport()),
      provideAnimationsAsync()
    ]
  }).catch(err => console.error(err));

if (document.readyState === 'complete') {
  bootstrap();
} else {
  document.addEventListener('DOMContentLoaded', bootstrap);
}
