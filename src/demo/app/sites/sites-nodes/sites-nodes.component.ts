import { Component, computed, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { map } from 'rxjs/operators';
import { BlankNodesKey, ISiteJSONLD, NodeType } from '@hestia-earth/schema';

import { NodeListBaseComponent } from '../../node-list-base.component';
import { SitesNodesComponent as SitesNodesComponent_1 } from '../../../../sites/sites-nodes/sites-nodes.component';
import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';

interface ISection {
  title: string;
  nodeKey: BlankNodesKey;
}

const sections: ISection[] = [
  {
    title: 'Management',
    nodeKey: BlankNodesKey.management
  },
  {
    title: 'Measurements',
    nodeKey: BlankNodesKey.measurements
  }
];

@Component({
  selector: 'app-sites-nodes',
  templateUrl: './sites-nodes.component.html',
  styleUrls: ['./sites-nodes.component.scss'],
  imports: [NodeSuggestFormComponent, NodeListComponent, FormsModule, SitesNodesComponent_1]
})
export class SitesNodesComponent extends NodeListBaseComponent<ISiteJSONLD, NodeType.Site> {
  private router = inject(Router);
  private route = inject(ActivatedRoute);

  protected get nodeType() {
    return NodeType.Site;
  }

  protected sections = sections;
  protected selectedSectionTitle = toSignal(
    this.route.queryParams.pipe(map(({ section }) => section || sections[0].title))
  );
  protected selectedSection = computed(() => sections.find(section => section.title === this.selectedSectionTitle()));

  protected selectSection(section: string) {
    return this.router.navigate(['./'], {
      relativeTo: this.route,
      queryParams: { section }
    });
  }
}
