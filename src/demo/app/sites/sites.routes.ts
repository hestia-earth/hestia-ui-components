import { Route } from '@angular/router';

import { SitesMapsComponent } from './sites-maps/sites-maps.component';
import { SitesNodesComponent } from './sites-nodes/sites-nodes.component';

export default [
  {
    path: 'maps',
    component: SitesMapsComponent
  },
  {
    path: 'nodes',
    component: SitesNodesComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'activity'
  }
] satisfies Route[];
