import { Component } from '@angular/core';
import { ISiteJSONLD, NodeType } from '@hestia-earth/schema';

import { NodeListBaseComponent } from '../../node-list-base.component';
import { SitesMapsComponent as SitesMapsComponent_1 } from '../../../../sites/sites-maps/sites-maps.component';
import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';

@Component({
  selector: 'app-sites-maps',
  templateUrl: './sites-maps.component.html',
  styleUrls: ['./sites-maps.component.scss'],
  imports: [NodeSuggestFormComponent, NodeListComponent, SitesMapsComponent_1]
})
export class SitesMapsComponent extends NodeListBaseComponent<ISiteJSONLD, NodeType.Site> {
  protected get nodeType() {
    return NodeType.Site;
  }
}
