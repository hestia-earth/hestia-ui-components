import { Component, inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { INavigationMenuLink } from '../../common/navigation-menu/navigation-menu.component';
import { DrawerContainerComponent } from '../../common/drawer-container/drawer-container.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PageLayoutService } from './page-layout.service';

const links: INavigationMenuLink[] = [
  {
    title: 'Home',
    icon: 'far-home',
    url: '/'
  },
  {
    title: 'Components',
    icon: 'far-circle-info',
    expanded: true,
    collapsible: false,
    links: [
      {
        title: 'Bibliographies',
        links: [
          {
            title: 'Search Confirm',
            url: '/bibliographies/search-confirm'
          }
        ]
      },
      {
        title: 'Cycles',
        links: [
          {
            title: 'Nodes',
            url: '/cycles/nodes'
          },
          {
            title: 'Completeness',
            url: '/cycles/completeness'
          },
          {
            title: 'Logs file',
            url: '/cycles/logs/file'
          }
        ]
      },
      {
        title: 'Engine',
        links: [
          {
            title: 'Orchestrator Edit',
            url: '/engine/orchestrator/edit'
          },
          {
            title: 'Models',
            url: '/engine/models'
          },
          {
            title: 'Calculation Status',
            url: '/engine/stages'
          }
        ]
      },
      {
        title: 'Files',
        links: [
          {
            title: 'Form',
            url: '/files/form'
          }
        ]
      },
      {
        title: 'Impact Assessments',
        links: [
          {
            title: 'Driver Chart',
            url: '/impact-assessments/graph'
          },
          {
            title: 'Products',
            url: '/impact-assessments/products'
          }
        ]
      },
      {
        title: 'Schema',
        links: [
          {
            title: 'Info',
            url: '/schema/info'
          }
        ]
      },
      {
        title: 'Sites',
        links: [
          {
            title: 'Maps',
            url: '/sites/maps'
          },
          {
            title: 'Nodes',
            url: '/sites/nodes'
          }
        ]
      },
      {
        title: 'Select',
        links: [
          {
            title: 'Simple select',
            url: '/select/selects'
          },
          {
            title: 'Select with filters',
            url: '/select/filters'
          }
        ]
      },
      {
        title: 'Shelf dialog',
        links: [
          {
            title: 'Shelf Dialog',
            url: '/shelf-dialog/simple-dialog'
          },
          {
            title: 'Custom Header',
            url: '/shelf-dialog/custom-header'
          }
        ]
      },
      {
        title: 'Terms',
        links: [
          {
            title: 'Property Content',
            url: '/terms/property-content'
          }
        ]
      },
      {
        title: 'Drawing Polygons on a Map',
        url: '/common/maps-drawing-confirm'
      }
    ]
  }
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  imports: [RouterOutlet, NavbarComponent, DrawerContainerComponent]
})
export class AppComponent {
  protected readonly pageLayoutService = inject(PageLayoutService);

  protected readonly links = links;
}
