import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { RouterLink } from '@angular/router';
import {
  faAngleDown,
  faCheckCircle,
  faExclamationTriangle,
  faInfoCircle,
  faLink,
  faSearch,
  faTimes,
  faTimesCircle
} from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IssueConfirmComponent } from '../../../common/issue-confirm/issue-confirm.component';
import { ResponsiveService } from '../../../common/responsive.service';

const cssClasses = (classes: string[]) => classes.map(c => `is-${c}`).join(' ');

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  imports: [RouterLink, FaIconComponent, FormsModule]
})
export class HomePageComponent {
  private readonly modalService = inject(NgbModal);
  protected readonly responsiveService = inject(ResponsiveService);

  protected readonly faLink = faLink;
  protected readonly faAngleDown = faAngleDown;
  protected readonly faTimes = faTimes;
  protected readonly faSearch = faSearch;
  protected readonly faInfoCircle = faInfoCircle;
  protected readonly faCheckCircle = faCheckCircle;
  protected readonly faExclamationTriangle = faExclamationTriangle;
  protected readonly faTimesCircle = faTimesCircle;

  protected readonly cssClasses = cssClasses;

  protected readonly buttons = [['primary'], ['secondary'], ['info'], ['danger'], ['warning'], ['ghost']];
  protected readonly buttonSizes = [['small'], ['normal'], ['medium'], ['large']];
  protected openDropdown = false;
  protected openDropdownGhost = false;
  protected search = '';
  protected switch = true;
  protected checkbox = true;
  protected readonly tabs = ['Tab 1', 'Tab 2', 'Tab 3'];
  protected selectedTab = 'Tab 1';

  protected showReportIssue() {
    this.modalService.open(IssueConfirmComponent);
  }
}
