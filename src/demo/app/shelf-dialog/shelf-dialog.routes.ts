import { Route } from '@angular/router';

import { CustomHeaderShelfDialogComponent } from './custom-header-shelf-dialog/custom-header-shelf-dialog.component';
import { SimpleShelfDialogComponent } from './simple-shelf-dialog/simple-shelf-dialog.component';

export default [
  {
    path: 'simple-dialog',
    component: SimpleShelfDialogComponent
  },
  {
    path: 'custom-header',
    component: CustomHeaderShelfDialogComponent
  }
] satisfies Route[];
