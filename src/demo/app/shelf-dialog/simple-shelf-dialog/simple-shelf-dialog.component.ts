import { Component } from '@angular/core';
import { ShelfDialogComponent } from '../../../../common/shelf-dialog/shelf-dialog.component';

@Component({
  selector: 'app-simple-shelf-dialog',
  templateUrl: './simple-shelf-dialog.component.html',
  styleUrls: ['./simple-shelf-dialog.component.scss'],
  imports: [ShelfDialogComponent]
})
export class SimpleShelfDialogComponent {}
