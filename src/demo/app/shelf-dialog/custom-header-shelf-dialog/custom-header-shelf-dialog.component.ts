import { Component } from '@angular/core';
import { ShelfDialogComponent } from '../../../../common/shelf-dialog/shelf-dialog.component';

@Component({
  selector: 'app-simple-shelf-dialog',
  templateUrl: './custom-header-shelf-dialog.component.html',
  styleUrls: ['./custom-header-shelf-dialog.component.scss'],
  imports: [ShelfDialogComponent]
})
export class CustomHeaderShelfDialogComponent {}
