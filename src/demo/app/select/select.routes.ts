import { Route } from '@angular/router';

import { SelectsComponent } from './selects/selects.component';
import { FiltersComponent } from './filters/filters.component';

export default [
  {
    path: 'filters',
    component: FiltersComponent
  },
  {
    path: 'selects',
    component: SelectsComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'search-confirm'
  }
] satisfies Route[];
