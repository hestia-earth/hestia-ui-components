import { Component } from '@angular/core';

import { FilterData } from '../../../../select/filter.model';
import { FilterComponent } from '../../../../select/filter.component';

const mockupData = <FilterData[]>[
  {
    type: 'group',
    label: 'Group 1',
    options: [
      {
        type: 'group',
        label: 'Group 2',
        options: [
          {
            type: 'group',
            label: 'Group 3',
            options: [
              {
                type: 'option',
                label: 'Option 8',
                value: '8'
              },
              {
                type: 'option',
                label: 'Option 9',
                value: '9'
              }
            ]
          },
          {
            type: 'option',
            label: 'Option 6',
            value: '6'
          },
          {
            type: 'option',
            label: 'Option 7',
            value: '7'
          }
        ]
      },
      {
        type: 'option',
        label: 'Option 4',
        value: '4'
      },
      {
        type: 'option',
        label: 'Option 5',
        value: '5'
      }
    ]
  },
  {
    type: 'group',
    label: 'Group 5',
    options: [
      {
        type: 'option',
        label: 'Option 10',
        value: '10'
      },
      {
        type: 'option',
        label: 'Option 11',
        value: '11'
      }
    ]
  }
];

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
  imports: [FilterComponent]
})
export class FiltersComponent {
  data: FilterData[] = mockupData;
}
