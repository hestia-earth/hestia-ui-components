import { Component } from '@angular/core';
import { SelectOptionComponent } from '../../../../select/select-option/select-option.component';
import { NodeElementDirective } from '../../../../select/node-element.directive';
import { SelectComponent } from '../../../../select/select.component';

@Component({
  selector: 'app-selects',
  templateUrl: './selects.component.html',
  styleUrls: ['./selects.component.scss'],
  imports: [SelectComponent, NodeElementDirective, SelectOptionComponent]
})
export class SelectsComponent {
  public show = false;
}
