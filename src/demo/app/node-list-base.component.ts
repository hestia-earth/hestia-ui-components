import { OnInit, Injectable, Directive, computed, DestroyRef, signal, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { DataState } from '@hestia-earth/api';
import { JSONLD, NodeType } from '@hestia-earth/schema';

import { HeNodeStoreService } from '../../node/node-store.service';
import { SharedService } from './shared/shared.service';

@Directive()
@Injectable()
export abstract class NodeListBaseComponent<C extends JSONLD<T>, T extends NodeType> implements OnInit {
  protected destroyRef = inject(DestroyRef);
  protected nodeStoreService = inject(HeNodeStoreService);
  protected sharedService = inject(SharedService);

  protected loading = true;
  protected dataState = DataState.recalculated;

  protected originalNodes = signal([] as C[]);
  protected recalculatedNodes = signal([] as C[]);
  protected nodes = computed(() => this.recalculatedNodes());

  public ngOnInit() {
    this.nodeStoreService
      .find$(this.nodeType)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(values => {
        this.originalNodes.set(values.map(value => value.original as C));
        this.recalculatedNodes.set(values.map(value => value.recalculated as C));
      });

    this.loading = false;
  }

  protected abstract get nodeType(): NodeType;
}
