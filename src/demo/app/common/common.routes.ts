import { Route } from '@angular/router';

import { MapsDrawingConfirmComponent } from './maps-drawing-confirm/maps-drawing-confirm.component';

export default [
  {
    path: 'maps-drawing-confirm',
    component: MapsDrawingConfirmComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'maps-drawing-confirm'
  }
] satisfies Route[];
