import { Component, inject } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { MapsDrawingComponent } from '../../../../common/maps-drawing/maps-drawing.component';
import { MapsDrawingConfirmComponent as MapsDrawingConfirmComponent_1 } from '../../../../common/maps-drawing-confirm/maps-drawing-confirm.component';

@Component({
  selector: 'app-maps-drawing-confirm',
  templateUrl: './maps-drawing-confirm.component.html',
  styleUrls: ['./maps-drawing-confirm.component.scss'],
  imports: [MapsDrawingComponent]
})
export class MapsDrawingConfirmComponent {
  protected readonly modalService = inject(NgbModal);

  protected inlineVisible = false;
  protected result?: google.maps.LatLngLiteral | string;

  protected showModal() {
    this.result = undefined;
    this.inlineVisible = false;
    const instance = this.modalService.open(MapsDrawingConfirmComponent_1);
    instance.closed.subscribe(value => this.confirmed(value));
  }

  protected showInline() {
    this.result = undefined;
    this.inlineVisible = true;
  }

  protected confirmed(value: google.maps.LatLngLiteral | string) {
    try {
      this.result = JSON.stringify(JSON.parse(value as any), null, 2);
    } catch (_err) {
      this.result = value;
    }
  }
}
