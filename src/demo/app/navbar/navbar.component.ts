import { Component } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';

import { gitHome } from '../../../common/utils';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  imports: [FaIconComponent]
})
export class NavbarComponent {
  protected readonly faGitlab = faGitlab;

  protected gitHome = gitHome;
  protected menuActive = false;
}
