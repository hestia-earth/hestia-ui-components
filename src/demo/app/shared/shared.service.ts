import { Injectable, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { JSONLD, NodeType } from '@hestia-earth/schema';
import { LocalStorageService } from 'ngx-webstorage';

import { HeNodeStoreService } from '../../../node';

const idsConfigKey = 'storedIds';

type storedData = {
  [nodeType in NodeType]?: string[];
};

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private readonly nodeStoreService = inject(HeNodeStoreService);
  private readonly localStorage = inject(LocalStorageService);

  private readonly nodes = toSignal(this.nodeStoreService.nodes$());

  constructor() {
    this.loadNodes();
  }

  private loadNodes() {
    const data: storedData = JSON.parse(this.localStorage.retrieve(idsConfigKey));
    const nodes = Object.entries(data || {}).flatMap(([type, values]) =>
      values.map(id => ({ '@type': type as NodeType, '@id': id }))
    );
    nodes.map(node => this.nodeStoreService.addNode(node));
  }

  private saveNodes() {
    const data: storedData = Object.fromEntries(
      Object.entries(this.nodes() || {}).map(([nodeType, values]) => [nodeType, values.map(({ id }) => id)])
    );
    this.localStorage.store(idsConfigKey, JSON.stringify(data));
  }

  public addNode(node: JSONLD<NodeType>, multiple = false) {
    multiple || this.nodeStoreService.clearNodes();
    this.nodeStoreService.addNode(node).subscribe(() => this.saveNodes());
    this.saveNodes();
  }

  public removeNode(type: NodeType, index: number) {
    this.nodeStoreService.removeNodeByIndex(type, index);
    this.saveNodes();
  }
}
