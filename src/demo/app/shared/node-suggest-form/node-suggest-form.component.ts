import { ChangeDetectionStrategy, Component, computed, inject, input } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { UntypedFormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JSONLD, NodeType } from '@hestia-earth/schema';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { keyToLabel } from '@hestia-earth/utils';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbTypeahead, NgbHighlight } from '@ng-bootstrap/ng-bootstrap';

import {
  HeSearchService,
  matchType,
  matchExactQuery,
  matchPhraseQuery,
  matchPhrasePrefixQuery,
  matchBoolPrefixQuery
} from '../../../../search';
import { HeNodeStoreService } from '../../../../node';
import { SharedService } from '../shared.service';
import { faPlus, faSpinner, faTimes } from '@fortawesome/free-solid-svg-icons';

const MIN_TYPEAHEAD_LENGTH = 1;

@Component({
  selector: 'app-node-suggest-form',
  templateUrl: './node-suggest-form.component.html',
  styleUrls: ['./node-suggest-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FormsModule, ReactiveFormsModule, NgbTypeahead, FaIconComponent, NgbHighlight]
})
export class NodeSuggestFormComponent<T extends NodeType> {
  private readonly formBuilder = inject(UntypedFormBuilder);
  private readonly searchService = inject(HeSearchService);
  private readonly nodeStoreService = inject(HeNodeStoreService);
  private readonly sharedService = inject(SharedService);

  protected readonly faTimes = faTimes;
  protected readonly faSpinner = faSpinner;
  protected readonly faPlus = faPlus;

  protected readonly nodeType = input.required<T>();
  protected readonly nodeTypeLabel = computed(() => keyToLabel(this.nodeType()));
  protected readonly multiple = input(true);

  private readonly _nodes = toSignal(this.nodeStoreService.nodes$());
  private readonly ids = computed(() => this._nodes()?.[this.nodeType()]?.map(({ id }) => id) || []);

  protected readonly form = this.formBuilder.group({
    search: ['', Validators.required]
  });
  protected suggesting = false;
  protected readonly formatter = ({ name }: JSONLD<T>) => name;
  protected readonly suggestNode = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => (this.suggesting = true)),
      switchMap(term => this.suggest(term)),
      tap(() => (this.suggesting = false))
    );

  private suggest(term: string) {
    return term.length < MIN_TYPEAHEAD_LENGTH
      ? of([])
      : this.searchService.suggest$(
          term,
          this.nodeType(),
          [],
          {
            bool: {
              must: [matchType(this.nodeType())],
              must_not: [...this.ids().map(id => matchExactQuery('@id', id))],
              should: [
                matchExactQuery('@id', term, 1000),
                matchExactQuery('name', term, 900),
                matchPhraseQuery(term, 100, '@id', 'name', 'product.name', 'product.term.name', 'country.name'),
                matchPhrasePrefixQuery(term, 20, '@id', 'name', 'product.name', 'product.term.name', 'country.name'),
                matchBoolPrefixQuery(term, 10, '@id', 'name', 'product.name', 'product.term.name', 'country.name')
              ],
              minimum_should_match: 1
            }
          },
          10,
          [
            '@type',
            'termType',
            'cycle.name',
            'country.name',
            'product.name',
            'product.term.name',
            'endDate',
            'aggregated'
          ]
        );
  }

  protected resetForm() {
    this.form.get('search')?.setValue('');
    this.form.updateValueAndValidity();
  }

  protected async submit() {
    const { search: node } = this.form.value;
    this.sharedService.addNode(node, this.multiple());
    this.resetForm();
  }
}
