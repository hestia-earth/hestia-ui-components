import { Component, computed, inject, input } from '@angular/core';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { DataState } from '@hestia-earth/api';
import { JSONLD, NodeType } from '@hestia-earth/schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { mergeMap } from 'rxjs/operators';

import { HeNodeStoreService } from '../../../../node';
import { NodeLinkComponent } from '../../../../node/node-link/node-link.component';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-node-list',
  templateUrl: './node-list.component.html',
  styleUrls: ['./node-list.component.scss'],
  imports: [NodeLinkComponent, FaIconComponent]
})
export class NodeListComponent<T extends NodeType> {
  protected readonly faTimes = faTimes;

  protected nodeType = input.required<T>();
  private nodeType$ = toObservable(this.nodeType);

  private nodeStoreService = inject(HeNodeStoreService);
  private sharedService = inject(SharedService);

  private _nodes = toSignal(this.nodeType$.pipe(mergeMap(type => this.nodeStoreService.find$<JSONLD<NodeType>>(type))));
  protected nodes = computed(() => this._nodes()?.map(data => data[DataState.original]) || []);

  protected remove(index: number) {
    this.sharedService.removeNode(this.nodeType(), index);
  }
}
