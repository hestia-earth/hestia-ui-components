import { Route } from '@angular/router';

import { TermsPropertyContentComponent } from './terms-property-content/terms-property-content.component';

export default [
  {
    path: 'property-content',
    component: TermsPropertyContentComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'activity'
  }
] satisfies Route[];
