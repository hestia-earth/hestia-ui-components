import { Component, OnInit, computed } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ITermJSONLD, NodeType, Property } from '@hestia-earth/schema';

import { NodeListBaseComponent } from '../../node-list-base.component';
import { TermsPropertyContentComponent as TermsPropertyContentComponent_1 } from '../../../../terms/terms-property-content/terms-property-content.component';
import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { FormsModule } from '@angular/forms';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';

@Component({
  selector: 'app-terms-property-content',
  templateUrl: './terms-property-content.component.html',
  styleUrls: ['./terms-property-content.component.scss'],
  imports: [NodeSuggestFormComponent, FormsModule, NodeListComponent, TermsPropertyContentComponent_1]
})
export class TermsPropertyContentComponent extends NodeListBaseComponent<ITermJSONLD, NodeType.Term> implements OnInit {
  public selectedProperty?: Property;

  protected get nodeType() {
    return NodeType.Term;
  }

  protected properties = computed(() => this.originalNodes()?.[0]?.defaultProperties || []);

  public async ngOnInit() {
    super.ngOnInit();
    this.nodeStoreService
      .nodes$()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => (this.selectedProperty = undefined));
  }
}
