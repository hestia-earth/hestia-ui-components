import { Route } from '@angular/router';

import { ImpactAssessmentsGraphComponent } from './impact-assessments-graph/impact-assessments-graph.component';
import { ImpactAssessmentsProductsComponent } from './impact-assessments-products/impact-assessments-products.component';

export default [
  {
    path: 'graph',
    component: ImpactAssessmentsGraphComponent
  },
  {
    path: 'products',
    component: ImpactAssessmentsProductsComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'graph'
  }
] satisfies Route[];
