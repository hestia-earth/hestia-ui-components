import { Component } from '@angular/core';
import { IImpactAssessmentJSONLD, NodeType, TermTermType } from '@hestia-earth/schema';

import { NodeListBaseComponent } from '../../node-list-base.component';
import { ImpactAssessmentsProductsComponent as ImpactAssessmentsProductsComponent_1 } from '../../../../impact-assessments/impact-assessments-products/impact-assessments-products.component';
import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { FormsModule } from '@angular/forms';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';

interface IKey {
  key: 'emissionsResourceUse' | 'impacts' | 'endpoints';
  filterTermTypes: TermTermType[];
  enableFilterMethodModel?: boolean;
}

const keys: IKey[] = [
  {
    key: 'emissionsResourceUse',
    filterTermTypes: [TermTermType.emission]
  },
  {
    key: 'emissionsResourceUse',
    filterTermTypes: [TermTermType.resourceUse]
  },
  {
    key: 'impacts',
    filterTermTypes: [TermTermType.characterisedIndicator],
    enableFilterMethodModel: true
  },
  {
    key: 'endpoints',
    filterTermTypes: [TermTermType.endpointIndicator],
    enableFilterMethodModel: true
  }
];

@Component({
  selector: 'app-impact-assessments-products',
  templateUrl: './impact-assessments-products.component.html',
  styleUrls: ['./impact-assessments-products.component.scss'],
  imports: [NodeSuggestFormComponent, FormsModule, NodeListComponent, ImpactAssessmentsProductsComponent_1]
})
export class ImpactAssessmentsProductsComponent extends NodeListBaseComponent<
  IImpactAssessmentJSONLD,
  NodeType.ImpactAssessment
> {
  public keys = keys;
  public selectedKey = keys[0];

  protected get nodeType() {
    return NodeType.ImpactAssessment;
  }
}
