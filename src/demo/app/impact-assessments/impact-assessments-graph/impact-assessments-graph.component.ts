import { Component } from '@angular/core';
import { IImpactAssessmentJSONLD, NodeType } from '@hestia-earth/schema';

import { NodeListBaseComponent } from '../../node-list-base.component';
import { ImpactAssessmentsGraphComponent as ImpactAssessmentsGraphComponent_1 } from '../../../../impact-assessments/impact-assessments-graph/impact-assessments-graph.component';
import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';

@Component({
  selector: 'app-impact-assessments-graph',
  templateUrl: './impact-assessments-graph.component.html',
  styleUrls: ['./impact-assessments-graph.component.scss'],
  imports: [NodeSuggestFormComponent, NodeListComponent, ImpactAssessmentsGraphComponent_1]
})
export class ImpactAssessmentsGraphComponent extends NodeListBaseComponent<
  IImpactAssessmentJSONLD,
  NodeType.ImpactAssessment
> {
  protected get nodeType() {
    return NodeType.ImpactAssessment;
  }
}
