import { Route } from '@angular/router';

import { FilesFormComponent } from './files-form/files-form.component';

export default [
  {
    path: 'form',
    component: FilesFormComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'form'
  }
] satisfies Route[];
