import { Component, computed, effect, inject, OnDestroy, signal, TemplateRef, viewChild } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faClone as farClone } from '@fortawesome/free-regular-svg-icons';
import { faComments, faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { loadSchemas } from '@hestia-earth/json-schema';
import { NodeType, JSON as HestiaJson, SchemaType } from '@hestia-earth/schema';
import { LocalStorageService } from 'ngx-webstorage';
import set from 'lodash.set';
import { combineLatest } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { hasValidationError, IValidationError, schemaTypeToDefaultValue } from '../../../../schema';
import { CollapsibleBoxComponent, distinctUntilChangedDeep, ResponsiveService } from '../../../../common';
import {
  ISummaryError,
  FilesFormComponent as FilesFormComponent_1,
  FilesFormEditableComponent,
  FilesErrorSummaryComponent,
  IValidationErrorWithIndex
} from '../../../../files';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { PageLayoutService } from '../../page-layout.service';

const lastNodeTypeId = (nodes: HestiaJson<any>[], type: string) =>
  nodes
    .filter(node => node.type === type && !isNaN(+node.id))
    .map(node => +node.id)
    .sort((a, b) => a - b)
    .pop() || 0;

const ignoreNodeTypes = [NodeType.Actor, NodeType.Term];

const nodeTypes = Object.values(NodeType).filter(type => !ignoreNodeTypes.includes(type));

const storageKey = 'files-form';

interface INodeWithErrors {
  node: HestiaJson<SchemaType>;
  index: number;
  errors?: IValidationError[];
}

const nodeErrors = (validationErrors: IValidationError[][], resolvedErrors: ISummaryError[], index: number) => {
  const errors = validationErrors?.[index] ?? [];
  const resolvedIndexes = resolvedErrors
    .filter(({ error }) => error.nodeIndex === index)
    .map(({ error }) => error.index);
  return errors.filter((error, index) => !resolvedIndexes.includes(index));
};

enum Mode {
  view = 'view',
  editable = 'editable',
  resolve = 'resolve'
}

@Component({
  selector: 'app-files-form',
  templateUrl: './files-form.component.html',
  styleUrls: ['./files-form.component.scss'],
  imports: [
    FormsModule,
    RouterLink,
    NgTemplateOutlet,
    FaIconComponent,
    FilesFormComponent_1,
    FilesFormEditableComponent,
    FilesErrorSummaryComponent,
    CollapsibleBoxComponent
  ]
})
export class FilesFormComponent implements OnDestroy {
  private readonly route = inject(ActivatedRoute);
  private readonly localStorage = inject(LocalStorageService);
  private readonly pageLayoutService = inject(PageLayoutService);
  protected readonly responsiveService = inject(ResponsiveService);

  private readonly rightDrawerContent = viewChild<TemplateRef<any>>('rightDrawer');

  protected readonly farClone = farClone;
  protected readonly faPlus = faPlus;
  protected readonly faTimes = faTimes;
  protected readonly faComments = faComments;

  protected readonly Mode = Mode;
  protected readonly modes = Object.values(Mode);
  protected readonly selectedMode = toSignal(
    this.route.queryParams.pipe(
      map(({ mode }) => (mode as Mode) || Mode.view),
      startWith(Mode.view)
    )
  );
  protected readonly errorMode = computed(() => this.selectedMode() === Mode.resolve);
  protected readonly editable = computed(() => this.selectedMode() === Mode.editable);

  protected readonly schemas = loadSchemas();
  protected readonly nodeTypes = nodeTypes;
  protected readonly newNodeType = signal(NodeType.Cycle);

  protected readonly nodes = signal<HestiaJson<SchemaType>[]>([]);
  protected readonly errors = signal<IValidationError[][]>([]);
  protected readonly nodesWithIndex = computed(() =>
    this.nodes().map((node, index) => ({ node, index, errors: this.errors()[index] }) as INodeWithErrors)
  );
  protected readonly nodeMap = computed(() =>
    this.nodes().reduce((prev, curr) => {
      prev[curr.type] = prev[curr.type] || [];
      prev[curr.type].push(curr.id);
      return prev;
    }, {})
  );
  protected readonly hasErrors = computed(() => this.errors().some(errors => errors.length > 0));
  protected readonly resolvedErrors = signal<ISummaryError[]>([]);
  protected readonly unresolvedIndexes = signal<number[]>([]);
  protected readonly errorsWithIndex = computed(() =>
    this.errors().map((error, index) => ({ error, index }) as IValidationErrorWithIndex)
  );
  protected readonly validationErrors = computed(() => this.errorsWithIndex().filter(({ error }) => error.length));
  protected readonly nodesWithErrors = toSignal(
    combineLatest([
      toObservable(this.nodes),
      toObservable(this.unresolvedIndexes),
      toObservable(this.errors),
      toObservable(this.resolvedErrors)
    ]).pipe(
      distinctUntilChangedDeep(),
      map(([nodes, unresolvedIndexes, validationErrors, resolvedErrors]) =>
        nodes
          .map((node, index) => ({ node, index }))
          .filter(({ index }) =>
            hasValidationError(validationErrors)
              ? validationErrors?.[index]?.length > 0 && (unresolvedIndexes ?? []).includes(index)
              : true
          )
          .map(
            data =>
              ({
                ...data,
                errors: nodeErrors(validationErrors, resolvedErrors, data.index)
              }) as INodeWithErrors
          )
      ),
      startWith([] as INodeWithErrors[])
    )
  );

  constructor() {
    const { nodes, errors } = JSON.parse(this.localStorage.retrieve(storageKey) || '{"nodes":[],"errors":[]}');
    this.nodes.set(nodes);
    this.errors.set(errors || []);

    effect(() => {
      const content =
        this.errorMode() && this.hasErrors() && !this.responsiveService.isTouch()
          ? this.rightDrawerContent()
          : undefined;
      this.pageLayoutService.rightDrawerContent.set(content);
    });
  }

  ngOnDestroy() {
    this.pageLayoutService.rightDrawerContent.set(undefined);
  }

  private onNodesChanged(nodes: HestiaJson<SchemaType>[], errors: IValidationError[][]) {
    this.nodes.set(nodes);
    this.errors.set(errors);
    this.localStorage.store(storageKey, JSON.stringify({ nodes, errors }));
  }

  protected addNode() {
    const data = schemaTypeToDefaultValue(this.schemas, this.schemas[this.newNodeType()]);
    const id = lastNodeTypeId(this.nodes(), this.newNodeType()) + 1;
    const nodes = this.nodes().slice();
    nodes.unshift({
      ...data,
      type: this.newNodeType(),
      id: `${id}`
    });
    this.onNodesChanged(nodes, [[], ...this.errors()]);
  }

  protected removeNode({ index }: INodeWithErrors) {
    const nodes = this.nodes().slice();
    nodes.splice(index, 1);
    const errors = this.errors().slice();
    errors.splice(index, 1);
    this.onNodesChanged(nodes, errors);
  }

  protected duplicateNode({ node, index }: INodeWithErrors) {
    const nodes = this.nodes().slice();
    const clone = {
      ...JSON.parse(JSON.stringify(node)),
      id: lastNodeTypeId(nodes, node.type) + 1
    };
    nodes.splice(index + 1, 0, clone);
    const errors = this.errors().slice();
    errors.splice(index + 1, 0, []);
    this.onNodesChanged(nodes, errors);
  }

  protected nodeChanged({ key, value }, { index }: INodeWithErrors) {
    if (key) {
      const nodes = this.nodes().slice();
      set(nodes[index], key, value);
      this.onNodesChanged(nodes, this.errors());
    }
  }

  protected nodeErorrResolved(errorIndex: number, { index }: INodeWithErrors) {
    const errors = this.errors();
    errors[index].splice(errorIndex, 1);
    this.onNodesChanged(this.nodes(), errors);
  }
}
