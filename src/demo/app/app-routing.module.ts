import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./home-page/home-page.component').then(m => m.HomePageComponent)
  },
  {
    path: 'common',
    loadChildren: () => import('./common/common.routes')
  },
  {
    path: 'bibliographies',
    loadChildren: () => import('./bibliographies/bibliographies.routes')
  },
  {
    path: 'cycles',
    loadChildren: () => import('./cycles/cycles.routes')
  },
  {
    path: 'engine',
    loadChildren: () => import('./engine/engine.routes')
  },
  {
    path: 'files',
    loadChildren: () => import('./files/files.routes')
  },
  {
    path: 'impact-assessments',
    loadChildren: () => import('./impact-assessments/impact-assessments.routes')
  },
  {
    path: 'schema',
    loadChildren: () => import('./schema/schema.routes')
  },
  {
    path: 'select',
    loadChildren: () => import('./select/select.routes')
  },
  {
    path: 'sites',
    loadChildren: () => import('./sites/sites.routes')
  },
  {
    path: 'terms',
    loadChildren: () => import('./terms/terms.routes')
  },
  {
    path: 'shelf-dialog',
    loadChildren: () => import('./shelf-dialog/shelf-dialog.routes')
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      anchorScrolling: 'enabled',
      useHash: false,
      enableTracing: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
