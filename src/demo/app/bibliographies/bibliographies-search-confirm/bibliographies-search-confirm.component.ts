import { Component } from '@angular/core';
import { BibliographiesSearchConfirmComponent as BibliographiesSearchConfirmComponent_1 } from '../../../../bibliographies/bibliographies-search-confirm/bibliographies-search-confirm.component';

@Component({
  selector: 'app-bibliographies-search-confirm',
  templateUrl: './bibliographies-search-confirm.component.html',
  styleUrls: ['./bibliographies-search-confirm.component.scss'],
  imports: [BibliographiesSearchConfirmComponent_1]
})
export class BibliographiesSearchConfirmComponent {
  public show = false;
}
