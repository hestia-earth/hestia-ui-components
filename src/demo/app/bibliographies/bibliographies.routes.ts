import { Route } from '@angular/router';

import { BibliographiesSearchConfirmComponent } from './bibliographies-search-confirm/bibliographies-search-confirm.component';

export default [
  {
    path: 'search-confirm',
    component: BibliographiesSearchConfirmComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'search-confirm'
  }
] satisfies Route[];
