import { Component, OnInit } from '@angular/core';
import { ICycleJSONLD, NodeType } from '@hestia-earth/schema';

import { NodeListBaseComponent } from '../../node-list-base.component';
import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';
import { EngineModelsStageDeepComponent } from '../../../../engine/engine-models-stage-deep/engine-models-stage-deep.component';

@Component({
  selector: 'app-engine-models-stage-deep-page',
  imports: [NodeSuggestFormComponent, NodeListComponent, EngineModelsStageDeepComponent],
  templateUrl: './engine-models-stage-deep-page.component.html',
  styleUrl: './engine-models-stage-deep-page.component.scss'
})
export class EngineModelsStageDeepPageComponent
  extends NodeListBaseComponent<ICycleJSONLD, NodeType.Cycle>
  implements OnInit
{
  protected get nodeType() {
    return NodeType.Cycle;
  }
}
