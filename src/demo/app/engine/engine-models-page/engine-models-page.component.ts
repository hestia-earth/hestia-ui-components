import { Component, OnInit } from '@angular/core';
import { ICycleJSONLD, NodeType } from '@hestia-earth/schema';

import { NodeListBaseComponent } from '../../node-list-base.component';
import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';
import { EngineModelsStageComponent } from '../../../../engine/engine-models-stage/engine-models-stage.component';
import { EngineModelsVersionLinkComponent } from '../../../../engine/engine-models-version-link/engine-models-version-link.component';

@Component({
  selector: 'app-engine-models-page',
  templateUrl: './engine-models-page.component.html',
  styleUrls: ['./engine-models-page.component.scss'],
  imports: [NodeSuggestFormComponent, NodeListComponent, EngineModelsVersionLinkComponent, EngineModelsStageComponent]
})
export class EngineModelsPageComponent extends NodeListBaseComponent<ICycleJSONLD, NodeType.Cycle> implements OnInit {
  protected get nodeType() {
    return NodeType.Cycle;
  }
}
