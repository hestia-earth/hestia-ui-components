import { Route } from '@angular/router';

import { EngineOrchestratorEditComponent } from './engine-orchestrator-edit/engine-orchestrator-edit.component';
import { EngineModelsPageComponent } from './engine-models-page/engine-models-page.component';
import { EngineModelsStageDeepPageComponent } from './engine-models-stage-deep-page/engine-models-stage-deep-page.component';

export default [
  {
    path: 'orchestrator/edit',
    component: EngineOrchestratorEditComponent
  },
  {
    path: 'models',
    component: EngineModelsPageComponent
  },
  {
    path: 'stages',
    component: EngineModelsStageDeepPageComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'orchestrator/edit'
  }
] satisfies Route[];
