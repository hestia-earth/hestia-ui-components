import { Component } from '@angular/core';
import { NodeType } from '@hestia-earth/schema';
import { EngineOrchestratorEditComponent as EngineOrchestratorEditComponent_1 } from '../../../../engine/engine-orchestrator-edit/engine-orchestrator-edit.component';
import { FormsModule } from '@angular/forms';

const nodeTypes = [NodeType.Cycle, NodeType.ImpactAssessment, NodeType.Site];

@Component({
  selector: 'app-engine-orchestrator-edit',
  templateUrl: './engine-orchestrator-edit.component.html',
  styleUrls: ['./engine-orchestrator-edit.component.scss'],
  imports: [FormsModule, EngineOrchestratorEditComponent_1]
})
export class EngineOrchestratorEditComponent {
  public nodeTypes = nodeTypes;
  public nodeType = nodeTypes[0];
}
