import { Injectable, signal, TemplateRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PageLayoutService {
  /**
   * The content to go in the right drawer container.
   * If no content is set, the drawer will not be displayed.
   * Use `canDeactivate: [disableRightDrawerGuard]` to automatically remove the content on deactivate.
   */
  public readonly rightDrawerContent = signal<TemplateRef<any>>(undefined);
}
