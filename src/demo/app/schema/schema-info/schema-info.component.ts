import { Component } from '@angular/core';
import { SchemaType } from '@hestia-earth/schema';
import { FormsModule } from '@angular/forms';

import { SchemaInfoComponent as SchemaInfoComponent_1 } from '../../../../schema/schema-info/schema-info.component';

@Component({
  selector: 'app-schema-info',
  templateUrl: './schema-info.component.html',
  styleUrls: ['./schema-info.component.scss'],
  imports: [FormsModule, SchemaInfoComponent_1]
})
export class SchemaInfoComponent {
  public SchemaType = SchemaType;
  public schemaTypes = Object.values(SchemaType);
  public selectedSchemaType = SchemaType.Cycle;
  public schemaField?: string;
}
