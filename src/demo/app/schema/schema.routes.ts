import { Route } from '@angular/router';

import { SchemaInfoComponent } from './schema-info/schema-info.component';

export default [
  {
    path: 'info',
    component: SchemaInfoComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'info'
  }
] satisfies Route[];
