import { Component } from '@angular/core';
import { ICycleJSONLD, NodeType } from '@hestia-earth/schema';

import { NodeListBaseComponent } from '../../node-list-base.component';
import { CyclesCompletenessComponent as CyclesCompletenessComponent_1 } from '../../../../cycles/cycles-completeness/cycles-completeness.component';
import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';

@Component({
  selector: 'app-cycles-completeness',
  templateUrl: './cycles-completeness.component.html',
  styleUrls: ['./cycles-completeness.component.scss'],
  imports: [NodeSuggestFormComponent, NodeListComponent, CyclesCompletenessComponent_1]
})
export class CyclesCompletenessComponent extends NodeListBaseComponent<ICycleJSONLD, NodeType.Cycle> {
  protected get nodeType() {
    return NodeType.Cycle;
  }
}
