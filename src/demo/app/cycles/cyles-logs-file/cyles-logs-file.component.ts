import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ICycleJSONLD, NodeType } from '@hestia-earth/schema';

import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';
import { NodeLogsFileComponent } from '../../../../node/node-logs-file/node-logs-file.component';
import { NodeListBaseComponent } from '../../node-list-base.component';

@Component({
  selector: 'app-cyles-logs-file',
  imports: [NodeSuggestFormComponent, NodeListComponent, FormsModule, NodeLogsFileComponent],
  templateUrl: './cyles-logs-file.component.html',
  styleUrl: './cyles-logs-file.component.scss'
})
export class CylesLogsFileComponent extends NodeListBaseComponent<ICycleJSONLD, NodeType.Cycle> {
  protected get nodeType() {
    return NodeType.Cycle;
  }
}
