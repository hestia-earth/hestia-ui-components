import { Route } from '@angular/router';

import { CyclesNodesComponent } from './cycles-nodes/cycles-nodes.component';
import { CyclesCompletenessComponent } from './cycles-completeness/cycles-completeness.component';
import { CylesLogsFileComponent } from './cyles-logs-file/cyles-logs-file.component';

export default [
  {
    path: 'nodes',
    component: CyclesNodesComponent
  },
  {
    path: 'completeness',
    component: CyclesCompletenessComponent
  },
  {
    path: 'logs/file',
    component: CylesLogsFileComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'nodes'
  }
] satisfies Route[];
