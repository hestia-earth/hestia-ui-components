import { Component, computed, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { toSignal } from '@angular/core/rxjs-interop';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { BlankNodesKey, ICycleJSONLD, NodeType } from '@hestia-earth/schema';

import { NodeListBaseComponent } from '../../node-list-base.component';
import { CycleNodesKeyGroup } from '../../../../cycles';
import { CyclesNodesComponent as CyclesNodesComponent_1 } from '../../../../cycles/cycles-nodes/cycles-nodes.component';
import { NodeListComponent } from '../../shared/node-list/node-list.component';
import { NodeSuggestFormComponent } from '../../shared/node-suggest-form/node-suggest-form.component';

interface ISection {
  title: string;
  nodeKeys: BlankNodesKey[];
  nodeKeyGroup?: CycleNodesKeyGroup;
}

const sections: ISection[] = [
  {
    title: 'Inputs & Products',
    nodeKeys: [BlankNodesKey.inputs, BlankNodesKey.products]
  },
  {
    title: 'Practices',
    nodeKeys: [BlankNodesKey.practices]
  },
  {
    title: 'Emissions',
    nodeKeys: [BlankNodesKey.emissions]
  },
  {
    title: 'Animals',
    nodeKeys: [BlankNodesKey.animals]
  },
  {
    title: 'Animals: Inputs',
    nodeKeys: [BlankNodesKey.inputs],
    nodeKeyGroup: CycleNodesKeyGroup.animals
  },
  {
    title: 'Animals: Practices',
    nodeKeys: [BlankNodesKey.practices],
    nodeKeyGroup: CycleNodesKeyGroup.animals
  },
  {
    title: 'Transformations: Inputs & Products',
    nodeKeys: [BlankNodesKey.inputs, BlankNodesKey.products],
    nodeKeyGroup: CycleNodesKeyGroup.transformations
  },
  {
    title: 'Transformations: Practices',
    nodeKeys: [BlankNodesKey.practices],
    nodeKeyGroup: CycleNodesKeyGroup.transformations
  },
  {
    title: 'Transformations: Emissions',
    nodeKeys: [BlankNodesKey.emissions],
    nodeKeyGroup: CycleNodesKeyGroup.transformations
  }
];

@Component({
  selector: 'app-cycles-nodes',
  templateUrl: './cycles-nodes.component.html',
  styleUrls: ['./cycles-nodes.component.scss'],
  imports: [NodeSuggestFormComponent, NodeListComponent, FormsModule, CyclesNodesComponent_1]
})
export class CyclesNodesComponent extends NodeListBaseComponent<ICycleJSONLD, NodeType.Cycle> {
  private router = inject(Router);
  private route = inject(ActivatedRoute);

  protected get nodeType() {
    return NodeType.Cycle;
  }

  protected sections = sections;
  protected selectedSectionTitle = toSignal(
    this.route.queryParams.pipe(map(({ section }) => section || sections[0].title))
  );
  protected selectedSection = computed(() => sections.find(section => section.title === this.selectedSectionTitle()));

  protected selectSection(section: string) {
    return this.router.navigate(['./'], {
      relativeTo: this.route,
      queryParams: { section }
    });
  }
}
