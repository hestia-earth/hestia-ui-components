import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeAuthService {
  private _token?: string;

  public set token(token: string) {
    this._token = token;
  }

  public get headers(): { [header: string]: string } {
    return this._token ? { 'X-ACCESS-TOKEN': this._token } : {};
  }
}
