import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { ICycleJSONLD, CycleFunctionalUnit } from '@hestia-earth/schema';

@Component({
  selector: 'he-cycles-functional-unit-measure',
  templateUrl: './cycles-functional-unit-measure.component.html',
  styleUrls: ['./cycles-functional-unit-measure.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true
})
export class CyclesFunctionalUnitMeasureComponent {
  protected cycle = input.required<ICycleJSONLD>();
  protected functionalUnit = computed(() => this.cycle()?.functionalUnit);

  protected CycleFunctionalUnit = CycleFunctionalUnit;
}
