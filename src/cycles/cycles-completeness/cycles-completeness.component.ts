import { ChangeDetectionStrategy, Component, computed, effect, inject, input, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { IconDefinition, faCalculator, faDownload, faList } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataState } from '@hestia-earth/api';
import { ICycleJSONLD, NodeType, NonBlankNodesKey } from '@hestia-earth/schema';
import { keyToLabel } from '@hestia-earth/utils';

import { HeNodeStoreService } from '../../node/node-store.service';
import { isValidKey } from '../../common/node-utils';
import { defaultLabel, schemaBaseUrl } from '../../common/utils';
import { logsKey } from '../cycles.model';
import { NodeCsvExportConfirmComponent } from '../../node/node-csv-export-confirm/node-csv-export-confirm.component';
import { NodeLogsModelsComponent } from '../../node/node-logs-models/node-logs-models.component';
import { BlankNodeStateNoticeComponent } from '../../common/blank-node-state-notice/blank-node-state-notice.component';
import { BlankNodeStateComponent } from '../../common/blank-node-state/blank-node-state.component';
import { NodeLinkComponent } from '../../node/node-link/node-link.component';
import { DataTableComponent } from '../../common/data-table/data-table.component';

enum View {
  table = 'Table view',
  logs = 'Recalculations logs'
}

const viewIcon: {
  [view in View]: IconDefinition;
} = {
  [View.logs]: faCalculator,
  [View.table]: faList
};

const headerKeys = ['cycle.id', 'cycle.@id', 'cycle.completeness.'];

// backward compatibility with schema version < 14
const getCompleteness = (cycle: any) => cycle?.completeness || cycle?.dataCompleteness || {};

@Component({
  selector: 'he-cycles-completeness',
  templateUrl: './cycles-completeness.component.html',
  styleUrls: ['./cycles-completeness.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FaIconComponent,
    DataTableComponent,
    NodeLinkComponent,
    BlankNodeStateComponent,
    BlankNodeStateNoticeComponent,
    FormsModule,
    NodeLogsModelsComponent
  ]
})
export class CyclesCompletenessComponent {
  private readonly modalService = inject(NgbModal);
  private readonly nodeStoreService = inject(HeNodeStoreService);

  protected readonly faDownload = faDownload;

  protected readonly dataState = input(DataState.original);

  protected readonly schemaBaseUrl = schemaBaseUrl();
  protected readonly defaultLabel = defaultLabel;
  protected readonly keyToLabel = keyToLabel;
  protected readonly getCompleteness = getCompleteness;
  protected readonly nodeKey = NonBlankNodesKey.completeness;

  protected readonly View = View;
  protected readonly viewIcon = viewIcon;
  private readonly showView = computed(
    () =>
      ({
        [View.logs]: !this.isOriginal(),
        [View.table]: true
      }) as { [view in View]: boolean }
  );
  protected readonly views = computed(() => Object.values(View).filter(view => this.showView()[view]) ?? []);
  protected readonly selectedView = signal(View.table);

  private readonly originalCycles = toSignal(
    this.nodeStoreService.findByState$<ICycleJSONLD>(NodeType.Cycle, DataState.original)
  );
  private readonly _allCycles = toSignal(this.nodeStoreService.find$<ICycleJSONLD>(NodeType.Cycle));
  protected readonly cycles = computed(() => this._allCycles()?.map(data => data[this.dataState()]) || []);

  private readonly selectedIndex = signal(0);
  private readonly ogirinalSelectedCycle = computed(() => this.originalCycles()?.[this.selectedIndex()]);
  private readonly selectedCycle = computed(() => this.cycles()?.[this.selectedIndex()]);
  protected readonly selectedLogsKey = computed(() => logsKey(this.selectedCycle()));
  protected readonly selectedOriginalValues = computed(() => getCompleteness(this.ogirinalSelectedCycle()));
  protected readonly selectedRecalculatedValues = computed(() => getCompleteness(this.selectedCycle()));
  protected readonly selectedNode = computed(() =>
    this.selectedCycle()
      ? {
          ...this.selectedCycle(),
          '@type': NodeType.Cycle,
          type: NodeType.Cycle,
          dataState: DataState.recalculated
        }
      : null
  );

  protected readonly hasData = computed(() => this.cycles().length);
  protected readonly isOriginal = computed(() => this.dataState() === DataState.original);

  protected readonly completenessKeys = computed(() =>
    Object.keys(getCompleteness(this.selectedCycle())).filter(isValidKey).sort()
  );

  constructor() {
    effect(() => {
      // make sure logs does not remain displayed when switching back to original view
      if (this.isOriginal() && this.selectedView() === View.logs) {
        this.selectedView.set(View.table);
      }
    });
  }

  protected trackById(_index: number, item: ICycleJSONLD) {
    return item['@id'];
  }

  protected selectIndex({ target: { value } }) {
    this.selectedIndex.set(+value);
  }

  protected showDownload() {
    const instance = this.modalService.open(NodeCsvExportConfirmComponent);
    const component = instance.componentInstance as NodeCsvExportConfirmComponent;
    component.nodes.set(this.cycles());
    component.filename.set('completeness.csv');
    component.isUpload.set(false);
    component.headerKeys.set(headerKeys);
  }
}
