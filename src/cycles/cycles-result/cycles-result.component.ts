import { Component, ChangeDetectionStrategy, input, computed, Signal } from '@angular/core';
import { ICycleJSONLD, Product } from '@hestia-earth/schema';
import { Chart, ChartConfiguration, ChartData } from 'chart.js';
import { propertyValue } from '@hestia-earth/utils/dist/term';

import { groupNodesByTerm } from '../../common/node-utils';
import { ellipsis, defaultLabel } from '../../common/utils';
import { getColor } from '../../common/color';
import { ChartComponent } from '../../chart/chart.component';

@Component({
  selector: 'he-cycles-result',
  templateUrl: './cycles-result.component.html',
  styleUrls: ['./cycles-result.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [ChartComponent]
})
export class CyclesResultComponent {
  protected cycles = input.required<ICycleJSONLD[]>();

  private productsPerCycle = computed(() => groupNodesByTerm<ICycleJSONLD, Product>(this.cycles(), 'products'));
  private chartLabels = computed(() =>
    this.cycles()
      .map((cycle, index) => ({ cycle, index }))
      .map(({ cycle, index }) => `${index + 1}. ${defaultLabel(cycle)}`)
  );
  private chartDatasets = computed(() =>
    Object.values(this.productsPerCycle() ?? {}).map(({ term: { name, units, '@id': termId }, values }, index) => {
      const color = getColor(index);
      return {
        label: `${name}${units ? ` (in ${units})` : ''}`,
        backgroundColor: color,
        borderColor: color,
        barPercentage: 0.5,
        data: this.cycles().map(({ '@id': id }) => (values[id] ? propertyValue(values[id].value, termId) : 0))
      };
    })
  );
  protected chartData: Signal<ChartData> = computed(() => ({
    datasets: this.chartDatasets(),
    labels: this.chartLabels()
  }));
  protected chartConfig: Partial<ChartConfiguration> = {
    type: 'horizontalBar',
    options: {
      legend: {
        display: true
      },
      tooltips: {
        callbacks: {
          title: (tooltipItems, data) => data.labels![tooltipItems[0].index!] as any
        }
      },
      scales: {
        xAxes: [
          {
            ticks: {
              min: 0
            }
          }
        ],
        yAxes: [
          {
            position: 'left',
            scaleLabel: {
              display: true,
              labelString: 'Cycle'
            }
          }
        ]
      }
    }
  };

  constructor() {
    Chart.scaleService.updateScaleDefaults('category', {
      ticks: {
        callback: tick => ellipsis(`${tick}`, 25)
      }
    });
  }
}
