import { Component, input, computed, signal, effect, Signal } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Emission, ICycleJSONLD, ITermJSONLD } from '@hestia-earth/schema';
import { toPrecision } from '@hestia-earth/utils';
import { propertyValue } from '@hestia-earth/utils/dist/term';
import { ChartConfiguration, ChartData } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';

import { groupNodesByTerm, IGroupedNodesValues } from '../../common/node-utils';
import { defaultLabel } from '../../common/utils';
import { listColor } from '../../common/color';
import { ChartComponent } from '../../chart/chart.component';

const cycleValue = (cycle: ICycleJSONLD, values: IGroupedNodesValues<Emission>) =>
  (values[cycle['@id']]?.nodes[0] || { value: [0] }).value;

const cycleName = (cycle: ICycleJSONLD, index: number) => `${index + 1}. ${defaultLabel(cycle)}`;

@Component({
  selector: 'he-cycles-emissions-chart',
  templateUrl: './cycles-emissions-chart.component.html',
  styleUrls: ['./cycles-emissions-chart.component.scss'],
  imports: [FormsModule, ChartComponent]
})
export class CyclesEmissionsChartComponent {
  protected cycles = input.required<ICycleJSONLD[]>();

  protected emissionPerCycle = computed(() => groupNodesByTerm<ICycleJSONLD, Emission>(this.cycles(), 'emissions'));
  protected terms = computed(() =>
    Object.values(this.emissionPerCycle() ?? {})
      .filter(({ values }) =>
        Object.values(values).some(({ nodes: [{ value }] }) => (propertyValue(value) as number) >= 0)
      )
      .map(({ term }) => term)
      .sort((a, b) => a.name.localeCompare(b.name))
  );

  protected selectedTerm = signal<ITermJSONLD>(undefined);

  private chartLabels = computed(() => this.cycles().map(cycleName));
  private chartColors = computed(() => this.cycles().map(listColor));
  private chartValues = computed(() => this.emissionPerCycle()?.[this.selectedTerm()?.name]?.values || {});
  protected chartData: Signal<ChartData> = computed(() => ({
    datasets: [
      {
        label: this.selectedTerm()?.name || '',
        data: this.cycles().map(
          cycle => propertyValue(cycleValue(cycle, this.chartValues())!, this.selectedTerm?.['@id']) as number
        ),
        backgroundColor: this.chartColors(),
        borderColor: this.chartColors()
      }
    ],
    labels: this.chartLabels()
  }));
  protected chartConfig: Signal<Partial<ChartConfiguration>> = computed(() => ({
    type: 'bar',
    plugins: [ChartDataLabels],
    options: {
      plugins: {
        datalabels: {
          color: 'black',
          formatter: value => toPrecision(value, 3),
          anchor: 'center',
          clamp: true
        }
      },
      scales: {
        xAxes: [
          {
            display: true
          }
        ],
        yAxes: [
          {
            position: 'left',
            ticks: {
              min: 0
            }
          }
        ]
      }
    }
  }));

  constructor() {
    effect(() => {
      // make sure selected term exists
      const terms = this.terms();
      const selectedTermId = this.selectedTerm()?.['@id'];
      if (!selectedTermId || !terms.find(term => term['@id'] === selectedTermId)) {
        this.selectedTerm.set(terms[0]);
      }
    });
  }

  protected selectTerm({ target: { value } }) {
    const term = this.terms().find(term => term['@id'] === value);
    this.selectedTerm.set(term);
  }
}
