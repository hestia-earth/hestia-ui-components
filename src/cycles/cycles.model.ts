import { NodeType, ICycleJSONLD } from '@hestia-earth/schema';

export interface ICycleJSONLDExtended extends ICycleJSONLD {
  related: {
    [type in NodeType]?: string[];
  };
}

export const primaryProduct = ({ products }: Partial<ICycleJSONLD>) => (products || []).find(({ primary }) => primary);

/**
 * If Transformation, return the Transformation Term @id.
 *
 * @param node Cycle or Transformation
 * @returns
 */
export const logsKey = (node: any) => ((node.type || node['@type']) === NodeType.Cycle ? null : node?.term?.['@id']);
