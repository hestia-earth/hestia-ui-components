import { ChangeDetectionStrategy, Component, computed, effect, inject, input, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { DataState } from '@hestia-earth/api';
import {
  ICycleJSONLD,
  Input as HestiaInput,
  Product,
  NodeType,
  Animal,
  Emission,
  Practice,
  TermTermType,
  EmissionMethodTier,
  Term,
  Transformation,
  BlankNodesKey,
  blankNodesType
} from '@hestia-earth/schema';
import orderBy from 'lodash.orderby';
import { propertyValue } from '@hestia-earth/utils/dist/term';
import { unique } from '@hestia-earth/utils';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbPopover, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

import { filterBlankNode, methodTierOrder } from '../../common/node-utils';
import { logsKey } from '../cycles.model';
import { HeNodeStoreService } from '../../node/node-store.service';
import { groupNodesByTerm, IGroupedKeys, grouppedKeys } from '../../common/node-utils';
import { schemaBaseUrl, defaultLabel } from '../../common/utils';
import { PrecisionPipe } from '../../common/precision.pipe';
import { KeyToLabelPipe } from '../../common/key-to-label.pipe';
import { EllipsisPipe } from '../../common/ellipsis.pipe';
import { DefaultPipe } from '../../common/default.pipe';
import { CompoundPipe } from '../../common/compound.pipe';
import { NodeValueDetailsComponent } from '../../node/node-value-details/node-value-details.component';
import { NodeCsvExportConfirmComponent } from '../../node/node-csv-export-confirm/node-csv-export-confirm.component';
import { NodeLogsModelsComponent } from '../../node/node-logs-models/node-logs-models.component';
import { CyclesNodesTimelineComponent } from '../cycles-nodes-timeline/cycles-nodes-timeline.component';
import { NgTemplateOutlet } from '@angular/common';
import { CyclesEmissionsChartComponent } from '../cycles-emissions-chart/cycles-emissions-chart.component';
import { CyclesResultComponent } from '../cycles-result/cycles-result.component';
import { BlankNodeStateNoticeComponent } from '../../common/blank-node-state-notice/blank-node-state-notice.component';
import { BlankNodeStateComponent } from '../../common/blank-node-state/blank-node-state.component';
import { CyclesFunctionalUnitMeasureComponent } from '../cycles-functional-unit-measure/cycles-functional-unit-measure.component';
import { TermsUnitsDescriptionComponent } from '../../terms/terms-units-description/terms-units-description.component';
import { NodeLinkComponent } from '../../node/node-link/node-link.component';
import { DataTableComponent } from '../../common/data-table/data-table.component';
import { SearchExtendComponent } from '../../common/search-extend/search-extend.component';
import { faCalculator, faChartBar, faDownload, faList, faListAlt } from '@fortawesome/free-solid-svg-icons';

enum View {
  table = 'Table view',
  chart = 'Chart view',
  timeline = 'Operations Timeline',
  logs = 'Recalculations logs'
}

const timelineTermType = [TermTermType.operation];

interface ICycleConfig {
  nodeKey: BlankNodesKey;
}

interface ICycleConfigAnimal extends ICycleConfig {
  nodeKey: BlankNodesKey.animals;
}

interface ICycleConfigEmission extends ICycleConfig {
  nodeKey: BlankNodesKey.emissions;
}

interface ICycleConfigInput extends ICycleConfig {
  nodeKey: BlankNodesKey.inputs;
}

interface ICycleConfigPractice extends ICycleConfig {
  nodeKey: BlankNodesKey.practices;
}

const viewIcon: {
  [view in View]: IconDefinition;
} = {
  [View.chart]: faChartBar,
  [View.logs]: faCalculator,
  [View.table]: faList,
  [View.timeline]: faListAlt
};

const nodeKeyViews: {
  [type in BlankNodesKey]?: View[];
} = {
  [BlankNodesKey.animals]: [View.table, View.logs],
  [BlankNodesKey.emissions]: [View.table, View.chart, View.logs],
  [BlankNodesKey.inputs]: [View.table, View.chart, View.logs],
  [BlankNodesKey.products]: [View.table, View.chart, View.logs],
  [BlankNodesKey.practices]: [View.table, View.timeline, View.logs]
};

const nodeKeyFilterTermTypes: {
  [type in BlankNodesKey]?: TermTermType[];
} = {
  [BlankNodesKey.emissions]: [TermTermType.emission]
};

const filterValuesTimeline = (values: blankNodesType[]) =>
  values.filter(p => timelineTermType.includes(p.term?.termType));

type groupedEmissions = {
  [methodTier in EmissionMethodTier]: IGroupedKeys<Emission>[];
};

export enum CycleNodesKeyGroup {
  animals = 'animals',
  transformations = 'transformations'
}

type groupedNode = Animal | Transformation;

const cycleGroupNode =
  (cycle?: ICycleJSONLD) =>
  (node?: groupedNode): groupedNode & { name: string; '@id': string } =>
    node
      ? {
          ...node,
          name: cycle?.name,
          '@id': cycle?.['@id'] // set @id so we can fetch the logs from the Cycle
        }
      : null;

const cycleGroupNodes = (groupKey: CycleNodesKeyGroup, cycle?: ICycleJSONLD): groupedNode[] =>
  (cycle?.[groupKey] || []).map(cycleGroupNode(cycle)).filter(Boolean);

const filterGroupNodesByTerm = (groupKey: CycleNodesKeyGroup, cycles: ICycleJSONLD[], term?: Term): groupedNode[] =>
  cycles
    .map(cycle =>
      cycleGroupNode(cycle)(
        ((cycle?.[groupKey] as groupedNode[]) || []).filter(Boolean).find(node => node.term?.name === term?.name)
      )
    )
    .filter(Boolean);

@Component({
  selector: 'he-cycles-nodes',
  templateUrl: './cycles-nodes.component.html',
  styleUrls: ['./cycles-nodes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FaIconComponent,
    SearchExtendComponent,
    DataTableComponent,
    NodeLinkComponent,
    TermsUnitsDescriptionComponent,
    CyclesFunctionalUnitMeasureComponent,
    NgbPopover,
    BlankNodeStateComponent,
    BlankNodeStateNoticeComponent,
    CyclesResultComponent,
    CyclesEmissionsChartComponent,
    NgTemplateOutlet,
    CyclesNodesTimelineComponent,
    NodeLogsModelsComponent,
    NodeValueDetailsComponent,
    FormsModule,
    CompoundPipe,
    DefaultPipe,
    EllipsisPipe,
    KeyToLabelPipe,
    PrecisionPipe
  ]
})
export class CyclesNodesComponent {
  private readonly modalService = inject(NgbModal);
  private readonly nodeStoreService = inject(HeNodeStoreService);

  protected readonly faDownload = faDownload;

  protected readonly dataState = input(DataState.original);
  protected readonly nodeKeys = input.required<BlankNodesKey[]>();
  protected readonly nodeKeyGroup = input<CycleNodesKeyGroup>();

  protected readonly BlankNodesKey = BlankNodesKey;
  protected readonly firstNodeKey = computed(() => this.nodeKeys()?.[0]);
  protected readonly selectedNodeKey = signal(undefined as BlankNodesKey);

  protected readonly selectedGroupTerm = signal(undefined as Term);

  protected readonly schemaBaseUrl = schemaBaseUrl();
  protected readonly propertyValue = propertyValue;
  protected readonly defaultLabel = defaultLabel;

  protected readonly headerKeys = computed(() => [
    'cycle.id',
    'cycle.@id',
    'transformation.id',
    'transformation.@id',
    ...this.nodeKeys().flatMap(key => [`cycle.${key}.`, `transformation.${key}.`])
  ]);

  protected readonly View = View;
  protected readonly viewIcon = viewIcon;
  private readonly showView = computed(
    () =>
      ({
        [View.chart]: this.selectedNodeKey() === BlankNodesKey.emissions ? this.cycles().length > 1 : this.hasData(),
        [View.logs]: !this.isOriginal() && this.hasRecalculatedNodes(),
        [View.table]: true,
        [View.timeline]: this.enableTimeline()
      }) as { [view in View]: boolean }
  );
  protected readonly views = computed(
    () => nodeKeyViews[this.selectedNodeKey()]?.filter(view => this.showView()[view]) ?? []
  );
  protected readonly selectedView = signal(View.table);

  private readonly _allNodes = toSignal(this.nodeStoreService.find$<ICycleJSONLD>(NodeType.Cycle));
  private readonly originalNodes = computed(() => this._allNodes()?.map(data => data[DataState.original]) || []);
  private readonly currentNodes = computed(() => this._allNodes()?.map(data => data[this.dataState()]) || []);

  // can be Cycle or Transformation
  protected readonly originalCycles = computed(() =>
    this.nodeKeyGroup()
      ? filterGroupNodesByTerm(this.nodeKeyGroup(), this.originalNodes(), this.selectedGroupTerm())
      : this.originalNodes()
  );
  protected readonly cycles = computed(() =>
    this.nodeKeyGroup()
      ? filterGroupNodesByTerm(this.nodeKeyGroup(), this.currentNodes(), this.selectedGroupTerm())
      : this.currentNodes()
  );

  private readonly selectedIndex = signal(0);
  private readonly ogirinalSelectedCycle = computed(() => this.originalCycles()?.[this.selectedIndex()]);
  private readonly selectedCycle = computed(() => this.cycles()?.[this.selectedIndex()]);
  protected readonly selectedLogsKey = computed(() => logsKey(this.selectedCycle()));
  protected readonly selectedOriginalValues = computed(
    () => this.ogirinalSelectedCycle()?.[this.selectedNodeKey()] || []
  );
  protected readonly selectedRecalculatedValues = computed(() => this.selectedCycle()?.[this.selectedNodeKey()] || []);
  // TODO: handle transformation and non-indexed nodes on CE
  protected readonly selectedNode = computed(() =>
    this.selectedCycle()
      ? {
          ...this.selectedCycle(),
          '@type': NodeType.Cycle,
          type: NodeType.Cycle,
          dataState: DataState.recalculated
        }
      : null
  );

  private readonly isOriginal = computed(() => this.dataState() === DataState.original);
  private readonly originalValues = computed(() => (this.isOriginal() ? null : this.originalCycles()) || []);

  private readonly hasRecalculatedNodes = computed(() =>
    this.cycles().some(({ aggregated }: { aggregated?: any }) => !aggregated)
  );
  protected readonly showSwitchToRecalculated = computed(() => this.isOriginal() && this.hasRecalculatedNodes());

  protected readonly timelineValues = computed(() =>
    filterValuesTimeline(this.selectedCycle()?.[this.selectedNodeKey()] || [])
  );
  private readonly enableTimeline = computed(() => this.timelineValues().length > 0);

  protected readonly isNodeKeyAllowed = computed(() => nodeKeyViews[this.firstNodeKey()].includes(this.selectedView()));

  private readonly groupedNodes = computed(() =>
    this.originalNodes().flatMap(cycle => cycleGroupNodes(this.nodeKeyGroup(), cycle))
  );
  protected readonly groupNodeTerms = computed(() => unique(this.groupedNodes().map(({ term }) => term)));
  protected readonly isGroupNode = computed(() => this.nodeKeyGroup() && this.groupedNodes()?.length > 0);

  protected readonly isEmission = computed(() => this.selectedNodeKey() === BlankNodesKey.emissions);

  protected readonly data = computed(() =>
    this.isEmission()
      ? this.groupEmissions()
      : Object.fromEntries(this.nodeKeys().map(nodeKey => [nodeKey, this.groupNodesByKey({ nodeKey })]))
  );
  protected readonly dataKeys = computed(() => Object.keys(this.data()));

  protected readonly hasData = computed(() => Object.values(this.data()).flat().length > 0);

  protected readonly filterTermTypes = computed(() => nodeKeyFilterTermTypes[this.selectedNodeKey()]);
  protected readonly filterTerm = signal('');

  protected readonly csvFilename = computed(() => `${this.nodeKeys().join('-')}.csv`);

  constructor() {
    effect(() => {
      // make sure logs does not remain displayed
      if ((this.isOriginal() && this.selectedView() === View.logs) || !this.views().includes(this.selectedView())) {
        this.selectedView.set(View.table);
      }
    });

    effect(() => {
      if (!this.selectedNodeKey() || !this.nodeKeys().includes(this.selectedNodeKey())) {
        this.selectedNodeKey.set(this.firstNodeKey());
        this.selectedView.set(View.table);
      }
    });

    effect(() => {
      if (!this.selectedGroupTerm() || !this.groupNodeTerms().includes(this.selectedGroupTerm())) {
        this.selectedGroupTerm.set(this.groupNodeTerms()[0]);
      }
    });
  }

  private groupNodesByKey<
    T extends ICycleConfig,
    R = T extends ICycleConfigAnimal
      ? Animal
      : T extends ICycleConfigEmission
        ? Emission
        : T extends ICycleConfigInput
          ? HestiaInput
          : T extends ICycleConfigPractice
            ? Practice
            : Product
  >({ nodeKey }: T): IGroupedKeys<R>[] {
    const nodesPerCycle = groupNodesByTerm<ICycleJSONLD | groupedNode, R>(
      this.cycles(),
      nodeKey,
      this.originalValues(),
      filterBlankNode(this.filterTerm())
    );
    return orderBy(grouppedKeys(nodesPerCycle), ['key'], ['asc']);
  }

  private groupEmissions() {
    const emissionsPerCycle = groupNodesByTerm<ICycleJSONLD | groupedNode, Emission>(
      this.cycles(),
      BlankNodesKey.emissions,
      this.originalValues(),
      filterBlankNode(this.filterTerm())
    );
    const values: IGroupedKeys<Emission>[] = orderBy(grouppedKeys(emissionsPerCycle), ['key'], ['asc']);
    const methodTiers = orderBy(
      unique(values.map(({ value }) => value.methodTier)).map(methodTier => ({
        methodTier,
        methodTierOrder: methodTierOrder(methodTier)
      })),
      ['methodTierOrder'],
      ['asc']
    );
    return Object.fromEntries(
      methodTiers.map(({ methodTier }) => [methodTier, values.filter(({ value }) => value.methodTier === methodTier)])
    ) as groupedEmissions;
  }

  protected trackById(_index: number, item: ICycleJSONLD | groupedNode) {
    return item['@id'];
  }

  protected selectIndex({ target: { value } }) {
    this.selectedIndex.set(+value);
  }

  protected cycleNode(cycle: ICycleJSONLD | groupedNode) {
    return 'term' in cycle ? cycle.term : cycle;
  }

  protected showDownload() {
    const instance = this.modalService.open(NodeCsvExportConfirmComponent);
    const component = instance.componentInstance as NodeCsvExportConfirmComponent;
    component.nodes.set(this.cycles());
    component.filename.set(this.csvFilename());
    component.isUpload.set(false);
    component.headerKeys.set(this.headerKeys());
  }
}
