import { Component, ElementRef, computed, inject, input } from '@angular/core';
import { DatePipe } from '@angular/common';
import { DataState } from '@hestia-earth/api';
import { TermTermType, blankNodesType } from '@hestia-earth/schema';

import { BlankNodeStateComponent } from '../../common/blank-node-state/blank-node-state.component';
import { SortByPipe } from '../../common/sort-by.pipe';
import { CompoundPipe } from '../../common/compound.pipe';

@Component({
  selector: 'he-cycles-nodes-timeline',
  templateUrl: './cycles-nodes-timeline.component.html',
  styleUrls: ['./cycles-nodes-timeline.component.scss'],
  imports: [BlankNodeStateComponent, DatePipe, CompoundPipe, SortByPipe]
})
export class CyclesNodesTimelineComponent {
  private elementRef = inject(ElementRef);

  protected recalculatedValues = input([] as blankNodesType[]);
  protected dataState = input(DataState.original);

  protected now = new Date();
  protected TermTermType = TermTermType;

  protected blankNodes = computed(() =>
    this.recalculatedValues().map(v =>
      'startDate' in v
        ? v
        : 'dates' in v
          ? v.dates?.length
            ? {
                ...v,
                startDate: v.dates[0]
              }
            : v
          : v
    )
  );

  public trackByBlankNode(_index: number, { term }: blankNodesType) {
    return term['@id'];
  }

  public get lineWidth() {
    return this.elementRef.nativeElement.offsetWidth - 60;
  }
}
