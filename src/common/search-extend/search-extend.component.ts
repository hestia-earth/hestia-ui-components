import { Component, ElementRef, input, model, output, signal, viewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'he-search-extend',
  templateUrl: './search-extend.component.html',
  styleUrls: ['./search-extend.component.scss'],
  imports: [FormsModule, FaIconComponent]
})
export class SearchExtendComponent {
  protected readonly faTimes = faTimes;
  protected readonly faSearch = faSearch;

  private readonly searchInput = viewChild<ElementRef>('searchInput');

  protected readonly value = model('');
  protected readonly disabled = input(false);
  protected readonly placeholder = input<string>(undefined);
  protected readonly class = input<string>(undefined);

  protected readonly search = output<any>();

  protected readonly extended = signal(false);

  private extend() {
    this.extended.set(true);
    setTimeout(() => this.searchInput().nativeElement.focus());
  }

  private applySearch() {
    !!this.value() || this.searchInput().nativeElement.focus();
    this.search.emit(this.value());
  }

  protected clear() {
    this.value.set('');
    return this.submit();
  }

  protected submit() {
    return this.extended() ? this.applySearch() : this.extend();
  }
}
