import { timer } from 'rxjs';
import { distinctUntilChanged, first, map, take } from 'rxjs/operators';
import isEqual from 'lodash.isequal';

export const distinctUntilChangedDeep = <T>() => distinctUntilChanged<T>((x, y) => isEqual(x, y));

export const takeAfterViewInit = <T>(fn: () => T, settings: { take: number } = { take: 10 }) =>
  timer(0, 50).pipe(
    take(settings.take),
    map(() => fn()),
    first(returnItem => !!returnItem)
  );
