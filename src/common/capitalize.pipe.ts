import { Pipe, PipeTransform } from '@angular/core';

import { capitalize } from './utils';

@Pipe({
  name: 'capitalize',
  standalone: true
})
export class CapitalizePipe implements PipeTransform {
  transform = capitalize;
}
