import {
  AfterContentChecked,
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  effect,
  inject,
  input,
  signal
} from '@angular/core';

import { ResizedEvent, ResizedDirective } from '../resized.directive';

const defaultSize = '100%';

@Component({
  selector: 'he-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  imports: [ResizedDirective],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataTableComponent implements AfterViewInit, AfterContentChecked {
  private readonly elementRef = inject(ElementRef);

  protected readonly minHeight = input<string | number>(200);
  protected readonly maxHeight = input<string | number>();
  protected readonly small = input(false);

  protected readonly height = signal(defaultSize);
  protected readonly width = signal(defaultSize);

  @HostBinding('class.is-small')
  public get isSmall() {
    return this.small();
  }

  ngAfterViewInit() {
    this.updateTableSize();
  }

  ngAfterContentChecked() {
    this.updateTableSize();
  }

  constructor() {
    effect(() => this.updateTableSize());
  }

  private get tableEl() {
    return this.elementRef.nativeElement.querySelector('table');
  }

  private updateTableSize() {
    const height = this.tableEl.offsetHeight;
    const minHeight = this.minHeight() ? +this.minHeight() : height;
    const maxHeight = this.maxHeight() ? +this.maxHeight() : height;
    this.height.set(`${Math.max(minHeight, Math.min(height, maxHeight))}px`);
  }

  protected onResized({ newRect: { width } }: ResizedEvent) {
    this.width.set(`${width}px`);
  }
}
