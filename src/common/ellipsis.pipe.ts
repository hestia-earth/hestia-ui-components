import { Pipe, PipeTransform } from '@angular/core';

import { ellipsis } from './utils';

@Pipe({
  name: 'ellipsis',
  standalone: true
})
export class EllipsisPipe implements PipeTransform {
  transform(value: any, maxLength?: number) {
    return ellipsis(`${value}`, maxLength);
  }
}
