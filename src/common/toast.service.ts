import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

export interface IToast {
  id: number;
  message: string;
  duration: number;
  color: 'success' | 'danger';
  showRawMessage?: boolean;
}

let toastId = 0;

@Injectable({
  providedIn: 'root'
})
export class HeToastService {
  public toasts$ = new ReplaySubject<IToast>(1);

  public success(message: string, duration = 3000) {
    this.toasts$.next({
      id: ++toastId,
      message,
      duration,
      color: 'success',
      showRawMessage: true
    });
  }

  public error(message: string, showRawMessage = true, duration = 3000) {
    this.toasts$.next({
      id: ++toastId,
      message: (`${message}` || '').trim(),
      duration,
      color: 'danger',
      showRawMessage
    });
  }
}
