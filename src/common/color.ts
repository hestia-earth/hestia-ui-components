import Gradient from 'javascript-color-gradient';
import { ShadeGenerator } from 'shade-generator/dist/shadeGenerator';

export enum ColorPalette {
  categorial,
  continuous,
  divergent
}

const palette: {
  [palette in ColorPalette]: string[];
} = Object.freeze({
  [ColorPalette.categorial]: [
    '#193957',
    '#FFC000',
    '#249DA5',
    '#DC7033',
    '#349B3E',
    '#CCA76D',
    '#6394CE',
    '#9AB04C',
    '#8B50A7',
    '#8A5402'
  ],
  [ColorPalette.continuous]: [
    '#193957',
    '#1D586F',
    '#207788',
    '#498F78',
    '#7FA55B',
    '#B0B43B',
    '#DDBB19',
    '#FFC514',
    '#FDD866',
    '#FCEBB8'
  ],
  [ColorPalette.divergent]: [
    '#193957',
    '#1D6176',
    '#228A96',
    '#58B3B5',
    '#ACD6CF',
    '#FCEBB8',
    '#FCE18F',
    '#FDD763',
    '#FECC34',
    '#FFC000'
  ]
});

const getGradients = (...colours: string[]): string[] =>
  new Gradient()
    .setColorGradient(...colours)
    .setMidpoint(10)
    .getColors();

const getColorGradient = (index: number, colors: string[]) => {
  const start = colors[index % colors.length];
  const end = colors[(index % colors.length) + 1];
  const gradientIndex = Math.ceil(index / colors.length);
  return getGradients(start, end)[gradientIndex];
};

const getColorShade = (index: number, colors: string[]) => {
  const color = colors[index % colors.length];
  const shade = Math.min(100, Math.max(10, Math.abs(100 - Math.ceil(index / colors.length) * 10)));
  return ShadeGenerator.hue(color)
    .shade(`${shade}` as any)
    .hex();
};

export const getColor = (index: number, scheme = ColorPalette.categorial) => {
  const colors = palette[scheme];
  return index < colors.length
    ? colors[index]
    : scheme === ColorPalette.categorial
      ? getColorShade(index, colors)
      : getColorGradient(index, colors);
};

export const listColor = <T>(_v: T, index: number): string => getColor(index);

export const listColorContinuous = <T>(_v: T, index: number): string => getColor(index, ColorPalette.continuous);
