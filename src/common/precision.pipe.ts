import { Pipe, PipeTransform } from '@angular/core';
import { isUndefined, isNumber, toPrecision, toComma } from '@hestia-earth/utils';

const parsePrecision = (value: string | number, precision: string | number) =>
  toPrecision(parseFloat(`${value}`), parseInt(`${precision}`, 10));

export const transform = (value: string | number | boolean, precision: string | number = 3, enableComma = true) =>
  typeof value !== 'boolean' && !isUndefined(value) && isNumber(value)
    ? `${(enableComma ? toComma : v => v)(parsePrecision(value, precision))}`.replace(/\.00$/, '')
    : value;

@Pipe({
  name: 'precision',
  standalone: true
})
export class PrecisionPipe implements PipeTransform {
  transform(value: string | number | boolean, precision: string | number = 3, enableComma = true) {
    return transform(value, precision, enableComma);
  }
}
