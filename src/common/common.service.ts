import { Injectable, InjectionToken, inject } from '@angular/core';

export const HE_API_BASE_URL = new InjectionToken<string>('HE_API_BASE_URL');

@Injectable({
  providedIn: 'root'
})
export class HeCommonService {
  private _apiBaseUrl = inject(HE_API_BASE_URL);

  public get apiBaseUrl() {
    return this._apiBaseUrl || '/api';
  }
}
