import get from 'lodash.get';
import {
  Term,
  ICycleJSONLD,
  ISiteJSONLD,
  IImpactAssessmentJSONLD,
  EmissionMethodTier,
  ITermJSONLD,
  Indicator,
  Animal,
  Transformation,
  blankNodesType,
  blankNodesKey
} from '@hestia-earth/schema';
import { propertyValue, propertyValueType } from '@hestia-earth/utils/dist/term';
import { getDefaultModelId } from '@hestia-earth/glossary';
import { monthsBefore, unique } from '@hestia-earth/utils';

interface IDates {
  /**
   * The end date in iso format.
   */
  end: string;
  /**
   * The start date in iso format.
   * If not provided, use the end date 1 month before.
   */
  start?: string;
}

/* eslint-disable-next-line complexity */
export const formatDate = (date: string, isStart = false) =>
  date?.length === 4
    ? isStart
      ? new Date(`${date}-01-01T12:00:00.000Z`)
      : new Date(`${date}-12-31T12:00:00.000Z`)
    : date?.length === 7
      ? isStart
        ? new Date(`${date}-15T12:00:00.000Z`)
        : new Date(`${date}-14T12:00:00.000Z`)
      : date?.length === 10
        ? new Date(`${date}T12:00:00.000Z`)
        : date
          ? new Date(date)
          : undefined;

export const getDatesBetween = (startDate: Date, endDate: Date) => {
  const dates: Date[] = [];

  let date = new Date(startDate);
  while (date <= new Date(endDate)) {
    dates.push(date);
    date = new Date(date);
    date.setMonth(date.getMonth() + 1);
    date.setUTCHours(12);
  }

  return dates;
};

export const sortedDates = (dates: Date[]) =>
  unique(dates).sort((a, b) => new Date(a).getTime() - new Date(b).getTime());

export const uniqueDatesBetween = (dates: Date[]) => {
  const allDates = sortedDates(dates);
  return getDatesBetween(allDates.shift(), allDates.pop());
};

export const isDateBetween = (date: string | Date, dates: IDates) => {
  const end = formatDate(dates.end, false);
  const start = dates.start ? formatDate(dates.start, true) : monthsBefore(end, 1);
  return [new Date(date).getTime() >= start.getTime(), new Date(date).getTime() <= end.getTime()].every(Boolean);
};

export const filterBlankNode =
  (filterTerm: string) =>
  ({ term }: blankNodesType) =>
    !filterTerm || term?.name?.toLowerCase()?.includes(filterTerm.toLowerCase());

export const ignoreKeys = ['@type', 'type', 'added', 'updated', 'addedVersion', 'updatedVersion'];

export const isValidKey = (key: string) => !ignoreKeys.includes(key);

export const isMethodModelAllowed = (filterMethod?: Term) => (node: Indicator) =>
  node.methodModel?.['@id'] === (filterMethod ? filterMethod['@id'] : getDefaultModelId(node.term?.['@id']));

export enum NodeKeyState {
  added = 'added',
  updated = 'updated',
  deleted = 'deleted',
  unchanged = 'unchanged'
}

export const isState = (node: blankNodesType, key: string, state: NodeKeyState) =>
  state in node! && (typeof node![state] === 'boolean' ? node![state] : node![state].includes(key));

export interface IGroupedNode<T> {
  // store the node with the highest "state"
  node: T;
  nodes: T[];
  index: number;
  value: propertyValueType[];
  dates?: any[];
  sd?: number[];
  min?: number[];
  max?: number[];
  inputs?: any[];
  depthUpper?: number[];
  depthLower?: number[];
  startDate?: string[];
  endDate?: string[];
}

export interface IGroupedNodesValues<T> {
  [nodeId: string]: IGroupedNode<T>;
}

export interface IGroupedNodesValue<T> {
  term: ITermJSONLD;
  methodTier?: EmissionMethodTier;
  /**
   * Enable ordering ny methodTier
   */
  methodTierOrder: number;
  values: IGroupedNodesValues<T>;
  originalValues: {
    [nodeId: string]: {
      value: propertyValueType;
    };
  };
}

export interface IGroupedNodes<T> {
  [termId: string]: IGroupedNodesValue<T>;
}

export interface IGroupedKeys<T> {
  key: string;
  value: IGroupedNodesValue<T>;
}

export const grouppedKeys = <T>(values: { [key: string]: T }): { key: string; value: T }[] =>
  Object.entries(values).map(([key, value]) => ({ key, value }));

const concatBlankNodeValue = (value: any, newValue: any) => {
  const valueArray = Array.isArray(value) ? value : [value];
  const newValueArray = Array.isArray(newValue) ? newValue : [newValue];
  return [...valueArray, ...newValueArray];
};

export const methodTierOrder = (methodTier: EmissionMethodTier) =>
  [
    EmissionMethodTier.measured,
    EmissionMethodTier['tier 3'],
    EmissionMethodTier['tier 2'],
    EmissionMethodTier['tier 1'],
    EmissionMethodTier.background,
    EmissionMethodTier['not relevant']
  ].indexOf(methodTier);

export const grouppedValueKeys = [
  'dates',
  'sd',
  'min',
  'max',
  'inputs',
  'depthUpper',
  'depthLower',
  'startDate',
  'endDate',
  'methodTier',
  'methodModel',
  'model',
  'method'
];

const isHigherState = (sourceNode: blankNodesType, newNode: blankNodesType) =>
  !sourceNode ||
  isState(newNode, 'value', NodeKeyState.updated) ||
  (!isState(sourceNode, 'value', NodeKeyState.updated) && isState(newNode, 'value', NodeKeyState.added));

const groupNodeKey = ({ term }: any) => term?.name || term?.['@id'];

export const groupNodesByTerm = <
  T extends ICycleJSONLD | IImpactAssessmentJSONLD | ISiteJSONLD | Animal | Transformation,
  R
>(
  nodes: T[] = [],
  key: blankNodesKey,
  originalValues: T[] = [],
  includeNode = (_node: any) => true
): IGroupedNodes<R> => {
  const groups: IGroupedNodes<R> = nodes.reduce(
    (prev, node, index) =>
      (node[key] || []).reduce((group, blankNode) => {
        // skip node based on condition
        if (!includeNode(blankNode)) {
          return group;
        }

        const nodeId = node['@id'];
        const groupedKey = groupNodeKey(blankNode);
        group[groupedKey] = group[groupedKey] || {
          term: blankNode.term,
          methodTier: blankNode.methodTier,
          methodTierOrder: methodTierOrder(blankNode.methodTier),
          values: {},
          originalValues: {}
        };
        group[groupedKey].values[nodeId] = group[groupedKey].values[nodeId] || { index, nodes: [], value: [] };
        group[groupedKey].values[nodeId].node = isHigherState(group[groupedKey].values[nodeId].node, blankNode)
          ? blankNode
          : group[groupedKey].values[nodeId].node;
        group[groupedKey].values[nodeId].nodes.push(blankNode);
        group[groupedKey].values[nodeId].value = concatBlankNodeValue(
          group[groupedKey].values[nodeId].value,
          blankNode.value
        );
        grouppedValueKeys.forEach(arrayKey => {
          const newValue = get(blankNode, arrayKey, []);
          group[groupedKey].values[nodeId][arrayKey] = [
            ...(group[groupedKey].values[nodeId][arrayKey] || []),
            ...(Array.isArray(newValue) ? newValue : [newValue])
          ];
        });
        return group;
      }, prev),
    {} as IGroupedNodes<R>
  );

  // compile original values
  Object.values(groups).map(group => {
    Object.keys(group.values).map(nodeId => {
      const { index } = group.values[nodeId];
      const termId = group.term['@id'];
      const originalValue = get(originalValues, `[${index}].${key}`, []).filter(
        (val: any) => val.term['@id'] === termId
      );
      if (originalValue.length > 0) {
        const value = originalValue.reduce((array: any[], curr: any) => concatBlankNodeValue(array, curr.value), []);
        group.originalValues[nodeId] = { value: propertyValue(value, termId) };
      }
    });
  });

  return groups;
};
