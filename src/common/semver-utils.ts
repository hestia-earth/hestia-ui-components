import * as semver from 'semver';

/* eslint-disable max-len */
const semverRegex = () =>
  /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/gi;
/* eslint-enable max-len */

const sort = (versions: string[], compare: (a: string, b: string) => number) =>
  versions.sort((v1, v2) => {
    const sv1 = semverRegex().exec(v1)[0] || v1;
    const sv2 = semverRegex().exec(v2)[0] || v2;
    return compare(sv1, sv2);
  });

export const asc = (versions: string[]) => sort(versions, semver.compare);

export const desc = (versions: string[]) => sort(versions, semver.rcompare);

export const major = (version = '') => version.split('.')[0];
export const minor = (version = '') => [version.split('.')[0], version.split('.')[1]].join('.');
