import { isEmpty } from '@hestia-earth/utils';
import { json2csv } from 'json-2-csv';

export enum Level {
  debug = 'DEBUG',
  info = 'INFO',
  warning = 'WARNING',
  error = 'ERROR'
}

export const levels = Object.values(Level);

export interface ILine {
  code: string;
  data: any;
  class: string;
  level: Level;
}

const parseLine = (text: string) => {
  try {
    return JSON.parse(text);
  } catch (_err) {
    return { level: Level.debug };
  }
};

const levelToClass = {
  [Level.debug]: 'has-text-info',
  [Level.error]: 'has-text-danger',
  [Level.info]: '',
  [Level.warning]: 'has-text-warning'
};

export const parseData = (data: any) =>
  ({
    code: JSON.stringify(data),
    data,
    class: levelToClass[data.level],
    level: data.level
  }) as ILine;

export const parseLines = (text: any) =>
  typeof text === 'string'
    ? (text || '').trim().split('\n').filter(Boolean).map(parseLine).map(parseData)
    : [parseData(text)];

const csvKey = (key: string) =>
  (
    ({
      key: 'key/term',
      term: 'key/term',
      time: '',
      unit: ''
    })[key] || key
  ).trim();

const csvValue = (value: string) => (value || '').replace('[', '').replace(']', '').trim();

export const parseMessage = (message = '') =>
  message.split(',').reduce((prev, parts) => {
    const [key, value] = parts.split('=');
    const val = csvValue(value);
    return {
      ...prev,
      ...(key && val ? { [csvKey(key)]: val } : {})
    };
  }, {});

const formatLine = ({ data: { timestamp, message } }: ILine) => ({
  timestamp,
  ...parseMessage(message)
});

export const logToCsv = (lines: ILine[]) =>
  json2csv(
    (lines || []).map(formatLine).filter(data => !isEmpty(data)),
    {
      emptyFieldValue: ''
    }
  );
