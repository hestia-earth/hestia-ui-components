import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'times',
  standalone: true
})
export class TimesPipe implements PipeTransform {
  transform(times: number): number[] {
    return new Array(times).fill(1).map((_v, index) => index);
  }
}
