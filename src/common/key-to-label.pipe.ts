import { Pipe, PipeTransform } from '@angular/core';

import { termTypeLabel } from '../terms/terms.model';

@Pipe({
  name: 'keyToLabel',
  standalone: true
})
export class KeyToLabelPipe implements PipeTransform {
  transform(value?: string) {
    // using termTypeLabel here which defaults to `keyToLabel` function
    return termTypeLabel(value);
  }
}
