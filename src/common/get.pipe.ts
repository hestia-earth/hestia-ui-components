import { Pipe, PipeTransform } from '@angular/core';
import get from 'lodash.get';

@Pipe({
  name: 'get',
  standalone: true
})
export class GetPipe implements PipeTransform {
  transform(value: any, key: string, defaultValue: any = ''): any {
    return get(value, key, defaultValue);
  }
}
