import { computed, inject, Injectable } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { distinctUntilChanged, map, startWith } from 'rxjs/operators';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { fromEvent, Observable, of } from 'rxjs';

/**
 * Represents a responsive breakpoint.
 * Can be a breakpoint name, a number or 'none'.
 * If a number is provided, it will be used as the width in pixels.
 * If 'none' is provided, the breakpoint will never be reached.
 * If a breakpoint name is provided, the breakpoint will be reached until that breakpoint.
 */
export type ResponsiveBreakpoint = BreakpointType | 'none' | number;

export enum Breakpoint {
  tablet = 'tablet',
  desktop = 'desktop',
  widescreen = 'widescreen',
  fullhd = 'fullhd'
}

export type BreakpointType = `${Breakpoint}`;

export const beakpointWidths: {
  [breakpoint in Breakpoint]: number;
} = {
  [Breakpoint.tablet]: 768,
  [Breakpoint.desktop]: 1024,
  [Breakpoint.widescreen]: 1216,
  [Breakpoint.fullhd]: 1408
};

const toSize = (size: number, dir: 'min' | 'max') => `(${dir}-width: ${dir === 'max' ? size - 1 : size}px)`;

const toBreakpoint = ({ min, max }: { min?: Breakpoint; max?: Breakpoint }) =>
  [min ? toSize(beakpointWidths[min], 'min') : '', max ? toSize(beakpointWidths[max], 'max') : '']
    .filter(Boolean)
    .join(' and ');

@Injectable({
  providedIn: 'root'
})
export class ResponsiveService {
  private readonly breakPointObserver = inject(BreakpointObserver);

  private readonly windowWidth$ = fromEvent(window, 'resize').pipe(
    startWith(window.innerWidth),
    map(() => window.innerWidth)
  );

  public readonly isMobile$ = this.breakPointObserver.observe(toBreakpoint({ max: Breakpoint.tablet })).pipe(
    map(state => state.matches),
    distinctUntilChanged()
  );

  public readonly isTablet$ = this.breakPointObserver
    .observe(toBreakpoint({ min: Breakpoint.tablet, max: Breakpoint.desktop }))
    .pipe(
      map(state => state.matches),
      distinctUntilChanged()
    );

  public readonly isDesktop$ = this.breakPointObserver
    .observe(toBreakpoint({ min: Breakpoint.desktop, max: Breakpoint.widescreen }))
    .pipe(
      map(state => state.matches),
      distinctUntilChanged()
    );

  public readonly isWidescreen$ = this.breakPointObserver
    .observe(toBreakpoint({ min: Breakpoint.widescreen, max: Breakpoint.fullhd }))
    .pipe(
      map(state => state.matches),
      distinctUntilChanged()
    );

  public readonly isFullHd$ = this.breakPointObserver.observe(toBreakpoint({ min: Breakpoint.fullhd })).pipe(
    map(state => state.matches),
    distinctUntilChanged()
  );

  public readonly isTouch$ = this.breakPointObserver.observe(toBreakpoint({ max: Breakpoint.desktop })).pipe(
    map(state => state.matches),
    distinctUntilChanged()
  );

  /**
   * Resolution for 1080p (or Full HD) on a normal screen, although bulma defines it differently.
   */
  public readonly is1080p$ = this.breakPointObserver.observe(toSize(1920, 'min')).pipe(
    map(state => state.matches),
    distinctUntilChanged()
  );

  public readonly isRetinaDisplay = () => {
    if (window.matchMedia) {
      const mq = window.matchMedia(
        [
          'min--moz-device-pixel-ratio: 1.3',
          '-o-min-device-pixel-ratio: 2.6/2',
          '-webkit-min-device-pixel-ratio: 1.3',
          'min-device-pixel-ratio: 1.3',
          'min-resolution: 1.3dppx'
        ]
          .map(v => `only screen and (${v})`)
          .join(', ')
      );
      return mq?.matches || window.devicePixelRatio > 1;
    }

    return false;
  };

  public readonly isMobile = toSignal(this.isMobile$);
  public readonly isTablet = toSignal(this.isTablet$);
  public readonly isDesktop = toSignal(this.isDesktop$);
  public readonly isWidescreen = toSignal(this.isWidescreen$);
  public readonly isFullHd = toSignal(this.isFullHd$);
  public readonly isTouch = toSignal(this.isTouch$);
  public readonly is1080p = toSignal(this.is1080p$);

  public readonly resolutionName = computed(() =>
    this.isMobile()
      ? 'mobile'
      : this.isTablet()
        ? 'tablet'
        : this.isDesktop()
          ? 'desktop'
          : this.isWidescreen()
            ? 'widescreen'
            : 'fullhd'
  );
  public readonly resolutionName$ = toObservable(this.resolutionName);

  public isAboveBreakpoint$(breakpoint: ResponsiveBreakpoint) {
    return typeof breakpoint === 'number'
      ? this.windowWidth$.pipe(map(width => width >= breakpoint))
      : breakpoint === 'none'
        ? of(false)
        : this._isAboveBreakpointName$(breakpoint);
  }

  private _isAboveBreakpointName$(breakpoint: BreakpointType): Observable<boolean> {
    return this.resolutionName$.pipe(
      map(resolutionName => {
        const width = beakpointWidths[resolutionName];
        const overlapBreakpointWidth = beakpointWidths[breakpoint];
        return width >= overlapBreakpointWidth;
      })
    );
  }
}
