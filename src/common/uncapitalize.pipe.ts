import { Pipe, PipeTransform } from '@angular/core';

import { uncapitalize } from './utils';

@Pipe({
  name: 'uncapitalize',
  standalone: true
})
export class UncapitalizePipe implements PipeTransform {
  transform = uncapitalize;
}
