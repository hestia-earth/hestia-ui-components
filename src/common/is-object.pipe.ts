import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isObject',
  standalone: true
})
export class IsObjectPipe implements PipeTransform {
  transform(value: any) {
    return typeof value === 'object' && !Array.isArray(value) && Object.keys(value).length > 0;
  }
}
