import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

import { schemaBaseUrl } from '../utils';

@Component({
  selector: 'he-schema-version-link',
  templateUrl: './schema-version-link.component.html',
  styleUrls: ['./schema-version-link.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FaIconComponent]
})
export class SchemaVersionLinkComponent {
  protected readonly faExternalLinkAlt = faExternalLinkAlt;

  protected readonly node = input.required<any>();
  protected readonly showExternalLink = input(true);
  protected readonly linkClass = input('');

  protected readonly url = computed(() =>
    [schemaBaseUrl(this.node().schemaVersion), this.node()['@type']].filter(Boolean).join('/')
  );
}
