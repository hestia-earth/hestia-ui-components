/* eslint-disable prefer-spread */
/**
 * Resolves issue with Typeahead bindings.
 *
 * @see: https://github.com/ng-bootstrap/ng-bootstrap/issues/4055#issuecomment-1192885359
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'applyPure',
  pure: true,
  standalone: true
})
export class ApplyPurePipe implements PipeTransform {
  public transform(templateValue: any, fnReference: any, ...fnArguments: any[]): any {
    fnArguments.unshift(templateValue);
    return fnReference.apply(null, fnArguments);
  }
}
