import { formatDate, getDatesBetween, uniqueDatesBetween, isDateBetween } from './node-utils';

describe('node-utils', () => {
  describe('getDatesBetween', () => {
    it('should return an array of dates between the given dates', () => {
      const startDate = formatDate('2002-01-01', true);
      const endDate = formatDate('2002-12-31', false);
      const dates = getDatesBetween(startDate, endDate);

      expect(dates).toEqual([
        new Date('2002-01-01T12:00:00.000Z'),
        new Date('2002-02-01T12:00:00.000Z'),
        new Date('2002-03-01T12:00:00.000Z'),
        new Date('2002-04-01T12:00:00.000Z'),
        new Date('2002-05-01T12:00:00.000Z'),
        new Date('2002-06-01T12:00:00.000Z'),
        new Date('2002-07-01T12:00:00.000Z'),
        new Date('2002-08-01T12:00:00.000Z'),
        new Date('2002-09-01T12:00:00.000Z'),
        new Date('2002-10-01T12:00:00.000Z'),
        new Date('2002-11-01T12:00:00.000Z'),
        new Date('2002-12-01T12:00:00.000Z')
      ]);
    });
  });

  describe('uniqueDatesBetween', () => {
    it('should return the unique list of dates in between the earlier and latest', () => {
      const dates = ['2001-01-24', '1990-03-25', '2010-02-13', '2005-10-23', '1991-02-03T11:00:00'].map(date =>
        formatDate(date, true)
      );
      expect(uniqueDatesBetween(dates).length).toEqual(239);
    });

    it('should handle unsorted days', () => {
      const dates = [
        { startDate: '2018-01-01', endDate: '2018-12-31' },
        { startDate: '2013-01-01', endDate: '2013-12-31' },
        { startDate: '2003-01-01', endDate: '2003-12-31' },
        { startDate: '2018-01-01', endDate: '2018-12-31' },
        { startDate: '2013-01-01', endDate: '2013-12-31' },
        { startDate: '2003-01-01', endDate: '2003-12-31' }
      ].flatMap(({ startDate, endDate }) => [formatDate(startDate, true), formatDate(endDate, false)]);
      const datesBetween = uniqueDatesBetween(dates);
      expect(datesBetween[0]).toEqual(new Date('2003-01-01T12:00:00.000Z'));
    });
  });

  describe('isDateBetween', () => {
    describe('with start and end dates', () => {
      const start = '2012-01-01';
      const end = '2012-12-31';

      fdescribe('date is between', () => {
        const date = '2012-06-01';

        it('should return true', () => {
          expect(isDateBetween(formatDate(date), { start, end })).toBe(true);
        });
      });

      describe('date is not between', () => {
        const date = '2013-06-01';

        it('should return false', () => {
          expect(isDateBetween(formatDate(date), { start, end })).toBe(false);
        });
      });
    });

    describe('with only endDate', () => {
      const end = '2012-09';

      describe('date is equal', () => {
        const date = end;

        it('should return true', () => {
          expect(isDateBetween(formatDate(date), { end })).toBe(true);
        });
      });
    });
  });
});
