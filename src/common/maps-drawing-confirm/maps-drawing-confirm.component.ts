import { Component, output, inject, input, viewChild } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { MapsDrawingComponent } from '../maps-drawing/maps-drawing.component';

@Component({
  selector: 'he-maps-drawing-confirm',
  templateUrl: './maps-drawing-confirm.component.html',
  styleUrls: ['./maps-drawing-confirm.component.scss'],
  imports: [FaIconComponent, MapsDrawingComponent]
})
export class MapsDrawingConfirmComponent {
  private readonly activeModal = inject(NgbActiveModal, { optional: true });

  protected readonly faTimes = faTimes;

  private readonly map = viewChild.required(MapsDrawingComponent);

  protected readonly value = input<string>();
  protected readonly modes = input<google.maps.drawing.OverlayType[]>();

  protected readonly closed = output<google.maps.LatLngLiteral | string>();

  protected confirm() {
    this.close(this.map().data());
  }

  protected close(value?: google.maps.LatLngLiteral | string) {
    this.closed.emit(value);
    this.activeModal?.close(value);
  }
}
