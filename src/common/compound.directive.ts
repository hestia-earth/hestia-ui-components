import { Directive, ElementRef, effect, inject, input } from '@angular/core';
import { TermTermType } from '@hestia-earth/schema';

import { compoundToHtml } from './compound';

@Directive({
  selector: '[appCompound]',
  standalone: true
})
export class CompoundDirective {
  private readonly elementRef = inject(ElementRef);

  protected readonly appCompound = input.required<string>();
  protected readonly compoundTermType = input<TermTermType>();

  constructor() {
    effect(() => {
      this.elementRef.nativeElement.innerHtml = compoundToHtml(this.appCompound(), this.compoundTermType());
    });
  }
}
