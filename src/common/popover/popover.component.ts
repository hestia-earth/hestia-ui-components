import { Component, HostListener, TemplateRef, input, signal } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';

export interface ContentContext {
  message: string;
  data: any;
}

@Component({
  selector: 'he-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
  imports: [NgTemplateOutlet]
})
export class PopoverComponent {
  protected readonly message = input('');
  protected readonly content = input<TemplateRef<ContentContext>>();
  protected readonly data = input<any>({});
  protected readonly position = input<'top' | 'right' | 'bottom' | 'left'>('left');

  @HostListener('click', ['$event'])
  protected onClick($event) {
    $event.stopPropagation();
    this.active.set(true);
  }

  public readonly active = signal(false);
}
