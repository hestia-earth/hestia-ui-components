import { TermTermType } from '@hestia-earth/schema';

const ignoreCompounds = (value: any) =>
  typeof value !== 'string' ||
  [
    value.startsWith('GWP'),
    value.startsWith('PM'),
    value.startsWith('AR'),
    value.startsWith('CML'),
    value.split('-').length > 2,
    value.includes('</a>')
  ].some(Boolean);

const allowedTermTypes = [
  TermTermType.emission,
  TermTermType.inorganicFertiliser,
  TermTermType.organicFertiliser,
  TermTermType.pesticideAI
];

export const isTermTypeAllowed = (termType?: TermTermType) => !termType || allowedTermTypes.includes(termType);

/**
 * Handles <sup> and <sub> html tags for compounds.
 *
 * @param value The compound as a string (e.g. NO3)
 * @param termType Optionally use a `TermType` to restrict the conversion further.
 * @returns HTML version with subscript and superscript tags.
 */
export const compoundToHtml = (value: any, termType?: TermTermType) =>
  isTermTypeAllowed(termType) && !ignoreCompounds(value)
    ? (value || '')
        .replace(/([A-Z]+[\d]+)([\d]{1}[-+])/g, '$1<sup>$2</sup>')
        .replace(/([A-Z]+)([\d]{1})([-+])/g, '$1<sub>$2</sub><sup>$3</sup>')
        .replace(/([A-LN-Za-ln-z])(\d+)/g, '$1<sub>$2</sub>')
        .replace(/(Ox)([^A-Za-z]|$)/g, 'O<sub>x</sub>$2')
        // handle m2, m3, etc.
        .replace(/([m])([\d]{1})\s/g, '$1<sup>$2</sup> ')
        .replace(/([m])([\d]{1})$/g, '$1<sup>$2</sup>')
    : value;
