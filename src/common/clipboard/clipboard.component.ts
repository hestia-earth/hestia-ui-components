import { Component, ElementRef, input, computed, viewChild } from '@angular/core';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faClipboard } from '@fortawesome/free-solid-svg-icons';

const defaultIcon = 'clipboard';

@Component({
  selector: 'he-clipboard',
  templateUrl: './clipboard.component.html',
  styleUrls: ['./clipboard.component.scss'],
  imports: [NgbTooltip, FaIconComponent]
})
export class ClipboardComponent {
  private readonly valueNode = viewChild<ElementRef>('valueNode');
  private readonly tooltip = viewChild<NgbTooltip>('t');

  protected readonly icon = input<IconDefinition>(faClipboard);
  protected readonly value = input<string>();
  protected readonly disabled = input(false);
  protected readonly hideText = input(false);
  protected readonly size = input('md');
  protected readonly rotate = input(0);
  protected readonly clipboardClass = input('no-print px-3');

  protected readonly defaultIcon = defaultIcon;

  private _value = computed(() =>
    typeof this.value() === 'object' ? JSON.stringify(this.value(), null, 2) : this.value()
  );

  protected onClick($event) {
    $event.stopPropagation();
    $event.preventDefault();
    this.clipboard();
  }

  public async clipboard() {
    const { Clipboard } = await import(/* webpackChunkName: "ts-clipboard" */ 'ts-clipboard');
    const range = document.createRange();
    range.selectNode(this.valueNode()?.nativeElement);
    const selection = window.getSelection();
    selection!.removeAllRanges();
    selection!.addRange(range);
    const val = this._value();
    Clipboard.copy(val || '');
    setTimeout(() => selection!.removeAllRanges());

    if (this.tooltip()) {
      this.tooltip()?.open();
      setTimeout(() => this.tooltip()?.close(), 1000);
    }
  }
}
