import { Pipe, PipeTransform } from '@angular/core';

import { toThousands } from './thousands';

@Pipe({
  name: 'thousandSuff',
  standalone: true
})
export class ThousandSuffixesPipe implements PipeTransform {
  transform(input: any, digits = 0): any {
    return Number.isNaN(input) || input < 1000 ? input : toThousands(input, digits);
  }
}
