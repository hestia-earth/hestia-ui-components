import { ColorPalette, getColor } from './color';

describe('color', () => {
  describe('getColor', () => {
    it('should handle basic colors', () => {
      expect(getColor(0, ColorPalette.categorial)).toEqual('#193957');
      expect(getColor(5, ColorPalette.continuous)).toEqual('#B0B43B');
      expect(getColor(9, ColorPalette.divergent)).toEqual('#FFC000');
      expect(getColor(91)).toEqual('#FFF9E6');
      expect(getColor(120)).toEqual('#D1D7DD');
    });

    it('should handle gradient colors', () => {
      expect(getColor(10, ColorPalette.continuous)).toEqual('#1a3f5c');
      expect(getColor(22, ColorPalette.divergent)).toEqual('#389aa2');
    });

    it('should handle shade colors', () => {
      expect(getColor(10, ColorPalette.categorial)).toEqual('#304D68');
      expect(getColor(90, ColorPalette.categorial)).toEqual('#E8EBEE');
    });
  });
});
