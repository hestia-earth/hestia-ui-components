import { Pipe, PipeTransform } from '@angular/core';

import { bytesSize } from './utils';

@Pipe({
  name: 'fileSize',
  standalone: true
})
export class FileSizePipe implements PipeTransform {
  transform(bytes: number) {
    return bytesSize(bytes);
  }
}
