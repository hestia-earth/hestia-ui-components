import { Component, OnDestroy, signal, inject, viewChild, effect, computed, input, output } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { GoogleMap } from '@angular/google-maps';
import { isNumber } from '@hestia-earth/utils';

import {
  HE_MAP_LOADED,
  IFeature,
  IFeatureCollection,
  defaultFeature,
  addPolygonToFeature,
  polygonsFromFeature,
  strokeStyle
} from '../maps-utils';

@Component({
  selector: 'he-maps-drawing',
  imports: [GoogleMap],
  templateUrl: './maps-drawing.component.html',
  styleUrl: './maps-drawing.component.scss'
})
export class MapsDrawingComponent implements OnDestroy {
  private readonly mapLoaded$ = inject(HE_MAP_LOADED);

  protected readonly mapLoaded = toSignal(this.mapLoaded$);

  private readonly map = viewChild(GoogleMap);
  private listeners: google.maps.MapsEventListener[] = [];
  private shapes: (google.maps.Marker | google.maps.Polygon)[] = [];

  protected readonly updated = output<string | google.maps.LatLngLiteral>();

  protected readonly value = input<string>();
  protected readonly modes = input<google.maps.drawing.OverlayType[]>();
  protected readonly center = input({ lat: 0, lng: 0 });
  protected readonly zoom = input(3);

  protected readonly options = signal({} as google.maps.MapOptions);

  public readonly feature = signal<IFeature | IFeatureCollection>(defaultFeature());
  public readonly coordinates = signal<google.maps.LatLngLiteral>(undefined);
  public readonly data = computed(() => this.coordinates() || JSON.stringify(this.feature()) || '');

  constructor() {
    effect(() => {
      if (this.mapLoaded()) {
        this.options.set({
          fullscreenControl: false,
          mapTypeControl: true,
          streetViewControl: false,
          zoomControl: true,
          zoomControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
          }
        });
      }
    });

    effect(() => {
      const feature = this.value() && !isNumber(this.value()) ? JSON.parse(this.value()) : this.feature();
      this.feature.set(feature);
    });
  }

  ngOnDestroy() {
    this.listeners.map(listener => google.maps.event.removeListener(listener));
  }

  private onPolygonAdded(polygon: google.maps.Polygon) {
    this.shapes.push(polygon);
    this.feature.set(this.feature() || defaultFeature());
    addPolygonToFeature(this.feature(), polygon);
    this.updated.emit(this.data());
  }

  private onMarkerAdded(marker: google.maps.Marker) {
    this.shapes.push(marker);
    const position = marker.getPosition();
    this.coordinates.set(position?.toJSON());
    this.updated.emit(this.data());
  }

  private loadData() {
    this.shapes = polygonsFromFeature(this.feature());
    this.shapes.forEach(polygon => polygon.setMap(this.map()?.googleMap));
    const drawingManager = new google.maps.drawing.DrawingManager({
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: this.modes() || [google.maps.drawing.OverlayType.POLYGON]
      },
      polygonOptions: strokeStyle
    });
    drawingManager.setMap(this.map()?.googleMap);
    this.listeners.push(
      google.maps.event.addListener(drawingManager, 'polygoncomplete', polygon => this.onPolygonAdded(polygon))
    );
    this.listeners.push(
      google.maps.event.addListener(drawingManager, 'markercomplete', marker => this.onMarkerAdded(marker))
    );
  }

  protected mapInitialized() {
    setTimeout(() => this.loadData());
  }

  public clear() {
    this.shapes.forEach(shape => shape.setMap(null));
    this.shapes = [];
    this.coordinates.set(undefined);
    this.feature.set(undefined);
    this.updated.emit(undefined);
  }
}
