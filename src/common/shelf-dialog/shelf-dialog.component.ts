import { ChangeDetectionStrategy, Component, computed, input, output, signal, TemplateRef } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { SvgIconComponent } from 'angular-svg-icon';
import { HeSvgIconsModule } from '../../svg-icons';

@Component({
  selector: 'he-shelf-dialog',
  templateUrl: './shelf-dialog.component.html',
  styleUrls: ['./shelf-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NgTemplateOutlet, SvgIconComponent, HeSvgIconsModule]
})
export class ShelfDialogComponent {
  protected readonly headerTemplate = input<TemplateRef<any>>();

  protected readonly title = input<string>();

  protected readonly bottom = input<string | number>('0');

  protected readonly closed = output<void>();

  protected readonly showContent = signal(true);

  protected readonly toggleIcon = computed(() => (this.showContent() ? 'far-chevron-down' : 'far-chevron-up'));

  public expand() {
    this.showContent.set(true);
  }

  public collapse() {
    this.showContent.set(false);
  }
}
