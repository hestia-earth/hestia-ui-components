import get from 'lodash.get';
import { ISiteJSONLD, IImpactAssessmentJSONLD, SchemaType, SCHEMA_VERSION } from '@hestia-earth/schema';

export const gitHome = 'https://gitlab.com/hestia-earth';
export const gitBranch = () => (['dev', 'staging'].some(env => baseUrl().includes(env)) ? 'develop' : 'master');

export const isChrome = () => window.navigator.userAgent.includes('Chrome');

export const baseUrl = (allowLocalhost = true) =>
  window.location.origin?.includes('localhost')
    ? allowLocalhost
      ? window.location.origin
      : 'https://www-dev.hestia.earth'
    : window.location.origin?.includes('hestia.earth')
      ? window.location.origin
      : 'https://www.hestia.earth';

export const baseApiUrl = () => baseUrl(false).replace('www', 'api');

export const isExternal = () => baseUrl() !== window.location.origin;

export const schemaBaseUrl = (version?: string) => [baseUrl(), 'schema', version].filter(Boolean).join('/');

export const schemaDataBaseUrl = (version?: string) =>
  [baseUrl(false), 'schema-data', version || SCHEMA_VERSION].filter(Boolean).join('/');

export const glossaryBaseUrl = (allowLocalhost = true) =>
  [baseUrl(allowLocalhost), 'glossary'].filter(Boolean).join('/');

const parseErrorStatus = (error: any) => (error?.statusText || '').toLowerCase().replace(/\s/g, '-');

const parseErrorMessage = (error: any) =>
  get(error, 'error.error', get(error, 'error.message', get(error, 'error', get(error, 'message', error))));

export const handleAPIError = (err: any) => {
  let error;
  try {
    error = parseErrorMessage(err);
  } catch (err) {
    // ignore error
  }
  throw error;
};

export const errorText = (error: any): string => {
  if (typeof error === 'string') {
    return error;
  }
  const err = parseErrorMessage(error);
  return parseErrorStatus(err) || err;
};

export const filterParams = (obj: any) => {
  const res: {
    [x: string]: string;
  } = {};
  Object.keys(obj)
    .sort()
    .forEach(key => {
      const value = obj[key];
      if (value && value !== 'undefined') {
        res[key] = `${value}`;
      }
    });
  return res;
};

export const waitFor = (variable: string, callback: () => void) =>
  get(window, variable, false) ? callback() : setTimeout(() => waitFor(variable, callback), 100);

export const bottom = (element: HTMLElement) => element.offsetTop + element.getBoundingClientRect().height;

export const isScrolledBelow = (element: HTMLElement) => (element ? window.scrollY > bottom(element) : false);

export const scrollToEl = (id: string, retries = 0) => {
  const el = document.getElementById(id);
  setTimeout(() => (el ? el.scrollIntoView() : retries < 10 ? scrollToEl(id, retries + 1) : null), 100);
};

export const scrollTop = () => window.scrollTo(0, 0);

export const safeJSONParse = <T>(value: string, defaultValue?: any) => {
  try {
    return typeof value === 'string' ? (JSON.parse(value) as T) : value;
  } catch (err) {
    return defaultValue;
  }
};

export const safeJSONStringify = (value: string) => (typeof value === 'string' ? value : JSON.stringify(value));

export const arrayValue = (values: any[], isAverage: boolean) =>
  (values || []).reduce((prev: number, curr) => prev + parseFloat(`${curr}`), 0) / (isAverage ? values.length : 1);

export const ellipsis = (text = '', maxlength = 20) =>
  text.length > maxlength ? `${text.substring(0, maxlength)}...` : text;

const nodeDefaultLabel: {
  [type in SchemaType]?: (data: any) => string;
} = {
  [SchemaType.ImpactAssessment]: ({ name, country, endDate, product }: IImpactAssessmentJSONLD) =>
    name
      ? name.replace(`${product?.term?.name}, `, '')
      : [product?.term?.name, country?.name, endDate].filter(Boolean).join(', '),
  [SchemaType.Site]: ({ name, description }: ISiteJSONLD) => name || description!,
  [SchemaType.Transformation]: ({ name, term }: any) => name || term?.name
};

export const defaultLabel = (node?: any) =>
  node
    ? (node['@type'] in nodeDefaultLabel ? nodeDefaultLabel[node['@type']](node) : node.name) || node['@id'] || node.id
    : '';

export const repeat = (times = 0) => Array.from(Array(times), Math.random);

export const copyObject = (data?: any) => (data ? JSON.parse(JSON.stringify(data)) : null);

export const isEqual = (a: any, b: any) => JSON.stringify(a) === JSON.stringify(b);

export const typeaheadFocus = (e: Event) => {
  if (e.bubbles) {
    return;
  }
  e.stopPropagation();
  setTimeout(() => e.target?.dispatchEvent(new Event('input')), 0);
};

export enum Repository {
  glossary = 'hestia-glossary',
  models = 'hestia-engine-models',
  orchestrator = 'hestia-engine-orchestrator',
  aggregation = 'hestia-aggregation-engine',
  community = 'hestia-community-edition',
  poorenemeck = 'hestia-convert-poore-nemecek',
  frontend = 'hestia-front-end',
  schema = 'hestia-schema'
}

export enum Template {
  bug = 'bug',
  feature = 'feature'
}

export const reportIssueUrl = (repository: Repository, template?: Template) =>
  `${gitHome}/${repository}/-/issues/new${template ? `?issuable_template=${template}` : ''}`;

export const changelogUrl = (repository: Repository) => `${gitHome}/${repository}/-/blob/master/CHANGELOG.md`;

export const contactUsEmail = 'community@hestia.earth';
export const externalLink = (href: string, text: string) => `<a href="${href}" target="_blank">${text}</a>`;
export const glossaryLink = (text: string) => externalLink(glossaryBaseUrl(), text);
export const nodeLink = ({ '@type': type, '@id': id, name }: { '@type': string; '@id': string; name?: string }) =>
  type && id ? `<a href="/${type.toLowerCase()}/${id}" target="_blank">${name || id}</a>` : null;
export const schemaLink = (type: string, title = type) =>
  `<a href="${schemaBaseUrl()}/${type}" target="_blank">${title}</a>`;
export const code = (text: string | number | boolean) => `<code>${text}</code>`;
export const contactUsLink = (text = 'contact us') => `<a href="mailto:${contactUsEmail}">${text}</a>`;
export const reportIssueLink = (repository: Repository, template?: Template, text = 'here') =>
  externalLink(reportIssueUrl(repository, template), text);

interface IGitlabRawParams {
  /**
   * The repository where the file is located.
   */
  repository: Repository;
  /**
   * The path of the file, from the root of the repository.
   */
  path: string;
  /**
   * The base url of the HESTIA API.
   */
  apiUrl?: string;
  /**
   * The branch, using the default branch based on the environment.
   */
  branch?: string;
}

/**
 * Url to fetch raw content from Gitlab, bypassing CORS issues.
 */
export const gitlabRawUrl = ({ repository, path, apiUrl, branch }: IGitlabRawParams) =>
  `${apiUrl || baseApiUrl()}/gitlab/raw?repository=${repository}&branch=${
    branch || gitBranch()
  }&path=${encodeURIComponent(path)}`;

const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

export const bytesSize = (bytes: number) => {
  const i = isNaN(bytes) || bytes === 0 ? 0 : Math.floor(Math.log(bytes) / Math.log(1024));
  return `${isNaN(bytes) ? 0 : parseFloat((bytes / Math.pow(1024, i)).toFixed(2))} ${sizes[i]}`;
};

export const capitalize = <T extends string>(text: T) =>
  (text.charAt(0).toUpperCase() + text.substring(1)) as Capitalize<T>;

export const uncapitalize = <T extends string>(text: T) =>
  (text.charAt(0).toLowerCase() + text.substring(1)) as Uncapitalize<T>;
