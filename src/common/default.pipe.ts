import { Pipe, PipeTransform } from '@angular/core';
import { isUndefined } from '@hestia-earth/utils';

@Pipe({
  name: 'default',
  standalone: true
})
export class DefaultPipe implements PipeTransform {
  transform(value, defaultValue) {
    return isUndefined(value) ? defaultValue : value;
  }
}
