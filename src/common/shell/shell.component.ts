import {
  Component,
  computed,
  ContentChild,
  EventEmitter,
  Input,
  Output,
  signal,
  TemplateRef,
  WritableSignal
} from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgTemplateOutlet, NgClass } from '@angular/common';

export interface IShellMenuButton {
  icon?: any;
  iconPath?: string;
  onClick: () => void;
  menuRef?: TemplateRef<any>;
  position?: 'top' | 'bottom';
  activeDisabled?: boolean;
  id?: string;
  className?: string;
  activeClassName?: string;
}

@Component({
  selector: 'he-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  imports: [NgTemplateOutlet, NgClass, FaIconComponent]
})
export class ShellComponent {
  @ContentChild('iconTemplate') iconTemplate: TemplateRef<any> | null = null;

  private _shelfButtons: WritableSignal<IShellMenuButton[]> = signal([]);
  private _defaultActiveButtonId: WritableSignal<string | null> = signal(null);

  @Input() set shelfButtons(value: IShellMenuButton[]) {
    this._shelfButtons.set(value);
  }

  @Input() menuOverlap = false;

  @Input() closeMenuOnOutsideClick = false;

  @Input() set defaultActiveButtonId(value: string | null) {
    this._defaultActiveButtonId.set(value);
  }

  @Output() menuShown = new EventEmitter();

  buttons = computed(() =>
    this._shelfButtons()
      .map((button, index) => ({
        ...button,
        id: button.id || index.toString()
      }))
      .map(button => {
        const activeMenuId = this.activeMenuId();
        return { ...button, isActive: button.id === (activeMenuId || this.activeButtonId()) };
      })
  );

  protected clickedButton: WritableSignal<string | null> = signal(null);
  protected expanded = signal(false);

  protected menuRef = signal(null as TemplateRef<any> | null);

  activeButtonId = computed(() => {
    return this.clickedButton() || this._defaultActiveButtonId();
  });

  activeMenuId = computed(() => {
    if (this.expanded() && this.menuRef()) {
      return this._shelfButtons()
        .findIndex(button => button.menuRef === this.menuRef())
        ?.toString();
    }
  });

  protected topMenuButtons = computed(() => {
    return this.buttons().filter(button => !button.position || button.position === 'top');
  });

  protected trackByFn(index: number, item: IShellMenuButton) {
    return item.id;
  }

  protected bottomMenuButtons = computed(() => {
    return this.buttons().filter(button => button.position === 'bottom');
  });

  hideMenu() {
    this.expanded.set(false);
    this.clickedButton.set(null);
  }

  showMenu(templateRef?: TemplateRef<any>) {
    if (templateRef) {
      this.menuRef.set(templateRef);
    }
    this.expanded.set(true);
    this.menuShown.emit();
  }

  protected onMenuButtonClick(button: IShellMenuButton) {
    if (button.id === this.clickedButton()) {
      this.hideMenu();
      return;
    }

    if (button.menuRef) {
      this.menuRef.set(button.menuRef);
      this.showMenu();
    } else {
      this.hideMenu();
    }

    if (!button.activeDisabled) {
      this.clickedButton.set(button.id);
    }

    button.onClick?.();
  }
}
