import { transform } from './precision.pipe';

describe('precision.pipe', () => {
  describe('transform', () => {
    it('should handle number', () => {
      expect(transform(123)).toEqual('123');
      expect(transform(10000)).toEqual('10,000');
      expect(transform(10000, 3, false)).toEqual('10000');
    });

    it('should handle string', () => {
      expect(transform('123')).toEqual('123');
      expect(transform('value')).toEqual('value');
    });

    it('should handle boolean', () => {
      expect(transform(true)).toEqual(true);
    });
  });
});
