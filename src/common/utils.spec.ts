import { bytesSize } from './utils';

describe('common > utils', () => {
  describe('bytesSize', () => {
    it('should return the sizes', () => {
      expect(bytesSize(0)).toBe('0 Bytes');
      expect(bytesSize(1024)).toBe('1 KB');
      expect(bytesSize(1024 * 1024)).toBe('1 MB');
      expect(bytesSize(1024 * 1024 * 1024)).toBe('1 GB');
      expect(bytesSize(1024 * 1024 * 1024 * 1024)).toBe('1 TB');
    });
  });
});
