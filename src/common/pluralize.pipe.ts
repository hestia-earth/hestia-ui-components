import { Pipe, PipeTransform } from '@angular/core';

import { pluralize } from './pluralize';

@Pipe({
  name: 'pluralize',
  standalone: true
})
export class PluralizePipe implements PipeTransform {
  transform(value: string, times = 0) {
    return pluralize(value || '', times);
  }
}
