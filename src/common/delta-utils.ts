const SUCCESS_CRITERION_MAX_DELTA_PERCENT = 5;
const WARNING_CRITERION_MAX_DELTA_PERCENT = 20;

export enum DeltaColour {
  Success = 'success',
  Warning = 'warning',
  Danger = 'danger'
}

export const evaluateSuccess = (deltaValue: number) =>
  Math.abs(deltaValue) < SUCCESS_CRITERION_MAX_DELTA_PERCENT
    ? DeltaColour.Success
    : Math.abs(deltaValue) < WARNING_CRITERION_MAX_DELTA_PERCENT
      ? DeltaColour.Warning
      : DeltaColour.Danger;
