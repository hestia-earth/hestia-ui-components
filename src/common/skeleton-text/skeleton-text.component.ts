import { Component, HostBinding, input } from '@angular/core';

@Component({
  selector: 'he-skeleton-text',
  templateUrl: './skeleton-text.component.html',
  styleUrls: ['./skeleton-text.component.scss'],
  standalone: true
})
export class SkeletonTextComponent {
  protected readonly animated = input(true);
  protected readonly width = input(100);
  protected readonly height = input('inherit');

  @HostBinding('class.is-animated')
  protected get _animated() {
    return this.animated();
  }

  @HostBinding('style.width')
  protected get _width() {
    return `${this.width()}%`;
  }

  @HostBinding('style.height')
  protected get _height() {
    return this.height();
  }
}
