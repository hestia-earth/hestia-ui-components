import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { emptyValue } from '@hestia-earth/utils/dist/term';
import { DeltaDisplayType, delta, customDeltaFuncs } from '@hestia-earth/utils/dist/delta';

import { evaluateSuccess } from '../delta-utils';
import { PrecisionPipe } from '../precision.pipe';

@Component({
  selector: 'he-blank-node-value-delta',
  templateUrl: './blank-node-value-delta.component.html',
  styleUrls: ['./blank-node-value-delta.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [DecimalPipe, PrecisionPipe]
})
export class BlankNodeValueDeltaComponent {
  protected readonly value = input<any>();
  protected readonly originalValue = input<any>();
  protected readonly displayType = input(DeltaDisplayType.percent);

  protected readonly DeltaDisplayType = DeltaDisplayType;

  protected readonly hide = computed(() => emptyValue(this.value()) || emptyValue(this.originalValue()));
  protected readonly delta = computed(() =>
    delta(this.value(), this.originalValue(), this.displayType(), customDeltaFuncs)
  );
  protected readonly color = computed(() =>
    this.displayType() === DeltaDisplayType.percent ? evaluateSuccess(this.delta()) : ''
  );
}
