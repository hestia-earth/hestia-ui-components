import { ChangeDetectionStrategy, Component, HostBinding, computed, effect, inject, input } from '@angular/core';
import { Meta } from '@angular/platform-browser';

interface ISocialTagsConfig {
  'og:type'?: string;
  'og:url'?: string;
  'og:title'?: string;
  'og:description'?: string;
  'og:image'?: string;
}

@Component({
  selector: 'he-social-tags',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true
})
export class SocialTagsComponent {
  private readonly meta = inject(Meta);

  protected readonly config = input<ISocialTagsConfig>({});

  @HostBinding('class')
  protected readonly classes = 'is-hidden';

  private readonly configs = computed(() => ({
    'og:url': window.location.href.split('?')[0],
    ...this.config
  }));

  constructor() {
    this.meta.addTag({ charset: 'UTF-8' });

    effect(() => {
      Object.entries(this.configs()).map(([name, content]) => {
        this.meta.updateTag({ name, content });
        if (name === 'og:description') {
          this.meta.updateTag({ name: 'description', content });
        }
      });
    });
  }
}
