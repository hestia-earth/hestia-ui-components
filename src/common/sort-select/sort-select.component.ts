import { ChangeDetectionStrategy, Component, Input, computed, output, signal } from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { NgClass } from '@angular/common';
import { faLongArrowAltDown, faLongArrowAltUp, faSortAmountUpAlt } from '@fortawesome/free-solid-svg-icons';

import { ClickOutsideDirective } from '../click-outside.directive';

export interface SortOption {
  id: string;
  label: string;
  color?: string;
}

export interface SortSelectEvent {
  sortBy: string;
  sortOrder: SortSelectOrder;
}

export type SortSelectOrder = 'asc' | 'desc';

@Component({
  selector: 'he-sort-select',
  templateUrl: './sort-select.component.html',
  styleUrls: ['./sort-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NgClass, ClickOutsideDirective, NgbTooltip, FaIconComponent]
})
export class SortSelectComponent {
  protected readonly faSortAmountUpAlt = faSortAmountUpAlt;

  @Input()
  public sortOptions: SortOption[];

  @Input() alignment: 'left' | 'right' = 'right';

  protected sortChange = output<SortSelectEvent>();

  protected sortBySignal = signal('');

  protected sortOrderSignal = signal(<SortSelectOrder>'desc');

  protected showSortBy = false;

  @Input() set sortBy(value: string) {
    this.sortBySignal.set(value);
  }

  @Input() set sortOrder(value: SortSelectOrder) {
    this.sortOrderSignal.set(value);
  }

  protected arrowIcon = computed(
    () => (this.sortOrderSignal() === 'desc' ? faLongArrowAltDown : faLongArrowAltUp) as IconProp
  );

  protected sort(option: SortOption) {
    if (this.sortBySignal() === option.id) {
      this._toggleSortBy();
    } else {
      this.sortBySignal.set(option.id);
      this.sortOrderSignal.set('desc');
    }
    this._emitSortChange();
  }

  private _toggleSortBy() {
    this.sortOrderSignal.update(value => (value === 'asc' ? 'desc' : 'asc'));
  }

  private _emitSortChange() {
    this.sortChange.emit({
      sortBy: this.sortBySignal(),
      sortOrder: this.sortOrderSignal()
    });
  }
}
