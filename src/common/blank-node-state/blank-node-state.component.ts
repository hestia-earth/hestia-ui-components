import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { NgClass } from '@angular/common';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { DataState } from '@hestia-earth/api';
import { NodeType, blankNodesType } from '@hestia-earth/schema';
import { combineLatest } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { isUndefined } from '@hestia-earth/utils';

import { findNodeModel } from '../../engine/engine.service';
import { NodeKeyState, isState } from '../node-utils';

const getState = (node: blankNodesType, key: string) =>
  [NodeKeyState.deleted, NodeKeyState.updated, NodeKeyState.added].find(state => isState(node, key, state)) ||
  NodeKeyState.unchanged;

const stateStars: {
  [state in NodeKeyState]: number;
} = {
  [NodeKeyState.unchanged]: 0,
  [NodeKeyState.added]: 1,
  [NodeKeyState.updated]: 2,
  [NodeKeyState.deleted]: 4
};

@Component({
  selector: 'he-blank-node-state',
  templateUrl: './blank-node-state.component.html',
  styleUrls: ['./blank-node-state.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NgClass, FaIconComponent]
})
export class BlankNodeStateComponent {
  protected readonly faExternalLinkAlt = faExternalLinkAlt;

  protected readonly dataState = input<DataState>();
  protected readonly nodeType = input<NodeType>();
  protected readonly dataKey = input<string>();
  protected readonly key = input<string>();
  protected readonly node = input.required<blankNodesType>();
  /**
   * Force override state.
   */
  protected readonly state = input<NodeKeyState>();
  protected readonly linkClass = input<string>();

  protected readonly NodeKeyState = NodeKeyState;

  protected readonly show = computed(() => this.dataState() !== DataState.original);

  protected readonly currentState = computed(() => this.state() || getState(this.node(), this.key()));

  protected readonly stars = computed(() => Array.from(Array(stateStars[this.currentState()]).keys()));
  protected readonly showLink = computed(() =>
    [
      !!this.dataKey(),
      ['term', 'value'].includes(this.key()),
      this.currentState() === NodeKeyState.added || this.currentState() === NodeKeyState.updated
    ].every(Boolean)
  );

  private readonly model$ = combineLatest([toObservable(this.node), toObservable(this.key)]).pipe(
    filter(([node, key]) => [!isUndefined(node), !!key].every(Boolean)),
    distinctUntilChanged(),
    map(([node, key]) => findNodeModel(node, key))
  );
  protected readonly model = toSignal(this.model$);
  protected readonly modelUrl = computed(
    () => this.model()?.apiDocsPath || this.model()?.docPath || this.model()?.path
  );
}
