const suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];

export const toThousands = (input: number, digits = 0) => {
  const exp = Math.floor(Math.log(input) / Math.log(1000));
  return (input / Math.pow(1000, exp)).toFixed(digits) + suffixes[exp - 1];
};
