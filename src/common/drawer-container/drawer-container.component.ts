import {
  ChangeDetectionStrategy,
  Component,
  computed,
  effect,
  ElementRef,
  HostListener,
  inject,
  input,
  model,
  signal,
  TemplateRef,
  viewChild
} from '@angular/core';
import { NgClass, NgTemplateOutlet } from '@angular/common';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faChevronDown, faChevronLeft, faChevronRight, faChevronUp } from '@fortawesome/free-solid-svg-icons';
import type { AnimationEvent } from '@angular/animations';
import { outputFromObservable, toObservable, toSignal } from '@angular/core/rxjs-interop';
import { debounceTime, filter, map, mergeMap, skip, startWith, switchMap, throttleTime } from 'rxjs/operators';
import { FormsModule } from '@angular/forms';
import { NgbDropdown, NgbDropdownMenu, NgbDropdownToggle } from '@ng-bootstrap/ng-bootstrap';
import { animationFrameScheduler, combineLatest, EMPTY, fromEvent, Observable } from 'rxjs';
import { LocalStorageService } from 'ngx-webstorage';

import { ANIMATIONS } from './drawer-container.animations';
import { sidenavStore, initialState, drawerPosition } from './sidenav.store';
import { INavigationMenuLink, NavigationMenuComponent } from '../navigation-menu/navigation-menu.component';
import { ResponsiveBreakpoint, ResponsiveService } from '../responsive.service';
import { SvgIconComponent } from 'angular-svg-icon';

const storageKey = 'he-drawer-container';

@Component({
  selector: 'he-drawer-container',
  imports: [
    NgTemplateOutlet,
    FaIconComponent,
    NgClass,
    FormsModule,
    NgbDropdown,
    NgbDropdownMenu,
    NgbDropdownToggle,
    NavigationMenuComponent,
    SvgIconComponent
  ],
  templateUrl: './drawer-container.component.html',
  styleUrl: './drawer-container.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: ANIMATIONS,
  host: {
    '[class]': 'hostClass()',
    '[attr.drawer-position]': 'position()',
    '[attr.drawer-state]': 'menuState()'
  },
  providers: [sidenavStore]
})
export class DrawerContainerComponent {
  private readonly sidenavStore = inject(sidenavStore);
  private readonly responsiveService = inject(ResponsiveService);
  private readonly localStorage = inject(LocalStorageService);

  private readonly contentContainer = viewChild('contentContainer', { read: ElementRef });

  protected readonly menuState = signal<'open' | 'close'>('close');
  protected readonly expanded = signal(initialState.expanded);

  /**
   * The breakpoint at which the drawer will no longer be compact.
   * If a number is provided, it will be used as the width in pixels.
   * If 'none' is provided, the drawer will never be compact.
   * If a breakpoint name is provided, the drawer will be compact until that breakpoint.
   * @default 'tablet' - the drawer will be compact until the tablet breakpoint.
   */
  protected readonly compactBreakpoint = input<ResponsiveBreakpoint>('tablet');
  protected readonly position = input<drawerPosition>(initialState.side);
  protected readonly min = input<number>(initialState.minWidth);
  protected readonly width = model<number>(initialState.width);
  protected readonly max = input<number>(initialState.maxWidth);
  protected readonly resizable = input(true);
  protected readonly contentPaddingStyles = input<string>();
  protected readonly menuOverlap = input(true);
  protected readonly links = input<INavigationMenuLink[]>([]);
  protected readonly drawerHeaderTemplate = input<TemplateRef<any>>();
  protected readonly drawerMenuHeaderTemplate = input<TemplateRef<any>>();
  protected readonly compactHeaderClass = input<NgClass['ngClass']>(null);
  /**
   * The minimum distance between the menu and the edge of the content container.
   */
  protected readonly minMenuDistance = input<number>(initialState.minMenuDistance);
  /**
   * Giving an id to the menu so that it can store the resized position.
   */
  protected readonly id = input('');

  protected readonly opened = outputFromObservable(
    toObservable(this.menuState).pipe(
      skip(1),
      filter(state => state === 'open'),
      map(() => true)
    )
  );
  protected readonly closed = outputFromObservable(
    toObservable(this.menuState).pipe(
      skip(1),
      filter(state => state === 'close'),
      map(() => true)
    )
  );

  protected readonly faChevronUp = faChevronUp;
  protected readonly faChevronDown = faChevronDown;

  protected readonly visible = computed(() =>
    [this.links()?.length > 0, !!this.drawerHeaderTemplate(), !!this.drawerMenuHeaderTemplate()].some(Boolean)
  );

  protected readonly isCompact = toSignal(
    toObservable(this.compactBreakpoint).pipe(
      switchMap(breakpoint =>
        this.responsiveService.isAboveBreakpoint$(breakpoint).pipe(map(isAboveBreakpoint => !isAboveBreakpoint))
      )
    )
  );
  protected readonly hostClass = computed(() =>
    ['is-flex', this.isCompact() ? 'is-flex-direction-column' : '', this.resizable() ? 'is-resizable' : ''].join(' ')
  );
  protected readonly isHoldingToggle = this.sidenavStore.isResizing;

  private readonly _updateSidenavEvent$ = toObservable(this.sidenavStore.isResizing).pipe(
    switchMap(listen => (listen ? (fromEvent(window, 'mousemove') as Observable<MouseEvent>) : EMPTY)),
    throttleTime(0, animationFrameScheduler)
  );
  private readonly _contentContainer$ = toObservable(this.contentContainer).pipe(filter(v => !!v));
  private readonly _hostComponentWidth$ = fromEvent(window, 'resize').pipe(startWith(window.innerWidth));

  /**
   * we use combineLatest instead of computed to avoid the animation flickering (wait for all sources to emit)
   */
  protected readonly reduceAnimation = toSignal(
    combineLatest([toObservable(this.expanded), toObservable(this.isCompact), toObservable(this.visible)]).pipe(
      map(([expanded, isCompact, visible]) => ([expanded, !isCompact, visible].every(Boolean) ? 'reduced' : 'full'))
    )
  );
  private readonly _contentContainerPadding$ = combineLatest([
    this._hostComponentWidth$,
    toObservable(this.contentPaddingStyles)
  ]).pipe(
    // debounce to make sure `contentPaddingStyles` is applied before calculating the total padding
    debounceTime(100),
    mergeMap(() => this._contentContainer$.pipe(map(el => this._getContentLeftSideSpace(el))))
  );

  private readonly storageKey = computed(() => (this.id() ? [storageKey, this.id()].join('-') : null));
  private readonly storedWith = computed(() =>
    this.storageKey() ? JSON.parse(this.localStorage.retrieve(this.storageKey()) || '{}').width || 0 : 0
  );

  constructor() {
    this.sidenavStore.setSide(this.position);
    this.sidenavStore.setMinWidth(this.min);
    this.sidenavStore.setMaxWidth(this.max);
    this.sidenavStore.setWidth(this.width);
    this.sidenavStore.updateResizing(this._updateSidenavEvent$);
    this.sidenavStore.setContentContainerSpacing(this._contentContainerPadding$);
    this.sidenavStore.setExpanded(this.expanded);
    this.sidenavStore.setMinMenuDistance(this.minMenuDistance);

    effect(() => {
      if (this.resizable() && this.storedWith()) {
        setTimeout(() => this.width.set(this.storedWith()));
      }
    });

    effect(() => {
      if (!this.visible()) {
        setTimeout(() => this.menuState.set('close'));
      }
    });
  }

  startResizing(event: MouseEvent): void {
    if (!this.resizable()) return;
    this.sidenavStore.startResizing(event);
  }

  @HostListener('window:mouseup')
  stopResizing() {
    this.sidenavStore.stopResizing();
    this.storageKey() &&
      this.localStorage.store(this.storageKey(), JSON.stringify({ width: this.sidenavStore.width() }));
  }

  protected toggleIcon() {
    return (this.expanded() && this.position() === 'left') || (!this.expanded() && this.position() === 'right')
      ? faChevronLeft
      : faChevronRight;
  }

  public toggleMenu() {
    if (this.sidenavStore.hasMenuWidthChanged() && this.expanded()) return;

    this.expanded() ? this.hideMenu() : this.showMenu();
  }

  public hideMenu() {
    this.expanded.set(false);
  }

  public showMenu() {
    this.expanded.set(true);
  }

  protected animationDone(event: AnimationEvent) {
    const didOpen = event.toState !== 'hidden';
    this.menuState.set(didOpen ? 'open' : 'close');
  }

  private _getContentLeftSideSpace(element: ElementRef<HTMLElement>) {
    const position = this.position();
    const styles = window.getComputedStyle(element.nativeElement, null);
    const padding = styles.getPropertyValue('padding-' + position);
    const margin = styles.getPropertyValue('margin-' + position);
    return parseInt(padding, 10) + parseInt(margin, 10);
  }
}
