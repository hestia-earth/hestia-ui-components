import { animate, state, style, transition, trigger } from '@angular/animations';

const animationDuration = '300ms';

export const ANIMATIONS = [
  trigger('sideMenu', [
    state(
      'void, hidden',
      style({
        transform: 'translateX(calc(var(--menu-transition-transform) * var(--sidenav-width)))'
      })
    ),
    state(
      '*',
      style({
        transform: 'translateX(0)'
      })
    ),
    transition('hidden => *', animate(`${animationDuration} ease-in`)),
    transition('* => hidden', animate(`${animationDuration} ease-out`))
  ]),
  trigger('reduceWidth', [
    state(
      '*',
      style({
        width: '100%',
        transform: 'translateX(0)'
      })
    ),
    state(
      'reduced',
      style({
        width: 'calc(100% - var(--content-transition-x))',
        transform: 'translateX(calc(var(--sidenav-side-transform) * var(--content-transition-x)))'
      })
    ),
    transition('void -> *', animate('0ms')),
    transition(':enter', animate(0)),
    transition('reduced => *', animate(`${animationDuration} ease-out`)),
    transition('* => reduced', animate(`${animationDuration} ease-in`))
  ])
];
