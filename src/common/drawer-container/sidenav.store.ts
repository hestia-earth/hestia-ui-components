import { computed } from '@angular/core';
import { patchState, signalStore, withComputed, withHooks, withMethods, withState } from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { pipe } from 'rxjs';
import { tap } from 'rxjs/operators';

export type drawerPosition = 'left' | 'right';

const initialResizeEvent = {
  isResizing: false,
  startingCursorX: 0,
  startingWidth: 0,
  newWidth: 0
};

export const initialState = {
  minWidth: 250,
  maxWidth: 600,
  width: 256,
  expanded: true,
  side: 'left' as drawerPosition,
  resizeEvent: initialResizeEvent,
  contentContainerSpacing: 0,
  // 20px handle size + padding
  minMenuDistance: 20 + 16
};

export const sidenavStore = signalStore(
  withState(initialState),
  withComputed(({ minWidth, width, maxWidth }) => ({
    sidenavWidth: computed(() => Math.min(Math.max(width(), minWidth()), maxWidth()))
  })),
  withMethods(store => ({
    setExpanded: rxMethod<boolean>(pipe(tap((expanded: boolean) => patchState(store, { expanded })))),
    setSide: rxMethod<drawerPosition>(pipe(tap((side: drawerPosition) => patchState(store, { side })))),
    setMinMenuDistance: rxMethod<number>(
      pipe(tap((minMenuDistance: number) => patchState(store, { minMenuDistance })))
    ),
    setMinWidth: rxMethod<number>(pipe(tap((minWidth: number) => patchState(store, { minWidth })))),
    setMaxWidth: rxMethod<number>(pipe(tap((maxWidth: number) => patchState(store, { maxWidth })))),
    setWidth: rxMethod<number>(pipe(tap((width: number) => patchState(store, { width })))),
    setSidenavWidthCss: rxMethod<{
      position: drawerPosition;
      width: number;
    }>(
      pipe(tap(({ position, width }) => document.body.style.setProperty(`--sidenav-${position}-width`, `${width}px`)))
    ),
    setContentTransformCss: rxMethod<{
      expanded: boolean;
      position: drawerPosition;
      value: number;
    }>(
      pipe(
        tap(({ expanded, position, value }) => {
          document.body.style.setProperty(`--content-transition-${position}-x`, `${expanded ? value : 0}px`);
        })
      )
    ),
    setContentContainerSpacing: rxMethod<number>(
      pipe(tap((contentContainerSpacing: number) => patchState(store, { contentContainerSpacing })))
    ),
    startResizing: (event: MouseEvent) =>
      patchState(store, {
        resizeEvent: {
          isResizing: true,
          startingCursorX: event.clientX,
          startingWidth: store.sidenavWidth(),
          newWidth: store.sidenavWidth()
        }
      }),
    updateResizing: rxMethod<MouseEvent>(
      tap((event: MouseEvent) => {
        const resizeEvent = store.resizeEvent();
        // No need to even continue if we're not resizing
        if (!resizeEvent.isResizing) {
          return;
        }

        // 1. Calculate how much mouse has moved on the x-axis
        const cursorDeltaX = event.clientX - resizeEvent.startingCursorX;

        // 2. Calculate the new width according to initial width and mouse movement
        const newWidth = resizeEvent.startingWidth - (store.side() === 'left' ? -1 : 1) * cursorDeltaX;

        patchState(store, state => ({
          ...state,
          resizeEvent: {
            ...state.resizeEvent,
            newWidth
          },
          width: newWidth
        }));
      })
    ),
    stopResizing: () =>
      patchState(store, {
        resizeEvent: {
          ...store.resizeEvent(),
          isResizing: false
        }
      })
  })),
  withComputed(({ sidenavWidth, minMenuDistance, contentContainerSpacing }) => ({
    overlappingDistance: computed(() => sidenavWidth() + minMenuDistance() - contentContainerSpacing())
  })),
  withComputed(({ overlappingDistance }) => ({
    isMenuOverlapping: computed(() => overlappingDistance() > 0)
  })),
  withComputed(({ resizeEvent, overlappingDistance, isMenuOverlapping }) => ({
    hasMenuWidthChanged: computed(() => resizeEvent().startingWidth !== resizeEvent().newWidth),
    isResizing: computed(() => resizeEvent().isResizing),
    contentContainerTransform: computed(() => (isMenuOverlapping() ? overlappingDistance() : 0))
  })),
  withHooks(
    ({ sidenavWidth, setSidenavWidthCss, setContentTransformCss, side, expanded, contentContainerTransform }) => ({
      onInit: () => {
        const contentTransform = computed(() => ({
          expanded: expanded(),
          position: side(),
          value: contentContainerTransform()
        }));
        setContentTransformCss(contentTransform);

        const width = computed(() => ({
          width: sidenavWidth(),
          position: side()
        }));
        setSidenavWidthCss(width);
      }
    })
  )
);
