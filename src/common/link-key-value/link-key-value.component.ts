import { ChangeDetectionStrategy, Component, HostBinding, computed, input } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { DataState } from '@hestia-earth/api';
import { isExpandable, NodeType } from '@hestia-earth/schema';
import { isUndefined, toPrecision } from '@hestia-earth/utils';
import get from 'lodash.get';

import { baseUrl, schemaBaseUrl } from '../utils';
import { BlankNodeStateComponent } from '../blank-node-state/blank-node-state.component';

const valueLink = (value: any) =>
  isExpandable(value) && value['@id'] ? [baseUrl(), value['@type'].toLowerCase(), value['@id']].join('/') : null;

const valueLinkRef = (value: any, href?: string) =>
  href ? `<a class="is-dark" href="${href}">${value.name || value['@id']}</a>` : null;

const valueValue = (value: any) => (Array.isArray(value) ? value.map(valueValue).join(', ') : toPrecision(value));

const minMaxValue = (min?: number, max?: number) =>
  [isUndefined(min) ? '' : `Min: ${valueValue(min)}`, isUndefined(max) ? '' : `Max: ${valueValue(max)}`].join('; ');

const blankNodeValue = ({
  term,
  value,
  min,
  max
}: {
  term: any;
  value?: number | boolean;
  min?: number;
  max?: number;
}) =>
  [
    [valueLinkRef(term, valueLink(term)), isUndefined(value) ? '' : valueValue(value)].join(': '),
    [isUndefined(min) ? '' : `Min: ${valueValue(min)}`, isUndefined(max) ? '' : `Max: ${valueValue(max)}`].join('; ')
  ].join(' - ');

const stringMapper: {
  [type: string]: (value?: any) => string;
} = {
  number: (value: number) => `${toPrecision(value, 3)}`,
  undefined: () => '',
  blankNode: value =>
    'term' in value ? blankNodeValue(value) : valueLinkRef(value, valueLink(value)) || value.name || value['@id'],
  object: value =>
    Array.isArray(value)
      ? value.map(toString).join(', ')
      : value instanceof Date
        ? value.toJSON()
        : isExpandable(value)
          ? stringMapper.blankNode(value)
          : JSON.stringify(value, null, 2)
};

const toString = (value: any) =>
  (typeof value) in stringMapper ? stringMapper[typeof value](value) : value.toString();

@Component({
  selector: 'he-link-key-value',
  templateUrl: './link-key-value.component.html',
  styleUrls: ['./link-key-value.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NgTemplateOutlet, BlankNodeStateComponent]
})
export class LinkKeyValueComponent {
  protected readonly node = input.required<any>();
  protected readonly nodeType = input.required<NodeType>();
  protected readonly dataState = input.required<DataState>();
  protected readonly dataKey = input.required<string>();

  protected readonly key = input<string>();
  protected readonly defaultValue = input<any>('');

  protected readonly isExpandable = isExpandable;
  protected readonly valueLink = valueLink;
  protected readonly isUndefined = isUndefined;
  protected readonly minMaxValue = minMaxValue;

  @HostBinding('class.is-inline-block')
  get isInlineBlock() {
    return this.isArray();
  }

  protected readonly schemaBaseUrl = schemaBaseUrl();
  protected readonly toString = toString;

  protected readonly type = computed(() => this.node()?.['@type'] || this.node()?.type);
  protected readonly value = computed(() => get(this.node(), this.key()));
  protected readonly hasValue = computed(() => !isUndefined(this.value()));
  protected readonly isArray = computed(
    () => Array.isArray(this.value()) && [this.value().length > 1, isExpandable(this.value()?.[0])].some(Boolean)
  );

  protected isBlankNode(value: any) {
    return isExpandable(value) && 'term' in value;
  }
}
