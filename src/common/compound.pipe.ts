import { Pipe, PipeTransform } from '@angular/core';
import { TermTermType } from '@hestia-earth/schema';

import { compoundToHtml } from './compound';

@Pipe({
  name: 'compound',
  standalone: true
})
export class CompoundPipe implements PipeTransform {
  transform(value: string, termType?: TermTermType) {
    return compoundToHtml(value, termType);
  }
}
