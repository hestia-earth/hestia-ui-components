import { Component, TemplateRef, output, viewChild, input } from '@angular/core';

import { ContentContext, PopoverComponent } from '../popover/popover.component';

@Component({
  selector: 'he-popover-confirm',
  templateUrl: './popover-confirm.component.html',
  styleUrls: ['./popover-confirm.component.scss'],
  imports: [PopoverComponent]
})
export class PopoverConfirmComponent {
  private popover = viewChild.required(PopoverComponent);

  protected message = input<string>();
  protected content = input<TemplateRef<ContentContext>>();
  protected position = input<string>();
  protected popoverClass = input<string>();

  protected confirmed = output<any>();

  /**
   * Assign data to the content.
   */
  public data = {};

  protected confirm() {
    this.confirmed.emit(this.data);
    this.popover().active.set(false);
  }
}
