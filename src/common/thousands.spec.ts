import { toThousands } from './thousands';

describe('common > thousands', () => {
  describe('toThousands', () => {
    it('should convert to multiple of thousands', () => {
      expect(toThousands(3000)).toBe('3k');
      expect(toThousands(3000000)).toBe('3M');
      expect(toThousands(3000000000)).toBe('3G');
      expect(toThousands(3000000000000)).toBe('3T');
      expect(toThousands(3000000000000000)).toBe('3P');
      expect(toThousands(3000000000000000000)).toBe('3E');
    });
  });
});
