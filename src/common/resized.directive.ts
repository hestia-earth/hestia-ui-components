import { Directive, ElementRef, NgZone, OnDestroy, OnInit, inject, output } from '@angular/core';

export class ResizedEvent {
  public newRect: DOMRectReadOnly;
  public oldRect?: DOMRectReadOnly;
  public isFirst: boolean;

  constructor(newRect: DOMRectReadOnly, oldRect: DOMRectReadOnly | undefined) {
    this.newRect = newRect;
    this.oldRect = oldRect;
    this.isFirst = oldRect == null;
  }
}

@Directive({
  selector: '[resized]',
  standalone: true
})
export class ResizedDirective implements OnInit, OnDestroy {
  private readonly element = inject(ElementRef);
  private readonly zone = inject(NgZone);

  private observer: ResizeObserver;
  private oldRect?: DOMRectReadOnly;

  protected readonly resized = output<ResizedEvent>();

  constructor() {
    this.observer = new ResizeObserver(entries => this.zone.run(() => this.observe(entries)));
  }

  ngOnInit() {
    this.observer.observe(this.element.nativeElement);
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }

  private observe(entries: ResizeObserverEntry[]) {
    const domSize = entries[0];
    const resizedEvent = new ResizedEvent(domSize.contentRect, this.oldRect);
    this.oldRect = domSize.contentRect;
    this.resized.emit(resizedEvent);
  }
}
