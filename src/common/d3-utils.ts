import { Selection, select as d3Select } from 'd3-selection';

export const d3ellipse = <Datum>(
  selection: Selection<SVGTextElement, Datum, any, any>,
  maxWidth: number,
  ellipsisStr = '...'
) => {
  selection.each(function () {
    const t = d3Select(this);
    if (t.node().getComputedTextLength() > maxWidth) {
      const chars = t.text().split('');
      const line = [];
      do {
        line.push(chars.shift());
        t.text(line.join('') + ellipsisStr);
      } while (t.node().getComputedTextLength() <= maxWidth);
    }
  });
};

const shouldBreakLine = (
  overflows: boolean,
  isMultiWordLine: boolean,
  isOwnLineWord: boolean,
  isUnderLineLimit: boolean
) => ((overflows && isMultiWordLine) || isOwnLineWord) && isUnderLineLimit;

export const d3wrap = <Datum>(
  t: Selection<SVGTextElement, Datum, any, any>,
  {
    maxWidth,
    maxLines,
    words,
    nHeight,
    lHeight,
    vcentre = false,
    ownLineWord = ''
  }: {
    maxWidth: number;
    maxLines: number;
    words: string[];
    nHeight: number;
    lHeight: number;
    vcentre?: boolean;
    ownLineWord?: string;
  }
) => {
  words = words.reverse();
  const y = t.attr('y');
  const x = t.attr('x');
  let lineNumber = 1;
  let word;
  let line = [];
  let tspan = t.text(null).append('tspan').attr('x', x).attr('y', y);

  while ((word = words.pop()) && lineNumber <= maxLines) {
    line.push(word);
    tspan.text(line.join(' '));

    // TODO: hyphenate words that are too long
    if (
      shouldBreakLine(
        tspan.node().getComputedTextLength() > maxWidth,
        line.length > 1,
        word === ownLineWord,
        lineNumber < maxLines
      )
    ) {
      line.pop();
      tspan.text(line.join(' '));
      line = [word];
      tspan = t
        .append('tspan')
        .attr('x', x)
        .attr('y', y)
        .attr('dy', lineNumber++ * lHeight + 'px')
        .text(word);
    }
  }
  d3ellipse<Datum>(tspan, maxWidth, '... ' + ownLineWord);

  if (vcentre) {
    const yOffset = nHeight / 2 - (t.selectAll('tspan').size() - 1) * (lHeight / 2);
    t.selectAll('tspan').attr('y', yOffset);
  }
};
