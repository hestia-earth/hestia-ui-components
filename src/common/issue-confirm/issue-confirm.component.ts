import { Component, computed, inject, model, output } from '@angular/core';
import { KeyValuePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Repository, Template, reportIssueUrl } from '../utils';

@Component({
  selector: 'he-issue-confirm',
  templateUrl: './issue-confirm.component.html',
  styleUrls: ['./issue-confirm.component.scss'],
  imports: [FormsModule, KeyValuePipe]
})
export class IssueConfirmComponent {
  private readonly activeModal = inject(NgbActiveModal, { optional: true });

  public readonly title = model('Submit Feedback');
  public readonly repository = model<Repository>();
  public readonly template = model<Template>();
  public readonly isCommunity = model(false);

  protected readonly closed = output();

  protected readonly Repository = Repository;
  protected readonly Template = Template;
  protected readonly repositories = computed(() =>
    Object.values(Repository).filter(value => value !== Repository.community || this.isCommunity())
  );
  protected readonly issueUrl = computed(() =>
    this.repository() && this.template() ? reportIssueUrl(this.repository(), this.template()) : null
  );

  protected close() {
    this.closed.emit();
    this.activeModal?.close();
  }
}
