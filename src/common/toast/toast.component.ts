import { Component, inject, DestroyRef } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

import { HeToastService, IToast } from '../toast.service';

interface IToastTimeout extends IToast {
  timeout?: any;
}

@Component({
  selector: 'he-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  standalone: true
})
export class ToastComponent {
  private readonly destroyRef = inject(DestroyRef);
  private readonly toastService = inject(HeToastService);

  protected readonly toasts: IToastTimeout[] = [];

  constructor() {
    this.toastService.toasts$.pipe(takeUntilDestroyed(this.destroyRef)).subscribe(toast => this.add(toast));
  }

  private add(toast: IToastTimeout) {
    this.toasts.push(toast);
    if (toast.duration) {
      toast.timeout = setTimeout(() => this.dismiss(toast), toast.duration);
    }
  }

  public dismiss(toast: IToastTimeout) {
    this.toasts.splice(
      this.toasts.findIndex(val => val.id === toast.id),
      1
    );
  }
}
