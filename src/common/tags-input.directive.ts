import { AfterViewInit, Directive, ElementRef, Input, OnDestroy, inject } from '@angular/core';
import BulmaTagsInput from '../tags-input';

interface ITagsInputOptions {
  enabled?: boolean;
  items?: any;
  allowDuplicates?: boolean;
  caseSensitive?: boolean;
  clearSelectionOnTyping?: boolean;
  closeDropdownOnItemSelect?: boolean;
  delimiter?: string;
  freeInput?: boolean;
  highlightDuplicate?: boolean;
  highlightMatchesString?: boolean;
  itemValue?: string;
  itemText?: string;
  maxTags?: number;
  maxChars?: number;
  minChars?: number;
  noResultsLabel?: string;
  placeholder?: string;
  removable?: boolean;
  searchMinChars?: number;
  searchOn?: 'text' | 'value';
  selectable?: boolean;
  source?: any;
  tagClass?: string;
  trim?: boolean;
}

const restrictToType: {
  [type: string]: (value: string) => string | number | boolean;
} = {
  number: val => (parseFloat(val) === +val ? val : false)
};

interface IEvent {
  event: any;
  listener: any;
}

@Directive({
  selector: '[appTagsInput]',
  standalone: true
})
export class TagsInputDirective implements AfterViewInit, OnDestroy {
  private elementRef = inject(ElementRef);

  private tagsInput?: BulmaTagsInput;
  private inputEvents: IEvent[] = [];

  @Input()
  private appTagsInput: ITagsInputOptions = {};

  ngAfterViewInit() {
    const { enabled } = this.appTagsInput;
    const { localName } = this.input;
    setTimeout(() => !this.tagsInput && enabled && localName === 'input' && this.enableTagsInput());
  }

  ngOnDestroy() {
    this.inputEvents.map(({ event, listener }) => this.tagsInputInput.removeEventListener(event, listener));
    this.inputEvents = [];
    this.tagsInput = undefined;
  }

  private get input() {
    return this.elementRef.nativeElement as HTMLInputElement;
  }

  private get tagsInputNode() {
    return this.elementRef.nativeElement?.parentNode?.querySelectorAll('.tags-input')?.[0];
  }

  private get tagsInputInput() {
    return this.tagsInputNode?.querySelectorAll('.input')?.[0] as HTMLInputElement;
  }

  private setInputEvent({ event, listener }: IEvent) {
    this.tagsInputInput.addEventListener(event, listener);
    this.inputEvents.push({ event, listener });
  }

  private mapValue(value: string) {
    return this.appTagsInput.items.type in restrictToType ? restrictToType[this.appTagsInput.items.type](value) : value;
  }

  private addInputValue(value?: string) {
    if (value && this.mapValue(value)) {
      this.tagsInput!.add(value);
      this.tagsInputInput && (this.tagsInputInput.value = '');
    }
  }

  private enableTagsInput() {
    try {
      const { enabled, items, ...options } = this.appTagsInput;
      this.tagsInput = new BulmaTagsInput(this.input, options);

      // TODO: remove when fixed, @see https://github.com/CreativeBulma/bulma-tagsinput/issues/23
      const classes = ['is-small', 'is-large'].filter(v => this.input.classList.contains(v));
      this.tagsInputNode && classes.map(v => this.tagsInputNode.classList.add(v));

      // regex pattern matching
      this.tagsInput.on('before.add', value => this.mapValue(value));

      // add item on blur if value
      const onBlur = () => this.addInputValue(this.tagsInputInput?.value);
      this.setInputEvent({ event: 'blur', listener: onBlur });
    } catch (err) {
      console.error(err);
    }
  }
}
