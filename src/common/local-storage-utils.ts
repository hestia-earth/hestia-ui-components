import { inject, signal } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';

/**
 * Signal utility to patch local storage.
 */
export const localStorageSignal = <T>(initialValue: T, localStorageKey: string) => {
  const localStorage = inject(LocalStorageService);
  // retrieve value from local storage
  const storedValueRaw = localStorage.retrieve(localStorageKey);
  if (storedValueRaw) {
    try {
      initialValue = JSON.parse(storedValueRaw);
    } catch (e) {
      console.error('Failed to parse stored value for key:', localStorageKey);
    }
  } else {
    localStorage.store(localStorageKey, JSON.stringify(initialValue));
  }

  const writableSignal = signal(initialValue);

  // monkey-patch signal setter to also store to local storage on set
  const setter = writableSignal.set;
  writableSignal.set = (value: T) => {
    localStorage.store(localStorageKey, JSON.stringify(value));
    setter(value);
  };

  return writableSignal;
};
