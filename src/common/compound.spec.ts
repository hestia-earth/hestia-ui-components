import { TermTermType } from '@hestia-earth/schema';

import { compoundToHtml } from './compound';

describe('compound', () => {
  describe('compoundToHtml', () => {
    it('should handle all compounds', () => {
      expect(compoundToHtml('BOD5')).toEqual('BOD<sub>5</sub>');
      expect(compoundToHtml('C2Cl2F4')).toEqual('C<sub>2</sub>Cl<sub>2</sub>F<sub>4</sub>');
      expect(compoundToHtml('C2Cl3F3')).toEqual('C<sub>2</sub>Cl<sub>3</sub>F<sub>3</sub>');
      expect(compoundToHtml('C2H2F4')).toEqual('C<sub>2</sub>H<sub>2</sub>F<sub>4</sub>');
      expect(compoundToHtml('C6H14')).toEqual('C<sub>6</sub>H<sub>14</sub>');
      expect(compoundToHtml('CBrClF2')).toEqual('CBrClF<sub>2</sub>');
      expect(compoundToHtml('CCl2F2')).toEqual('CCl<sub>2</sub>F<sub>2</sub>');
      expect(compoundToHtml('CH4')).toEqual('CH<sub>4</sub>');
      expect(compoundToHtml('CHClF2')).toEqual('CHClF<sub>2</sub>');
      expect(compoundToHtml('CO2')).toEqual('CO<sub>2</sub>');
      expect(compoundToHtml('COD')).toEqual('COD');
      expect(compoundToHtml('H2S')).toEqual('H<sub>2</sub>S');
      expect(compoundToHtml('K2O')).toEqual('K<sub>2</sub>O');
      expect(compoundToHtml('N')).toEqual('N');
      expect(compoundToHtml('N2')).toEqual('N<sub>2</sub>');
      expect(compoundToHtml('N2O')).toEqual('N<sub>2</sub>O');
      expect(compoundToHtml('NH3')).toEqual('NH<sub>3</sub>');
      expect(compoundToHtml('NH4')).toEqual('NH<sub>4</sub>');
      expect(compoundToHtml('NMVOC')).toEqual('NMVOC');
      expect(compoundToHtml('NO')).toEqual('NO');
      expect(compoundToHtml('NO3')).toEqual('NO<sub>3</sub>');
      expect(compoundToHtml('NOx')).toEqual('NO<sub>x</sub>');
      expect(compoundToHtml('NOx, to air')).toEqual('NO<sub>x</sub>, to air');
      expect(compoundToHtml('P')).toEqual('P');
      expect(compoundToHtml('P2O5')).toEqual('P<sub>2</sub>O<sub>5</sub>');
      expect(compoundToHtml('PO43-')).toEqual('PO<sub>4</sub><sup>3-</sup>');
      expect(compoundToHtml('SO2')).toEqual('SO<sub>2</sub>');
      expect(compoundToHtml('NO3-')).toEqual('NO<sub>3</sub><sup>-</sup>');
      expect(compoundToHtml('m2')).toEqual('m<sup>2</sup>');
      expect(compoundToHtml('m3')).toEqual('m<sup>3</sup>');
      expect(compoundToHtml('the volume in m3 at 1 atm')).toEqual('the volume in m<sup>3</sup> at 1 atm');
    });

    it('should ignore no compound', () => {
      let value = '14797-55-8';
      expect(compoundToHtml(value)).toEqual(value);
      value = 'This is ok 3-4 is fine m45';
      expect(compoundToHtml(value)).toEqual(value);
      value = 'GWP100';
      expect(compoundToHtml(value)).toEqual(value);
      value = 'PM10';
      expect(compoundToHtml(value)).toEqual(value);
      value = 'PM2.5';
      expect(compoundToHtml(value)).toEqual(value);
      value = 'da3ec527-f242-3265-9b89-4bb219942fd4';
      expect(compoundToHtml(value)).toEqual(value);
      value = 'AR2';
      expect(compoundToHtml(value)).toEqual(value);
      value = 'Oxygen';
      expect(compoundToHtml(value)).toEqual(value);
      value = 'CML2001 Baseline';
      expect(compoundToHtml(value)).toEqual(value);
    });

    it('should ignore text with hyperlinks', () => {
      const value = 'this is a link <a href="/term/co2toAir">co2toAir</a>';
      expect(compoundToHtml(value)).toEqual(value);
    });

    it('should ignore non allowed termType', () => {
      expect(compoundToHtml('CH4', TermTermType.crop)).toEqual('CH4');
    });
  });
});
