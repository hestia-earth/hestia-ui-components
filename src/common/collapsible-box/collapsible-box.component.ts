import { Component, HostBinding, model } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'he-collapsible-box',
  imports: [FaIconComponent],
  templateUrl: './collapsible-box.component.html',
  styleUrl: './collapsible-box.component.scss'
})
export class CollapsibleBoxComponent {
  protected readonly faAngleUp = faAngleUp;
  protected readonly faAngleDown = faAngleDown;

  @HostBinding('class')
  protected readonly hostClass = 'box';

  public readonly open = model(true);
}
