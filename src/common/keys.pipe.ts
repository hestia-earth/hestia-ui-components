import { Pipe, PipeTransform } from '@angular/core';

/**
 * @deprecated Use angular `KeyValuePipe` instead.
 */
@Pipe({
  name: 'keys',
  standalone: true
})
export class KeysPipe implements PipeTransform {
  transform(values: any, sort = false): any {
    const entries = Object.entries(values).map(([key, value]) => ({ key, value }));
    if (sort) {
      entries.sort((a, b) => a.key.localeCompare(b.key));
    }
    return entries;
  }
}
