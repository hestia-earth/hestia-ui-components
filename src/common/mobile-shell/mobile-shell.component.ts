import {
  Component,
  computed,
  ContentChild,
  EventEmitter,
  Input,
  Output,
  signal,
  TemplateRef,
  WritableSignal
} from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgTemplateOutlet } from '@angular/common';

import { IShellMenuButton } from '../shell/shell.component';

export interface IMobileShellMenuButton {
  id?: string;
  icon?: any;
  label?: string;
  onClick: () => void;
  menuRef?: TemplateRef<any>;
  iconPath?: string;
  disabled?: boolean;
}

@Component({
  selector: 'he-mobile-shell',
  templateUrl: './mobile-shell.component.html',
  styleUrls: ['./mobile-shell.component.scss'],
  imports: [NgTemplateOutlet, FaIconComponent]
})
export class MobileShellComponent {
  private readonly _shelfButtons: WritableSignal<IMobileShellMenuButton[]> = signal([]);
  private readonly _defaultActiveButtonId: WritableSignal<string | null> = signal(null);

  protected readonly clickedButton: WritableSignal<string | null> = signal(null);
  protected readonly expanded = signal(false);
  protected readonly menuRef = signal(null as TemplateRef<any> | null);

  @ContentChild('iconTemplate') iconTemplate: TemplateRef<any> | null = null;

  @Input() set menuButtons(value: IMobileShellMenuButton[]) {
    this._shelfButtons.set(value);
  }

  @Input() set defaultActiveButtonId(value: string | null) {
    this._defaultActiveButtonId.set(value);
  }

  @Output() menuShown = new EventEmitter();

  protected activeButton: IMobileShellMenuButton | null;

  protected readonly buttons = computed(() =>
    this._shelfButtons()
      .map((button, index) => ({
        ...button,
        id: button.id || index.toString()
      }))
      .map(button => {
        const activeMenuId = this.activeMenuId();
        return { ...button, isActive: button.id === (activeMenuId || this.activeButtonId()) };
      })
  );

  protected readonly activeButtonId = computed(() => {
    return this.clickedButton() || this._defaultActiveButtonId();
  });

  protected readonly activeMenuId = computed(() => {
    if (this.expanded() && this.menuRef()) {
      return this._shelfButtons()
        .findIndex(button => button.menuRef === this.menuRef())
        ?.toString();
    }
  });

  hideMenu() {
    this.expanded.set(false);
    this.clickedButton.set(null);
  }

  showMenu(templateRef?: TemplateRef<any>) {
    if (templateRef) {
      this.menuRef.set(templateRef);
    }
    this.expanded.set(true);
    this.menuShown.emit();
  }

  protected onMenuButtonClick(button: IShellMenuButton) {
    if (button.id === this.clickedButton()) {
      this.hideMenu();
      return;
    }

    if (button.menuRef) {
      this.menuRef.set(button.menuRef);
      this.showMenu();
    } else {
      this.hideMenu();
    }

    if (!button.activeDisabled) {
      this.clickedButton.set(button.id);
    }

    button.onClick?.();
  }
}
