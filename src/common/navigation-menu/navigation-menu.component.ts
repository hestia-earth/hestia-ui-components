import { ChangeDetectionStrategy, Component, computed, inject, input, output, ViewEncapsulation } from '@angular/core';
import { IsActiveMatchOptions, RouterLink, RouterLinkActive } from '@angular/router';
import { NgClass, NgTemplateOutlet } from '@angular/common';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faAngleDown, faAngleUp, faCaretDown, faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { SvgIconComponent } from 'angular-svg-icon';

import { ResponsiveService } from '../responsive.service';

export interface INavigationMenuLink {
  /**
   * The title displayed. Can be HTML or plain text.
   */
  title: string;
  /**
   * When no URL is provided, link is not clickable.
   */
  url?: string;
  icon?: string;
  /**
   * Children links to display under this menu item.
   */
  links?: INavigationMenuLink[];
  /**
   * Query parameters to use with the URL.
   */
  queryParams?: { [key: string]: string };
  /**
   * URL fragment.
   */
  fragment?: string;
  /**
   * Router active match options override.
   */
  activeMatchOptions?: IsActiveMatchOptions;
  /**
   * If there is no icon and links are provided, top-level links are not expanded by default.
   */
  expanded?: boolean;
  /**
   * If the menu has children, can it be open/closed.
   */
  collapsible?: boolean;
}

const primaryLinkExpandable = (link: INavigationMenuLink) => link.links?.length && !link.icon;

@Component({
  selector: 'he-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  imports: [FaIconComponent, NgTemplateOutlet, RouterLinkActive, RouterLink, NgClass, SvgIconComponent],
  host: {
    class: 'no-print navigation-menu'
  }
})
export class NavigationMenuComponent {
  protected readonly responsiveService = inject(ResponsiveService);

  protected readonly faAngleUp = faAngleUp;
  protected readonly faAngleDown = faAngleDown;
  protected readonly faCaretDown = faCaretDown;
  protected readonly faCaretRight = faCaretRight;

  protected readonly links = input<INavigationMenuLink[]>([]);
  protected readonly sticky = input(false);
  protected readonly collapsible = input(false);
  protected readonly routerLinkMatchOptions = input<IsActiveMatchOptions>({
    queryParams: 'subset',
    fragment: 'exact',
    paths: 'exact',
    matrixParams: 'ignored'
  });

  protected readonly closed = output();

  protected trackByLink({ title, url, queryParams, fragment }: INavigationMenuLink) {
    const params = new URLSearchParams(queryParams);
    return `${title}:${url}?${params.toString()}#${fragment}`;
  }

  protected readonly primaryLinkExpandable = primaryLinkExpandable;
  protected readonly withPrimaryIcons = computed(() =>
    this.links().some(link => link.icon || primaryLinkExpandable(link))
  );

  private collapseOtherLinks(links: INavigationMenuLink[]) {
    this.responsiveService.isTablet() && links.forEach(link => (link.expanded = false));
  }

  protected primaryLinkActiveChange(item: INavigationMenuLink) {
    this.links().forEach(link => (link.collapsible ? (link.expanded = false) : null));
    item.expanded = true;
  }

  protected toggleSecondaryLink(item: INavigationMenuLink, index: number) {
    const siblings = this.links()[index].links.filter(link => link !== item);
    this.collapseOtherLinks(siblings);
    item.expanded = !item.expanded;
  }

  protected secondaryLinkActiveChange(item: INavigationMenuLink, index: number) {
    const links = this.links()
      .flatMap(({ links }) => links)
      .filter(Boolean);
    this.collapseOtherLinks(links);
    item.expanded = true;
    this.links()[index].expanded = true;
  }

  protected primaryLinkActiveOptions(link: INavigationMenuLink): IsActiveMatchOptions | { exact: boolean } {
    // if the link has children, it should not be active unless exact
    // otherwise, use defaults
    return link.activeMatchOptions || (link.links?.length ? { exact: true } : this.routerLinkMatchOptions());
  }

  protected withSublinks(link: INavigationMenuLink) {
    return link.links.some(v => !!v?.links?.length);
  }

  protected close() {
    // delay closing to show selection
    setTimeout(() => this.closed.emit(), 200);
  }
}
