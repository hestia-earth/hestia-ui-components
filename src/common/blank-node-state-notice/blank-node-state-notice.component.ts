import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { DataState } from '@hestia-earth/api';

@Component({
  selector: 'he-blank-node-state-notice',
  templateUrl: './blank-node-state-notice.component.html',
  styleUrls: ['./blank-node-state-notice.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true
})
export class BlankNodeStateNoticeComponent {
  protected readonly dataState = input<DataState>();
  protected readonly showDeleted = input(false);

  protected readonly show = computed(() => this.dataState() !== DataState.original);
}
