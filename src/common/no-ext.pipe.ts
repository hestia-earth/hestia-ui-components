import { Pipe, PipeTransform } from '@angular/core';
import { filenameWithoutExt } from '@hestia-earth/api';

@Pipe({
  name: 'noExt',
  standalone: true
})
export class NoExtPipe implements PipeTransform {
  transform(value: string) {
    return filenameWithoutExt(value || '');
  }
}
