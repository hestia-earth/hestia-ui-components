/* eslint-disable @angular-eslint/directive-selector */
import {
  AfterViewInit,
  DestroyRef,
  Directive,
  ElementRef,
  Injectable,
  NgZone,
  inject,
  input,
  output
} from '@angular/core';
import { fromEvent, Subject, timer } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { skipUntil, take, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class DocumentClickService {
  private ngZone = inject(NgZone);

  public documentClick$ = new Subject<Event>();

  constructor() {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(document, 'click')
        .pipe(
          takeUntilDestroyed(),
          tap(event => this.documentClick$.next(event))
        )
        .subscribe();
    });
  }
}

@Directive({
  selector: '[clickOutside]',
  standalone: true
})
export class ClickOutsideDirective implements AfterViewInit {
  private readonly elementRef = inject(ElementRef);
  private readonly zone = inject(NgZone);
  private readonly destroyRef = inject(DestroyRef);
  private readonly documentClickService = inject(DocumentClickService);

  protected clickOutsideListenAfter = input(0);
  protected clickOutside = output<void>();

  ngAfterViewInit() {
    const skip$ = timer(this.clickOutsideListenAfter()).pipe(take(1));
    this.zone.runOutsideAngular(() => {
      this.documentClickService.documentClick$
        .pipe(
          skipUntil(skip$),
          takeUntilDestroyed(this.destroyRef),
          tap(event => {
            if (!this.elementRef.nativeElement.contains(event.target)) {
              this.zone.run(() => {
                this.clickOutside.emit();
              });
            }
          })
        )
        .subscribe();
    });
  }
}
