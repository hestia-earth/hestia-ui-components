import { major, minor } from './semver-utils';

describe('common > semver-utils', () => {
  const version = '5.4.31';

  describe('major', () => {
    it('should return the major version only', () => {
      expect(major(version)).toBe('5');
    });
  });

  describe('minor', () => {
    it('should return the full minor version', () => {
      expect(minor(version)).toBe('5.4');
    });
  });
});
