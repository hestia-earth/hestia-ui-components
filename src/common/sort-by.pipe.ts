import { Pipe, PipeTransform } from '@angular/core';
import orderBy from 'lodash.orderby';

type order = 'asc' | 'desc';

@Pipe({
  name: 'sortBy',
  standalone: true
})
export class SortByPipe<T> implements PipeTransform {
  transform(value: T[], keys: string | string[], orders: order | order[] = ['asc']): T[] {
    return orderBy(value, keys, orders);
  }
}
