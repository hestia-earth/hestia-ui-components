import { HttpClient } from '@angular/common/http';
import { InjectionToken } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, delay, map, mergeMap, shareReplay } from 'rxjs/operators';

const loadMapUrl = 'https://maps.googleapis.com/maps/api/js';
const buildMapsUrl = (params: Record<string, string[]>) =>
  Object.entries(params)
    .filter(([_key, values]) => values.length)
    .flatMap(([key, values]) => values.map(value => `${key}=${value}`))
    .join('&');

export const HE_MAP_LOADED = new InjectionToken<Observable<boolean>>('Google Maps API Loaded');

/**
 * Usage:
 * ```
 * // import HttpClientJsonpModule
 * imports: [HttpClientJsonpModule]
 *
 * // Inject in module
 * providers: [{ provide: HE_MAP_LOADED, useFactory: loadMapApi(apiKey), deps: [HttpClient] } ]
 *
 * // Inject in component
 * protected mapLoaded$ = inject(HE_MAP_LOADED);
 * ```
 * @param apiKey
 * @param libraries
 * @returns
 */
export const loadMapApi =
  (apiKey: string, libraries = ['visualization', 'drawing']) =>
  (httpClient: HttpClient) =>
    of(`${loadMapUrl}?${buildMapsUrl({ key: [apiKey], libraries })}`).pipe(
      mergeMap(url => httpClient.jsonp(url, 'callback')),
      shareReplay({ bufferSize: 1, refCount: true }),
      delay(100),
      map(() => true),
      catchError(e => {
        console.error(e);
        return of(false);
      })
    );

export const locationQuery = (bounds: google.maps.LatLngBounds, searchField = 'location') => {
  const topRight = bounds.getNorthEast();
  const bottomLeft = bounds.getSouthWest();
  return {
    geo_bounding_box: {
      [searchField]: {
        top_left: { lat: topRight.lat(), lon: bottomLeft.lng() },
        bottom_right: { lat: bottomLeft.lat(), lon: topRight.lng() }
      }
    }
  };
};

const mapsQuery = 'http://maps.google.com/?q=';

export const mapsUrl = (location?: { lat?: number; lng?: number }) =>
  location?.lat && location?.lng ? `${mapsQuery}${location.lat},${location.lng}` : undefined;

export const fillColor = '#193957';
export const strokeColor = '#e13939';

export const fillStyle = {
  fillColor,
  fillOpacity: 0.6
};

export const strokeStyle = {
  strokeColor,
  strokeOpacity: 0.6,
  strokeWeight: 1
};

export const clustererImage = 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m';

const getCoordinatesForPercent = (percent: number) => {
  const x = Math.cos(2 * Math.PI * percent);
  const y = Math.sin(2 * Math.PI * percent);
  return [x, y];
};

interface IMarkerPieConfig {
  percent: number;
  color: string;
}

const pieUrl = (configs: IMarkerPieConfig[], size: number) => {
  let cumulativePercent = 0;

  return (
    'data:image/svg+xml;utf-8,' +
    encodeURIComponent(
      `
    <svg width="${size}" height="${size}" viewBox="-1 -1 2 2" xmlns="http://www.w3.org/2000/svg">${configs
      .map(({ percent, color }) => {
        const [startX, startY] = getCoordinatesForPercent(cumulativePercent);
        cumulativePercent += percent;
        const [endX, endY] = getCoordinatesForPercent(cumulativePercent);
        const largeArcFlag = percent > 0.5 ? 1 : 0;
        const pathData = [
          `M ${startX} ${startY}`, // Move
          `A 1 1 0 ${largeArcFlag} 1 ${endX} ${endY}`, // Arc
          `L 0 0` // Line
        ].join(' ');
        return `<path fill="${color}" d="${pathData}"></path>`;
      })
      .join('')}</svg>
  `.trim()
    )
  );
};

export const markerPie = (configs: IMarkerPieConfig[], size = 7) => ({
  anchor: new google.maps.Point(size / 2, size / 2),
  url: pieUrl(configs, size),
  labelOrigin: new google.maps.Point(0, -2)
});

export const markerIcon = (scale = 7): google.maps.Symbol => ({
  path: google.maps.SymbolPath.CIRCLE,
  ...fillStyle,
  ...strokeStyle,
  scale,
  labelOrigin: new google.maps.Point(0, -2)
});

export interface IMarker {
  position: google.maps.LatLng | google.maps.LatLngLiteral;
  label: google.maps.MarkerLabel;
  icon: google.maps.Symbol;
}

export const createMarker = (
  position?: google.maps.LatLng | google.maps.LatLngLiteral,
  text = '',
  color = fillColor,
  scale?: number
): IMarker =>
  position?.lat && position?.lng
    ? {
        position,
        label: { color, text },
        icon: markerIcon(scale)
      }
    : undefined;

interface IPolygon {
  type: 'Polygon';
  coordinates: number[][][];
}

interface IMultiPolygon {
  type: 'MultiPolygon';
  coordinates: number[][][];
}

type geometry = IPolygon | IMultiPolygon;

export interface IFeature {
  type: 'Feature';
  properties: any;
  geometry: geometry;
}

export interface IFeatureCollection {
  type: 'FeatureCollection';
  features: IFeature[];
}

export interface IGeometryCollection {
  type: 'GeometryCollection';
  geometries: geometry[];
}

export type feature = IFeature | IFeatureCollection | IGeometryCollection | IPolygon | IMultiPolygon;

export const defaultFeature = (): IFeature => ({
  type: 'Feature',
  properties: {},
  geometry: {
    type: 'Polygon',
    coordinates: []
  } as geometry
});

export const pointToCoordinates = (point: google.maps.LatLng) => [point.lng(), point.lat()];

export const coordinatesToPoint = (values: (number | number[])[]) => {
  try {
    return Array.isArray(values[0])
      ? values.map((v: any) => coordinatesToPoint(v))
      : (values as number[])?.some(isNaN)
        ? null
        : new google.maps.LatLng((values as number[])[1], (values as number[])[0]);
  } catch (err) {
    return null;
  }
};

export const polygonToCoordinates = (polygon: google.maps.Polygon) =>
  polygon
    .getPaths()
    .getArray()
    .map(path => {
      const points = path.getArray();
      return [...points.map(pointToCoordinates), pointToCoordinates(points[0])];
    })[0];

export interface IPolygonMap {
  paths: google.maps.MVCArray<google.maps.MVCArray<google.maps.LatLng>>;
  options: google.maps.PolygonOptions;
  id?: string;
}

export const polygonToMap = (polygon: google.maps.Polygon, color = strokeColor): IPolygonMap => ({
  paths: polygon.getPaths(),
  options: {
    ...strokeStyle,
    strokeColor: color
  }
});

const coordinatesToPolygon = (coordinates: number[][], color = strokeColor) =>
  new google.maps.Polygon({
    paths: coordinates.map(coordinatesToPoint).filter(Boolean),
    ...strokeStyle,
    strokeColor: color
  });

const getCoordinates = (feat: feature) =>
  feat.type === 'Polygon' || feat.type === 'MultiPolygon'
    ? feat.coordinates
    : feat.type === 'Feature'
      ? feat.geometry?.coordinates
      : [];

export const addPolygonToFeature = (feat: feature, polygon: google.maps.Polygon) =>
  feat.type === 'FeatureCollection'
    ? feat.features.push(addPolygonToFeature(defaultFeature(), polygon))
    : feat.type === 'GeometryCollection'
      ? feat.geometries.map(geometry => (geometry?.coordinates || []).push(polygonToCoordinates(polygon)))
      : (getCoordinates(feat) || []).push(polygonToCoordinates(polygon));

export const polygonsFromFeature = (feat: feature, color?: string): google.maps.Polygon[] =>
  feat.type === 'FeatureCollection'
    ? feat.features.flatMap(values => polygonsFromFeature(values, color))
    : feat.type === 'GeometryCollection'
      ? feat.geometries?.flatMap(geometry =>
          (geometry.coordinates || []).map(values => coordinatesToPolygon(values, color))
        )
      : (getCoordinates(feat) || []).map(values => coordinatesToPolygon(values, color));

export const polygonBounds = (polygon?: google.maps.Polygon | google.maps.Polygon[]) => {
  const paths = polygon
    ? Array.isArray(polygon)
      ? polygon.flatMap(p => p.getPath()?.getArray() || [])
      : polygon.getPath()?.getArray() || []
    : [];
  return paths.reduce((prev, curr) => prev.extend(curr), new google.maps.LatLngBounds());
};
