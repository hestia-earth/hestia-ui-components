import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bibliography } from '@hestia-earth/schema';

import { filterParams, handleAPIError } from '../common/utils';
import { HeCommonService } from '../common/common.service';

interface IMendeleySearchParams {
  title?: string;
  limit?: number;
  doi?: string;
  scopus?: string;
}

export class MendeleySearchResult {
  count = 0;
  results: Partial<Bibliography>[] = [];
}

@Injectable({
  providedIn: 'root'
})
export class HeMendeleyService {
  protected http = inject(HttpClient);
  protected commonService = inject(HeCommonService);

  private get path() {
    return `${this.commonService.apiBaseUrl}/mendeley`;
  }

  find(params: IMendeleySearchParams) {
    return this.http.get<MendeleySearchResult>(this.path, { params: filterParams(params) });
  }

  get(id: string) {
    return this.http.get<Bibliography>(`${this.path}/${id}`).toPromise().catch(handleAPIError);
  }
}
