import { readFixture } from '../test-utils';

import { nodeVersion } from './node-model-version';

describe('node > node-models-version', () => {
  describe('nodeVersion', () => {
    let node: any;

    describe('original version', () => {
      beforeEach(async () => {
        node = await readFixture('cycle/fsks-tk3n7xj/original.json');
      });

      it('should return null', () => {
        expect(nodeVersion(node)).toBeNull();
      });
    });

    describe('recalculated version', () => {
      beforeEach(async () => {
        node = await readFixture('cycle/fsks-tk3n7xj/recalculated.json');
      });

      it('should return the version', () => {
        expect(nodeVersion(node)).not.toBeNull();
      });
    });

    describe('completeness', () => {
      beforeEach(() => {
        node = {
          electricityFuel: true,
          material: true,
          fertiliser: true,
          soilAmendments: true,
          pesticidesAntibiotics: true,
          water: true,
          animalFeed: true,
          other: true,
          products: true,
          cropResidue: true,
          excretaManagement: true,
          '@type': 'Completeness',
          updated: ['material', 'other', 'cropResidue'],
          updatedVersion: ['0.32.2', '0.32.2', '0.32.2'],
          added: [],
          addedVersion: []
        };
      });

      it('should return the version', () => {
        expect(nodeVersion(node)).toEqual('0.32.2');
      });
    });

    describe('with null values', () => {
      beforeEach(async () => {
        node = await readFixture('impactAssessment/jxgaya5qa10-/recalculated.json');
      });

      it('should return the version', () => {
        expect(nodeVersion(node)).not.toBeNull();
      });
    });
  });
});
