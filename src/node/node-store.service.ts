import { Injectable, inject } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { JSONLD, NodeType } from '@hestia-earth/schema';
import { DataState } from '@hestia-earth/api';
import { isEmpty } from '@hestia-earth/utils';
import { catchError, map, mergeMap, shareReplay, toArray } from 'rxjs/operators';
import { Observable, ReplaySubject, forkJoin, of } from 'rxjs';
import orderBy from 'lodash.orderby';

import { HeNodeService } from './node.service';

export interface IStoredNode {
  index: number;
  type: NodeType;
  id: string;
}

type nodeStates = {
  [dataState in DataState]?: JSONLD<NodeType>;
};

export type nodes = {
  [nodeType in NodeType]?: (nodeStates & IStoredNode)[];
};

type nodeLoadRequest = {
  [dataState in DataState]: Observable<JSONLD<NodeType>>;
};

type nodesLoadRequest = {
  [nodeType in NodeType]?: {
    id: string;
    request: Observable<nodeStates & IStoredNode>;
  }[];
};

const defaultDataStates = [DataState.original, DataState.recalculated];

export const nodeTypeDataState: {
  [type in NodeType]?: {
    default: DataState[];
    aggregated?: DataState[];
  };
} = {
  [NodeType.Cycle]: {
    default: defaultDataStates,
    aggregated: [DataState.original]
  },
  [NodeType.ImpactAssessment]: {
    default: defaultDataStates,
    aggregated: [DataState.recalculated]
  },
  [NodeType.Site]: {
    default: defaultDataStates,
    aggregated: [DataState.original]
  }
};

const nodeTypeDataStates = (nodeType: NodeType, aggregated = false) => {
  const dataStatesMapping = nodeTypeDataState[nodeType] || {
    default: [DataState.original],
    aggregated: [DataState.original]
  };
  return aggregated ? dataStatesMapping.aggregated : dataStatesMapping.default;
};

const sortNodes = (values?: IStoredNode[]) =>
  values?.length
    ? orderBy(
        values.filter(Boolean),
        ['original.aggregated', 'original.site.@id', 'original.cycle.@id', 'original.@id'],
        ['asc', 'asc', 'asc', 'asc']
      )
    : [];

const isAggregated = (id = '') => !!id?.match(/^[a-zA-Z]+[-][a-zA-Z]+[-][\d]{4,}[-][\d]{4,}/g);

type dataTransformFunc = <T>(node: T) => T;
const pickHeaderKeys = ['stage', 'maxstage'];

export const mergeDataWithHeaders = <T extends JSONLD<NodeType>>(data: T, headers: HttpHeaders): T => ({
  ...data,
  ...Object.fromEntries(pickHeaderKeys.map(key => [key, headers?.get(key)]).filter(([_key, value]) => !isEmpty(value)))
});

@Injectable({
  providedIn: 'root'
})
export class HeNodeStoreService {
  private readonly nodeService = inject(HeNodeService);

  private _nodesLoadRequests: nodesLoadRequest = {};
  private _nodes: nodes = {};
  private readonly _nodes$ = new ReplaySubject<nodes>(1);

  private findNodeIndex<T extends Pick<JSONLD<NodeType>, '@type' | '@id'>>(node: T) {
    const nodes = this._nodes?.[node['@type']] || [];
    return nodes.findIndex(n => n.type === node['@type'] && n.id === node['@id']);
  }

  private loadNode<T extends Pick<JSONLD<NodeType>, '@type' | '@id'>>(
    node: T,
    params = {},
    dataTransform: dataTransformFunc = data => data
  ) {
    const aggregated = 'aggregated' in node ? (node.aggregated as boolean) : isAggregated(node['@id']);
    const dataStates = nodeTypeDataStates(node['@type'], aggregated);
    const index = this._nodesLoadRequests[node['@type']].length;

    const request = forkJoin(
      dataStates.reduce(
        (prev, dataState) => ({
          ...prev,
          [dataState]: this.nodeService.getWithHeaders$<JSONLD<NodeType>>({ ...node, dataState }, params).pipe(
            map(({ data, headers }) => mergeDataWithHeaders(dataTransform(data), headers)),
            catchError(() => of(undefined))
          )
        }),
        {} as nodeLoadRequest
      )
    ).pipe(
      shareReplay({ bufferSize: 1, refCount: true }),
      map(
        (values: nodeStates) =>
          Object.fromEntries(
            Object.values(DataState).map(dataState => [
              dataState,
              values[dataState] || values[DataState.original] || node
            ])
          ) as nodeStates
      ),
      map(
        values =>
          ({
            index,
            type: node['@type'],
            id: node['@id'],
            ...values
          }) as nodeStates & IStoredNode
      )
    );

    this._nodesLoadRequests[node['@type']].push({
      id: node['@id'],
      request
    });

    request.subscribe(data => {
      this._nodes[node['@type']] = this._nodes[node['@type']] || [];
      this._nodes[node['@type']][index] = data;
      this._nodes$.next(this._nodes);
    });

    return request;
  }

  /**
   * Fetch node data from the API, and add to the store.
   *
   * @param node
   * @param params Optional query parameters to send with the request to the API.
   * @param dataTransform Function to transform the received data.
   * @returns
   */
  public addNode<T extends Pick<JSONLD<NodeType>, '@type' | '@id'>>(
    node: T,
    params = {},
    dataTransform?: dataTransformFunc
  ) {
    this._nodesLoadRequests[node['@type']] = this._nodesLoadRequests[node['@type']] || [];
    this._nodes[node['@type']] = this._nodes[node['@type']] || [];

    const existingRequest = this._nodesLoadRequests[node['@type']].find(({ id }) => id === node['@id']);
    return existingRequest ? existingRequest.request : this.loadNode<T>(node, params, dataTransform);
  }

  public removeNodeByIndex(nodeType: NodeType, index: number) {
    const requests = this._nodesLoadRequests[nodeType].slice();
    requests.splice(index, 1);
    this._nodesLoadRequests[nodeType] = requests;
    const nodes = this._nodes[nodeType].slice();
    nodes.splice(index, 1);
    this._nodes[nodeType] = nodes;
    this._nodes$.next(this._nodes);
  }

  public removeNode<T extends JSONLD<NodeType>>(node: T) {
    const index = this.findNodeIndex(node);
    return index >= 0 ? this.removeNodeByIndex(node['@type'], index) : null;
  }

  public clearNodes() {
    this._nodesLoadRequests = {};
    this._nodes = {};
    this._nodes$.next(this._nodes);
  }

  public nodes$() {
    return this._nodes$.asObservable();
  }

  public sortedNodes$() {
    return this.nodes$().pipe(
      map(
        nodes =>
          Object.fromEntries(Object.entries(nodes).map(([nodeKey, values]) => [nodeKey, sortNodes(values)])) as nodes
      )
    );
  }

  public ids$(nodeType: NodeType) {
    return this.nodes$().pipe(
      mergeMap(values => values[nodeType]),
      map(({ id }) => id),
      toArray()
    );
  }

  public find$<T extends JSONLD<NodeType>>(
    nodeType: NodeType,
    id?: string
  ): Observable<{ [dataState in DataState]?: T }[]> {
    return this.nodes$().pipe(map(values => ((values[nodeType] as any[]) || []).filter(node => !id || node.id === id)));
  }

  public findByState$<T extends JSONLD<NodeType>>(nodeType: NodeType, dataState: DataState, id?: string) {
    return this.find$<T>(nodeType, id).pipe(map(values => values.map(v => v[dataState]).filter(Boolean)));
  }
}
