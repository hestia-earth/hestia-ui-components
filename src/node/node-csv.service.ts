import { Injectable, inject } from '@angular/core';
import { toJson } from '@hestia-earth/schema-convert';
import { definitions } from '@hestia-earth/json-schema';

import { HeSchemaService } from '../schema/schema.service';

@Injectable({
  providedIn: 'root'
})
export class HeNodeCsvService {
  protected schemaService = inject(HeSchemaService);

  /**
   * Convert CSV content into the HESTIA format.
   *
   * @param content CSV content as a string
   * @param schemas By default, will load the schemas from the latest version.
   * @returns List of JSON nodes matching HESTIA schema.
   */
  async csvToJson(content: string, schemas?: definitions) {
    return toJson(schemas || (await this.schemaService.schemas()), content);
  }
}
