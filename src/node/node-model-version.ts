import { isUndefined } from '@hestia-earth/utils';

import { desc } from '../common/semver-utils';

const isVersionKey = (key: string) => ['addedVersion', 'updatedVersion'].includes(key);
const isValidValue = (value: any) =>
  !isUndefined(value) &&
  typeof value === 'object' &&
  (Array.isArray(value) || value.addedVersion || value.updatedVersion);

/**
 * Get the most recent version of the models used to recalculate the node.
 *
 * @param node
 * @returns The latest version
 */
export const nodeVersion = (node: any) => {
  const versions: string[] = Object.entries(node || {})
    .filter(([key, value]: any) => isVersionKey(key) || isValidValue(value))
    .flatMap(([key, value]: any) =>
      isVersionKey(key)
        ? value
        : typeof value === 'object'
          ? Array.isArray(value)
            ? value.map(nodeVersion)
            : value.addedVersion || value.updatedVersion
          : value
    )
    .filter(Boolean);

  return versions?.length ? desc(versions)[0] : null;
};
