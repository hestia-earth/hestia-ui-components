import { ChangeDetectionStrategy, Component, computed, inject, input, signal } from '@angular/core';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { KeyValuePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { DataState, fileToExt } from '@hestia-earth/api';
import { JSONLD, NodeType } from '@hestia-earth/schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faDownload, faFilter } from '@fortawesome/free-solid-svg-icons';
import { faClone as farClone } from '@fortawesome/free-regular-svg-icons';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { isUndefined } from '@hestia-earth/utils';
import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

import { HeNodeService } from '../node.service';
import { Level, levels, parseLines, logToCsv } from '../../common/logs-utils';
import { ClipboardComponent } from '../../common/clipboard/clipboard.component';
import { distinctUntilChangedDeep } from '../../common/rxjs-utils';

@Component({
  selector: 'he-node-logs-file',
  templateUrl: './node-logs-file.component.html',
  styleUrls: ['./node-logs-file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FormsModule, NgbTooltip, FaIconComponent, ClipboardComponent, KeyValuePipe]
})
export class NodeLogsFileComponent {
  private readonly domSanitizer = inject(DomSanitizer);
  private readonly nodeService = inject(HeNodeService);

  protected readonly faFilter = faFilter;
  protected readonly faDownload = faDownload;
  protected readonly farClone = farClone;

  protected readonly node = input.required<JSONLD<NodeType>>();
  protected readonly dataState = input<DataState>();

  private readonly nodeLog$ = combineLatest([toObservable(this.node), toObservable(this.dataState)]).pipe(
    filter(values => values.every(v => !isUndefined(v))),
    distinctUntilChangedDeep(),
    tap(() => this.loading.set(true)),
    mergeMap(([node, dataState]) =>
      this.nodeService.getLog$({
        ...node,
        dataState
      })
    ),
    map(value => value || 'No logs.'),
    tap(() => this.loading.set(false))
  );
  protected readonly nodeLog = toSignal(this.nodeLog$);
  private readonly allLogs$ = this.nodeLog$.pipe(
    map(value => parseLines(value).filter(({ data }) => data.logger === 'hestia_earth.models'))
  );
  private readonly allLogs = toSignal(this.allLogs$);

  protected readonly Level = Level;
  protected readonly loading = signal(false);
  protected readonly showFilters = signal(false);
  protected readonly modelFilter = signal('');
  protected readonly termFilter = signal('');
  protected readonly selectedLevel = signal(Level.info);

  protected readonly fileToExt = fileToExt;

  protected readonly nodeLogLines = computed(() =>
    this.allLogs()
      ?.filter(({ level }) => levels.indexOf(level) >= levels.indexOf(this.selectedLevel()))
      ?.filter(
        ({ code }) =>
          (!this.modelFilter() || code.toLocaleLowerCase().includes(`model=${this.modelFilter().toLowerCase()}`)) &&
          (!this.termFilter() || code.toLocaleLowerCase().includes(`term=${this.termFilter().toLowerCase()}`))
      )
  );
  protected readonly csvContent = computed(() =>
    this.domSanitizer.bypassSecurityTrustResourceUrl(
      `data:text/html;charset=utf-8,${encodeURIComponent(logToCsv(this.allLogs()))}`
    )
  );
  protected readonly csvFilename = computed(() => fileToExt(this.node()?.['@id'] ?? '', 'csv'));
}
