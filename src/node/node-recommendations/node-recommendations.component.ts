import { Component, computed, inject, input, output, signal } from '@angular/core';
import { toObservable } from '@angular/core/rxjs-interop';
import { NodeType, ISiteJSONLD, SchemaType, blankNodesType } from '@hestia-earth/schema';
import { isUndefined } from '@hestia-earth/utils';
import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faPlusCircle, faSpinner } from '@fortawesome/free-solid-svg-icons';

import { HeEngineService, ICalculationsModelsParams } from '../../engine/engine.service';
import { filterParams } from '../../common/utils';
import { distinctUntilChangedDeep } from '../../common/rxjs-utils';
import { KeyToLabelPipe } from '../../common/key-to-label.pipe';

enum NodeField {
  inputs = 'inputs',
  measurements = 'measurements',
  practices = 'practices',
  products = 'products'
}

const filtersByType: {
  [type in NodeType]?: (node: any) => ICalculationsModelsParams;
} = {
  [NodeType.Cycle]: cycle => {
    const primaryProduct = (cycle.products || []).find(p => p.primary && !!p.term?.['@id']);
    return primaryProduct
      ? {
          productTermId: primaryProduct.term['@id']
        }
      : {};
  },
  [NodeType.Site]: ({ siteType }: ISiteJSONLD) => ({
    siteType
  })
};

const termTypesByField: {
  [property in NodeField]: SchemaType;
} = {
  [NodeField.inputs]: SchemaType.Input,
  [NodeField.measurements]: SchemaType.Measurement,
  [NodeField.practices]: SchemaType.Practice,
  [NodeField.products]: SchemaType.Product
};

const isValid = (node: any, nodeField: NodeField) =>
  Object.values(NodeField).includes(nodeField) && (node?.type || node?.['@type']) in filtersByType;

@Component({
  selector: 'he-node-recommendations',
  templateUrl: './node-recommendations.component.html',
  styleUrls: ['./node-recommendations.component.scss'],
  imports: [FaIconComponent, KeyToLabelPipe]
})
export class NodeRecommendationsComponent {
  private readonly engineService = inject(HeEngineService);

  protected readonly faSpinner = faSpinner;
  protected readonly faPlusCircle = faPlusCircle;

  protected readonly node = input.required<any>();
  protected readonly nodeField = input.required<NodeField>();
  protected readonly buttonClass = input<string>();

  protected readonly selectRecommendation = output<string>();

  protected readonly loading = signal(false);
  protected readonly visible = signal(false);
  protected readonly recommendations = signal<string[]>([]);

  private readonly type = computed(() => this.node()?.type || this.node()?.['@type']);
  private readonly termType = computed(() => termTypesByField[this.nodeField()]);
  private readonly validField = computed(() => isValid(this.node(), this.nodeField()));
  private readonly existingIds = computed(() =>
    (this.node()[this.nodeField()] || []).map((n: blankNodesType) => n.term?.['@id']).filter(Boolean)
  );
  private readonly filters = computed(() =>
    this.validField() ? (filterParams(filtersByType[this.type()](this.node())) as ICalculationsModelsParams) : undefined
  );
  private readonly filters$ = toObservable(this.filters);

  constructor() {
    this.filters$
      .pipe(
        filter(filters => !isUndefined(filters)),
        distinctUntilChangedDeep(),
        tap(() => this.loading.set(true)),
        mergeMap(filters => this.engineService.getTermIds$({ ...filters, termType: this.termType() })),
        map(ids => ids.filter(id => !this.existingIds().includes(id)).sort()),
        tap(() => this.loading.set(false))
      )
      .subscribe(ids => this.recommendations.set(ids));
  }

  protected add(id: string, index: number) {
    this.selectRecommendation.emit(id);
    const recommendations = this.recommendations().slice();
    recommendations.splice(index, 1);
    this.recommendations.set(recommendations);
  }
}
