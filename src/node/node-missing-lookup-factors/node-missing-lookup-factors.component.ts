import { ChangeDetectionStrategy, Component, inject, input, signal } from '@angular/core';
import { DataState } from '@hestia-earth/api';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { JSONLD, NodeType } from '@hestia-earth/schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { isUndefined } from '@hestia-earth/utils';

import { HeNodeService } from '../node.service';
import { distinctUntilChangedDeep } from '../../common/rxjs-utils';
import { filter, mergeMap, tap } from 'rxjs/operators';

@Component({
  selector: 'he-node-missing-lookup-factors',
  templateUrl: './node-missing-lookup-factors.component.html',
  styleUrls: ['./node-missing-lookup-factors.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FaIconComponent]
})
export class NodeMissingLookupFactorsComponent {
  private nodeService = inject(HeNodeService);

  protected readonly faSpinner = faSpinner;

  protected node = input.required<JSONLD<NodeType>>();
  private node$ = toObservable(this.node);

  protected loading = signal(true);
  protected logs = toSignal(
    this.node$.pipe(
      filter(v => !isUndefined(v)),
      distinctUntilChangedDeep(),
      tap(() => this.loading.set(true)),
      mergeMap(node =>
        this.nodeService.getMissingLookupsLog$({
          ...node,
          dataState: DataState.recalculated
        })
      ),
      tap(() => this.loading.set(false))
    )
  );
}
