import { ChangeDetectionStrategy, Component, computed, inject, model, output, signal } from '@angular/core';
import { RouterLink } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { fileToExt, SupportedExtensions } from '@hestia-earth/api';
import { JSONLD, JSON as HestiaJSON } from '@hestia-earth/schema';
import { toCsv } from '@hestia-earth/schema-convert';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { NodeCsvSelectHeadersComponent } from '../node-csv-select-headers/node-csv-select-headers.component';
import { NodeLinkComponent } from '../node-link/node-link.component';
import { DataTableComponent } from '../../common/data-table/data-table.component';

interface INodeIncluded {
  node: JSONLD<any>;
  included: boolean;
}

@Component({
  selector: 'he-node-csv-export-confirm',
  templateUrl: './node-csv-export-confirm.component.html',
  styleUrls: ['./node-csv-export-confirm.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [RouterLink, DataTableComponent, NodeLinkComponent, NodeCsvSelectHeadersComponent, FaIconComponent]
})
export class NodeCsvExportConfirmComponent {
  private readonly domSanitizer = inject(DomSanitizer);
  private readonly activeModal = inject(NgbActiveModal, { optional: true });

  protected readonly faDownload = faDownload;

  protected readonly headers = signal<string[]>([]);

  public readonly nodes = model<(JSONLD<any> | HestiaJSON<any>)[]>([]);
  public readonly filename = model<string>('download');
  public readonly headerKeys = model<string[]>([]);
  public readonly extension = model(SupportedExtensions.csv);
  public readonly isUpload = model(true);

  protected readonly closed = output();

  protected readonly showIncludeNodes = signal(false);
  protected readonly includedNodes = computed(() =>
    (this.nodes() ?? []).map(node => ({ node, included: true }) as INodeIncluded)
  );
  private readonly included = computed(() =>
    this.includedNodes()
      .filter(({ included }) => included)
      .map(({ node }) => node)
  );

  protected readonly csvData = computed(() => toCsv(this.nodes() || [], { includeExising: !this.isUpload() }));
  protected readonly csvContent = computed(() =>
    this.headers().length
      ? this.domSanitizer.bypassSecurityTrustResourceUrl(
          `data:text/html;charset=utf-8,${encodeURIComponent(
            toCsv(this.included(), { includeExising: !this.isUpload(), selectedHeaders: this.headers() })
          )}`
        )
      : undefined
  );

  protected readonly downloadFilename = computed(() => fileToExt(this.filename(), this.extension()));

  protected close() {
    this.closed.emit();
    this.activeModal?.close();
  }
}
