import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { NodeType } from '@hestia-earth/schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { SvgIconComponent } from 'angular-svg-icon';

const nodeTypeToIcon: {
  [type in NodeType]?: IconDefinition;
} = {
  [NodeType.Actor]: faUser
};

@Component({
  selector: 'he-node-icon',
  templateUrl: './node-icon.component.html',
  styleUrls: ['./node-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FaIconComponent, SvgIconComponent]
})
export class NodeIconComponent {
  protected readonly type = input.required<NodeType>();
  protected readonly size = input(24);

  protected readonly isFaIcon = computed(() => this.type() in nodeTypeToIcon);
  protected readonly icon = computed(() => nodeTypeToIcon[this.type()] || `node-${this.type()?.toLowerCase()}`);
}
