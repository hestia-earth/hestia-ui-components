import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { NgClass, NgTemplateOutlet } from '@angular/common';
import { JSONLD, NodeType } from '@hestia-earth/schema';
import { DataState } from '@hestia-earth/api';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

import { linkTypeEnabled } from '../../schema/schema.service';
import { baseUrl, isExternal } from '../../common/utils';

@Component({
  selector: 'he-node-link',
  templateUrl: './node-link.component.html',
  styleUrls: ['./node-link.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NgClass, NgTemplateOutlet, FaIconComponent]
})
export class NodeLinkComponent {
  protected readonly faExternalLinkAlt = faExternalLinkAlt;

  protected readonly node = input.required<Partial<JSONLD<NodeType>>>();
  protected readonly showExternalLink = input(false);
  protected readonly linkClass = input<string>();

  protected readonly id = computed(() => this.node()?.['@id']);
  protected readonly type = computed(() => this.node()?.['@type']);
  protected readonly aggregated = computed(() => (this.node() as any).aggregated as boolean);

  private readonly dataState = computed(() =>
    // aggregated ImpactAsessment only have a recalculated version
    this.aggregated() && this.type() === NodeType.ImpactAssessment ? DataState.recalculated : DataState.original
  );
  protected readonly url = computed(
    () =>
      `${[baseUrl(), this.node()?.['@type']?.toLowerCase(), this.id()]
        .filter(Boolean)
        .join('/')}${this.dataState() !== DataState.original ? `?dataState=${this.dataState()}` : ''}`
  );
  protected readonly showLink = computed(() => linkTypeEnabled(this.node()?.['@type']));
  protected readonly target = computed(() => (this.showExternalLink() || isExternal() ? '_blank' : '_self'));
}
