import { ChangeDetectionStrategy, Component, Signal, computed, effect, inject, input, signal } from '@angular/core';
import { KeyValuePipe, NgTemplateOutlet } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { toSignal, toObservable } from '@angular/core/rxjs-interop';
import { Observable, combineLatest, forkJoin, from, of } from 'rxjs';
import {
  distinct,
  filter,
  reduce,
  toArray,
  map,
  distinctUntilChanged,
  mergeMap,
  tap,
  mergeAll,
  startWith
} from 'rxjs/operators';
import {
  BlankNodesKey,
  CycleFunctionalUnit,
  EmissionMethodTier,
  ICycleJSONLD,
  ITermJSONLD,
  NodeType,
  TermTermType,
  blankNodesKey,
  emissionTermTermType,
  impactAssessmentTermTermType,
  inputTermTermType,
  measurementTermTermType,
  nonBlankNodesKey,
  productTermTermType
} from '@hestia-earth/schema';
import { allowedType, IOrchestratorConfig, ENGINE_VERSION } from '@hestia-earth/engine-models';
import { isUndefined, keyToLabel, unique } from '@hestia-earth/utils';
import { loadResourceKey } from '@hestia-earth/glossary';
import { DataState } from '@hestia-earth/api';
import { NgbTypeahead, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import {
  IconDefinition,
  faAngleDown,
  faAngleRight,
  faCheck,
  faCircle,
  faDotCircle,
  faExclamationTriangle,
  faExternalLinkAlt,
  faSpinner,
  faTimes
} from '@fortawesome/free-solid-svg-icons';
import { faClone as farClone, faCircle as farCircle } from '@fortawesome/free-regular-svg-icons';

import { schemaBaseUrl, isExternal, typeaheadFocus } from '../../common/utils';
import { distinctUntilChangedDeep } from '../../common/rxjs-utils';
import { minor } from '../../common/semver-utils';
import { TimesPipe } from '../../common/times.pipe';
import { PrecisionPipe } from '../../common/precision.pipe';
import { KeyToLabelPipe } from '../../common/key-to-label.pipe';
import { PluralizePipe } from '../../common/pluralize.pipe';
import { DefaultPipe } from '../../common/default.pipe';
import { CompoundPipe } from '../../common/compound.pipe';
import { IsArrayPipe } from '../../common/is-array.pipe';
import { BlankNodeValueDeltaComponent } from '../../common/blank-node-value-delta/blank-node-value-delta.component';
import { DataTableComponent } from '../../common/data-table/data-table.component';
import {
  blankNodesTypeValue,
  nonBlankNodesTypeValue,
  IBlankNodeLogSubValue,
  IBlankNodeLog,
  LogStatus,
  IConfigModel,
  groupLogsByModel,
  groupLogsByTerm,
  modelCount,
  computeTerms,
  INodeTermLogs,
  INodeLogs,
  groupdLogsByKey,
  INonBlankNodeLog,
  ILog,
  computeKeys
} from './node-logs-models.model';
import { IJSONNode, HeNodeService, nodeType, nodeId } from '../node.service';
import { nodeVersion } from '../node-model-version';
import { HeEngineService, IModelExtended, findOrchestratorModel } from '../../engine/engine.service';
import { matchExactQuery, matchId, matchTermType, matchType } from '../../search/search.model';
import { HeSearchService } from '../../search/search.service';
import { termTypeLabel } from '../../terms/terms.model';
import { NodeLinkComponent } from '../node-link/node-link.component';
import { NodeLogsModelsLogsComponent } from './node-logs-models-logs/node-logs-models-logs.component';
import { NodeLogsModelsLogsStatusComponent } from './node-logs-models-logs-status/node-logs-models-logs-status.component';

interface ITermsByIdMapping {
  [id: string]: ITermJSONLD;
}

const logIcon: {
  [status in LogStatus]?: IconDefinition;
} = {
  [LogStatus.success]: faCheck,
  [LogStatus.error]: faTimes,
  [LogStatus.skipHierarchy]: farCircle,
  [LogStatus.dataProvided]: faCircle,
  [LogStatus.notRequired]: faDotCircle
};

const logColor: {
  [status in LogStatus]?: string;
} = {
  [LogStatus.success]: 'success',
  [LogStatus.error]: 'danger',
  [LogStatus.skipHierarchy]: 'dark',
  [LogStatus.dataProvided]: 'dark',
  [LogStatus.notRequired]: 'grey'
};

const shouldShowBlankNode = (blankNodeLog: IBlankNodeLog) =>
  [
    blankNodeLog?.configModels?.length,
    blankNodeLog?.keys?.length,
    blankNodeLog?.subValues?.length,
    blankNodeLog?.original?.length,
    blankNodeLog?.recalculated?.length
  ].some(Boolean);

const methodIdLabel = (methodId: string, _model?: IModelExtended) =>
  (methodId
    ? {
        'cycle/transformation': 'Data From Transformation',
        impact_assessment: 'Data From Cycle'
      }[methodId]
    : '') || (methodId.includes('/') ? methodId.split('/')[0] : keyToLabel(methodId));

const getModelsAt = (log: IBlankNodeLogSubValue | IBlankNodeLog, index: number) =>
  ('modelsInSubValues' in log ? !log.modelsInSubValues || !log.isOpen : true) && log.configModels[index];

const isSystemBoundary =
  (termId?: string) =>
  ({ isRecalculated, isRequired }: Pick<ILog | IBlankNodeLogSubValue, 'isRecalculated' | 'isRequired'>) => {
    // only exists for some terms
    const inSystemBoundary = loadResourceKey('inHestiaDefaultSystemBoundary.json', termId);
    return !inSystemBoundary ? [isRecalculated, isRequired].some(Boolean) : `${inSystemBoundary}` === 'true';
  };

const isGenericTerm = (termId: string) => [termId.startsWith('excreta')].every(Boolean);

const extraTermIdsFromConfig = (config: IOrchestratorConfig, key: blankNodesKey, logs: INodeLogs | INodeTermLogs) =>
  unique([
    ...(config?.models || [])
      .flat()
      .filter(m => m.key === key)
      .map(m => m.value)
      .filter(Boolean)
      .filter(v => !isGenericTerm(v)),
    // match logs with a configuration
    ...Object.entries(logs || {})
      .filter(
        ([_id, value]) =>
          typeof value === 'object' &&
          Object.entries(value).some(([model, v]: [model: string, value: any]) =>
            findOrchestratorModel(config, { key, model, value: v.model_key })
          )
      )
      .map(([key]) => key)
  ]);

/**
 * Extend terms by finding all terms in those `termType`.
 */
const matchTermTypeFromKey: {
  [key in BlankNodesKey]?: TermTermType[];
} = {
  // Cycle
  [BlankNodesKey.emissions]: emissionTermTermType.term,
  // Site
  [BlankNodesKey.measurements]: measurementTermTermType.term,
  // ImpactAssessment
  // disabled as it will show terms in both `emissions` and `resourceUse` categories, see issue #273
  // emissionsResourceUse: impactAssessmentTermTermType.emissionsResourceUse.term,
  [BlankNodesKey.impacts]: impactAssessmentTermTermType.impacts.term,
  [BlankNodesKey.endpoints]: impactAssessmentTermTermType.endpoints.term
};

/**
 * Restrict terms found in the logs by these `termType`.
 */
const restrictTermTypeFromKey: {
  [key in BlankNodesKey]?: TermTermType[];
} = {
  [BlankNodesKey.inputs]: inputTermTermType.term,
  [BlankNodesKey.products]: productTermTermType.term,
  ...matchTermTypeFromKey
};

const filterBlankNode = (
  { term: { name, '@id': id }, subValues, ...log }: IBlankNodeLog,
  selectedTerm?: string,
  filterTermTypes?: TermTermType[],
  onlyRequired?: boolean
) =>
  selectedTerm
    ? name.toLowerCase().includes(selectedTerm.toLowerCase())
    : !filterTermTypes?.length || !onlyRequired || isSystemBoundary(id)(log) || subValues.some(isSystemBoundary(id));

const termSearch = (term = '') => term.replace(/[,\s]/g, '').toLowerCase();

const listTermId = (terms: ITermJSONLD[]) =>
  terms
    .map(({ name }) => name)
    .sort()
    .join(',');

@Component({
  selector: 'he-node-logs-models',
  templateUrl: './node-logs-models.component.html',
  styleUrls: ['./node-logs-models.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    KeyValuePipe,
    FaIconComponent,
    DataTableComponent,
    FormsModule,
    NgbTypeahead,
    NodeLinkComponent,
    BlankNodeValueDeltaComponent,
    NgTemplateOutlet,
    NgbPopover,
    CompoundPipe,
    DefaultPipe,
    PluralizePipe,
    KeyToLabelPipe,
    PrecisionPipe,
    TimesPipe,
    IsArrayPipe,
    NodeLogsModelsLogsComponent,
    NodeLogsModelsLogsStatusComponent
  ]
})
export class NodeLogsModelsComponent {
  private readonly nodeService = inject(HeNodeService);
  private readonly searchService = inject(HeSearchService);
  private readonly engineService = inject(HeEngineService);

  protected readonly faExternalLinkAlt = faExternalLinkAlt;
  protected readonly faExclamationTriangle = faExclamationTriangle;
  protected readonly faTimes = faTimes;
  protected readonly faAngleDown = faAngleDown;
  protected readonly faAngleRight = faAngleRight;
  protected readonly faSpinner = faSpinner;
  protected readonly farClone = farClone;

  protected readonly node = input.required<IJSONNode>();
  private readonly node$ = toObservable(this.node);
  private readonly data$ = this.node$.pipe(
    filter(node => !isUndefined(node)),
    distinctUntilChangedDeep(),
    tap(() => this.loading.set(true)),
    mergeMap(node =>
      forkJoin({
        logs: this.nodeService.getLog$({ ...node, dataState: DataState.recalculated }),
        config: this.engineService.ochestratorConfig$(nodeType(node) as allowedType, nodeId(node))
      })
    ),
    tap(() => this.loading.set(false))
  );

  protected readonly nodeKey = input.required<blankNodesKey | nonBlankNodesKey>();
  protected readonly originalValues = input([] as blankNodesTypeValue[] | nonBlankNodesTypeValue);
  protected readonly recalculatedValues = input([] as blankNodesTypeValue[] | nonBlankNodesTypeValue);
  protected readonly terms = input([] as ITermJSONLD[]);
  protected readonly filterTermTypes = input([] as TermTermType[]);
  protected readonly filterTermTypesLabel = input('');
  protected readonly logsKey = input('');
  protected readonly noDataMessage = input('');

  protected readonly schemaBaseUrl = schemaBaseUrl();
  protected readonly isExternal = isExternal();
  protected readonly CycleFunctionalUnit = CycleFunctionalUnit;
  protected readonly EmissionMethodTier = EmissionMethodTier;
  protected readonly LogStatus = LogStatus;
  protected readonly logIcon = logIcon;
  protected readonly logColor = logColor;
  protected readonly getModelsAt = getModelsAt;
  protected readonly isSystemBoundary = isSystemBoundary;
  protected readonly ENGINE_VERSION = ENGINE_VERSION;
  protected readonly loading = signal(true);
  protected readonly showLegend = signal(true);

  protected readonly onlyRequired = signal(true);

  protected readonly filteredType = computed(
    () => this.filterTermTypesLabel() || this.filterTermTypes()?.map(termTypeLabel).join(' & ')
  );
  protected readonly functionalUnit = computed(() => (this.node() as ICycleJSONLD)?.functionalUnit);

  protected readonly logsUrl = computed(() => this.nodeService.nodeLogsUrl(this.node()));
  protected readonly nodeType = computed(() => nodeType(this.node()));
  private readonly nodeId = computed(() => nodeId(this.node()));
  private readonly config = signal(undefined as IOrchestratorConfig);

  private readonly allLogs = signal('');
  private readonly groupedLogs = computed(() => (this.allLogs() ? groupLogsByModel(this.allLogs()) : {}));
  private readonly logs = computed(
    () => (this.logsKey() ? this.groupedLogs()?.[this.logsKey()] : this.groupedLogs()) || {}
  );

  private readonly allBlankNodes = signal([] as (IBlankNodeLog | INonBlankNodeLog)[]);
  protected readonly blankNodes = computed(() =>
    this.allBlankNodes().filter(
      log => !('term' in log) || filterBlankNode(log, this.term(), this.filterTermTypes(), this.onlyRequired())
    )
  );

  protected readonly isBlankNodes = computed(
    () => Array.isArray(this.originalValues()) || Array.isArray(this.recalculatedValues())
  );
  protected readonly methodModelsCount = computed(() =>
    this.allBlankNodes()?.length && 'term' in this.allBlankNodes()[0]
      ? modelCount(this.allBlankNodes() as IBlankNodeLog[])
      : 1
  );

  private readonly extraTermsFromLogs = toSignal(
    combineLatest([
      this.data$.pipe(map(({ config }) => config)),
      toObservable(this.nodeKey as Signal<blankNodesKey>),
      toObservable(this.logs)
    ]).pipe(
      filter(values => values.every(value => !isUndefined(value))),
      map(([config, nodeKey, logs]) => extraTermIdsFromConfig(config, nodeKey, logs)),
      mergeMap(values =>
        values.length > 0
          ? this.searchTerms({
              bool: {
                must: [matchType(NodeType.Term)],
                should: values.map(matchId),
                minimum_should_match: 1
              }
            })
          : of([] as ITermJSONLD[])
      ),
      map(terms =>
        this.nodeKey() in restrictTermTypeFromKey
          ? terms.filter(term => restrictTermTypeFromKey[this.nodeKey()].includes(term.termType))
          : terms
      ),
      startWith([] as ITermJSONLD[])
    )
  );
  private readonly extraTermsByTermTypes = toSignal(
    combineLatest([toObservable(this.nodeKey), toObservable(this.filterTermTypes)]).pipe(
      filter(values => values.every(value => !isUndefined(value))),
      map(([nodeKey, termTypes]) => [...(matchTermTypeFromKey[nodeKey] || []), ...(termTypes || [])]),
      mergeMap(values =>
        values.length > 0
          ? this.searchTerms({
              bool: {
                must: [matchType(NodeType.Term)],
                should: values.map(matchTermType),
                minimum_should_match: 1
              }
            })
          : of([] as ITermJSONLD[])
      ),
      startWith([] as ITermJSONLD[])
    )
  );
  private readonly extraTerms = computed(() => [...this.extraTermsFromLogs(), ...this.extraTermsByTermTypes()]);
  private readonly allTerms = computed(() =>
    computeTerms(
      this.originalValues() as blankNodesTypeValue[],
      this.recalculatedValues() as blankNodesTypeValue[],
      this.terms(),
      this.filterTermTypes(),
      this.extraTerms()
    )
  );

  private readonly methodsById = toSignal(
    this.searchTerms({
      bool: {
        must: [matchType(NodeType.Term), matchTermType(TermTermType.model)]
      }
    }).pipe(
      mergeAll(),
      filter(v => !!v),
      distinct(v => v['@id']),
      reduce((prev, curr) => ({ ...prev, [curr['@id']]: curr }), {} as ITermsByIdMapping)
    )
  );

  private readonly termsById = signal({} as ITermsByIdMapping);

  private readonly currentModelVersion = computed(() => nodeVersion(this.node()));
  protected readonly isLatestVersion = computed(() => minor(this.currentModelVersion()) === minor(ENGINE_VERSION));
  protected readonly showOutdatedLogs = signal(false);

  // filter list of results by a single Term
  protected readonly term = signal('');
  protected readonly enableFilterByTerm = computed(() => this.allTerms()?.length > 0);
  protected readonly suggestTerm = (text$: Observable<string>) =>
    text$.pipe(
      distinctUntilChanged(),
      map(v => this.suggestByTerm(v))
    );

  protected readonly typeaheadFocus = typeaheadFocus;

  constructor() {
    this.data$.subscribe(({ logs, config }) => {
      this.config.set(config);
      this.allLogs.set(logs);
    });

    effect(() => {
      of(this.isBlankNodes())
        .pipe(
          distinctUntilChanged(),
          mergeMap(isBlankNodes => (isBlankNodes ? this.loadBlankNodes() : this.loadNonBlankNodes()))
        )
        .subscribe(values => this.allBlankNodes.set(values));
    });
  }

  private loadBlankNodes() {
    return of(this.allTerms()).pipe(
      distinctUntilChanged((prev, curr) => listTermId(prev) !== listTermId(curr)),
      mergeAll(),
      map(
        groupLogsByTerm(
          this.node(),
          this.logs(),
          this.config(),
          this.originalValues() as blankNodesTypeValue[],
          this.recalculatedValues() as blankNodesTypeValue[],
          this.nodeKey() as blankNodesKey
        )
      ),
      filter(shouldShowBlankNode),
      toArray(),
      mergeMap(values => this.fetchTermIds(values).pipe(map(() => values)))
    );
  }

  private loadNonBlankNodes() {
    const mapFunc = groupdLogsByKey(
      this.node(),
      this.nodeKey() as nonBlankNodesKey,
      this.logs(),
      this.originalValues() as nonBlankNodesTypeValue,
      this.recalculatedValues() as nonBlankNodesTypeValue
    );
    const keys = computeKeys(
      this.originalValues() as nonBlankNodesTypeValue,
      this.recalculatedValues() as nonBlankNodesTypeValue
    );
    return from(keys).pipe(map(mapFunc), toArray());
  }

  private fetchTermIds(values: IBlankNodeLog[]) {
    return of(values).pipe(
      mergeMap(v => v.flatMap(vv => vv.subValues)),
      map(v => v.id),
      distinct(),
      filter(v => !!v),
      toArray(),
      mergeMap((ids: string[]) =>
        ids.length
          ? this.searchTerms({
              bool: {
                must: [matchType(NodeType.Term)],
                should: ids.map(id => matchExactQuery('@id', id)),
                minimum_should_match: 1
              }
            })
          : of([])
      ),
      reduce((prev, curr) => ({ ...prev, [curr['@id']]: curr }), {} as ITermsByIdMapping),
      tap(values => this.termsById.set(values))
    );
  }

  private suggestByTerm(term: string) {
    return this.allTerms()
      .map(({ name }) => name)
      .filter(v => termSearch(v).includes(termSearch(term)));
  }

  private searchTerms(query: any) {
    this.loading.set(true);
    return this.searchService
      .search$<ITermJSONLD, NodeType.Term>({
        fields: ['@type', '@id', 'name', 'units', 'termType'],
        limit: 10000,
        query
      })

      .pipe(
        tap(() => this.loading.set(false)),
        map(({ results }) => results)
      );
  }

  protected trackByBlankNode(_index: number, node: any) {
    return node.termId || node.key;
  }

  protected trackBySubValue({ id, key }: IBlankNodeLogSubValue) {
    return id + key;
  }

  protected methodName({ methodId, model }: IConfigModel) {
    return this.methodsById()?.[methodId]?.name || methodIdLabel(methodId, model);
  }

  protected termById(id: string) {
    return (
      this.termsById()?.[id] ||
      ({
        '@type': NodeType.Term,
        '@id': id,
        name: termTypeLabel(id)
      } as ITermJSONLD)
    );
  }
}
