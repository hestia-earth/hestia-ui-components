import { ICycleJSONLD, IImpactAssessmentJSONLD, ISiteJSONLD, NodeType } from '@hestia-earth/schema';
import { IOrchestratorConfig } from '@hestia-earth/engine-models';

import { readFixture } from '../../test-utils';
import {
  LogStatus,
  INodeTermLogs,
  IConfigModel,
  IBlankNodeLog,
  INodeLogs,
  groupLogsByModel,
  groupLogsByTerm,
  computeTerms,
  logValueArray,
  groupdLogsByKey,
  INonBlankNodeLog,
  computeKeys,
  blankNodesTypeValue,
  nonBlankNodesTypeValue
} from './node-logs-models.model';

const getLogByTermId = (nodeLogs: IBlankNodeLog[], value: string) => nodeLogs.find(({ termId }) => termId === value);

const getLogByKey = (nodeLogs: INonBlankNodeLog[], value: string) => nodeLogs.find(({ key }) => key === value);

describe('groupLogsByModel', () => {
  let logs: INodeLogs;

  describe('Cycle', () => {
    describe('_pog8zzxq1s4', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/_pog8zzxq1s4/run.txt'));
      });

      describe('#products', () => {
        it('should group wheatGrain logs', async () => {
          const { wheatGrain } = logs;

          expect(Object.keys(wheatGrain).sort()).toEqual([
            'currency',
            'economicValueShare',
            'isKey',
            'isProperty',
            'models',
            'nitrogenContent',
            'pooreNemecek2018',
            'price',
            'revenue',
            'rootingDepth'
          ]);
          expect(wheatGrain.rootingDepth.globalCropWaterModel2008.shouldRun).toEqual(true);
        });
      });
    });

    describe('qinfual6-ckv', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/qinfual6-ckv/run.txt'));
      });

      describe('#inputs', () => {
        describe('seed', () => {
          let seed: INodeTermLogs;

          beforeEach(() => {
            seed = logs.seed;
          });

          it('should have properties', () => {
            expect(seed.models).toEqual(['hestiaAggregatedData', 'cycle', 'faostat2018', 'ecoinventV3', 'hestia']);
            expect(seed.hestia.model_key).toEqual('seed_emissions');
            expect(seed.hestia.shouldRun).toEqual(true);
          });
        });
      });
    });

    describe('rlhgb4zo4iiz', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/rlhgb4zo4iiz/run.txt'));
      });

      describe('#products', () => {
        describe('feedMix', () => {
          let feedMix: INodeTermLogs;

          beforeEach(() => {
            feedMix = logs.feedMix;
          });

          it('should have many properties', () => {
            expect(Object.keys(feedMix).length).toEqual(28);
          });
        });
      });
    });

    describe('wl1tf799nvco', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/wl1tf799nvco/run.txt'));
      });

      describe('#transformations', () => {
        it('should handle excretaPigsKgN', async () => {
          const { excretaPigsKgN } = logs.uncoveredAnaerobicLagoon;

          expect(excretaPigsKgN.models).toEqual(['hestiaAggregatedData', 'ecoinventV3']);
        });
      });
    });

    describe('fsks-tk3n7xj', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/fsks-tk3n7xj/run.txt'));
      });

      describe('#emissions', () => {
        it('should not include Transformation emissions for ecoinventV3', () => {
          const { bromochlorodifluoromethaneToAirInputsProduction } = logs;

          expect(bromochlorodifluoromethaneToAirInputsProduction.models).toContain('ecoinventV3');
          expect(bromochlorodifluoromethaneToAirInputsProduction.ecoinventV3.input).not.toContain(
            'electricityGridMarketMix'
          );
        });
      });

      describe('#transformations', () => {
        describe('#emissions', () => {
          it('should only include Transformation emissions for ecoinventV3AndEmberClimate', async () => {
            const { bromochlorodifluoromethaneToAirInputsProduction } = logs.dryingMachineUnspecified;

            expect(bromochlorodifluoromethaneToAirInputsProduction.models).toContain('ecoinventV3AndEmberClimate');
            expect(bromochlorodifluoromethaneToAirInputsProduction.ecoinventV3AndEmberClimate.input).not.toEqual(
              'electricityGridMarketMix'
            );
          });

          it('should log emissions', () => {
            const { ch4ToAirExcreta } = logs.dryingMachineUnspecified;

            expect(ch4ToAirExcreta.models).toEqual(['ipcc2019']);
            expect(ch4ToAirExcreta.ipcc2019.shouldRunOrchestrator).toEqual(true);
            expect(ch4ToAirExcreta.ipcc2019.runRequired).toEqual(false);
          });
        });
      });
    });

    describe('cvfpxhvhclk_', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/cvfpxhvhclk_/run.txt'));
      });

      describe('#inputs', () => {
        it('should log "rootingDepth" property', () => {
          const { barleyGrainInHusk } = logs;

          expect(barleyGrainInHusk.rootingDepth.isProperty).toEqual(true);
          expect(barleyGrainInHusk.rootingDepth.models).toEqual(['cycle']);
          expect(barleyGrainInHusk.rootingDepth.cycle.shouldRun).toEqual(true);
        });

        it('should log "crudeProteinContent" property', async () => {
          const { concentrateFeedBlend } = logs;

          expect(concentrateFeedBlend.crudeProteinContent.models).toEqual(['cycle']);
          expect(concentrateFeedBlend.crudeProteinContent.cycle.shouldRun).toEqual(true);
        });
      });

      describe('#products', () => {
        it('should log "nitrogenContent" property', () => {
          const { concentrateFeedBlend } = logs;

          expect(concentrateFeedBlend.nitrogenContent.isProperty).toEqual(true);
          expect(concentrateFeedBlend.nitrogenContent.models).toEqual([
            'ipcc2019', // only debugging values
            'cycle'
          ]);
          expect(concentrateFeedBlend.nitrogenContent.cycle.shouldRun).toEqual(true);
        });

        it('should log "crudeProteinContent" property', async () => {
          const { concentrateFeedBlend } = logs;

          expect(concentrateFeedBlend.crudeProteinContent.models).toEqual(['cycle']);
          expect(concentrateFeedBlend.crudeProteinContent.cycle.shouldRun).toEqual(true);
        });
      });
    });

    describe('chvhq93smlaj', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/chvhq93smlaj/run.txt'));
      });

      describe('#completeness', () => {
        describe('cropResidue', () => {
          it('should run', () => {
            const { cropResidue } = logs;
            expect(cropResidue.completeness.shouldRun).toEqual(true);
          });
        });
      });
    });

    describe('_erhm6nrwvg8', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/_erhm6nrwvg8/run.txt'));
      });

      describe('#completeness', () => {
        describe('cropResidue', () => {
          it('should have logs', () => {
            const { cropResidue } = logs;
            expect(cropResidue.completeness.shouldRun).toEqual(false);
          });
        });
      });

      describe('#products', () => {
        describe('pig', () => {
          let product: INodeTermLogs;

          beforeAll(() => {
            product = logs.pig;
          });

          it('should have logs', () => {
            expect(product.models).toEqual(['cycle']);
            expect(product.cycle.shouldRun).toEqual(true);
          });
        });
      });
    });

    describe('cagxkemuyx7l', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/cagxkemuyx7l/run.txt'));
      });

      describe('#emissions', () => {
        describe('co2ToAirSoilOrganicCarbonStockChangeManagementChange', () => {
          let emission: INodeTermLogs;

          beforeAll(() => {
            emission = logs.co2ToAirSoilOrganicCarbonStockChangeManagementChange;
          });

          it('should have models', () => {
            expect(emission.models).toEqual(['ipcc2019', 'emissionNotRelevant']);
          });
        });
      });
    });

    describe('_ey3xfvoklbo', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/_ey3xfvoklbo/run.txt'));
      });

      describe('seed', () => {
        let seed: INodeTermLogs;

        beforeAll(() => {
          seed = logs.seed;
        });

        it('should have 2 models', () => {
          expect(seed.models).toEqual(['faostat2018', 'completeness']);
        });
      });
    });

    describe('0dmazf5pwy_5', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/0dmazf5pwy_5/run.txt'));
      });

      describe('#emissions', () => {
        describe('co2ToAirInputsProduction', () => {
          let emission: INodeTermLogs;

          beforeAll(() => {
            emission = logs.co2ToAirInputsProduction;
          });

          it('should have logs', () => {
            expect(emission.models.length).toEqual(4);

            expect(emission.models).toContain('ecoinventV3');
            expect(emission.ecoinventV3.input).toEqual(['fuelOil']);
            expect(emission.models).toContain('ecoinventV3AndEmberClimate');
            expect(emission.ecoinventV3AndEmberClimate.input).toEqual(['electricityGridMarketMix']);
            expect(emission.models).toContain('linkedImpactAssessment');
            expect(emission.linkedImpactAssessment.input).toEqual(['feedMix']);
          });
        });
      });
    });

    describe('rzjv-coivdxp', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('cycle/rzjv-coivdxp/run.txt'));
      });

      describe('#animals', () => {
        describe('dairyCattleCalfWeaned', () => {
          let animal: INodeTermLogs;

          beforeAll(() => {
            animal = logs.dairyCattleCalfWeaned;
          });

          describe('#properties', () => {
            describe('liveweightGain', () => {
              let property: INodeTermLogs;

              beforeAll(() => {
                property = animal.liveweightGain;
              });

              it('should have 1 model', () => {
                expect(property.models).toEqual(['ipcc2019']);
              });
            });
          });
        });
      });
    });
  });

  describe('ImpactAssessment', () => {
    describe('k4fhvsu9nixf', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('impactAssessment/k4fhvsu9nixf/run.txt'));
      });

      describe('freshwaterWithdrawalsDuringCycle', () => {
        const termId = 'freshwaterWithdrawalsDuringCycle';
        let termLogs: INodeTermLogs;

        beforeEach(() => {
          termLogs = logs[termId];
        });

        it('should run', () => {
          expect(termLogs.models).toEqual(['pooreNemecek2018']);
          expect(termLogs.pooreNemecek2018.shouldRun).toEqual(true);
          expect(termLogs.pooreNemecek2018.product).toEqual(['cerealGrain']);
        });
      });
    });

    describe('jxgaya5qa10-', () => {
      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture('impactAssessment/jxgaya5qa10-/run.txt'));
      });

      describe('ch4ToAirExcreta', () => {
        const termId = 'ch4ToAirExcreta';
        let termLogs: INodeTermLogs;

        beforeEach(() => {
          termLogs = logs[termId];
        });

        it('should have logs', () => {
          expect(termLogs.models).toEqual(['impact_assessment']);
          expect(termLogs.impact_assessment.requirements?.economicValueShare).toEqual('84.12797883699969');
        });
      });
    });
  });
});

describe('groupLogsByTerm', () => {
  let config: IOrchestratorConfig;
  let logs: INodeLogs;

  describe('Cycle', () => {
    const nodeType = NodeType.Cycle;
    let original: ICycleJSONLD;
    let recalculated: ICycleJSONLD;

    beforeAll(async () => {
      config = await readFixture('cycle/config.json');
    });

    describe('_ey3xfvoklbo', () => {
      const cycleId = '_ey3xfvoklbo';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#completeness', () => {
        let originalValues: nonBlankNodesTypeValue;
        let recalculatedValues: nonBlankNodesTypeValue;
        let nodeLogs: INonBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.completeness;
          recalculatedValues = recalculated.completeness;
          const keys = computeKeys(originalValues, recalculatedValues);
          nodeLogs = keys.map(groupdLogsByKey(recalculated, 'completeness', logs, originalValues, recalculatedValues));
        });

        describe('seed', () => {
          const key = 'seed';
          let completeness: INonBlankNodeLog;

          beforeAll(() => {
            completeness = getLogByKey(nodeLogs, key);
          });

          it('should have 1 model', () => {
            expect(completeness.configModels.length).toBe(1);
          });

          it('should recalculate', () => {
            const configModel = completeness.configModels[0] as IConfigModel;

            expect(completeness.originalValue).toEqual(false);
            expect(completeness.recalculatedValue).toEqual(true);
            expect(completeness.isRecalculated).toEqual(true);
            expect(configModel.status).toBe(LogStatus.success);
          });
        });
      });

      describe('#inputs', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.inputs;
          recalculatedValues = recalculated.inputs;
          const terms = [
            ...computeTerms(originalValues, recalculatedValues),
            {
              '@id': 'seed'
            }
          ];
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'inputs')
          );
        });

        describe('seed', () => {
          const termId = 'seed';
          let input: IBlankNodeLog;

          beforeAll(() => {
            input = getLogByTermId(nodeLogs, termId);
          });

          it('should have 1 model', () => {
            expect(input.configModels.length).toEqual(1);
          });
        });
      });
    });

    describe('qinfual6-ckv', () => {
      const cycleId = 'qinfual6-ckv';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#inputs', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.inputs;
          recalculatedValues = recalculated.inputs;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'inputs')
          );
        });

        describe('seed', () => {
          const termId = 'seed';
          let input: IBlankNodeLog;

          beforeAll(() => {
            input = getLogByTermId(nodeLogs, termId);
          });

          it('should map the hestia seed model as background data', () => {
            const model = input.subValues[0].configModels.find(m => m.methodId === 'hestia');
            expect(model.model?.model).toEqual('hestia');
            expect(model.model?.modelKey).toEqual('seed_emissions');
            expect(model.logs.shouldRun).toEqual(true);
            expect(model.status).toEqual(LogStatus.success);
          });
        });
      });
    });

    describe('fsks-tk3n7xj', () => {
      const cycleId = 'fsks-tk3n7xj';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#inputs', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.inputs;
          recalculatedValues = recalculated.inputs;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'inputs')
          );
        });

        describe('seed', () => {
          const termId = 'seed';
          let input: IBlankNodeLog;

          beforeAll(() => {
            input = getLogByTermId(nodeLogs, termId);
          });

          it('should have 1 model', () => {
            expect(input.configModels.length).toEqual(1);
          });
        });
      });

      describe('#emissions', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.emissions;
          recalculatedValues = recalculated.emissions;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'emissions')
          );
        });

        it('should handle ecoinventV3 inputs', () => {
          const emission = getLogByTermId(nodeLogs, 'bromochlorodifluoromethaneToAirInputsProduction');
          const limeInput = emission.subValues.find(({ id }) => id === 'lime');

          expect(limeInput.configModels[0].methodId).toEqual('ecoinventV3');
          expect(limeInput.configModels[0].logs.shouldRun).toEqual(true);
          expect(limeInput.originalValue).toEqual('');
          expect(limeInput.recalculatedValue).toEqual(2.5813996018710297e-8);
          expect(limeInput.isRecalculated).toEqual(true);
          expect(limeInput.isRequired).toEqual(true);
        });
      });

      describe('#transformations', () => {
        const termId = 'dryingMachineUnspecified';

        describe('#inputs', () => {
          let originalValues: blankNodesTypeValue[] = [];
          let recalculatedValues: blankNodesTypeValue[] = [];
          let nodeLogs: IBlankNodeLog[];

          beforeAll(async () => {
            originalValues = original.transformations[0].inputs;
            recalculatedValues = recalculated.transformations[0].inputs;
            const terms = computeTerms(originalValues, recalculatedValues);
            nodeLogs = terms.map(
              groupLogsByTerm(recalculated, logs[termId], config, originalValues, recalculatedValues, 'inputs')
            );
          });

          describe('sunflowerSeedWhole', () => {
            const inputId = 'sunflowerSeedWhole';
            let input: IBlankNodeLog;

            beforeAll(() => {
              input = getLogByTermId(nodeLogs, inputId);
            });

            it('should have 0 keys', () => {
              expect(input.keys.length).toEqual(0);
            });

            it('should not show "price" key', () => {
              const priceKey = input.keys.find(({ key }) => key === 'price');
              expect(priceKey).toBeUndefined();
            });

            it('should not show "impactAssessment" key', () => {
              const impactAssessmentKey = input.keys.find(({ key }) => key === 'impactAssessment');
              expect(impactAssessmentKey).toBeUndefined();
            });
          });
        });

        describe('#products', () => {
          let originalValues: blankNodesTypeValue[] = [];
          let recalculatedValues: blankNodesTypeValue[] = [];
          let nodeLogs: IBlankNodeLog[];

          beforeAll(async () => {
            originalValues = original.transformations[0].products;
            recalculatedValues = recalculated.transformations[0].products;
            const terms = computeTerms(originalValues, recalculatedValues);
            nodeLogs = terms.map(
              groupLogsByTerm(recalculated, logs[termId], config, originalValues, recalculatedValues, 'products')
            );
          });

          describe('sunflowerSeedWhole', () => {
            const productId = 'sunflowerSeedWhole';
            let product: IBlankNodeLog;

            beforeAll(() => {
              product = getLogByTermId(nodeLogs, productId);
            });

            it('should have 4 keys', () => {
              expect(product.keys.length).toEqual(4);
            });

            it('should have 30 subValues', () => {
              expect(product.subValues.length).toEqual(30);
            });

            it('should show price success on faotstat2018 model', () => {
              const priceKey = product.keys.find(({ key }) => key === 'price');
              expect(priceKey.isRecalculated).toEqual(true);
              expect(priceKey.configModels[0].status).toEqual(LogStatus.error);
              expect(priceKey.configModels[1].methodId).toEqual('faostat2018');
              expect(priceKey.configModels[1].status).toEqual(LogStatus.success);
            });

            it('should not show "impactAssessment" key', () => {
              const impactAssessmentKey = product.keys.find(({ key }) => key === 'impactAssessment');
              expect(impactAssessmentKey).toBeUndefined();
            });
          });
        });

        describe('#emissions', () => {
          let originalValues: blankNodesTypeValue[] = [];
          let recalculatedValues: blankNodesTypeValue[] = [];
          let nodeLogs: IBlankNodeLog[];

          beforeAll(async () => {
            originalValues = original.transformations[0].emissions;
            recalculatedValues = recalculated.transformations[0].emissions;
            const terms = [
              ...computeTerms(originalValues, recalculatedValues),
              {
                '@id': 'ch4ToAirExcreta'
              }
            ];
            nodeLogs = terms.map(
              groupLogsByTerm(recalculated, logs[termId], config, originalValues, recalculatedValues, 'emissions')
            );
          });

          it('should handle ecoinventV3AndEmberClimate inputs', () => {
            const emission = getLogByTermId(nodeLogs, 'bromochlorodifluoromethaneToAirInputsProduction');

            // include failed inputs
            expect(emission.subValues.length).toEqual(14);

            const electricityGridMarketMixInput = emission.subValues.find(
              ({ id }) => id === 'electricityGridMarketMix'
            );

            expect(electricityGridMarketMixInput.configModels[0].methodId).toEqual('ecoinventV3AndEmberClimate');
            expect(electricityGridMarketMixInput.configModels[0].logs.shouldRun).toEqual(true);
            expect(electricityGridMarketMixInput.originalValue).toEqual('');
            expect(electricityGridMarketMixInput.recalculatedValue).toEqual(1.0693688504492687e-7);
            expect(electricityGridMarketMixInput.isRecalculated).toEqual(true);
            expect(electricityGridMarketMixInput.isRequired).toEqual(true);
          });

          describe('ch4ToAirExcreta', () => {
            const emissionId = 'ch4ToAirExcreta';
            let emission: IBlankNodeLog;

            beforeAll(() => {
              emission = getLogByTermId(nodeLogs, emissionId);
            });

            it('should set runRequired', () => {
              expect(emission.isRequired).toEqual(false);
            });
          });
        });
      });
    });

    describe('cvfpxhvhclk_', () => {
      const cycleId = 'cvfpxhvhclk_';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#inputs', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.inputs;
          recalculatedValues = recalculated.inputs;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'inputs')
          );
        });

        describe('barleyGrainInHusk', () => {
          const termId = 'barleyGrainInHusk';
          let input: IBlankNodeLog;

          beforeAll(() => {
            input = getLogByTermId(nodeLogs, termId);
          });

          it('should have "rootingDepth" property', () => {
            const rootingDepth = input.subValues.find(({ id }) => id === 'rootingDepth');

            expect(rootingDepth.originalValue).toEqual('');
            expect(rootingDepth.recalculatedValue).toEqual(1);
            expect(rootingDepth.isRecalculated).toEqual(true);
            expect(rootingDepth.configModels.length).toEqual(2);
            expect(rootingDepth.configModels[0].status).toEqual(LogStatus.success);
          });
        });

        describe('tallow', () => {
          const termId = 'tallow';
          let input: IBlankNodeLog;

          beforeAll(() => {
            input = getLogByTermId(nodeLogs, termId);
          });

          it('should have "impactAssessment" key', () => {
            const impactAssessmentKey = input.keys.find(({ key }) => key === 'impactAssessment');

            expect(impactAssessmentKey.originalValue).toEqual('');
            expect(impactAssessmentKey.recalculatedValue).toEqual(null);
            expect(impactAssessmentKey.isRecalculated).toEqual(false);
            expect(impactAssessmentKey.configModels.length).toEqual(1);
            expect(impactAssessmentKey.configModels[0].status).toEqual(LogStatus.error);
          });
        });
      });

      describe('#products', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.products;
          recalculatedValues = recalculated.products;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'products')
          );
        });

        describe('concentrateFeedBlend', () => {
          const termId = 'concentrateFeedBlend';
          let product: IBlankNodeLog;

          beforeAll(() => {
            product = getLogByTermId(nodeLogs, termId);
          });

          it('should have 4 keys', () => {
            expect(product.keys.length).toEqual(4);
          });

          it('should have "nitrogenContent" property', () => {
            const nitrogenContent = product.subValues.find(({ id }) => id === 'nitrogenContent');

            expect(nitrogenContent.isRecalculated).toEqual(true);
            expect(nitrogenContent.configModels.length).toEqual(2);
            expect(nitrogenContent.configModels[0].status).toEqual(LogStatus.success);
          });

          it('should have "crudeProteinContent" property', () => {
            const crudeProteinContent = product.subValues.find(({ id }) => id === 'crudeProteinContent');

            expect(crudeProteinContent.isRecalculated).toEqual(true);
            expect(crudeProteinContent.configModels.length).toEqual(1);
            expect(crudeProteinContent.configModels[0].status).toEqual(LogStatus.success);
          });
        });
      });
    });

    describe('chvhq93smlaj', () => {
      const cycleId = 'chvhq93smlaj';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#completeness', () => {
        let originalValues: nonBlankNodesTypeValue;
        let recalculatedValues: nonBlankNodesTypeValue;
        let nodeLogs: INonBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.completeness;
          recalculatedValues = recalculated.completeness;
          const keys = computeKeys(originalValues, recalculatedValues);
          nodeLogs = keys.map(groupdLogsByKey(recalculated, 'completeness', logs, originalValues, recalculatedValues));
        });

        describe('cropResidue', () => {
          const key = 'cropResidue';
          let completeness: INonBlankNodeLog;

          beforeAll(() => {
            completeness = getLogByKey(nodeLogs, key);
          });

          it('should recalculate', () => {
            expect(completeness.originalValue).toEqual(false);
            expect(completeness.recalculatedValue).toEqual(true);
            expect(completeness.isRecalculated).toEqual(true);
          });

          it('should run successfully', () => {
            expect(completeness.configModels.length).toEqual(1);
            const model = completeness.configModels[0] as IConfigModel;
            expect(model.status).toEqual(LogStatus.success);
          });
        });

        describe('waste', () => {
          const key = 'waste';
          let completeness: INonBlankNodeLog;

          beforeAll(() => {
            completeness = getLogByKey(nodeLogs, key);
          });

          it('should not recalculate', () => {
            expect(completeness.originalValue).toEqual(true);
            expect(completeness.recalculatedValue).toEqual(true);
            expect(completeness.isRecalculated).toEqual(false);
          });
        });
      });
    });

    describe('rlhgb4zo4iiz', () => {
      const cycleId = 'rlhgb4zo4iiz';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#products', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.products;
          recalculatedValues = recalculated.products;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'products')
          );
        });

        describe('feedMix', () => {
          const termId = 'feedMix';
          let product: IBlankNodeLog;

          beforeAll(() => {
            product = getLogByTermId(nodeLogs, termId);
          });

          it('should have subValues', () => {
            expect(product.subValues.length).toEqual(20);
          });
        });
      });
    });

    describe('_erhm6nrwvg8', () => {
      const cycleId = '_erhm6nrwvg8';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#products', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.products;
          recalculatedValues = recalculated.products;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'products')
          );
        });

        describe('pig', () => {
          const termId = 'pig';
          let product: IBlankNodeLog;

          beforeAll(() => {
            product = getLogByTermId(nodeLogs, termId);
          });

          it('should have a model with docs', () => {
            expect(product.configModels.length).toEqual(1);
            expect((product.configModels[0] as any).model).toBeDefined();
          });
        });
      });

      describe('#emissions', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.emissions;
          recalculatedValues = recalculated.emissions;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'emissions')
          );
        });

        it('should not show models for ecoinventV3 grouped emissions', () => {
          const emission = getLogByTermId(nodeLogs, 'bromochlorodifluoromethaneToAirInputsProduction');

          expect(emission.configModels.length).toEqual(0);
        });
      });
    });

    describe('jeaojbdcmpxg', () => {
      const cycleId = 'jeaojbdcmpxg';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#products', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.products;
          recalculatedValues = recalculated.products;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'products')
          );
        });

        describe('pig', () => {
          const termId = 'pig';
          let product: IBlankNodeLog;

          beforeAll(() => {
            product = getLogByTermId(nodeLogs, termId);
          });

          it('should have logs for economicValueShare', () => {
            const economicValueShare = product.keys.find(({ key }) => key === 'economicValueShare');

            expect(economicValueShare.configModels.length).toEqual(1);
            expect(economicValueShare.configModels[0].status).toEqual(LogStatus.success);
          });
        });

        describe('excretaPigsKgVs', () => {
          const termId = 'excretaPigsKgVs';
          let product: IBlankNodeLog;

          beforeAll(() => {
            product = getLogByTermId(nodeLogs, termId);
          });

          it('should have a price', () => {
            const price = product.keys.find(({ key }) => key === 'price');

            expect(price.configModels.length).toEqual(1);
            expect(price.configModels[0].status).toEqual(LogStatus.success);
          });
        });
      });

      describe('#emissions', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.emissions;
          recalculatedValues = recalculated.emissions;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'emissions')
          );
        });

        describe('ch4ToAirExcreta', () => {
          const emissionId = 'ch4ToAirExcreta';
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, emissionId);
          });

          it('should not be required', () => {
            expect(emission.isRequired).toEqual(false);
          });

          it('should have models in subValue', () => {
            expect(emission.modelsInSubValues).toEqual(true);
          });

          it('should show 3 subValues', () => {
            expect(emission.subValues.length).toEqual(3);
          });

          it('should show the cycle as first subValue', () => {
            expect(emission.subValues[0].key).toEqual('cycle');
            expect(emission.subValues[0].isRequired).toEqual(false);
            expect(emission.subValues[0].isRecalculated).toEqual(true);
            expect(emission.subValues[0].originalValue).toBeUndefined();
            expect(emission.subValues[0].recalculatedValue).toEqual(0);
            expect(emission.subValues[0].configModels.length).toEqual(1);
          });

          it('should show the transformations as subValues', () => {
            const subValues = emission.subValues.filter(({ key }) => key === 'transformation');

            expect(subValues.length).toEqual(2);
            expect(subValues[0].isRequired).toEqual(true);
            expect(subValues[0].isRecalculated).toEqual(true);
            expect(subValues[0].originalValue).toEqual('');
            expect(subValues[0].recalculatedValue).toEqual(37.292535000000015);
            expect(subValues[0].configModels[0].status).toEqual(LogStatus.success);
            expect(subValues[0].configModels[0].model.modelKey).toEqual('transformation');
          });
        });

        describe('co2ToAirFuelCombustion', () => {
          const emissionId = 'co2ToAirFuelCombustion';
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, emissionId);
          });

          it('should be required', () => {
            expect(emission.isRequired).toEqual(true);
          });

          it('should show the units', () => {
            expect(emission.term.units).toEqual('kg CO2');
          });

          it('should be recalculated', () => {
            expect(emission.isRequired).toEqual(true);
            expect(emission.isRecalculated).toEqual(true);
            expect(emission.originalValue).toBeUndefined();
            expect(emission.recalculatedValue).toEqual(0.7862776000000001);
          });
        });

        describe('n2OToAirCropResidueDecompositionDirect', () => {
          const emissionId = 'n2OToAirCropResidueDecompositionDirect';
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, emissionId);
          });

          it('should not be required', () => {
            expect(emission.isRequired).toEqual(false);
          });
        });
      });

      describe('#transformations', () => {
        describe('pitStorageBelowAnimalConfinements', () => {
          const termId = 'pitStorageBelowAnimalConfinements';

          describe('#inputs', () => {
            let originalValues: blankNodesTypeValue[] = [];
            let recalculatedValues: blankNodesTypeValue[] = [];
            let nodeLogs: IBlankNodeLog[];

            beforeAll(async () => {
              originalValues = original.transformations[0].inputs;
              recalculatedValues = recalculated.transformations[0].inputs;
              const terms = computeTerms(originalValues, recalculatedValues);
              nodeLogs = terms.map(
                groupLogsByTerm(recalculated, logs[termId], config, originalValues, recalculatedValues, 'inputs')
              );
            });

            describe('excretaPigsKgMass', () => {
              const inputId = 'excretaPigsKgMass';
              let input: IBlankNodeLog;

              beforeAll(() => {
                input = getLogByTermId(nodeLogs, inputId);
              });

              it('should have 2 models', () => {
                expect(input.configModels.length).toEqual(2);
              });

              it('should be linked to the docs', () => {
                const configModels = input.configModels as IConfigModel[];

                expect(configModels[0].model.docPath).toBeDefined();
                expect(configModels[0].model.model).toEqual('transformation');
                expect(configModels[0].model.modelKey).toEqual('input.excreta');
              });

              it('should succeed on the 2nd model', () => {
                const configModels = input.configModels as IConfigModel[];

                expect(configModels[0].status).toEqual(LogStatus.success);
                expect(configModels[1].status).toEqual(LogStatus.skipHierarchy);
              });
            });

            describe('excretaPigsKgN', () => {
              const inputId = 'excretaPigsKgN';
              let input: IBlankNodeLog;

              beforeAll(() => {
                input = getLogByTermId(nodeLogs, inputId);
              });

              it('should have 0 model', () => {
                expect(input.configModels.length).toEqual(0);
              });
            });
          });

          describe('#products', () => {
            let originalValues: blankNodesTypeValue[] = [];
            let recalculatedValues: blankNodesTypeValue[] = [];
            let nodeLogs: IBlankNodeLog[];

            beforeAll(async () => {
              originalValues = original.transformations[0].products;
              recalculatedValues = recalculated.transformations[0].products;
              const terms = computeTerms(originalValues, recalculatedValues);
              nodeLogs = terms.map(
                groupLogsByTerm(recalculated, logs[termId], config, originalValues, recalculatedValues, 'products')
              );
            });

            describe('excretaPigsKgMass', () => {
              const productId = 'excretaPigsKgMass';
              let product: IBlankNodeLog;

              beforeAll(() => {
                product = getLogByTermId(nodeLogs, productId);
              });

              it('should have 2 models', () => {
                expect(product.configModels.length).toEqual(2);
              });

              it('should be linked to the docs', () => {
                const configModels = product.configModels as IConfigModel[];

                // TODO: fix this mapping between excreta terms and `excretaKgMass` model
                // expect(configModels[0].model.docPath).toBeDefined();
                // expect(configModels[0].model.model).toEqual('cycle');
                // expect(configModels[0].model.modelKey).toEqual('excretaKgMass');

                expect(configModels[1].model.docPath).toBeDefined();
                expect(configModels[1].model.model).toEqual('transformation');
                expect(configModels[1].model.modelKey).toEqual('product.excreta');
              });

              it('should succeed on the 2nd model', () => {
                const configModels = product.configModels as IConfigModel[];

                expect(configModels[0].status).toEqual(LogStatus.error);
                expect(configModels[1].status).toEqual(LogStatus.success);
              });
            });

            describe('excretaPigsKgN', () => {
              const productId = 'excretaPigsKgN';
              let product: IBlankNodeLog;

              beforeAll(() => {
                product = getLogByTermId(nodeLogs, productId);
              });

              it('should have 1 model', () => {
                expect(product.configModels.length).toEqual(1);
              });

              it('should succeed', () => {
                const configModels = product.configModels as IConfigModel[];

                expect(configModels[0].status).toEqual(LogStatus.success);
              });

              it('should update the value', () => {
                expect(product.isRecalculated).toEqual(true);
              });
            });
          });

          describe('#emissions', () => {
            let originalValues: blankNodesTypeValue[] = [];
            let recalculatedValues: blankNodesTypeValue[] = [];
            let nodeLogs: IBlankNodeLog[];

            beforeAll(async () => {
              originalValues = original.transformations[0].emissions;
              recalculatedValues = recalculated.transformations[0].emissions;
              const terms = [
                ...computeTerms(originalValues, recalculatedValues),
                {
                  '@id': 'ch4ToAirFloodedRice'
                },
                {
                  '@id': 'no3ToGroundwaterExcreta'
                }
              ];
              nodeLogs = terms.map(
                groupLogsByTerm(recalculated, logs[termId], config, originalValues, recalculatedValues, 'emissions')
              );
            });

            describe('ch4ToAirFloodedRice', () => {
              const emissionId = 'ch4ToAirFloodedRice';
              let emission: IBlankNodeLog;

              beforeEach(() => {
                emission = getLogByTermId(nodeLogs, emissionId);
              });

              it('should not be required', () => {
                expect(emission.isRequired).toEqual(false);
              });
            });

            describe('no3ToGroundwaterExcreta', () => {
              const emissionId = 'no3ToGroundwaterExcreta';
              let emission: IBlankNodeLog;

              beforeAll(() => {
                emission = getLogByTermId(nodeLogs, emissionId);
              });

              it('should be required', () => {
                expect(emission.isRequired).toEqual(true);
              });

              it('should have 2 models', () => {
                expect(emission.configModels.length).toEqual(2);
              });

              it('should succeed on the 1st model', () => {
                const configModels = emission.configModels as IConfigModel[];

                expect(configModels[0].status).toEqual(LogStatus.success);
              });
            });
          });
        });

        describe('uncoveredAnaerobicLagoon', () => {
          const termId = 'uncoveredAnaerobicLagoon';

          describe('#emissions', () => {
            let originalValues: blankNodesTypeValue[] = [];
            let recalculatedValues: blankNodesTypeValue[] = [];
            let nodeLogs: IBlankNodeLog[];

            beforeAll(async () => {
              originalValues = original.transformations[0].emissions;
              recalculatedValues = recalculated.transformations[0].emissions;
              const terms = [...computeTerms(originalValues, recalculatedValues)];
              nodeLogs = terms.map(
                groupLogsByTerm(recalculated, logs[termId], config, originalValues, recalculatedValues, 'emissions')
              );
            });

            describe('n2OToAirExcretaDirect', () => {
              const emissionId = 'n2OToAirExcretaDirect';
              let emission: IBlankNodeLog;

              beforeAll(() => {
                emission = getLogByTermId(nodeLogs, emissionId);
              });

              it('should be required', () => {
                expect(emission.isRequired).toEqual(true);
              });

              it('should have 1 models', () => {
                expect(emission.configModels.length).toEqual(1);
              });

              it('should fail', () => {
                const configModels = emission.configModels as IConfigModel[];

                expect(configModels[0].status).toEqual(LogStatus.error);
              });
            });
          });
        });
      });
    });

    describe('dvsqerxisjti', () => {
      const cycleId = 'dvsqerxisjti';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#products', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.products;
          recalculatedValues = recalculated.products;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'products')
          );
        });

        describe('belowGroundCropResidue', () => {
          const termId = 'belowGroundCropResidue';
          let input: IBlankNodeLog;

          beforeAll(() => {
            input = getLogByTermId(nodeLogs, termId);
          });

          it('should have "nitrogenContent" property', () => {
            const subValue = input.subValues.find(({ id }) => id === 'nitrogenContent');

            expect(subValue.configModels.length).toEqual(1);
            expect(subValue.configModels[0].methodId).toEqual('ipcc2019');
            expect(subValue.configModels[0].status).toEqual(LogStatus.success);
          });
        });

        describe('maizeGrain', () => {
          const termId = 'maizeGrain';
          let input: IBlankNodeLog;

          beforeAll(() => {
            input = getLogByTermId(nodeLogs, termId);
          });

          it('should have "rootingDepth" property', () => {
            const subValue = input.subValues.find(({ id }) => id === 'rootingDepth');

            expect(subValue.configModels.length).toEqual(1);
            expect(subValue.configModels[0].methodId).toEqual('globalCropWaterModel2008');
            expect(subValue.configModels[0].status).toEqual(LogStatus.success);
          });
        });
      });
    });

    describe('cagxkemuyx7l', () => {
      const cycleId = 'cagxkemuyx7l';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#emissions', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.emissions;
          recalculatedValues = recalculated.emissions;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'emissions')
          );
        });

        describe('co2ToAirSoilOrganicCarbonStockChangeManagementChange', () => {
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, 'co2ToAirSoilOrganicCarbonStockChangeManagementChange');
          });

          it('should be required', () => {
            expect(emission.isRequired).toEqual(true);
          });

          it('should show the units', () => {
            expect(emission.term.units).toEqual('kg CO2');
          });

          it('should have 1 model', () => {
            expect(emission.configModels.length).toEqual(1);
          });

          it('should be succeed', () => {
            const [configModel] = emission.configModels as IConfigModel[];

            expect(configModel.methodId).toEqual('ipcc2019');
            expect(configModel.status).toEqual(LogStatus.success);
          });
        });

        describe('ch4ToAirAquacultureSystems', () => {
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, 'ch4ToAirAquacultureSystems');
          });

          it('should be not be required', () => {
            expect(emission.isRequired).toEqual(false);
          });

          it('should show the units', () => {
            expect(emission.term.units).toEqual('kg CH4');
          });

          it('should have 1 model', () => {
            expect(emission.configModels.length).toEqual(1);
          });

          it('should be not relevant', () => {
            const [configModel] = emission.configModels as IConfigModel[];

            expect(configModel.methodId).toEqual('emissionNotRelevant');
            expect(configModel.status).toEqual(LogStatus.notRequired);
          });
        });

        describe('ch4ToAirCropResidueBurning', () => {
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, 'ch4ToAirCropResidueBurning');
          });

          it('should be required', () => {
            expect(emission.isRequired).toEqual(true);
          });

          it('should not merge result', () => {
            const [configModel] = emission.configModels as IConfigModel[];

            expect(configModel.status).toEqual(LogStatus.dataProvided);
            expect(configModel.logs.shouldMerge).toEqual(false);
            expect(configModel.logs.replaceThreshold).toEqual(true);
            expect(configModel.logs.replaceLowerTier).toEqual(false);
          });
        });

        describe('co2ToAirUreaHydrolysis', () => {
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, 'co2ToAirUreaHydrolysis');
          });

          it('should be required', () => {
            expect(emission.isRequired).toEqual(true);
          });

          it('should not merge result', () => {
            const [configModel] = emission.configModels as IConfigModel[];

            expect(configModel.status).toEqual(LogStatus.dataProvided);
            expect(configModel.logs.shouldMerge).toEqual(false);
            expect(configModel.logs.replaceThreshold).toEqual(false);
            expect(configModel.logs.replaceLowerTier).toEqual(true);
          });
        });

        describe('co2ToAirFuelCombustion', () => {
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, 'co2ToAirFuelCombustion');
          });

          it('should be required', () => {
            expect(emission.isRequired).toEqual(true);
          });

          it('should not merge result', () => {
            const [configModel] = emission.configModels as IConfigModel[];

            expect(configModel.status).toEqual(LogStatus.success);
            expect(configModel.logs.shouldMerge).toEqual(true);
            expect(configModel.logs.replaceThreshold).toEqual(true);
            expect(configModel.logs.replaceLowerTier).toEqual(true);
          });
        });
      });
    });

    describe('0dmazf5pwy_5', () => {
      const cycleId = '0dmazf5pwy_5';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#emissions', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.emissions;
          recalculatedValues = recalculated.emissions;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'emissions')
          );
        });

        describe('co2ToAirInputsProduction', () => {
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, 'co2ToAirInputsProduction');
          });

          it('should be required', () => {
            expect(emission.isRequired).toEqual(true);
          });

          it('should have 0 models', () => {
            expect(emission.configModels.length).toEqual(0);
          });

          it('should have multiple subValues', () => {
            // include failed inputs
            expect(emission.subValues.length).toEqual(6);

            const fuelOil = emission.subValues.find(({ id }) => id === 'fuelOil');
            expect(fuelOil.configModels[0].methodId).toEqual('ecoinventV3');
            expect(fuelOil.configModels[0].status).toEqual(LogStatus.error);
            expect(fuelOil.recalculatedValue).toEqual(6.406910849189828);

            const electricityGridMarketMix = emission.subValues.find(({ id }) => id === 'electricityGridMarketMix');
            // both ecoinvent models are currently running
            expect(electricityGridMarketMix.configModels[0].methodId).toEqual('ecoinventV3AndEmberClimate');
            expect(electricityGridMarketMix.configModels[0].status).toEqual(LogStatus.success);
            expect(electricityGridMarketMix.recalculatedValue).toEqual(652.6761168455789);

            const feedMix = emission.subValues.find(({ id }) => id === 'feedMix');
            expect(feedMix.configModels[0].methodId).toEqual('linkedImpactAssessment');
            expect(feedMix.configModels[0].status).toEqual(LogStatus.success);
            expect(feedMix.recalculatedValue).toEqual(0);
          });
        });
      });
    });

    describe('me2ydcwq3jfx', () => {
      const cycleId = 'me2ydcwq3jfx';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#emissions', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.emissions;
          recalculatedValues = recalculated.emissions;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'emissions')
          );
        });

        describe('ch4ToAirAquacultureSystems', () => {
          const emissionId = 'ch4ToAirAquacultureSystems';
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, emissionId);
          });

          it('should not be required', () => {
            expect(emission.isRequired).toEqual(false);
          });

          it('should be recalculated to 0', () => {
            expect(emission.isRecalculated).toEqual(true);
            expect(emission.recalculatedValue).toEqual(0);
          });

          it('should have not relevant model', () => {
            const configModels = emission.configModels as IConfigModel[];
            expect(configModels[0].methodId).toEqual('emissionNotRelevant');
          });
        });
      });
    });

    describe('wogyvr6be1qg', () => {
      const cycleId = 'wogyvr6be1qg';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#products', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.products;
          recalculatedValues = recalculated.products;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'products')
          );
        });

        describe('bananaFruit', () => {
          const termId = 'bananaFruit';
          let product: IBlankNodeLog;

          beforeAll(() => {
            product = getLogByTermId(nodeLogs, termId);
          });

          it('should have 5 keys', () => {
            expect(product.keys.length).toEqual(5);
          });

          it('should have "value" key', () => {
            const valueContent = product.keys.find(({ key }) => key === 'value');

            expect(valueContent.isRecalculated).toEqual(true);
            expect(valueContent.recalculatedValue).toEqual(44345);
            expect(valueContent.configModels.length).toEqual(1);
            expect(valueContent.configModels[0].status).toEqual(LogStatus.success);
          });
        });
      });
    });

    describe('8yfrgqp5afuk', () => {
      const cycleId = '8yfrgqp5afuk';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#emissions', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.emissions;
          recalculatedValues = recalculated.emissions;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'emissions')
          );
        });

        describe('co2ToAirInputsProduction', () => {
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, 'co2ToAirInputsProduction');
          });

          it('should be required', () => {
            expect(emission.isRequired).toEqual(true);
          });

          it('should sum the subValues with the same term', () => {
            const elecInput = emission.subValues.find(({ id }) => id === 'electricityGridMarketMix');
            expect(elecInput.recalculatedValue).toEqual(173.35354010499222);
          });
        });
      });
    });

    describe('ouootgxvtehk', () => {
      const cycleId = 'ouootgxvtehk';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`cycle/${cycleId}/run.txt`));
        original = await readFixture(`cycle/${cycleId}/original.json`);
        recalculated = await readFixture(`cycle/${cycleId}/recalculated.json`);
      });

      describe('#inputs', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.inputs;
          recalculatedValues = recalculated.inputs;
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'inputs')
          );
        });

        describe('seed', () => {
          let blankNode: IBlankNodeLog;

          beforeEach(() => {
            blankNode = getLogByTermId(nodeLogs, 'seed');
          });

          it('should be required', () => {
            expect(blankNode.isRequired).toEqual(true);
          });

          it('should have 1 subValue', () => {
            expect(blankNode.subValues.length).toEqual(1);
          });

          it('should have failed background data model', () => {
            const backgroundData = blankNode.subValues.find(({ key }) => key === 'backgroundData');
            expect(backgroundData.configModels[0].status).toEqual(LogStatus.error);
          });
        });
      });
    });
  });

  describe('ImpactAssessment', () => {
    const nodeType = NodeType.ImpactAssessment;
    let original: IImpactAssessmentJSONLD;
    let recalculated: IImpactAssessmentJSONLD;

    beforeAll(async () => {
      config = await readFixture('impactAssessment/config.json');
    });

    describe('jxgaya5qa10-', () => {
      const nodeId = 'jxgaya5qa10-';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`impactAssessment/${nodeId}/run.txt`));
        original = await readFixture(`impactAssessment/${nodeId}/original.json`);
        recalculated = await readFixture(`impactAssessment/${nodeId}/recalculated.json`);
      });

      describe('#emissionsResourceUse', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.emissionsResourceUse || [];
          recalculatedValues = recalculated.emissionsResourceUse || [];
          const terms = [
            ...computeTerms(originalValues, recalculatedValues),
            {
              '@id': 'ch4ToAirExcreta'
            }
          ];
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'emissionsResourceUse')
          );
        });

        describe('ch4ToAirExcreta', () => {
          let emission: IBlankNodeLog;

          beforeEach(() => {
            emission = getLogByTermId(nodeLogs, 'ch4ToAirExcreta');
          });

          it('should not be required', () => {
            expect(emission.isRequired).toEqual(true);
          });

          it('should have 1 model', () => {
            expect(emission.configModels.length).toEqual(1);
          });

          it('should have logs', () => {
            const model = emission.configModels[0] as IConfigModel;
            expect(model.showLogs).toEqual(true);
          });
        });
      });
    });

    describe('0svpe5krkfat', () => {
      const nodeId = '0svpe5krkfat';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`impactAssessment/${nodeId}/run.txt`));
        original = await readFixture(`impactAssessment/${nodeId}/original.json`);
        recalculated = await readFixture(`impactAssessment/${nodeId}/recalculated.json`);
      });

      describe('#impacts', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.impacts || [];
          recalculatedValues = recalculated.impacts || [];
          const terms = computeTerms(originalValues, recalculatedValues);
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'impacts')
          );
        });

        describe('gwp100', () => {
          let impact: IBlankNodeLog;

          beforeEach(() => {
            impact = getLogByTermId(nodeLogs, 'gwp100');
          });

          it('should be required', () => {
            expect(impact.isRequired).toEqual(true);
          });

          it('should be recalculated with a different method', () => {
            expect(impact.recalculatedValue).toEqual(2.7380109710780953);
          });
        });
      });
    });

    describe('cyk7_k6snnli', () => {
      const nodeId = 'cyk7_k6snnli';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`impactAssessment/${nodeId}/run.txt`));
        original = await readFixture(`impactAssessment/${nodeId}/original.json`);
        recalculated = await readFixture(`impactAssessment/${nodeId}/recalculated.json`);
      });

      describe('#impacts', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.impacts || [];
          recalculatedValues = recalculated.impacts || [];
          const terms = [
            ...computeTerms(originalValues, recalculatedValues),
            {
              '@id': 'fossilResourceScarcity'
            }
          ];
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'impacts')
          );
        });

        describe('freshwaterEcotoxicityPotentialPaf', () => {
          let impact: IBlankNodeLog;

          beforeEach(() => {
            impact = getLogByTermId(nodeLogs, 'freshwaterEcotoxicityPotentialPaf');
          });

          it('should be required', () => {
            expect(impact.isRequired).toEqual(true);
          });

          it('should have 1 model', () => {
            expect(impact.configModels.length).toEqual(1);
          });

          it('should not have models in subValue', () => {
            expect(impact.modelsInSubValues).not.toEqual(false);
          });

          it('should show 4 subValues', () => {
            expect(impact.subValues.length).toEqual(4);
          });

          it('should be successful', () => {
            const configModel = (impact.configModels as IConfigModel[])[0];

            expect(impact.isRequired).toEqual(true);
            expect(impact.isRecalculated).toEqual(true);
            expect(impact.originalValue).toBeUndefined();
            expect(impact.recalculatedValue).toEqual(1516.5966861326372);
            expect(configModel.status).toEqual(LogStatus.success);
          });
        });

        describe('fossilResourceScarcity', () => {
          let impact: IBlankNodeLog;

          beforeEach(() => {
            impact = getLogByTermId(nodeLogs, 'fossilResourceScarcity');
          });

          it('should have logs', () => {
            const model = impact.configModels[0][0] as IConfigModel;

            expect(model.showLogs).toEqual(true);
          });
        });
      });
    });
  });

  describe('Site', () => {
    const nodeType = NodeType.Site;
    let original: ISiteJSONLD;
    let recalculated: ISiteJSONLD;

    beforeAll(async () => {
      config = await readFixture('site/config.json');
    });

    describe('a9_o9xbcxler', () => {
      const nodeId = 'a9_o9xbcxler';

      beforeAll(async () => {
        logs = groupLogsByModel(await readFixture(`site/${nodeId}/run.txt`));
        original = await readFixture(`site/${nodeId}/original.json`);
        recalculated = await readFixture(`site/${nodeId}/recalculated.json`);
      });

      describe('measurements', () => {
        let originalValues: blankNodesTypeValue[] = [];
        let recalculatedValues: blankNodesTypeValue[] = [];
        let nodeLogs: IBlankNodeLog[];

        beforeAll(async () => {
          originalValues = original.measurements || [];
          recalculatedValues = recalculated.measurements || [];
          const terms = [
            ...computeTerms(originalValues, recalculatedValues),
            {
              '@id': 'potentialEvapotranspirationMonthly'
            },
            {
              '@id': 'precipitationLongTermAnnualMean'
            }
          ];
          nodeLogs = terms.map(
            groupLogsByTerm(recalculated, logs, config, originalValues, recalculatedValues, 'measurements')
          );
        });

        describe('potentialEvapotranspirationMonthly', () => {
          let measurement: IBlankNodeLog;

          beforeEach(() => {
            measurement = getLogByTermId(nodeLogs, 'potentialEvapotranspirationMonthly');
          });

          it('should be required', () => {
            expect(measurement.isRequired).toEqual(true);
          });

          it('should have 1 model', () => {
            expect(measurement.configModels.length).toEqual(2);
          });

          it('should fail', () => {
            const model = measurement.configModels[0] as IConfigModel;

            expect(measurement.originalValue).toEqual(undefined);
            expect(measurement.recalculatedValue).toEqual(undefined);
            expect(model.showLogs).toEqual(true);
            expect(model.methodId).toEqual('site');
            expect(model.status).toEqual(LogStatus.error);
          });
        });
      });
    });
  });
});

describe('logValueArray', () => {
  it('should return null for non-array', () => {
    const value = 'my value 10';

    expect(logValueArray(value)).toBeNull();
  });

  it('should handle simple values', () => {
    const value = 'excretaPigsKgVs;excretaPigsKgN';

    expect(logValueArray(value)).toEqual(['excretaPigsKgVs', 'excretaPigsKgN']);
  });

  it('should handle single table values', () => {
    const value = 'id:excretaPigsKgN_value:1.1199999999999999_EF:0.2725';

    expect(logValueArray(value)).toEqual([{ id: 'excretaPigsKgN', value: '1.1199999999999999', EF: '0.2725' }]);
  });

  it('should handle multiple table values', () => {
    const value = 'id:excretaPigsKgVs_value:13.3_b0:0.45;id:excretaPigsKgN_value:18.059359708424743_b0:0.45';

    expect(logValueArray(value)).toEqual([
      { id: 'excretaPigsKgVs', value: '13.3', b0: '0.45' },
      { id: 'excretaPigsKgN', value: '18.059359708424743', b0: '0.45' }
    ]);
  });
});
