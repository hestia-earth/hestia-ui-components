import { KeyValuePipe, NgTemplateOutlet } from '@angular/common';
import { Component, computed, input } from '@angular/core';
import { JSONLD, NodeType } from '@hestia-earth/schema';
import { faClone as farClone } from '@fortawesome/free-regular-svg-icons';
import { SvgIconComponent } from 'angular-svg-icon';

import { ClipboardComponent, KeyToLabelPipe, IsObjectPipe, SortByPipe } from '../../../common';
import { NodeLinkComponent } from '../../node-link/node-link.component';
import { INodeTermLog, logValueArray } from '../node-logs-models.model';

const requirementColor = (value: any) => (!value || ['None', 'False', '0', '0.0'].includes(value) ? 'danger' : 'white');

const nodeTypesLowerCase = Object.values(NodeType).map(v => v.toLowerCase());

const requirementKeys = (requirements: { [key: string]: any }) =>
  Object.keys(requirements).filter(k => !nodeTypesLowerCase.includes(k) && !['model_key'].includes(k));

const requirementLinkedNodeByKey: {
  [key: string]: (value: string) => Partial<JSONLD<NodeType>>;
} = {
  related_cycles: v => ({ '@type': NodeType.Cycle, '@id': v }),
  cycle_id: v => ({ '@type': NodeType.Cycle, '@id': v }),
  impact_assessment_id: v => ({ '@type': NodeType.ImpactAssessment, '@id': v }),
  site_id: v => ({ '@type': NodeType.Site, '@id': v }),
  source_id: v => ({ '@type': NodeType.Source, '@id': v }),
  property_id: v => ({ '@type': NodeType.Term, '@id': v }),
  input_id: v => ({ '@type': NodeType.Term, '@id': v }),
  product_id: v => ({ '@type': NodeType.Term, '@id': v }),
  practice_id: v => ({ '@type': NodeType.Term, '@id': v }),
  crop_product_id: v => ({ '@type': NodeType.Term, '@id': v })
};

const requirementLinkedNode = (key: string, value: any) =>
  key in requirementLinkedNodeByKey && !['None', '', null].includes(value)
    ? Array.isArray(value)
      ? value.map(v => requirementLinkedNode(key, v))
      : requirementLinkedNodeByKey[key](value)
    : null;

const completenessCompleteSuffix = '_complete';
const completenessIncompleteSuffix = '_incomplete';
const parseLogCompleteness = (key?: string) =>
  [
    key?.startsWith('term_type_'),
    key?.endsWith(completenessCompleteSuffix) || key?.endsWith(completenessIncompleteSuffix)
  ].every(Boolean)
    ? {
        key: key
          .replace('term_type_', '')
          .replace(completenessCompleteSuffix, '')
          .replace(completenessIncompleteSuffix, ''),
        value: key.endsWith(completenessCompleteSuffix)
      }
    : null;

interface ISorting {
  sortBy: string;
  sortOrder: 'asc' | 'desc';
}

@Component({
  selector: 'he-node-logs-models-logs',
  imports: [
    NgTemplateOutlet,
    KeyValuePipe,
    SvgIconComponent,
    NodeLinkComponent,
    ClipboardComponent,
    KeyToLabelPipe,
    IsObjectPipe,
    SortByPipe
  ],
  templateUrl: './node-logs-models-logs.component.html',
  styleUrl: './node-logs-models-logs.component.scss'
})
export class NodeLogsModelsLogsComponent {
  protected readonly farClone = farClone;

  protected readonly logs = input.required<INodeTermLog>();

  protected readonly logValueArray = logValueArray;
  protected readonly requirementLinkedNode = requirementLinkedNode;
  protected readonly requirementColor = requirementColor;
  protected readonly parseLogCompleteness = parseLogCompleteness;

  protected readonly requirements = computed(() => this.logs()?.requirements ?? {});
  protected readonly requirementKeys = computed(() => requirementKeys(this.requirements()));

  protected toggleSort(sortBy: string, sorting: ISorting) {
    const sortOrder = sorting.sortOrder === 'asc' ? 'desc' : 'asc';
    sorting.sortBy = sortBy;
    sorting.sortOrder = sortOrder;
  }
}
