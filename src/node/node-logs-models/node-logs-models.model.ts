/* eslint-disable complexity */
import { fileToExt, SupportedExtensions } from '@hestia-earth/api';
import {
  Emission,
  EmissionMethodTier,
  ITermJSONLD,
  Product,
  Input as HestiaInput,
  NodeType,
  TermTermType,
  SchemaType,
  isTypeNode,
  Measurement,
  nonBlankNodesType,
  blankNodesType,
  blankNodesKey,
  nonBlankNodesKey,
  Infrastructure,
  Bibliography,
  Transformation,
  ICycleJSONLD,
  Term,
  Property
} from '@hestia-earth/schema';
import { IOrchestratorConfig, IOrchestratorModelConfig } from '@hestia-earth/engine-models';
import { unique, isUndefined, isEmpty, isNumber } from '@hestia-earth/utils';
import { propertyValue, propertyValueType } from '@hestia-earth/utils/dist/term';
import get from 'lodash.get';
import set from 'lodash.set';
import orderBy from 'lodash.orderby';
import uniqBy from 'lodash.uniqby';

import {
  findModels,
  IModelExtended,
  findMatchingModel,
  modelParams,
  modelKeyParams
} from '../../engine/engine.service';
import { isValidKey } from '../../common/node-utils';
import { IJSONNode, nodeType as nodeNodeType } from '../node.service';

export interface INodeMissingLookupLog {
  filename: string;
  termId: string;
  column: string;
  // attributes linked to the models
  model?: string;
  term?: string;
  key?: string;
}

export interface INodeTermLog {
  runRequired?: boolean;
  shouldRun?: boolean;
  shouldRunOrchestrator?: boolean;
  shouldMerge?: boolean;
  replaceLowerTier?: boolean;
  replaceThreshold?: boolean;
  missingLookups?: INodeMissingLookupLog[];
  requirements?: { [key: string]: any };
  logs?: { [key: string]: any };
  [key: string]: any;
  // possible sub values
  input?: string | string[];
  property?: string | string[];
}

export interface INodeTermLogs {
  [modelKey: string]: any | INodeTermLog;
  models: string[];
  isKey?: boolean;
  isProperty?: boolean;
}

export interface INodeLogs {
  [term: string]: INodeTermLogs;
}

export type blankNodesTypeValue = Exclude<blankNodesType, Infrastructure | Transformation>;
export type nonBlankNodesTypeValue = Exclude<nonBlankNodesType, Bibliography>;

export const isNonNodeModelKey = (value: string) => ['completeness'].includes(value);

const termTypes = Object.values(TermTermType);

const blankNodeTypes = Object.values(SchemaType).filter(t => !isTypeNode(t));

const schemaTypesLowerCase = Object.values(SchemaType).map(v => v.toLowerCase());

const blankNodeTypesLowerCase = blankNodeTypes.map(v => v.toLowerCase());

const missingLookupPrefix = 'Missing lookup';

const noValue = 'None';

// add any other models that are set as "not relevant"
const notRelevantModels = ['emissionNotRelevant'];

const hasValue = (value?: string) => !!value && value !== noValue;

const parseFilename = (filepath: string) => {
  const [filename] = filepath.split('.');
  const ext = termTypes.includes(filename as any) ? SupportedExtensions.xlsx : SupportedExtensions.csv;
  return fileToExt(filename, ext);
};

const parseLookup = ({ column, termid, [missingLookupPrefix]: missingLookup }) => ({
  filename: parseFilename(missingLookup),
  termId: termid,
  column
});

const csvValue = (value: string) => (value || '').replace('[', '').replace(']', '');

const parseMessage = (message: string) => {
  try {
    const data = JSON.parse(message);
    return data.message.split(',').reduce(
      (prev, parts) => {
        const [key, value] = parts.split('=');
        const val = csvValue(value);
        return {
          ...prev,
          ...(key && val ? { [key.trim()]: val } : {})
        };
      },
      { logger: data.logger }
    );
  } catch (err) {
    // make sure it works even if one log is malformed
    return {};
  }
};

const parseLogMultipleValue = (values: string[][]) =>
  values
    .map(v => (v.filter(Boolean).length === 2 ? v.join(':') : null))
    .filter(Boolean)
    .join('_');

const parseLog = (log: any) =>
  ['value' in log, 'coefficient' in log, 'node' in log || 'operation' in log].every(Boolean)
    ? {
        [log.node || log.operation]: parseLogMultipleValue([
          ['value', log.value],
          ['coefficient', log.coefficient],
          ['operation', !log.node ? log.operation : null]
        ])
      }
    : omitLogKeys(log);

const omitLogKeys = (log: any, ...keys: string[]) =>
  Object.fromEntries(Object.entries(log).filter(([key]) => !schemaTypesLowerCase.includes(key) && !keys.includes(key)));

const includeBlankNodes = (logs, log) =>
  Object.keys(log)
    .filter(key => blankNodeTypesLowerCase.includes(key))
    .reduce(
      (prev, key) => ({
        ...prev,
        [key]: unique([...((typeof prev[key] === 'string' ? [prev[key]] : prev[key]) || []), log[key]])
      }),
      logs
    );

const groupLog = (
  group,
  { logger, term, model, key, should_run, should_merge, run_required, property, ...log }: any
) => {
  const isOrchestrator = logger.includes('orchestrator');
  const isKey = !isOrchestrator && !!key;
  const isProperty = !isOrchestrator && !!property;
  const parentLogKey = [hasValue(term) && `["${term}"]`, (!isOrchestrator && key) || property]
    .filter(Boolean)
    .join('.');
  const logModelKey = [parentLogKey, hasValue(model) && model].filter(Boolean).join('.');

  set(group, `${parentLogKey}.models`, unique([...get(group, `${parentLogKey}.models`, []), model]));
  set(group, `${parentLogKey}.isKey`, isKey);
  set(group, `${parentLogKey}.isProperty`, isProperty);

  let data = get(group, logModelKey, {});

  data = includeBlankNodes(data, log);

  if (typeof should_run !== 'undefined') {
    const shouldRun = should_run === 'True';

    data = isOrchestrator
      ? {
          ...data,
          ...omitLogKeys(log, 'value'),
          shouldRunOrchestrator: shouldRun
        }
      : {
          ...data,
          ...omitLogKeys(log),
          shouldRun
        };
  } else if (typeof run_required !== 'undefined') {
    const runRequired = run_required === 'True';

    data = {
      ...data,
      ...(runRequired ? {} : omitLogKeys(log)), // no need to save logs if we need to run the model
      runRequired
    };
  } else if ('requirements' in log) {
    const { requirements, ...logData } = log;

    data.requirements = {
      ...(data.requirements || {}),
      ...logData
    };
  } else if (missingLookupPrefix in log) {
    data.missingLookups = unique([...(data.missingLookups || []), parseLookup(log)]);
  } else if (!isOrchestrator) {
    data.logs = {
      ...(data.logs || {}),
      ...parseLog(log)
    };
  }
  if (typeof should_merge !== 'undefined') {
    data.shouldMerge = should_merge === 'True';
  }
  if (typeof log.replaceLowerTier !== 'undefined') {
    data.replaceLowerTier = log.replaceLowerTier === 'True';
  }
  if (typeof log.replaceThreshold !== 'undefined') {
    data.replaceThreshold = log.replaceThreshold === 'True';
  }

  set(group, logModelKey, data);
  return group;
};

const subValueKeys = ['transformation', 'animal'];

const groupLogSubValue = (group, log, key: string) => {
  const id = log[key];
  const data = groupLog(group[id] || {}, log);
  group[id] = data;

  if (subValueKeys.includes(key)) {
    // add value to the same term/model on parent group
    Object.entries(group[id]).map(([term, models]) =>
      Object.keys(models).map(model => {
        if (typeof group?.[term]?.[model] === 'object') {
          group[term][model][key] = unique([...(group[term][model][key] || []), id]);
        }
      })
    );
  }

  return group;
};

export const groupLogsByModel = (data = ''): INodeLogs =>
  data
    .trim()
    .split('\n')
    .map(parseMessage)
    .filter(v => [hasValue(v?.model), hasValue(v?.term) || hasValue(v?.key)].every(Boolean))
    .reduce((group, log) => {
      const subValue = subValueKeys.find(v => !!log[v]);
      return subValue && !log.cycle ? groupLogSubValue(group, log, subValue) : groupLog(group, log);
    }, {});

const asArray = (values: any[]) => (Array.isArray(values) ? values : []);

export const computeTerms = (
  originalValues: blankNodesTypeValue[],
  recalculatedValues: blankNodesTypeValue[],
  terms?: ITermJSONLD[],
  filterTermTypes?: TermTermType[],
  extraTerms: ITermJSONLD[] = []
): ITermJSONLD[] =>
  orderBy(
    uniqBy(
      [
        ...(terms?.length
          ? terms
          : ([
              ...asArray(originalValues).map(({ term }) => term),
              ...asArray(recalculatedValues).map(({ term }) => term)
            ] as ITermJSONLD[])),
        ...extraTerms
      ].filter(({ termType }) => !filterTermTypes?.length || filterTermTypes.includes(termType)),
      '@id'
    ),
    ['name'],
    ['asc']
  );

export const computeKeys = (originalValues: nonBlankNodesTypeValue, recalculatedValues: nonBlankNodesTypeValue) =>
  unique([...Object.keys(originalValues || {}), ...Object.keys(recalculatedValues || {})].filter(isValidKey));

export enum LogStatus {
  success = 'successful',
  error = 'failed',
  skipHierarchy = 'not run (model higher up hierarchy run instead)',
  dataProvided = 'not run (user provided data retained)',
  notRequired = 'not relevant'
}

const hasLogs = (logs?: INodeTermLog) => !!logs?.requirements || !!logs?.logs || logs?.missingLookups?.length > 0;

const hasLogDetails = (status: LogStatus, logs?: INodeTermLog) =>
  [LogStatus.success, LogStatus.error, LogStatus.dataProvided].includes(status) && hasLogs(logs);

const logStatus = (
  data: IBlankNodeLogSubValue | IBlankNodeLog | INonBlankNodeLog,
  logs?: INodeTermLog,
  hasPreviousSuccess = false
) => {
  const withLogs = hasLog(logs);
  const isCalculated = [
    typeof data.originalValue === 'undefined', // no original value
    data.isRecalculated // is recalculated
  ].every(Boolean);
  // can be not required for a specific model
  const isRequired = !!logs && 'runRequired' in logs ? logs.runRequired : data.isRequired;
  return withLogs
    ? !isRequired
      ? LogStatus.notRequired
      : isRunOrchestrator(logs)
        ? 'shouldRun' in logs
          ? logs.shouldRun
            ? data.isRecalculated
              ? LogStatus.success
              : isEmpty(data.originalValue)
                ? LogStatus.error
                : LogStatus.dataProvided
            : hasPreviousSuccess
              ? LogStatus.skipHierarchy
              : LogStatus.error
          : LogStatus.dataProvided
        : data.isRecalculated
          ? LogStatus.skipHierarchy
          : LogStatus.dataProvided
    : isCalculated
      ? LogStatus.skipHierarchy
      : LogStatus.dataProvided;
};

export interface IConfigModel {
  methodId: string;
  logs?: INodeTermLog;
  showLogs?: boolean;
  status?: LogStatus;
  model?: IModelExtended;
  config?: IOrchestratorModelConfig;
}

interface IPartialSubValue {
  blankNode?: boolean;
  id?: string;
  term?: Term | ITermJSONLD;
  key: string;
  showUnits: boolean;
  modelKey?: string;
  configModels: string[];
  originalValue?: number | string | boolean;
  recalculatedValue?: number | string | boolean;
}

export interface IBlankNodeLogSubValue {
  blankNode: true;
  id?: string;
  key: string;
  showUnits: boolean;
  originalValue: propertyValueType;
  recalculatedValue: propertyValueType;
  isRecalculated: boolean;
  isRequired?: boolean;
  configModels?: IConfigModel[];
  logs?: INodeTermLogs;
}

export interface ILog {
  logs?: INodeTermLogs;
  isOpen: boolean;
  canOpen: boolean;
  type?: SchemaType;
  configModels: (IConfigModel | IConfigModel[])[];
  originalValue: propertyValueType;
  recalculatedValue: propertyValueType;
  isOriginal: boolean;
  isRecalculated: boolean;
  hasData: boolean;
  isRequired: boolean;
  // contains multiple models all running in parallel
  allParallel?: boolean;
}

export interface IBlankNodeLog extends ILog {
  blankNode: true;
  term: ITermJSONLD;
  termId: string;
  keys: IBlankNodeLogSubValue[];
  subValues: IBlankNodeLogSubValue[];
  // models are present as well in one of the SubValues, do not show twice
  modelsInSubValues?: boolean;
  original: blankNodesTypeValue[];
  // when multiple models are running in parallel
  originalValueByMethodId?: {
    [methodId: string]: propertyValueType;
  };
  recalculated: blankNodesTypeValue[];
  // when multiple models are running in parallel
  recalculatedValueByMethodId?: {
    [methodId: string]: propertyValueType;
  };
}

export interface INonBlankNodeLog extends ILog {
  blankNode: false;
  key: string;
}

const removeConfigModelByStatus =
  (...statuses: LogStatus[]) =>
  (model: IConfigModel) =>
    !statuses.includes(model?.status);

const keepConfigModelByStatus =
  (...statuses: LogStatus[]) =>
  (model: IConfigModel) =>
    statuses.includes(model?.status);

const filterConfigModels = (
  models: IConfigModel | (IConfigModel | IConfigModel[])[],
  filterFunc: (model: IConfigModel) => boolean
) =>
  (Array.isArray(models) ? models : [models])
    .filter(v => Array.isArray(v) || filterFunc(v))
    .map(v => (Array.isArray(v) ? v.filter(filterFunc) : v))
    .filter(v => (Array.isArray(v) ? v?.length : !!v));

const mergeSubValues = (values: IPartialSubValue[]) =>
  Object.values(
    values.reduce(
      (prev, { id, key, configModels, ...data }) => {
        const uniqueKey = [key, id].filter(Boolean).join('.');
        prev[uniqueKey] = {
          ...Object.assign({}, prev[uniqueKey], data),
          id,
          key,
          configModels: unique([...((prev[uniqueKey] || {}).configModels || []), ...(configModels || [])])
        };
        return prev;
      },
      {} as { [key: string]: IPartialSubValue }
    )
  );

const reduceValues = (values: blankNodesTypeValue[], termId: string) => {
  const propertyValues = values
    .map(({ value }) => propertyValue(value, termId))
    // propertyValue may return null if the value is null or undefined, therefore remove them from total
    .filter(v => v !== null);
  return propertyValues.length ? propertyValue(propertyValues, termId) : undefined;
};

const isRunOrchestrator = (log: INodeTermLog) => !('shouldRunOrchestrator' in log) || log.shouldRunOrchestrator;

const hasLog = (log: INodeTermLog, withOrchestrator = true) =>
  !!log && ('shouldRun' in log || 'runRequired' in log || (withOrchestrator && 'shouldRunOrchestrator' in log));

const isRecalculated = (key: string) => (value: blankNodesTypeValue | nonBlankNodesTypeValue) =>
  [...(value.added || []), ...(value.updated || [])].includes(key);

const hasRecalculatedKeys = (value: blankNodesTypeValue | nonBlankNodesTypeValue) =>
  [...(value.added || []), ...(value.updated || [])].length > 0;

const hasRecalculatedValue = (values: blankNodesTypeValue[], key = 'value') => values.some(isRecalculated(key));

const filterDeleted = (blankNodes: blankNodesTypeValue[]) =>
  (blankNodes || []).filter(value => !(value as any).deleted);

const blankNodeValueByKey: {
  [key: string]: (values: blankNodesTypeValue[], key?: string, id?: string) => number | string | boolean;
} = {
  property: (values: (Emission | Product | HestiaInput)[], _k, id) =>
    !!id && values?.length ? (values[0].properties || []).find(p => p.term['@id'] === id)?.value || '' : '',
  input: (values: Emission[], _k, id) =>
    !!id && (values || []).filter(v => v['@type'] === SchemaType.Emission).length
      ? reduceValues(
          values.filter(({ inputs }) => (inputs || []).some(input => input['@id'] === id)),
          id
        )
      : '',
  transformation: (values: Emission[], _k, id) =>
    values.length
      ? reduceValues(
          values.filter(({ transformation }) => transformation?.['@id'] === id),
          id
        )
      : '',
  default: (blankNodes, key) => {
    const value = blankNodes?.length ? blankNodes[0][key] : null;
    return ['string', 'number', 'boolean', 'undefined'].includes(typeof value)
      ? isUndefined(value)
        ? ''
        : value
      : Array.isArray(value)
        ? value.length > 1
          ? value.join(';')
          : value[0]
        : null;
  }
};

const blankNodeValue = (blankNodes: blankNodesTypeValue[], key: string, id?: string) =>
  blankNodeValueByKey[key in blankNodeValueByKey ? key : 'default'](blankNodes, key, id);

const subValueLogKey = ({ key, id }: IBlankNodeLogSubValue): string =>
  subValueKeys.includes(key) ? key : id || key || null;

const inputValue = ({ value, coefficient }: any = {}) =>
  [value, coefficient].every(isNumber) ? +value * +coefficient : undefined;

const logSubValue = (logs: INodeTermLogs, key: string, prop: string, showUnits = false) => {
  const log = logs[key]?.[prop];
  return log
    ? Array.isArray(log)
      ? (log as string[]).map(
          id =>
            ({
              key: prop,
              id,
              showUnits
              // recalculatedValue: inputValue(logs[key]?.logs?.[id])
            }) as IBlankNodeLogSubValue
        )
      : ({
          key: prop,
          id: log,
          showUnits
          // recalculatedValue: inputValue(logs[key]?.logs?.[log])
        } as IBlankNodeLogSubValue)
    : undefined;
};

const logProperties = (nodes: blankNodesTypeValue[]) =>
  nodes
    .flatMap(node => (((node as any).properties as Property[]) || []).map(({ term }) => term))
    .map(
      term =>
        ({
          key: 'property',
          id: term['@id'],
          term,
          showUnits: false,
          configModels: findModels(term['@id']).map(({ model }) => model)
        }) as IPartialSubValue
    );

/**
 * Include transformation model (copy Emissions from Transformation to Cycle) as subValue.
 *
 * @param emissions
 * @returns
 */
const logEmissionTransformations = (emissions: Emission[]) =>
  emissions
    .filter(({ transformation }) => !!transformation?.['@id'])
    .map(
      ({ transformation }) =>
        ({
          id: transformation?.['@id'],
          key: 'transformation',
          showUnits: true,
          configModels: ['cycle/transformation']
        }) as IPartialSubValue
    );

const logEmissionFailedInputs = (node: IJSONNode, termId: string, emissions: Emission[]) => {
  // only log for background emissions
  const data = emissions.filter(
    ({ methodTier, term }) => term['@id'] === termId && methodTier === EmissionMethodTier.background
  );
  const existingInputs = data.flatMap(emission => emission.inputs?.map(input => input['@id'])).filter(Boolean);
  const configModels = unique(data.flatMap(emission => emission.methodModel?.['@id']).filter(Boolean));
  const missingInputs = (node as ICycleJSONLD).inputs?.filter(input => !existingInputs.includes(input.term?.['@id']));
  return data.length
    ? missingInputs.map(
        input =>
          ({
            blankNode: true,
            key: 'input',
            id: input.term['@id'],
            term: input.term,
            showUnits: true,
            configModels
          }) as IPartialSubValue
      )
    : [];
};

const logBackgroundEmissions = (logs: INodeTermLogs) =>
  (logs.models || [])
    .filter(model => logs[model].methodTier === EmissionMethodTier.background)
    .map(
      model =>
        ({
          blankNode: false,
          key: 'backgroundData',
          showUnits: false,
          modelKey: logs[model].model_key,
          configModels: [model],
          recalculatedValue: '-'
        }) as IPartialSubValue
    );

const includeCycleSubValue = (subValues: IBlankNodeLogSubValue[]) =>
  subValues.some(v => !['property', 'backgroundData'].includes(v.key));

const cycleSubValue = ({ termId, configModels, isRequired, subValues, original, recalculated }: IBlankNodeLog) => {
  const originalValue = reduceValues(
    original.filter((v: any) => !v.inputs?.length && !v.transformation),
    termId
  );
  const recalculatedValues = recalculated.filter((v: any) => !v.inputs?.length && !v.transformation);
  const recalculatedValue = reduceValues(recalculatedValues, termId);
  return includeCycleSubValue(subValues) && !isEmpty(recalculatedValue)
    ? ({
        blankNode: true,
        key: 'cycle',
        showUnits: true,
        configModels,
        isRequired,
        originalValue,
        recalculatedValue,
        isRecalculated: hasRecalculatedValue(recalculatedValues)
      } as IBlankNodeLogSubValue)
    : null;
};

const logSubValues = (
  node: IJSONNode,
  allLogs: INodeLogs,
  logs: INodeTermLogs,
  original: blankNodesTypeValue[],
  recalculated: blankNodesTypeValue[],
  { nodeType, type, termId }: IConfigModelDocsData
) =>
  // preserve order, as the last one will set the run status
  mergeSubValues([
    ...logProperties(original),
    // include properties
    ...Object.keys(logs)
      .filter(id => logs[id].isProperty)
      .map(
        id =>
          ({
            id,
            key: 'property',
            showUnits: false,
            configModels: (logs[id].models || []).filter(v => hasLog(logs[id][v]))
          }) as IPartialSubValue
      ),
    ...(type === SchemaType.Emission ? logEmissionTransformations(recalculated as Emission[]) : []),
    ...(type === SchemaType.Emission ? logEmissionFailedInputs(node, termId, recalculated as Emission[]) : []),
    ...(type === SchemaType.Input ? logBackgroundEmissions(logs) : []),
    ...Object.keys(logs).flatMap(key =>
      [type !== SchemaType.Input && logSubValue(logs, key, 'input', true), logSubValue(logs, key, 'property')]
        .flat()
        .filter(Boolean)
        .map(v => ({ ...v, configModels: (v.configModels as any[]) || [key] }))
    ),
    ...logProperties(recalculated)
  ])
    .map(({ key, id, modelKey, showUnits, configModels, originalValue, recalculatedValue, blankNode }) => {
      originalValue = originalValue || blankNodeValue(original, key, id);
      recalculatedValue = recalculatedValue || blankNodeValue(recalculated, key, id);

      return {
        blankNode: typeof blankNode === 'undefined' ? true : blankNode,
        key,
        id,
        showUnits,
        configModels: configModels.map(
          configModelWithDocs({ nodeType, type, termId }, { key, id, modelKey, showUnits, configModels })
        ),
        originalValue,
        recalculatedValue,
        isRecalculated: !isUndefined(recalculatedValue) && recalculatedValue !== originalValue,
        isRequired: true
      } as IBlankNodeLogSubValue;
    })
    .filter(({ originalValue, recalculatedValue, configModels }) =>
      [originalValue !== '', recalculatedValue !== '', !!configModels?.length].some(Boolean)
    )
    .map(v => (['input'].includes(v.key) ? dataWithConfigModelLogs(allLogs)(v) : dataWithConfigModelLogs(logs)(v)));

const logKeys = (
  logs: INodeTermLogs,
  original: blankNodesTypeValue[],
  recalculated: blankNodesTypeValue[],
  { nodeType, type, termId }
) =>
  Object.keys(logs)
    .filter(key => logs[key].isKey && !subValueKeys.includes(key))
    .map(
      key =>
        ({
          blankNode: true,
          key,
          showUnits: false,
          configModels: (logs[key].models || [])
            .map(configModelWithDocs({ nodeType, type, termId }, { key } as any))
            // only take those we found a matching model (avoid matching key on wrong node type)
            .filter(v => !!v.model),
          originalValue: blankNodeValue(original, key),
          recalculatedValue: blankNodeValue(recalculated, key),
          isRecalculated: hasRecalculatedValue(recalculated, key),
          isRequired: true
        }) as IBlankNodeLogSubValue
    )
    .filter(v => v.configModels.length > 0)
    .map(dataWithConfigModelLogs(logs));

const isModelForInputProduct = (type: SchemaType, key: string) =>
  [
    key !== 'hestiaAggregatedData',
    !key.includes('/input/') || type === SchemaType.Input,
    !key.includes('/product/') || type === SchemaType.Product
  ].every(Boolean);

const isBackgroundInput = (log: INodeTermLog) =>
  log.methodTier === EmissionMethodTier.background && !!log.input && log.shouldRun;

const isModelLog = (logs: INodeTermLogs, type: SchemaType) => (key: string) => {
  const log = logs[key];
  return (
    !!log &&
    [
      // handle Product same as Input
      isModelForInputProduct(type, key),
      log.methodTier !== EmissionMethodTier.background,
      !log.isKey,
      // !log.input,
      !log.property
    ].every(Boolean) &&
    ['shouldRun' in log, 'shouldRunOrchestrator' in log, 'runRequired' in log].some(Boolean)
  );
};

const modelConfigMatch = (termId: string, modelKey: string, model: string) => (config: IOrchestratorModelConfig) =>
  [termId === config.value, modelKey === config.key, model === config.model].every(Boolean);

const modelConfig = (
  models: (IOrchestratorModelConfig | IOrchestratorModelConfig[])[],
  termId: string,
  modelKey: string,
  model: string
) => models.flat().find(modelConfigMatch(termId, modelKey, model));

const modelConfigOrder = (
  models: (IOrchestratorModelConfig | IOrchestratorModelConfig[])[],
  termId: string,
  modelKey: string,
  model: string
): string => {
  const indexes = models
    .map((m, index) => {
      const arrayIndex = Array.isArray(m) ? modelConfigOrder(m, termId, modelKey, model) : '';
      return Array.isArray(m)
        ? arrayIndex
          ? [index, arrayIndex].join('.')
          : ''
        : modelConfigMatch(termId, modelKey, model)(m)
          ? `${index}`
          : '';
    })
    .filter(Boolean);
  return indexes.length ? indexes[0] : '';
};

interface IConfigModelDocsData {
  nodeType: NodeType;
  type: SchemaType;
  termId?: string;
  termType?: TermTermType;
  key?: string;
}

const configModelWithDocs =
  ({ nodeType, type, termId, termType, key }: IConfigModelDocsData, subValue?: IPartialSubValue) =>
  (methodId: string) => {
    const node = {
      '@type': nodeType,
      term: { '@id': subValue?.id || termId },
      methodModel: { '@id': methodId }
    } as any;
    return {
      methodId,
      model:
        // handle "hestia.seed_emissions"
        findMatchingModel(modelParams(node, !['backgroundData'].includes(subValue?.key), subValue?.modelKey)) ||
        findMatchingModel(modelParams(node, !['backgroundData'].includes(subValue?.key))) ||
        // handle "liveAnimal"
        (termType && findMatchingModel(modelKeyParams(node, termType))) ||
        // handle "completeness.cropResidue"
        (key && findMatchingModel(modelKeyParams(node, `${methodId}.${key}`))) ||
        findMatchingModel(modelKeyParams(node, `${subValue?.key}.${methodId}`)) ||
        // handle "input.price"
        findMatchingModel(modelKeyParams(node, `${type?.toLowerCase()}.${subValue?.key}`)) ||
        // handle "input.hestiaAggregatedData"
        findMatchingModel(modelKeyParams(node, `${type?.toLowerCase()}.${methodId}`)) ||
        // handle "transformation"
        (subValue?.key ? findMatchingModel(modelKeyParams(node, subValue?.key)) : undefined) ||
        // handle "transformation/input/excreta" and other models with "/"
        (methodId.includes('/')
          ? findMatchingModel({
              model: methodId.split('/')[0],
              modelKey: methodId.split('/').slice(1).join('.')
            })
          : undefined)
    };
  };

const configModelWithLogs =
  (data: IBlankNodeLog | INonBlankNodeLog | IBlankNodeLogSubValue, logs: INodeTermLogs, previousModelSuccess = false) =>
  (model: IConfigModel) => {
    const status = logStatus(data, logs?.[model.methodId], previousModelSuccess);
    return {
      ...model,
      logs: logs?.[model.methodId],
      status,
      showLogs: hasLogDetails(status, logs?.[model.methodId])
    } as IConfigModel;
  };

/**
 * Handle hierarchy skip if a model before succeeded.
 *
 * @param data
 * @param index
 */
const hasPreviousModelSuccess = (models: (IConfigModel | IConfigModel[])[], index = 0) =>
  filterConfigModels(models[index], keepConfigModelByStatus(LogStatus.success)).length > 0 ||
  (index <= models.length && hasPreviousModelSuccess(models, index + 1));

const dataWithConfigModelLogs =
  (logs: INodeLogs | INodeTermLogs) =>
  <T extends IBlankNodeLog | INonBlankNodeLog | IBlankNodeLogSubValue>(data: T) => {
    const subLogKey = data.blankNode ? subValueLogKey(data as IBlankNodeLogSubValue) : null;
    const log = subLogKey ? logs[subLogKey] : logs;
    const configModels: (IConfigModel | IConfigModel[])[] = [];

    // go through models one by one to handle cases where next models in hierarchy have failed
    for (const index in data.configModels) {
      if (Object.prototype.hasOwnProperty.call(data.configModels, index)) {
        const model = data.configModels[index];
        const previousModelSuccess = +index > 0 && hasPreviousModelSuccess(configModels);
        const newModel = Array.isArray(model)
          ? model.map(configModelWithLogs(data, log, previousModelSuccess))
          : configModelWithLogs(data, log, previousModelSuccess)(model);
        configModels.push(newModel);
      }
    }

    return {
      ...data,
      configModels: filterConfigModels(
        configModels,
        (model: IConfigModel) =>
          ![LogStatus.notRequired].includes(model?.status) || notRelevantModels.includes(model.methodId)
      )
    } as T;
  };

/**
 * Group models running in parallel under the same array, as they should appear vertically.
 *
 * @param config The configuration.
 * @param models The list of models
 */
const groupParallelModels = (
  config: IOrchestratorConfig,
  termId: string,
  modelKey: blankNodesKey,
  models: IConfigModel[]
) => {
  const modelsWithOrder = models.map(model => ({
    model: {
      ...model,
      config: modelConfig(config.models, termId, modelKey, model.methodId)
    },
    order: modelConfigOrder(config.models, termId, modelKey, model.methodId)
  }));
  return modelsWithOrder
    .reduce((prev, { model, order }) => {
      const isArray = order.includes('.');
      const index = isArray ? order.split('.')[0] : order || prev.length;
      prev[index] = isArray ? [...(prev[index] || []), model] : [model];
      return prev;
    }, [] as IConfigModel[][])
    .filter(Boolean)
    .map(v => (v.length === 1 ? v[0] : v));
};

const allParallel = (models: (IConfigModel | IConfigModel[])[]) =>
  models.length === 1 && Array.isArray(models[0]) && models[0].length > 1;

const blankNodeModelId = (value: blankNodesTypeValue) =>
  value['@type'] === SchemaType.Measurement
    ? (value as Measurement).method?.['@id']
    : (value as any).methodModel?.['@id'];

const valueByMethodId = (values: blankNodesTypeValue[], methodId: string, termId: string) =>
  propertyValue(values.find(v => blankNodeModelId(v) === methodId)?.value as number[], termId);

const groupParallelValues = (values: blankNodesTypeValue[], models: IConfigModel | IConfigModel[], termId: string) =>
  Array.isArray(models)
    ? Object.fromEntries(models.map(model => [model.methodId, valueByMethodId(values, model.methodId, termId)]))
    : {};

export const groupLogsByTerm = (
  node: IJSONNode,
  logs: INodeLogs,
  config: IOrchestratorConfig,
  allOriginalValues: blankNodesTypeValue[],
  allRecalculatedValues: blankNodesTypeValue[],
  nodeKey: blankNodesKey
) => {
  const nodeType = nodeNodeType(node);
  const originalValues = filterDeleted(allOriginalValues || []);
  const recalculatedValues = filterDeleted(allRecalculatedValues || []).filter(hasRecalculatedKeys);
  const type: SchemaType = originalValues.length
    ? originalValues[0]['@type'] || originalValues[0].type
    : recalculatedValues.length
      ? recalculatedValues[0]['@type'] || recalculatedValues[0].type
      : undefined;

  return (term: ITermJSONLD): IBlankNodeLog => {
    const termId = term['@id'];
    const termType = term.termType;
    const termLogs = get(logs, termId, {}) as INodeTermLogs;
    const original = originalValues.filter(v => termId === v.term!['@id']);
    const recalculated = recalculatedValues.filter(v => termId === v.term!['@id']);
    const hasData = !!original.length || !!recalculated.length || Object.keys(termLogs).length > 0;
    const configModelsData = { nodeType, type, termId, termType };
    const configModels = groupParallelModels(
      config,
      termId,
      nodeKey!,
      unique([
        // remove completeness as should be using `groupdLogsByKey`
        ...(termLogs.models || []).filter(isModelLog(termLogs, type)).filter(v => !isNonNodeModelKey(v))
        // ...findConfigModels(config, termId, nodeKey!, models).map(({ model }) => model as string)
      ]).map(configModelWithDocs(configModelsData))
    );

    const keys = logKeys(termLogs, original, recalculated, configModelsData);
    const subValues = logSubValues(node, logs, termLogs, original, recalculated, configModelsData);
    const isRequired = !Object.values(termLogs)
      .filter(v => typeof v === 'object' && !Array.isArray(v) && !v.isKey)
      .every(v => v.runRequired === false);

    const nodeLog: IBlankNodeLog = dataWithConfigModelLogs(termLogs)({
      blankNode: true,
      isOpen: true,
      canOpen: keys.length > 0 || subValues.length > 0,
      termId,
      term,
      type,
      configModels,
      original,
      originalValue: reduceValues(original, termId),
      recalculated,
      recalculatedValue: reduceValues(recalculated, termId),
      isOriginal: !!original.length,
      isRecalculated: hasRecalculatedValue(recalculated),
      hasData,
      isRequired,
      logs: termLogs,
      keys,
      subValues,
      allParallel: allParallel(configModels),
      originalValueByMethodId: groupParallelValues(original, configModels[0], termId),
      recalculatedValueByMethodId: groupParallelValues(recalculated, configModels[0], termId)
    });
    const subValue = cycleSubValue(nodeLog);

    return subValue && subValues?.length
      ? {
          ...nodeLog,
          modelsInSubValues: true,
          subValues: [subValue, ...subValues]
        } // if we include the Cycle value, it becomes the main reference of the models
      : nodeLog;
  };
};

export const groupdLogsByKey = (
  node: IJSONNode,
  nodeKey: nonBlankNodesKey,
  logs: INodeLogs,
  originalValue?: nonBlankNodesTypeValue,
  recalculatedValue?: nonBlankNodesTypeValue
) => {
  const nodeType = nodeNodeType(node);
  const type: SchemaType =
    originalValue?.['@type'] || originalValue?.type || recalculatedValue?.['@type'] || recalculatedValue?.type;

  return (key: string): INonBlankNodeLog => {
    const keyLogs = get(logs, key, {}) as INodeTermLogs;
    const original = originalValue?.[key];
    const recalculated = recalculatedValue?.[key];
    const hasData = !!original || !!recalculated || Object.keys(keyLogs).length > 0;
    const configModelsData = { nodeType, type, key };
    const configModels = unique([
      ...(keyLogs.models || []).filter(isModelLog(keyLogs, type)).filter(v => v === nodeKey)
    ]).map(configModelWithDocs(configModelsData));
    const isRequired = !Object.values(keyLogs)
      .filter(v => typeof v === 'object' && !Array.isArray(v) && !v.isKey)
      .every(v => v.runRequired === false);

    return dataWithConfigModelLogs(keyLogs)({
      blankNode: false,
      isOpen: true,
      canOpen: false,
      key,
      type,
      configModels,
      originalValue: original,
      recalculatedValue: recalculated,
      isOriginal: !isUndefined(original),
      isRecalculated: isRecalculated(key)(recalculatedValue),
      hasData,
      isRequired,
      logs: keyLogs,
      allParallel: false
    });
  };
};

export const modelCount = (blankNodeLogs: IBlankNodeLog[]) =>
  Math.max.apply(Math.max, [
    0,
    ...blankNodeLogs.flatMap(v => [
      v.configModels.length,
      ...v.subValues.map(s => s.configModels?.length || 0),
      ...v.keys.map(s => s.configModels?.length || 0)
    ])
  ]);

export const logValueArray = (value: string): any[] =>
  [';', ':'].some(v => value.includes(v))
    ? value.split(';').flatMap(v => {
        const res = v.split('_').map(vv => vv.split(':'));
        return res.length <= 1 ? res.flat() : Object.fromEntries(res);
      })
    : null;
