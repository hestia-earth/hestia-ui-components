import { Component, input } from '@angular/core';
import { EmissionMethodTier, NodeType } from '@hestia-earth/schema';

import { PluralizePipe } from '../../../common';
import { blankNodesTypeValue, IBlankNodeLog, IConfigModel, LogStatus } from '../node-logs-models.model';

@Component({
  selector: 'he-node-logs-models-logs-status',
  imports: [PluralizePipe],
  templateUrl: './node-logs-models-logs-status.component.html',
  styleUrl: './node-logs-models-logs-status.component.scss'
})
export class NodeLogsModelsLogsStatusComponent {
  protected readonly EmissionMethodTier = EmissionMethodTier;
  protected readonly LogStatus = LogStatus;

  protected readonly nodeType = input.required<NodeType>();
  protected readonly model = input.required<IConfigModel>();
  protected readonly data = input.required<IBlankNodeLog>();

  protected hasMethodTier(values: blankNodesTypeValue[], methodTier: EmissionMethodTier) {
    return values.some(v => 'methodTier' in v && v.methodTier === methodTier);
  }
}
