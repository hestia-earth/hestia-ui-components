import { Injectable, inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { DataState, nodeTypeToParam } from '@hestia-earth/api';
import { ICycleJSONLD, IImpactAssessmentJSONLD, ISiteJSONLD, NodeType } from '@hestia-earth/schema';

import { filterParams, glossaryBaseUrl, handleAPIError } from '../common/utils';
import { HeCommonService } from '../common/common.service';
import { INodeMissingLookupLog } from './node-logs-models/node-logs-models.model';

export const nodeType = (node?: IJSONNode) => node?.['@type'] || node?.type;
export const nodeId = (node?: IJSONNode) => node?.['@id'] || node?.id;

const nodeTypeUrl = (apiBaseUrl: string, type?: NodeType) => (type ? `${apiBaseUrl}/${nodeTypeToParam(type)}` : '');

/**
 * Get the full url to fetch the node data.
 *
 * @param apiBaseUrl The url of the API hosting the data.
 * @param node The node with a type, id and optionally a dataState
 * @returns The full url.
 */
export const nodeUrl = (apiBaseUrl: string, { dataState, ...node }: IJSONNode) =>
  `${nodeTypeUrl(apiBaseUrl, node['@type'] || node.type)}/${node['@id'] || node.id}${
    dataState ? `?dataState=${dataState}` : ''
  }`;

export const nodeLogsUrl = (apiBaseUrl: string, { dataState, ...node }: IJSONNode) =>
  `${nodeUrl(apiBaseUrl, node)}/log${dataState ? `?dataState=${dataState}` : ''}`;

export const lookupUrl = (filename: string) =>
  filename.startsWith('http') ? filename : `${glossaryBaseUrl(false)}/lookups/${filename}.csv`;

export const nodeColours = {
  [NodeType.ImpactAssessment]: 'blue',
  [NodeType.Cycle]: 'red',
  [NodeType.Site]: 'green'
};

export interface IJSONNode {
  '@type'?: NodeType;
  '@id'?: string;
  type?: NodeType;
  id?: string;
  dataState?: DataState;
  aggregated?: boolean;
  aggregatedDataValidated?: boolean;
}

export interface IRelatedNode {
  '@type': NodeType;
  '@id': string;
  [x: string]: string;
}

export class RelatedNodeResult {
  count = 0;
  results: IRelatedNode[] = [];
}

export interface IJSONData {
  [NodeType.Cycle]?: ICycleJSONLD[];
  [NodeType.Site]?: ISiteJSONLD[];
  [NodeType.ImpactAssessment]?: IImpactAssessmentJSONLD[];
}

// TODO: should come from shared config with the calculation-pipeline
export const NODE_MAX_STAGE = {
  [NodeType.Cycle]: 2,
  [NodeType.ImpactAssessment]: 1,
  [NodeType.Site]: 2
};

type onHeadersCallback = (headers: HttpHeaders) => any;

@Injectable({
  providedIn: 'root'
})
export class HeNodeService {
  protected http = inject(HttpClient);
  protected commonService = inject(HeCommonService);

  public getWithHeaders$<T>({ dataState, ...node }: IJSONNode, params = {}) {
    return this.http
      .get<T>(this.nodeUrl(node), {
        params: filterParams({ dataState, ...params }),
        observe: 'response'
      })
      .pipe(
        map(resp => ({
          headers: resp.headers,
          data: resp.body
        }))
      );
  }

  public get$<T>(node: IJSONNode, params = {}, onHeaders: onHeadersCallback = () => true) {
    return this.getWithHeaders$<T>(node, params).pipe(
      tap(({ headers }) => onHeaders(headers)),
      map(({ data }) => data)
    );
  }

  public async get<T>(node: IJSONNode, defaultForState = true) {
    return await this.get$<T>(node)
      .toPromise()
      .catch(() => (node.dataState && defaultForState ? (node as any as T) : ({} as T)));
  }

  public getRelated$(node: IJSONNode, relatedType: NodeType, limit = 100, offset = 0, relationship?: string) {
    return this.http
      .get<RelatedNodeResult>(`${this.nodeUrl(node)}/${nodeTypeToParam(relatedType)}`, {
        params: filterParams({ limit, offset, relationship })
      })
      .pipe(catchError(() => of({ count: 0, results: [] } as RelatedNodeResult)));
  }

  public async getRelated(node: IJSONNode, relatedType: NodeType, limit = 100, offset = 0, relationship?: string) {
    return this.getRelated$(node, relatedType, limit, offset, relationship).toPromise();
  }

  public getLog$({ dataState, aggregated, ...node }: IJSONNode) {
    return this.http
      .get(`${this.nodeUrl(node)}/log`, {
        responseType: 'text',
        params: filterParams(dataState === DataState.recalculated ? { dataState } : { aggregated })
      })
      .pipe(catchError(() => of('')));
  }

  public getMissingLookupsLog$({ dataState, ...node }: IJSONNode) {
    return this.http.get<INodeMissingLookupLog[]>(`${this.nodeUrl(node)}/log/lookups`, {
      params: filterParams({ dataState })
    });
  }

  public async triggerPipeline({ dataState, ...node }: IJSONNode) {
    return await this.http
      .post(`${this.nodeUrl(node)}/pipeline`, filterParams({ dataState }), {})
      .toPromise()
      .catch(handleAPIError);
  }

  public downloadRaw$<T = any>(url: string, responseType: any = 'json') {
    return this.http.get<T>(url, { responseType }).pipe(catchError(() => of((responseType === 'json' ? {} : '') as T)));
  }

  public downloadRaw<T = any>(url: string, responseType: any = 'json') {
    return this.downloadRaw$<T>(url, responseType).toPromise();
  }

  public nodeTypeUrl(type: any) {
    return nodeTypeUrl(this.commonService.apiBaseUrl, type);
  }

  public nodeUrl(node: IJSONNode) {
    return nodeUrl(this.commonService.apiBaseUrl, node);
  }

  public nodeLogsUrl(node: IJSONNode) {
    return nodeLogsUrl(this.commonService.apiBaseUrl, node);
  }
}
