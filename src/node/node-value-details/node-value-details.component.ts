import { ChangeDetectionStrategy, Component, computed, inject, input, signal } from '@angular/core';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { DataState } from '@hestia-earth/api';
import { NodeType, SchemaType } from '@hestia-earth/schema';
import { isUndefined } from '@hestia-earth/utils';
import { LocalStorageService } from 'ngx-webstorage';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import { NgbDropdownModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CdkDrag, CdkDragDrop, CdkDropList, moveItemInArray } from '@angular/cdk/drag-drop';
import { combineLatest } from 'rxjs';
import { filter } from 'rxjs/operators';
import get from 'lodash.get';

import { grouppedValueKeys, IGroupedNode } from '../../common/node-utils';
import { schemaBaseUrl } from '../../common/utils';
import { LinkKeyValueComponent } from '../../common/link-key-value/link-key-value.component';
import { HeSchemaService } from '../../schema/schema.service';

// TODO: compute from the list of unique properties
const additionalKeys = [
  'key',
  'methodModelDescription',
  'methodDescription',
  'description',
  'primary',
  'emissionDuration',
  'observations',
  'statsDefinition',
  'term',
  'source',
  'variety',
  'reliability',
  'price',
  'currency',
  'cost',
  'revenue',
  'economicValueShare',
  'operation',
  'impactAssessment',
  'properties',
  'isAnimalFeed',
  'fate'
];
const defaultTableKeys = [
  'animals',
  'inputs',
  'properties',
  'transformation',
  'operation',
  'impactAssessment',
  'value',
  'dates',
  'startDate',
  'endDate',
  'methodModel',
  'methodTier',
  'statsDefinition',
  'depthUpper',
  'depthLower'
];

const storageKey = 'he-node-value-details';

const ignoreSchemaKeys = [
  'id',
  '@id',
  'type',
  '@type',
  // included inline
  'term'
];

interface IStoredKey {
  key: string;
  selected: boolean;
}

type storedKeys = { [key in SchemaType]?: IStoredKey[] };

@Component({
  selector: 'he-node-value-details',
  templateUrl: './node-value-details.component.html',
  styleUrls: ['./node-value-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CdkDrag, CdkDropList, LinkKeyValueComponent, FaIconComponent, NgbTooltipModule, NgbDropdownModule]
})
export class NodeValueDetailsComponent<T> {
  private readonly schemaService = inject(HeSchemaService);
  private readonly localStorage = inject(LocalStorageService);

  protected readonly faCog = faCog;

  protected readonly schemaBaseUrl = schemaBaseUrl();
  protected readonly keys = ['value', ...grouppedValueKeys];
  protected readonly additionalKeys = additionalKeys;

  private readonly schemas = toSignal(this.schemaService.schemas$);
  private readonly schemaKeys = computed(() =>
    Object.entries(this.schemas()?.[this.type()]?.properties ?? {})
      .filter(([key, { internal }]) => !ignoreSchemaKeys.includes(key) && !internal)
      .map(([key]) => key)
  );
  private readonly schemaKeys$ = toObservable(this.schemaKeys);

  private readonly storedTableKeys = signal({} as storedKeys);
  protected readonly tableKeys = computed(() => this.storedTableKeys()[this.type()] || []);
  private readonly availableTableKeys = computed(() => this.tableKeys().map(({ key }) => key));
  protected readonly visibleTableKeys = computed(() =>
    this.tableKeys()
      .filter(({ key, selected }) => selected && this.nodes().some(node => !isUndefined(get(node, key, undefined))))
      .map(({ key }) => key)
  );

  protected readonly data = input.required<IGroupedNode<T>>();
  protected readonly nodeType = input.required<NodeType>();
  protected readonly dataState = input.required<DataState>();
  protected readonly dataKey = input.required<string>();

  protected readonly nodes = computed(() => this.data()?.nodes || []);
  protected readonly node = computed(() => this.nodes()?.[0]);
  protected readonly type = computed(() => this.node()?.['@type'] as SchemaType);
  private readonly type$ = toObservable(this.type);
  protected readonly showInline = computed(() => this.nodes()?.length === 1);

  constructor() {
    combineLatest([this.type$, this.schemaKeys$])
      .pipe(filter(([type, schemaKeys]) => !!type && !!schemaKeys?.length))
      .subscribe(([type]) => {
        const storedTableKeys = JSON.parse(this.localStorage.retrieve(storageKey) || '{}') as storedKeys;
        this.storedTableKeys.set(storedTableKeys);

        // first time creating the table keys for this type
        const hasStoredTableKeys = !!storedTableKeys[type]?.length;
        return hasStoredTableKeys ? this.updateSchemaTableKeys() : this.createSchemaTableKeys();
      });
  }

  private updateSchemaTableKeys() {
    // remove any key that is no longer in the schema
    const removedKeys = this.availableTableKeys().filter(key => !this.schemaKeys().includes(key));

    // add any key that has been added in the schema
    const addedkeys = this.schemaKeys().filter(key => !this.availableTableKeys().includes(key));

    const selectedKeys = [
      ...(this.tableKeys() || []).slice().filter(({ key }) => !removedKeys.includes(key)),
      ...addedkeys.map(key => ({ key, selected: defaultTableKeys.includes(key) }))
    ];
    this.updateKeys(selectedKeys);
  }

  private createSchemaTableKeys() {
    const keys = this.schemaKeys().map(key => ({ key, selected: defaultTableKeys.includes(key) }));
    this.updateKeys(keys);
  }

  private updateKeys(keys: IStoredKey[]) {
    const storedTableKeys = { ...(this.storedTableKeys() || {}) };
    storedTableKeys[this.type()] = keys;
    this.storedTableKeys.set(storedTableKeys);
    this.localStorage.store(storageKey, JSON.stringify(storedTableKeys));
  }

  protected defaultValue(key: string) {
    return ['impactAssessment', 'transformation'].includes(key) ? 'N/A (from Cycle)' : '';
  }

  protected onTableKeyChange(key: IStoredKey, index: number, selected: boolean) {
    const selectedKeys = (this.tableKeys() || []).slice();
    key.selected = selected;
    selectedKeys[index] = key;
    this.updateKeys(selectedKeys);
  }

  protected dropTableKey({ previousIndex, currentIndex }: CdkDragDrop<any, any, IStoredKey>) {
    const selectedKeys = (this.tableKeys() || []).slice();
    moveItemInArray(selectedKeys, previousIndex, currentIndex);
    this.updateKeys(selectedKeys);
  }
}
