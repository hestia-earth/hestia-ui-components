import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  computed,
  effect,
  inject,
  input,
  output,
  signal
} from '@angular/core';
import { takeUntilDestroyed, toObservable } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { CdkDragDrop, moveItemInArray, CdkDropList, CdkDrag } from '@angular/cdk/drag-drop';
import { isDefaultCSVSelected, isCSVIncluded } from '@hestia-earth/json-schema/schema-utils';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbDropdown, NgbDropdownToggle, NgbDropdownMenu, NgbDropdownItem } from '@ng-bootstrap/ng-bootstrap';
import { filter, mergeMap, tap } from 'rxjs/operators';
import { isUndefined } from '@hestia-earth/utils';
import { combineLatest, forkJoin, of } from 'rxjs';

import { HeSchemaService } from '../../schema/schema.service';
import { faAngleDown, faAngleLeft, faFilter, faSpinner } from '@fortawesome/free-solid-svg-icons';

interface IHeader {
  key: string;
  selected: boolean;
  /**
   * `false` if it is an `internal` field.
   */
  included: boolean;
}

interface IGroup {
  key: string;
  selected: boolean;
  partialSelected: boolean;
  open: boolean;
  headers: IHeader[];
}

interface IGroupedHeaders {
  [group: string]: IGroup;
}

const headerGroup = (header: string) => {
  const parts = header.split('.');
  return parts.length === 2 ? '' : [parts[0], parts[1]].join('.');
};

const termFields = ['@id', 'name', 'units', 'termType'];

const isTermField = (field: string, { key }: IHeader) =>
  ['term', 'methodModel', 'country', 'product', 'operation', 'key'].some(parent =>
    key.endsWith([parent, field].join('.'))
  );

const updateGroupHeadersSelected = ({ headers, ...group }: IGroup) => ({
  ...group,
  headers: headers.map(header => ({ ...header, selected: group.selected }))
});

const updateGroupSelected = (group: IGroup) => {
  const allSelected = group.headers.every(({ selected }) => selected);
  return {
    ...group,
    selected: allSelected,
    partialSelected: !allSelected && group.headers.some(({ selected }) => selected)
  };
};

@Component({
  selector: 'he-node-csv-select-headers',
  templateUrl: './node-csv-select-headers.component.html',
  styleUrls: ['./node-csv-select-headers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormsModule,
    NgbDropdown,
    NgbDropdownToggle,
    FaIconComponent,
    NgbDropdownMenu,
    NgbDropdownItem,
    CdkDropList,
    CdkDrag
  ]
})
export class NodeCsvSelectHeadersComponent {
  private readonly destroyRef = inject(DestroyRef);
  private readonly schemaService = inject(HeSchemaService);

  protected readonly faSpinner = faSpinner;
  protected readonly faFilter = faFilter;
  protected readonly faAngleDown = faAngleDown;
  protected readonly faAngleLeft = faAngleLeft;

  protected readonly csv = input.required<string>();
  private readonly csv$ = toObservable(this.csv);
  protected readonly keys = input<string[]>([]);
  private readonly keys$ = toObservable(this.keys);
  protected readonly includeDefaultCSV = input(false);

  protected readonly headersChanged = output<string[]>();

  protected readonly loading = signal(true);
  protected readonly termFields = termFields;
  protected readonly selectedTermFields = signal(['@id']);
  protected readonly showNonIncluded = signal(false);

  protected readonly allSelected = computed(() =>
    Object.keys(this.headers()).every(key => this.headers()[key].selected)
  );
  private readonly selectedHeaders = computed(() =>
    Object.values(this.headers())
      .flatMap(({ headers }) => headers)
      .filter(({ selected, included }) => selected && (included || this.showNonIncluded()))
      .map(({ key }) => key)
  );

  private readonly schemas$ = this.schemaService.schemas$.pipe(takeUntilDestroyed(this.destroyRef));

  private readonly csvHeaders = signal<string[]>([]);
  protected readonly headers = signal<IGroupedHeaders>({});
  protected readonly groups = computed(() => Object.values(this.headers()));

  constructor() {
    combineLatest([this.csv$, this.keys$, this.schemas$])
      .pipe(
        filter(([csv, _keys, schemas]) => [!isUndefined(csv), !isUndefined(schemas)].every(Boolean)),
        tap(() => this.loading.set(true)),
        mergeMap(([csv, keys, schemas]) =>
          forkJoin({
            headers: this.schemaService.parseHeaders$(csv),
            keys: of(keys),
            schemas: of(schemas)
          })
        ),
        tap(() => this.loading.set(false))
      )
      .subscribe(({ headers, keys, schemas }) => {
        this.csvHeaders.set(headers);

        const isIncluded = isCSVIncluded(schemas);
        const isDefaultSelected = this.includeDefaultCSV() ? () => true : isDefaultCSVSelected(schemas);
        const isSelected = (key: string) =>
          (!keys.length || keys.some(v => key.startsWith(v))) && isDefaultSelected(key);

        this.headers.set(
          headers.reduce((prev, headerKey) => {
            const groupKey = headerGroup(headerKey);
            prev[groupKey] = prev[groupKey] || {
              key: groupKey,
              headers: [],
              selected: false,
              partialSelected: false,
              open: true
            };
            const group = prev[groupKey];
            const groupHeader = {
              key: headerKey,
              selected: isSelected(headerKey),
              included: isIncluded(headerKey)
            };
            group.headers.push(groupHeader);
            prev[groupKey] = updateGroupSelected(group);
            return prev;
          }, {} as IGroupedHeaders)
        );
      });

    effect(() => {
      if (this.selectedHeaders()?.length) {
        this.headersChanged.emit(this.selectedHeaders());
      }
    });
  }

  private updateHeadersGroup(group: IGroup) {
    const headers = { ...this.headers(), [group.key]: group };
    this.headers.set(headers);
  }

  protected dropHeader(event: CdkDragDrop<IHeader[]>, group: IGroup) {
    const headers = group.headers.slice();
    moveItemInArray(headers, event.previousIndex, event.currentIndex);
    const updatedGroup = { ...group, headers };
    this.updateHeadersGroup(updatedGroup);
  }

  protected selectAll(allSelected: boolean) {
    // select all term fields
    this.selectedTermFields.set(termFields);

    // select all keys
    const headers = Object.fromEntries(
      Object.entries(this.headers()).map(([key, group]) => [
        key,
        updateGroupHeadersSelected({
          ...group,
          selected: allSelected
        } as IGroup)
      ])
    ) as IGroupedHeaders;
    this.headers.set(headers);
  }

  protected updateGroup(selected: boolean, group: IGroup) {
    const updatedGroup = updateGroupHeadersSelected({ ...group, selected });
    this.updateHeadersGroup(updatedGroup);
  }

  protected updateHeader(selected: boolean, group: IGroup, header: IHeader) {
    const updatedGroup = updateGroupSelected({
      ...group,
      headers: group.headers.map(v => ({ ...v, selected: v.key === header.key ? selected : v.selected }))
    });
    this.updateHeadersGroup(updatedGroup);
  }

  // Term fields

  protected isTermFieldSelect(field: string) {
    return this.selectedTermFields().includes(field);
  }

  protected toggleTermField(selected: boolean, field: string) {
    // update term field
    const selectedTermFields = this.selectedTermFields();
    this.selectedTermFields.set(selectedTermFields.filter(value => value !== field || selected));

    const headers = Object.fromEntries(
      Object.entries(this.headers()).map(([key, group]) => [
        key,
        updateGroupSelected({
          ...group,
          headers: group.headers.map(header => ({
            ...header,
            selected: isTermField(field, header) ? selected : header.selected
          }))
        } as IGroup)
      ])
    ) as IGroupedHeaders;
    this.headers.set(headers);
  }
}
