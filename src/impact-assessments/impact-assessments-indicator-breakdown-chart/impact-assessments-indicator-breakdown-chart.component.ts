import { ChangeDetectionStrategy, Component, Signal, computed, effect, inject, input, signal } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { filter, groupBy, map, mergeAll, mergeMap, take, tap, toArray } from 'rxjs/operators';
import { ChartConfiguration, ChartData } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { DataState } from '@hestia-earth/api';
import { IImpactAssessmentJSONLD, Indicator, ITermJSONLD, NodeType, Term, TermTermType } from '@hestia-earth/schema';
import { toPrecision, unique } from '@hestia-earth/utils';
import { FormsModule } from '@angular/forms';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faDownload, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { of } from 'rxjs';

import { matchTermType, matchType } from '../../search/search.model';
import { HeSearchService } from '../../search/search.service';
import { HeNodeService } from '../../node/node.service';
import { distinctUntilChangedDeep } from '../../common/rxjs-utils';
import { Level, parseLines, parseMessage } from '../../common/logs-utils';
import { listColor } from '../../common/color';
import { ChartComponent } from '../../chart/chart.component';

interface ILog {
  modelId: string;
  impactTermId: string;
  impactTermUnits?: string;
  blankNodeTermId: string;
  coefficient: number;
  value: number;
  // store all indicators with inputs
  inputs?: {
    name: string;
    value: number;
  }[];
  inputsValue?: number;
}

const parseLog = (data: any): ILog => ({
  modelId: data.model,
  impactTermId: data['key/term'] || data.term || data.indicator,
  blankNodeTermId: data.node,
  coefficient: +data.coefficient,
  value: +data.value * +data.coefficient
});

const filterTermTypes = [TermTermType.emission, TermTermType.characterisedIndicator];

const csvHeaders = [
  'Impact',
  'Impact Unit',
  'Method',
  'Emission',
  'Value',
  'Inputs',
  'Inputs value',
  'Functional Unit'
];

const logToCsv = (logs: ILog[], impact: IImpactAssessmentJSONLD) =>
  [
    csvHeaders.join(','),
    ...logs
      .sort((a, b) => a.impactTermId.localeCompare(b.impactTermId))
      .flatMap(({ impactTermId, impactTermUnits, modelId, blankNodeTermId, value, inputs }) => [
        [impactTermId, impactTermUnits, modelId, blankNodeTermId, value, '', ''],
        ...inputs.map(v => [impactTermId, impactTermUnits, modelId, blankNodeTermId, '', v.name, v.value])
      ])
      .map(v => [...v, impact.product?.term?.units].join(','))
  ].join('\n');

@Component({
  selector: 'he-impact-assessments-indicator-breakdown-chart',
  templateUrl: './impact-assessments-indicator-breakdown-chart.component.html',
  styleUrls: ['./impact-assessments-indicator-breakdown-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FaIconComponent, FormsModule, ChartComponent]
})
export class ImpactAssessmentsIndicatorBreakdownChartComponent {
  private domSanitizer = inject(DomSanitizer);
  private searchService = inject(HeSearchService);
  private nodeService = inject(HeNodeService);

  protected readonly faSpinner = faSpinner;
  protected readonly faDownload = faDownload;

  protected impactAssessment = input(undefined as IImpactAssessmentJSONLD);
  private impactAssessment$ = toObservable(this.impactAssessment);

  protected indicators = input([] as Indicator[]);

  private allLogs = signal([] as ILog[]);
  private logs = computed(() =>
    this.allLogs()
      .filter(
        ({ impactTermId, modelId }) =>
          impactTermId === this.selectedTerm()?.['@id'] &&
          (!this.selectedMethod() || modelId === this.selectedMethod()?.['@id'])
      )
      .sort((a, b) => b.value - a.value)
  );

  private emissions = toSignal(
    this.searchService
      .search$<ITermJSONLD, NodeType.Term>({
        fields: ['@type', '@id', 'name'],
        limit: 1000,
        query: {
          bool: {
            must: [matchType(NodeType.Term)],
            should: filterTermTypes.map(termType => matchTermType(termType)),
            minimum_should_match: 1
          }
        }
      })
      .pipe(map(({ results }) => results))
  );

  protected loading = signal(true);

  protected selectedTerm = signal(undefined as Term);
  protected terms = computed(() =>
    unique(
      (this.indicators() || [])
        .map(({ term }) => term!)
        .filter(Boolean)
        .sort((a, b) => a.name.localeCompare(b.name))
    )
  );

  protected selectedMethod = signal(undefined as Term);
  protected methods = computed(() =>
    unique((this.indicators() || []).map(({ methodModel }) => methodModel!).filter(Boolean))
  );

  private impacts = computed(() => [
    ...(this.impactAssessment()?.impacts || []),
    ...(this.impactAssessment()?.endpoints || [])
  ]);
  protected noData = computed(() => this.logs()?.length === 0);

  protected csvContent = computed(() =>
    this.domSanitizer.bypassSecurityTrustResourceUrl(
      `data:text/html;charset=utf-8,${encodeURIComponent(logToCsv(this.logs(), this.impactAssessment()))}`
    )
  );
  private id = computed(() => this.impactAssessment()?.['@id'] || (this.impactAssessment() as any)?.id);
  protected downloadFilename = computed(() => `${this.id()}-logs.csv`);

  private total = computed(() => this.logs().reduce((prev, curr) => prev + curr.value, 0));
  private labels = computed(() =>
    this.logs().map(
      ({ blankNodeTermId }) => this.emissions()?.find(v => v['@id'] === blankNodeTermId)?.name || blankNodeTermId
    )
  );
  private datasets = computed(() => [
    {
      label: this.selectedTerm()?.name ?? '',
      data: this.logs().map(({ value }) => value),
      backgroundColor: this.logs().map(listColor),
      borderColor: this.logs().map(listColor)
    }
  ]);

  protected chartData: Signal<ChartData> = computed(() => ({
    datasets: this.datasets(),
    labels: this.labels()
  }));
  protected chartConfig: Signal<Partial<ChartConfiguration>> = computed(() => ({
    type: 'horizontalBar',
    plugins: [ChartDataLabels],
    options: {
      plugins: {
        datalabels: {
          color: 'black',
          formatter: value => {
            const ratio = toPrecision((value * 100) / this.total(), 2);
            return value > 0 ? `${toPrecision(value, 3)} (${ratio}%)` : '';
          },
          align: 'end'
        }
      },
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        enabled: false,
        callbacks: {
          title: (tooltipItems, data) => data.datasets![tooltipItems[0].datasetIndex!]?.label || '',
          label: tooltipItem => {
            const value = +(tooltipItem.value || '0');
            const ratio = toPrecision((value * 100) / this.total(), 2);
            return value > 0 ? `${toPrecision(value, 3)} (${ratio}%)` : '';
          }
        }
      },
      scales: {
        xAxes: [
          {
            display: true
          }
        ],
        yAxes: [
          {
            position: 'left'
          }
        ]
      }
    }
  }));

  constructor() {
    this.impactAssessment$
      .pipe(
        filter(v => !!v),
        distinctUntilChangedDeep(),
        take(1),
        tap(() => this.loading.set(true)),
        mergeMap(node => (node ? this.nodeService.getLog$({ ...node, dataState: DataState.recalculated }) : of(''))),
        map(value => (value ? parseLines(value) : [])),
        mergeAll(),
        filter(({ data }) => data.logger === 'hestia_earth.models' && data.level === Level.debug),
        filter(({ data: { message } }) => !!message),
        map(({ data: { message } }) => parseMessage(message)),
        filter(message => 'node' in message),
        map(parseLog),
        filter(log => !!log.impactTermId && !!log.blankNodeTermId && !isNaN(log.value) && log.value > 0),
        groupBy(log => [log.impactTermId, log.blankNodeTermId, log.modelId].join('/')),
        mergeMap(group => group.pipe(toArray())),
        map((values: ILog[]) => {
          const log = values[0];
          const total = values.reduce((prev, curr) => prev + curr.value, 0);
          const blankNodes = [
            ...(this.impactAssessment()?.emissionsResourceUse ?? []),
            ...(this.impactAssessment()?.impacts ?? [])
          ].filter(v => v.term['@id'] === log.blankNodeTermId);

          const inputs = blankNodes
            .filter(v => v.inputs?.length)
            .map(v => ({ name: v.inputs.map(i => i['@id']).join(';'), value: v.value * log.coefficient }));
          const inputsValue = inputs.reduce((prev, curr) => prev + curr.value, 0);

          const impact = this.impacts().find(v => v.term['@id'] === log.impactTermId);

          // logs might exist but impact was not added => skip logs
          return impact
            ? {
                ...log,
                impactTermUnits: impact.term?.units,
                value: total,
                inputs,
                inputsValue
              }
            : null;
        }),
        filter(log => !!log),
        toArray(),
        tap(() => this.loading.set(false))
      )
      .subscribe((logs: ILog[]) => this.allLogs.set(logs));

    effect(() => {
      // make sure selected term exists
      const terms = this.terms();
      const selectedTermId = this.selectedTerm()?.['@id'];
      if (!selectedTermId || !terms.find(term => term['@id'] === selectedTermId)) {
        this.selectedTerm.set(terms[0]);
      }
    });
  }

  protected selectTerm({ target: { value } }) {
    const term = this.terms().find(term => term['@id'] === value);
    this.selectedTerm.set(term);
  }

  protected selectMethod({ target: { value } }) {
    const term = this.methods().find(term => term['@id'] === value);
    this.selectedMethod.set(term);
  }
}
