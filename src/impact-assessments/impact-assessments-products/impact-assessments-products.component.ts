import { ChangeDetectionStrategy, Component, computed, effect, inject, input, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { DataState } from '@hestia-earth/api';
import { FormsModule } from '@angular/forms';
import {
  ICycleJSONLD,
  IImpactAssessmentJSONLD,
  Indicator,
  TermTermType,
  Term,
  NodeType,
  BlankNodesKey,
  IndicatorMethodTier,
  ITermJSONLD
} from '@hestia-earth/schema';
import { propertyValue } from '@hestia-earth/utils/dist/term';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbPopover, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IconDefinition, faCalculator, faChartBar, faDownload, faList } from '@fortawesome/free-solid-svg-icons';
import { unique } from '@hestia-earth/utils';
import orderBy from 'lodash.orderby';
import uniqBy from 'lodash.uniqby';

import {
  groupNodesByTerm,
  IGroupedKeys,
  grouppedKeys,
  isMethodModelAllowed,
  methodTierOrder
} from '../../common/node-utils';
import { schemaBaseUrl } from '../../common/utils';
import { PrecisionPipe } from '../../common/precision.pipe';
import { EllipsisPipe } from '../../common/ellipsis.pipe';
import { DefaultPipe } from '../../common/default.pipe';
import { CompoundPipe } from '../../common/compound.pipe';
import { KeyToLabelPipe } from '../../common/key-to-label.pipe';
import { BlankNodeStateNoticeComponent } from '../../common/blank-node-state-notice/blank-node-state-notice.component';
import { DataTableComponent } from '../../common/data-table/data-table.component';
import { BlankNodeStateComponent } from '../../common/blank-node-state/blank-node-state.component';
import { SearchExtendComponent } from '../../common/search-extend/search-extend.component';
import { HeNodeStoreService } from '../../node/node-store.service';
import { NodeValueDetailsComponent } from '../../node/node-value-details/node-value-details.component';
import { NodeCsvExportConfirmComponent } from '../../node/node-csv-export-confirm/node-csv-export-confirm.component';
import { NodeLogsModelsComponent } from '../../node/node-logs-models/node-logs-models.component';
import { ImpactAssessmentsIndicatorBreakdownChartComponent } from '../impact-assessments-indicator-breakdown-chart/impact-assessments-indicator-breakdown-chart.component';
import { ImpactAssessmentsIndicatorsChartComponent } from '../impact-assessments-indicators-chart/impact-assessments-indicators-chart.component';
import { TermsUnitsDescriptionComponent } from '../../terms/terms-units-description/terms-units-description.component';
import { NodeLinkComponent } from '../../node/node-link/node-link.component';

enum View {
  table = 'Table view',
  chart = 'Chart view',
  breakdown = 'Breakdown view',
  logs = 'Recalculations logs'
}

const viewIcon: {
  [view in View]: IconDefinition;
} = {
  [View.breakdown]: faChartBar,
  [View.chart]: faChartBar,
  [View.logs]: faCalculator,
  [View.table]: faList
};

const nodeKeyViews: {
  [type in BlankNodesKey]?: View[];
} = {
  [BlankNodesKey.emissionsResourceUse]: [View.table, View.chart, View.logs],
  [BlankNodesKey.impacts]: [View.table, View.breakdown, View.chart, View.logs],
  [BlankNodesKey.endpoints]: [View.table, View.breakdown, View.chart, View.logs]
};

type groupedEmissions = {
  [methodTier in IndicatorMethodTier]: IGroupedKeys<Indicator>[];
};

const termAllowed = (term?: Term | ITermJSONLD, filterTermTypes: TermTermType[] = []) =>
  !filterTermTypes?.length || (filterTermTypes || []).includes(term?.termType);

const filterNode = (filterByMethodModel: boolean, filterMethodModel?: Term, filterTerm?: string) => (node: Indicator) =>
  [
    !filterByMethodModel || isMethodModelAllowed(filterMethodModel)(node),
    !filterTerm || node?.term?.name?.toLowerCase()?.includes(filterTerm.toLowerCase())
  ].every(Boolean);

@Component({
  selector: 'he-impact-assessments-products',
  templateUrl: './impact-assessments-products.component.html',
  styleUrls: ['./impact-assessments-products.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FaIconComponent,
    SearchExtendComponent,
    DataTableComponent,
    FormsModule,
    NodeLinkComponent,
    TermsUnitsDescriptionComponent,
    NgbPopover,
    BlankNodeStateComponent,
    BlankNodeStateNoticeComponent,
    ImpactAssessmentsIndicatorsChartComponent,
    ImpactAssessmentsIndicatorBreakdownChartComponent,
    NodeLogsModelsComponent,
    NodeValueDetailsComponent,
    CompoundPipe,
    DefaultPipe,
    EllipsisPipe,
    PrecisionPipe,
    KeyToLabelPipe
  ]
})
export class ImpactAssessmentsProductsComponent {
  private readonly modalService = inject(NgbModal);
  private readonly nodeStoreService = inject(HeNodeStoreService);

  protected readonly faDownload = faDownload;

  protected readonly dataState = input(DataState.original);
  protected readonly nodeKey = input(BlankNodesKey.impacts);
  protected readonly filterTermTypes = input([] as TermTermType[]);
  protected readonly enableFilterMethodModel = input(false);

  protected readonly propertyValue = propertyValue;
  protected readonly schemaBaseUrl = schemaBaseUrl();

  protected readonly headerKeys = computed(() => [
    'impactAssessment.id',
    'impactAssessment.@id',
    `impactAssessment.${this.nodeKey()}.`
  ]);

  protected readonly View = View;
  protected readonly viewIcon = viewIcon;
  private readonly showView = computed(
    () =>
      ({
        [View.breakdown]: !this.isOriginal() && this.impactAssessments().length === 1,
        [View.chart]: this.impactAssessments().length > 1,
        [View.logs]: !this.isOriginal() && this.hasRecalculatedNodes(),
        [View.table]: true
      }) as { [view in View]: boolean }
  );
  protected readonly views = computed(() => nodeKeyViews[this.nodeKey()]?.filter(view => this.showView()[view]) ?? []);
  protected readonly selectedView = signal(View.table);

  private readonly cycles = toSignal(
    this.nodeStoreService.findByState$<ICycleJSONLD>(NodeType.Cycle, DataState.original)
  );

  private readonly originalImpactAssessments = toSignal(
    this.nodeStoreService.findByState$<IImpactAssessmentJSONLD>(NodeType.ImpactAssessment, DataState.original)
  );
  private readonly _allImpactAssessments = toSignal(
    this.nodeStoreService.find$<IImpactAssessmentJSONLD>(NodeType.ImpactAssessment)
  );
  protected readonly impactAssessments = computed(
    () => this._allImpactAssessments()?.map(data => data[this.dataState()]) || []
  );

  private readonly selectedIndex = signal(0);
  private readonly ogirinalSelectedImpactAssessment = computed(
    () => this.originalImpactAssessments()?.[this.selectedIndex()]
  );
  private readonly selectedImpactAssessment = computed(() => this.impactAssessments()?.[this.selectedIndex()]);
  protected readonly selectedOriginalValues = computed(
    () => this.ogirinalSelectedImpactAssessment()?.[this.nodeKey()] || []
  );
  protected readonly selectedRecalculatedValues = computed(
    () => this.selectedImpactAssessment()?.[this.nodeKey()] || []
  );
  protected readonly selectedNode = computed(() =>
    this.selectedImpactAssessment()
      ? {
          ...this.selectedImpactAssessment(),
          dataState: DataState.recalculated
        }
      : null
  );

  protected readonly isOriginal = computed(() => this.dataState() === DataState.original);
  private readonly originalValues = computed(() => (this.isOriginal() ? null : this.originalImpactAssessments()) || []);

  protected readonly isNodeKeyAllowed = computed(() => nodeKeyViews[this.nodeKey()].includes(this.selectedView()));

  private readonly shouldFilterByMethodModel = computed(() => this.enableFilterMethodModel() && !this.isOriginal());
  protected readonly filterTerm = signal('');

  protected readonly csvFilename = computed(() => `${this.nodeKey()}.csv`);

  protected readonly isEmission = computed(() => this.filterTermTypes().includes(TermTermType.emission));

  // aggregated IA have recalculated values as well
  private readonly hasRecalculatedNodes = computed(() => true);
  protected readonly showSwitchToRecalculated = computed(() => this.isOriginal() && this.hasRecalculatedNodes());
  protected readonly data = computed(() =>
    this.isEmission() ? this.groupEmissions() : { [this.nodeKey()]: this.groupIndicators() }
  );
  protected readonly dataKeys = computed(() => Object.keys(this.data()));

  // as we filter by methodModel, we might have an empty table for the default models but not for other models
  protected readonly hasData = computed(() =>
    this.enableFilterMethodModel() ? this.methodModels().length : Object.values(this.data()).flat().length > 0
  );

  protected readonly noDataMessage = computed(() =>
    this.nodeKey() === BlankNodesKey.emissionsResourceUse
      ? 'We cannot calculate these data, most likely because we are missing the quantity produced or because we are missing data to allocate environmental impact data between co-products.'
      : undefined
  );

  protected readonly methodModels = computed(() =>
    uniqBy(
      this.impactAssessments()
        .flatMap(impact => (impact[this.nodeKey()] || []).map(v => v.methodModel))
        .filter(Boolean) as Term[],
      '@id'
    ).sort((a, b) => a.name.localeCompare(b.name))
  );

  private readonly _selectedMethodModel = signal(undefined as Term);

  protected groupIndicators() {
    // TODO: when there are no indicators for the default model, the table appears empty
    // should default back to the first model in this case
    const indicatorPerImpactAssessment = groupNodesByTerm<IImpactAssessmentJSONLD, Indicator>(
      this.impactAssessments(),
      this.nodeKey(),
      this.originalValues(),
      filterNode(this.shouldFilterByMethodModel(), this._selectedMethodModel(), this.filterTerm())
    );
    return orderBy(grouppedKeys(indicatorPerImpactAssessment), ['value.methodTierOrder', 'key'], ['asc', 'asc']).filter(
      ({ value }) => termAllowed(value?.term, this.filterTermTypes())
    ) as IGroupedKeys<Indicator>[];
  }

  private groupEmissions() {
    const emissionsPerImpactAssessment = groupNodesByTerm<IImpactAssessmentJSONLD, Indicator>(
      this.impactAssessments(),
      this.nodeKey(),
      this.originalValues(),
      filterNode(this.shouldFilterByMethodModel(), this._selectedMethodModel(), this.filterTerm())
    );
    const values: IGroupedKeys<Indicator>[] = orderBy(grouppedKeys(emissionsPerImpactAssessment), ['key'], ['asc']);
    const methodTiers = orderBy(
      unique(values.map(({ value }) => value.methodTier)).map(methodTier => ({
        methodTier,
        methodTierOrder: methodTierOrder(methodTier)
      })),
      ['methodTierOrder'],
      ['asc']
    );
    return Object.fromEntries(
      methodTiers.map(({ methodTier }) => [
        methodTier || '',
        values.filter(
          ({ value }) => value.methodTier === methodTier && termAllowed(value?.term, this.filterTermTypes())
        )
      ])
    ) as groupedEmissions;
  }

  constructor() {
    effect(() => {
      // make sure logs does not remain displayed
      if ((this.isOriginal() && this.selectedView() === View.logs) || !this.views().includes(this.selectedView())) {
        this.selectedView.set(View.table);
      }
    });
  }

  protected trackById(_index: number, item: IImpactAssessmentJSONLD) {
    return item['@id'];
  }

  protected updateSelectedMethodModel(termName: string) {
    const term = this.methodModels().find(({ name }) => name === termName);
    this._selectedMethodModel.set(term);
  }

  protected impactName(impact: IImpactAssessmentJSONLD) {
    return impact.name || (impact.cycle ? this.cycleLabel(impact.cycle) : impact['@id']) || (impact as any).id;
  }

  protected cycleLabel({ '@id': id, name }: any) {
    return name || this.cycles().find(v => v['@id'] === id)?.name;
  }

  protected selectIndex({ target: { value } }) {
    this.selectedIndex.set(+value);
  }

  protected showDownload() {
    const instance = this.modalService.open(NodeCsvExportConfirmComponent);
    const component = instance.componentInstance as NodeCsvExportConfirmComponent;
    component.nodes.set(this.impactAssessments());
    component.filename.set(this.csvFilename());
    component.isUpload.set(false);
    component.headerKeys.set(this.headerKeys());
  }
}
