import { Component, computed, ChangeDetectionStrategy, signal, effect, input, inject, Signal } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { toSignal } from '@angular/core/rxjs-interop';
import { DataState } from '@hestia-earth/api';
import { IImpactAssessmentJSONLD, Indicator, ITermJSONLD, NodeType, Term, TermTermType } from '@hestia-earth/schema';
import { toPrecision } from '@hestia-earth/utils';
import { ChartConfiguration, ChartData } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';

import { HeNodeStoreService } from '../../node/node-store.service';
import { groupNodesByTerm, IGroupedNodesValues } from '../../common/node-utils';
import { defaultLabel } from '../../common/utils';
import { listColor } from '../../common/color';
import { modelKey } from '../impact-assessments.model';
import { ChartComponent } from '../../chart/chart.component';

const impactValue = (impact: IImpactAssessmentJSONLD, values: IGroupedNodesValues<Indicator>) =>
  (values[impact['@id']]?.nodes[0] || { value: 0 }).value;

const impactName = (impact: IImpactAssessmentJSONLD, index: number) =>
  `${index + 1}. ${defaultLabel(impact)} (${impact.product?.term?.units})`;

const termAllowed = (filterTermTypes: TermTermType[] = [], term?: Term | ITermJSONLD) =>
  !filterTermTypes?.length || (filterTermTypes || []).includes(term?.termType);

@Component({
  selector: 'he-impact-assessments-indicators-chart',
  templateUrl: './impact-assessments-indicators-chart.component.html',
  styleUrls: ['./impact-assessments-indicators-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FormsModule, ChartComponent]
})
export class ImpactAssessmentsIndicatorsChartComponent {
  private nodeStoreService = inject(HeNodeStoreService);

  protected key = input('impacts' as modelKey);
  protected filterTermTypes = input([] as TermTermType[]);

  private impactAssessments = toSignal(
    this.nodeStoreService.findByState$<IImpactAssessmentJSONLD>(NodeType.ImpactAssessment, DataState.recalculated)
  );

  private indicatorPerImpactAssessment = computed(() =>
    groupNodesByTerm<IImpactAssessmentJSONLD, Indicator>(this.impactAssessments(), this.key())
  );

  protected terms = computed(() =>
    Object.values(this.indicatorPerImpactAssessment() ?? {})
      .filter(
        ({ term, values }) =>
          termAllowed(this.filterTermTypes(), term) &&
          Object.values(values).some(({ nodes: [{ value }] }) => value >= 0)
      )
      .map(({ term }: any) => term as Term)
      .sort((a, b) => a.name.localeCompare(b.name))
  );

  private selectedTerm = signal(undefined as Term);
  private labels = computed(() => this.impactAssessments().map(impactName));
  private colors = computed(() => this.impactAssessments().map(listColor));
  private values = computed(() => this.indicatorPerImpactAssessment()?.[this.selectedTerm()?.name]?.values || {});
  private datasets = computed(() => [
    {
      label: this.selectedTerm()?.name || '',
      data: this.impactAssessments().map(impact => impactValue(impact, this.values())),
      backgroundColor: this.colors(),
      borderColor: this.colors()
    }
  ]);
  protected chartData: Signal<ChartData> = computed(() => ({
    datasets: this.datasets(),
    labels: this.labels()
  }));
  protected chartConfig: Partial<ChartConfiguration> = {
    type: 'bar',
    plugins: [ChartDataLabels],
    options: {
      plugins: {
        datalabels: {
          color: 'black',
          formatter: value => toPrecision(value, 3),
          anchor: 'center',
          clamp: true
        }
      },
      tooltips: {
        enabled: false
      },
      scales: {
        xAxes: [
          {
            display: true
          }
        ],
        yAxes: [
          {
            position: 'left',
            ticks: {
              min: 0
            }
          }
        ]
      }
    }
  };

  constructor() {
    effect(() => {
      // make sure selected term exists
      const terms = this.terms();
      const selectedTermId = this.selectedTerm()?.['@id'];
      if (!selectedTermId || !terms.find(term => term['@id'] === selectedTermId)) {
        this.selectedTerm.set(terms[0]);
      }
    });
  }

  protected selectTerm({ target: { value } }) {
    const term = this.terms().find(term => term['@id'] === value);
    this.selectedTerm.set(term);
  }
}
