import { NodeType, IImpactAssessmentJSONLD } from '@hestia-earth/schema';

export type modelKey = 'emissionsResourceUse' | 'endpoints' | 'impacts';

export interface IImpactAssessmentJSONLDExtended extends IImpactAssessmentJSONLD {
  related: {
    [type in NodeType]?: string[];
  };
}
