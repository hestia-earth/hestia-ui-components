import { Component, inject, signal, input, effect, computed } from '@angular/core';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { filter, groupBy, map, mergeMap, catchError, tap, toArray, startWith } from 'rxjs/operators';
import { combineLatest, from, Observable, of } from 'rxjs';
import { DataState } from '@hestia-earth/api';
import { IImpactAssessmentJSONLD, Indicator, ITermJSONLD, NodeType, TermTermType } from '@hestia-earth/schema';
import { getDefaultModelId } from '@hestia-earth/glossary';
import { isEmpty, unique } from '@hestia-earth/utils';
import { FormsModule } from '@angular/forms';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExclamationTriangle, faSpinner } from '@fortawesome/free-solid-svg-icons';

import { ILine, Level, parseLines, parseMessage } from '../../common/logs-utils';
import { distinctUntilChangedDeep } from '../../common/rxjs-utils';
import { findMatchingModel } from '../../engine/engine.service';
import { HeNodeService } from '../../node/node.service';
import { HeSearchService } from '../../search/search.service';
import { matchTermType, matchExactQuery, matchType } from '../../search/search.model';
import {
  getUnitsAndLabel,
  IChartData,
  IChartInputLog,
  ITermMapping,
  term
} from '../../chart/hierarchy-chart/hierarchy-chart.model';
import { HierarchyChartComponent } from '../../chart/hierarchy-chart/hierarchy-chart.component';

const filterTermTypes = [
  TermTermType.resourceUse,
  TermTermType.emission,
  TermTermType.characterisedIndicator,
  TermTermType.endpointIndicator,
  TermTermType.model
];

const searchParams = (should: any[]) => ({
  fields: ['@type', '@id', 'name', 'units', 'termType'],
  limit: 9999, // do not use scroll
  query: {
    bool: {
      must: [matchType(NodeType.Term)],
      should,
      minimum_should_match: 1
    }
  }
});

const parseLog = (data: any): IChartData => ({
  indicator: data['key/term'] || data.term || data.indicator,
  contributor: data.node,
  coefficient: +data.coefficient,
  modelId: data.model || 'Unknown',
  weightedValue: +data.value * +data.coefficient,
  value: +data.value
});

const defaultIndicatorsModelId = 'defaultIndicatorsModelId';
const defaultIndicatorsModelName = 'Default Indicators (multi-model)';

const warnings = (logs: IChartData[], terms: { [id: string]: term }) =>
  [!logs.every(log => terms[log.indicator] && terms[log.contributor]) && 'missing-terms'].filter(Boolean);

const filterLog = (indicators: Indicator[], log: IChartData) =>
  [
    !!log.indicator,
    !!log.contributor,
    !isNaN(log.weightedValue),
    log.coefficient > 0,
    indicators.some(eru => eru.term['@id'] === log.indicator)
  ].every(Boolean);

const parseLogValues = (impactAssessment: IImpactAssessmentJSONLD, values: IChartData[]) => {
  const log = values[0];
  const { weightedTotal, total } = values.reduce(
    (prev, curr) => ({
      weightedTotal: prev.weightedTotal + curr.weightedValue,
      total: prev.total + curr.value
    }),
    { weightedTotal: 0, total: 0 }
  );

  const inputsObj = (impactAssessment.emissionsResourceUse || [])
    .filter(eru => eru.term['@id'] === log.contributor && eru.inputs?.length)
    .reduce((acc, eru) => {
      const inputId = eru.inputs.map(i => i['@id']).join(';');
      const currentOperations = eru.operation
        ? [
            {
              id: eru.operation['@id'],
              ...getUnitsAndLabel(eru.operation),
              modelId: log.modelId,
              weightedValue: eru.value
            }
          ]
        : [];

      acc[inputId] = acc[inputId]
        ? {
            ...acc[inputId],
            weightedValue: acc[inputId].weightedValue + eru.value,
            modelId: log.modelId,
            operations: [...acc[inputId].operations, ...currentOperations]
          }
        : {
            id: inputId,
            ...getUnitsAndLabel(eru.inputs.length === 1 ? eru.inputs[0] : { name: 'Input group (under development)' }),
            modelId: log.modelId,
            weightedValue: eru.value,
            operations: [...currentOperations]
          };
      return acc;
    }, {});
  const inputs: IChartInputLog[] = Object.values(inputsObj);
  return { ...log, weightedValue: weightedTotal, value: total, inputs } as IChartData;
};

const parseNodeLogs = (impactAssessment: IImpactAssessmentJSONLD, indicators: Indicator[], lines: ILine[]) =>
  from(lines).pipe(
    filter(({ data }) => data.logger === 'hestia_earth.models' && data.level === Level.debug),
    filter(({ data: { message } }) => !!message),
    map(({ data: { message } }) => parseMessage(message)),
    filter(message => 'node' in message),
    map(data => parseLog(data)),
    filter(log => filterLog(indicators, log)),
    groupBy(log => [log.indicator, log.contributor, log.modelId].join('/')),
    mergeMap(group => group.pipe(toArray())),
    map(data => parseLogValues(impactAssessment, data)),
    toArray()
  ) as any as Observable<IChartData[]>;

@Component({
  selector: 'he-impact-assessments-graph',
  templateUrl: './impact-assessments-graph.component.html',
  styleUrls: ['./impact-assessments-graph.component.scss'],
  imports: [FaIconComponent, FormsModule, HierarchyChartComponent]
})
export class ImpactAssessmentsGraphComponent {
  private readonly searchService = inject(HeSearchService);
  private readonly nodeService = inject(HeNodeService);

  protected readonly faSpinner = faSpinner;
  protected readonly faExclamationTriangle = faExclamationTriangle;

  private readonly initialTerms = toSignal(
    this.searchService
      .search$<ITermJSONLD, NodeType.Term>(searchParams(filterTermTypes.map(termType => matchTermType(termType))))
      .pipe(
        mergeMap(({ results }) =>
          from(results).pipe(
            map(term => {
              const modelDocs = term.termType === TermTermType.model ? findMatchingModel({ model: term['@id'] }) : null;
              return {
                ...term,
                docs: modelDocs?.apiDocsPath || modelDocs?.docPath || modelDocs?.path
              } as term;
            }),
            toArray()
          )
        ),
        map(terms => Object.fromEntries(terms.map(term => [term['@id'], term])) as ITermMapping)
      )
  );
  private readonly initialTerms$ = toObservable(this.initialTerms);

  protected readonly impactAssessments = input<IImpactAssessmentJSONLD[]>([]);
  protected readonly dataState = input(DataState.recalculated);

  protected readonly loading = signal(true);
  protected readonly isRecalculated = computed(() => this.dataState() === DataState.recalculated);

  protected readonly filteredImpactAssessments = computed(
    () => this.impactAssessments()?.filter(ia => ia.endpoints?.length || ia.impacts?.length) ?? []
  );
  protected readonly selectedImpactAssessmentId = signal<string>(undefined);
  private readonly selectedImpactAssessment = computed(
    () =>
      this.filteredImpactAssessments()?.find(ia => ia['@id'] === this.selectedImpactAssessmentId()) ||
      this.filteredImpactAssessments()?.[0]
  );
  private readonly indicators = computed(() => [
    ...(this.selectedImpactAssessment()?.endpoints ?? []),
    ...(this.selectedImpactAssessment()?.impacts ?? [])
  ]);

  private readonly nodeLogsRaw$ = toObservable(this.selectedImpactAssessment).pipe(
    distinctUntilChangedDeep(),
    filter(v => !isEmpty(v)),
    tap(() => this.loading.set(true)),
    mergeMap(node =>
      this.nodeService.getLog$({
        ...node,
        dataState: DataState.recalculated
      })
    ),
    map(value => (value ? parseLines(value) : ([] as ILine[]))),
    startWith([] as ILine[])
  );

  private readonly nodeLogs$ = combineLatest([
    this.nodeLogsRaw$,
    toObservable(this.isRecalculated),
    toObservable(this.indicators),
    toObservable(this.selectedImpactAssessment)
  ]).pipe(
    distinctUntilChangedDeep(),
    filter(([logs, isRecalculated, indicators]) =>
      [!!logs?.length, isRecalculated, !!indicators?.length].every(Boolean)
    ),
    mergeMap(([logs, _isRecalculated, indicators, impactAssessment]) =>
      parseNodeLogs(impactAssessment, indicators, logs)
    ),
    catchError(err => {
      console.error(err);
      this.error.set(err.toString());
      return of([] as IChartData[]);
    }),
    startWith([] as IChartData[])
  );

  private readonly nodeLogs = toSignal(this.nodeLogs$);

  protected readonly allTerms = toSignal(
    combineLatest([this.nodeLogs$, this.initialTerms$]).pipe(
      distinctUntilChangedDeep(),
      filter(values => values.every(v => !isEmpty(v))),
      mergeMap(([logs, terms]) =>
        this.searchService
          .search$<
            ITermJSONLD,
            NodeType.Term
          >(searchParams(unique(logs.flatMap(l => [l.contributor, l.indicator]).filter(termId => !terms?.[termId])).map(id => matchExactQuery('@id', id))))
          .pipe(
            map(({ results }) => ({
              ...(Object.fromEntries(results.map(term => [term['@id'], term])) as ITermMapping),
              ...terms
            }))
          )
      ),
      startWith({} as ITermMapping),
      tap(() => this.loading.set(false))
    )
  );

  protected readonly noData = computed(() => !this.nodeLogs()?.length);
  protected readonly warnings = computed(() =>
    [this.nodeLogs(), this.allTerms()].some(v => isEmpty(v)) ? [] : warnings(this.nodeLogs(), this.allTerms())
  );
  protected readonly showWarnings = signal(false);
  protected readonly error = signal<string>(undefined);

  protected readonly selectedModelId = signal<string>(undefined);
  private readonly hasDefaultModelLogs = computed(() =>
    this.nodeLogs()?.some(log => log.modelId === getDefaultModelId(log.indicator))
  );
  protected readonly models = computed(
    () =>
      [
        ...(this.hasDefaultModelLogs() ? [{ '@id': defaultIndicatorsModelId, name: defaultIndicatorsModelName }] : []),
        ...unique(this.nodeLogs().map(({ modelId }) => this.allTerms()[modelId] || { '@id': modelId }))
      ] as Partial<ITermJSONLD>[]
  );

  protected readonly chartData = computed(() =>
    this.nodeLogs().filter(
      log =>
        log.modelId ===
        (this.selectedModelId() === defaultIndicatorsModelId
          ? getDefaultModelId(log.indicator)
          : this.selectedModelId())
    )
  );
  protected readonly showChart = computed(() =>
    [!isEmpty(this.chartData()), !isEmpty(this.allTerms()), !this.loading(), !this.error(), !this.showWarnings()].every(
      Boolean
    )
  );

  constructor() {
    effect(() => {
      const defaultId = this.filteredImpactAssessments()?.[0]?.['@id'];
      if (!this.selectedImpactAssessmentId() && defaultId) {
        this.selectedImpactAssessmentId.set(defaultId);
      }
    });

    effect(() => {
      if (!this.selectedModelId()) {
        this.selectedModelId.set(this.models()?.[0]?.['@id'] || defaultIndicatorsModelId);
      }
    });

    effect(() => this.showWarnings.set(this.warnings()?.length > 0));
  }
}
