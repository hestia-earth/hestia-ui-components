import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  inject,
  Input,
  Output
} from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

import { SELECT_TOKEN } from '../select.token';
import { SELECT_OPTION_TOKEN, SelectOptionToken } from './select-option.token';
import { NgIf } from '@angular/common';

@Component({
  selector: 'he-select-option',
  templateUrl: './select-option.component.html',
  styleUrls: ['./select-option.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SELECT_OPTION_TOKEN,
      useExisting: forwardRef(() => SelectOptionComponent)
    }
  ],
  imports: [NgIf]
})
export class SelectOptionComponent<T> implements SelectOptionToken<T> {
  static nextId = 0;

  @Input() value: T | null = null;

  @Input() staticChecked: boolean | undefined;

  @Input()
  @HostBinding('class.disabled')
  disabled = false;

  @Output() selected = new EventEmitter<SelectOptionToken<T>>();

  @HostBinding('class.selected') isSelected = false;
  @HostBinding('attr.id') id = `option-${SelectOptionComponent.nextId++}`;
  private _cd = inject(ChangeDetectorRef);
  private _parent = inject(SELECT_TOKEN);

  protected get isChecked() {
    if (this.staticChecked !== undefined) {
      return this.staticChecked;
    }

    return this.isSelected;
  }

  @HostListener('click') select() {
    if (!this.disabled) {
      this.highlightAsSelected();
      this.selected.emit(this);
    }
  }

  highlightAsSelected() {
    this.isSelected = true;
    this._cd.markForCheck();
  }

  deselect() {
    this.isSelected = false;
    this._cd.markForCheck();
  }

  checkboxClicked($event: any) {
    $event.stopPropagation();
    this.select();
  }

  protected isMultiple() {
    return this._parent && coerceBooleanProperty(this._parent.isMultiple());
  }
}
