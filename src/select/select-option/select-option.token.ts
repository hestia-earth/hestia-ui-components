import { EventEmitter, InjectionToken } from '@angular/core';

export interface SelectOptionToken<T> {
  value: T | null;
  staticChecked: boolean | undefined;
  disabled: boolean;
  selected: EventEmitter<SelectOptionToken<T>>;
  isSelected: boolean;
  id: string;

  select(): void;

  highlightAsSelected(): void;

  deselect(): void;

  checkboxClicked($event: any): void;
}

export const SELECT_OPTION_TOKEN = new InjectionToken<SelectOptionToken<any>>('SELECT_OPTION_TOKEN');
