import { InjectionToken } from '@angular/core';
import { Highlightable } from '@angular/cdk/a11y';

export const NODE_ELEMENT_TOKEN = new InjectionToken<NodeElementToken>('Node element token');

export interface NodeElementToken<T = any> extends Highlightable {
  isActive: boolean;
  readonly isVisible: boolean;
  readonly isOption: boolean;
  readonly isOptionGroup: boolean;
  readonly value: T;

  expanded(): any;

  showChildren(): void;

  hideChildren(): void;

  scrollIntoView(options?: ScrollIntoViewOptions): void;

  enterClick(): void;
}
