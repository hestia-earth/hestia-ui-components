import { FilterData } from '../filter.model';

const getChildrenOptions = (option: FilterData): FilterData[] =>
  option.type === 'group' && option.options ? [option, ...option.options.flatMap(getChildrenOptions)] : [option];

export const flatFilterData = (nodes?: FilterData[]): FilterData[] => nodes.flatMap(getChildrenOptions);

export const flatFilterNode = (node?: FilterData): FilterData[] =>
  [node, ...(node?.type === 'group' ? [node.options?.flatMap(flatFilterNode)] : [])].flat().filter(Boolean);
