import { FilterData } from '../filter.model';
import { searchFilterData } from './search-filter-data';

describe('searchFilterData', () => {
  it('should filter 1 level tree', () => {
    const simpleFilterData: FilterData[] = [
      {
        type: 'option',
        label: 'option1',
        value: 'option1'
      },
      {
        type: 'option',
        label: 'option2',
        value: 'option2'
      },
      {
        type: 'option',
        label: 'option3',
        value: 'option3'
      }
    ];

    const filterFn = (node: FilterData) => {
      return node.label === 'option1';
    };

    const filteredData = searchFilterData(simpleFilterData, filterFn);

    expect(filteredData).toEqual([
      {
        type: 'option',
        label: 'option1',
        value: 'option1'
      }
    ]);
  });

  it('should filter 2 level tree', () => {
    const simpleFilterData: FilterData[] = [
      {
        type: 'group',
        label: 'root',
        options: [
          {
            type: 'option',
            label: 'option1',
            value: 'option1'
          },
          {
            type: 'option',
            label: 'option2',
            value: 'option2'
          }
        ]
      }
    ];

    const filterFn = (node: FilterData) => {
      return node.label === 'option1';
    };

    const filteredData = searchFilterData(simpleFilterData, filterFn);

    expect(filteredData).toEqual([
      {
        type: 'group',
        label: 'root',
        options: [
          {
            type: 'option',
            label: 'option1',
            value: 'option1'
          }
        ]
      }
    ]);
  });

  it('should filter 3 level tree', () => {
    const simpleFilterData: FilterData[] = [
      {
        type: 'group',
        label: 'root',
        options: [
          {
            type: 'option',
            label: 'option1',
            value: 'option1'
          },
          {
            type: 'group',
            label: 'group1',
            options: [
              {
                type: 'option',
                label: 'option2',
                value: 'option2'
              }
            ]
          }
        ]
      }
    ];

    const filterFn = (node: FilterData) => {
      return node.label === 'option1';
    };

    const filteredData = searchFilterData(simpleFilterData, filterFn);

    expect(filteredData).toEqual([
      {
        type: 'group',
        label: 'root',
        options: [
          {
            type: 'option',
            label: 'option1',
            value: 'option1'
          }
        ]
      }
    ]);
  });

  it('should filter 4 level tree', () => {
    const simpleFilterData: FilterData[] = [
      {
        type: 'group',
        label: 'root',
        options: [
          {
            type: 'option',
            label: 'option1',
            value: 'option1'
          },
          {
            type: 'group',
            label: 'group1',
            options: [
              {
                type: 'option',
                label: 'option2',
                value: 'option2'
              },
              {
                type: 'group',
                label: 'group2',
                options: [
                  {
                    type: 'option',
                    label: 'option3',
                    value: 'option3'
                  }
                ]
              }
            ]
          }
        ]
      }
    ];

    const filterFn = (node: FilterData) => {
      return node.label === 'option1';
    };

    const filteredData = searchFilterData(simpleFilterData, filterFn);

    expect(filteredData).toEqual([
      {
        type: 'group',
        label: 'root',
        options: [
          {
            type: 'option',
            label: 'option1',
            value: 'option1'
          }
        ]
      }
    ]);
  });

  it('should filter 4 level tree with multiple nodes', () => {
    const simpleFilterData: FilterData[] = [
      {
        type: 'group',
        label: 'root',
        options: [
          {
            type: 'option',
            label: 'option1',
            value: 'option1'
          },
          {
            type: 'group',
            label: 'group1',
            options: [
              {
                type: 'option',
                label: 'option2',
                value: 'option2'
              },
              {
                type: 'group',
                label: 'group2',
                options: [
                  {
                    type: 'option',
                    label: 'option3',
                    value: 'option3'
                  }
                ]
              }
            ]
          }
        ]
      }
    ];

    const filterFn = (node: FilterData) => {
      return node.label === 'option1' || node.label === 'option3';
    };

    const filteredData = searchFilterData(simpleFilterData, filterFn);

    expect(filteredData).toEqual([
      {
        type: 'group',
        label: 'root',
        options: [
          {
            type: 'option',
            label: 'option1',
            value: 'option1'
          },
          {
            type: 'group',
            label: 'group1',
            options: [
              {
                type: 'group',
                label: 'group2',
                options: [
                  {
                    type: 'option',
                    label: 'option3',
                    value: 'option3'
                  }
                ]
              }
            ]
          }
        ]
      }
    ]);
  });
});
