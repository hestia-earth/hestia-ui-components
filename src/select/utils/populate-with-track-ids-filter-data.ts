import { v4 as uuidv4 } from 'uuid';

import { FilterData } from '../filter.model';
import { mapFilterData } from './map-filter-data';

export const populateWithTrackIdsFilterData = (data: FilterData[]) =>
  mapFilterData(data, d => ({ ...d, trackId: uuidv4() }));
