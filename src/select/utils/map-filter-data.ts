import { FilterData } from '../filter.model';

const mapTree =
  (fn: (node: FilterData) => FilterData) =>
  (root: FilterData): FilterData => {
    const children = root.type === 'group' ? root.options?.map(mapTree(fn)) : [];

    return root.type === 'group'
      ? ({
          ...fn(root),
          options: children
        } as FilterData)
      : fn(root);
  };

export const mapFilterData = (data: FilterData[], fn: (node: FilterData) => FilterData) => data.map(mapTree(fn));
