import { FilterData } from '../filter.model';

const filterTree =
  (filterFn: (node: FilterData) => boolean) =>
  (root?: FilterData): FilterData | undefined => {
    const children = root?.type === 'group' ? root.options?.map(filterTree(filterFn)).filter(Boolean) : [];

    return !filterFn(root) && !children?.length
      ? undefined
      : root.type === 'group'
        ? ({
            ...root,
            options: children
          } as FilterData)
        : root;
  };

export const searchFilterData = (data: FilterData[], filterFn: (node: FilterData) => boolean): FilterData[] =>
  data.map(filterTree(filterFn)).filter(Boolean);
