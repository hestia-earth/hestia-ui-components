import { mapFilterData } from './map-filter-data';
import { FilterData } from '../filter.model';

describe('mapFilterData', () => {
  it('should map simple tree', () => {
    const data: FilterData = {
      type: 'group',
      label: 'root',
      options: [
        {
          type: 'option',
          label: 'option1',
          value: 'option1'
        },
        {
          type: 'option',
          label: 'option2',
          value: 'option2'
        }
      ]
    };
    const mapFn = node => {
      return {
        ...node,
        label: 'new label'
      };
    };
    const mappedData = mapFilterData([data], mapFn);
    expect(mappedData).toEqual([
      {
        type: 'group',
        label: 'new label',
        options: [
          {
            type: 'option',
            label: 'new label',
            value: 'option1'
          },
          {
            type: 'option',
            label: 'new label',
            value: 'option2'
          }
        ]
      }
    ]);
  });
  it('should map 2 level tree', () => {
    const data: FilterData = {
      type: 'group',
      label: 'root',
      options: [
        {
          type: 'option',
          label: 'option1',
          value: 'option1'
        },
        {
          type: 'group',
          label: 'group1',
          options: [
            {
              type: 'option',
              label: 'option2',
              value: 'option2'
            }
          ]
        }
      ]
    };
    const mapFn = node => {
      return {
        ...node,
        label: 'new label'
      };
    };
    const mappedData = mapFilterData([data], mapFn);
    expect(mappedData).toEqual([
      {
        type: 'group',
        label: 'new label',
        options: [
          {
            type: 'option',
            label: 'new label',
            value: 'option1'
          },
          {
            type: 'group',
            label: 'new label',
            options: [
              {
                type: 'option',
                label: 'new label',
                value: 'option2'
              }
            ]
          }
        ]
      }
    ]);
  });
  it('should map 3 level tree', () => {
    const data: FilterData = {
      type: 'group',
      label: 'root',
      options: [
        {
          type: 'option',
          label: 'option1',
          value: 'option1'
        },
        {
          type: 'group',
          label: 'group1',
          options: [
            {
              type: 'option',
              label: 'option2',
              value: 'option2'
            },
            {
              type: 'group',
              label: 'group2',
              options: [
                {
                  type: 'option',
                  label: 'option3',
                  value: 'option3'
                }
              ]
            }
          ]
        }
      ]
    };
    const mapFn = node => {
      return {
        ...node,
        label: 'new label'
      };
    };
    const mappedData = mapFilterData([data], mapFn);
    expect(mappedData).toEqual([
      {
        type: 'group',
        label: 'new label',
        options: [
          {
            type: 'option',
            label: 'new label',
            value: 'option1'
          },
          {
            type: 'group',
            label: 'new label',
            options: [
              {
                type: 'option',
                label: 'new label',
                value: 'option2'
              },
              {
                type: 'group',
                label: 'new label',
                options: [
                  {
                    type: 'option',
                    label: 'new label',
                    value: 'option3'
                  }
                ]
              }
            ]
          }
        ]
      }
    ]);
  });
});
