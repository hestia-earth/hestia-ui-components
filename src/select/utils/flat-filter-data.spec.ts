import { flatFilterNode } from './flat-filter-data';
import { FilterData } from '../filter.model';

describe('flattenTree', () => {
  it('should flat simple tree', () => {
    const data: FilterData = {
      type: 'group',
      label: 'root',
      options: [
        {
          type: 'option',
          label: 'option1',
          value: 'option1'
        },
        {
          type: 'option',
          label: 'option2',
          value: 'option2'
        }
      ]
    };
    const flattenData = flatFilterNode(data);
    expect(flattenData.length).toEqual(3);
  });

  it('should flat 2 level  tree', () => {
    const data: FilterData = {
      type: 'group',
      label: 'root',
      options: [
        {
          type: 'option',
          label: 'option1',
          value: 'option1'
        },
        {
          type: 'group',
          label: 'group1',
          options: [
            {
              type: 'option',
              label: 'option2',
              value: 'option2'
            }
          ]
        }
      ]
    };
    const flattenData = flatFilterNode(data);
    expect(flattenData.length).toEqual(4);
  });
});
