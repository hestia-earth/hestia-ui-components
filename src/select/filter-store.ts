import { ComponentStore } from '@ngrx/component-store';
import { Injectable } from '@angular/core';
import castArray from 'lodash.castarray';
import keyBy from 'lodash.keyby';

import { flatFilterData, mapFilterData, populateWithTrackIdsFilterData, searchFilterData } from './utils';
import { FilterData, FilterOption, FilterState, initialFilterState } from './filter.model';
import { SelectValue } from './select.model';

@Injectable()
export class FilterStore extends ComponentStore<FilterState> {
  readonly setValue = this.updater((state, value: SelectValue<any>) => ({
    ...state,
    value
  }));

  readonly setData = this.updater((state, data: FilterData[]) => ({
    ...state,
    data: populateWithTrackIdsFilterData(data)
  }));

  readonly flattenOptions = this.selectSignal(({ data }) => {
    const flattenData = flatFilterData(data);
    return flattenData.filter(item => item.type === 'option') as FilterOption[];
  });
  // Flatten options dictionary key by value
  readonly flattenOptionsDictionary = this.selectSignal(this.flattenOptions, flattenOptions => {
    return keyBy(flattenOptions, 'value');
  });
  readonly value = this.selectSignal(({ value }) => value);
  readonly selectedOptions = this.selectSignal(
    this.flattenOptionsDictionary,
    this.value,
    (flatOptionsDictionary, value) => {
      return castArray(value ?? [])
        .map(value => flatOptionsDictionary[value])
        .filter(Boolean);
    }
  );
  readonly filteredData = this.selectSignal(({ filter, filterFn, data, mapFn }) => {
    const filteredData = this._getFilteredData(filter, filterFn, data);

    if (mapFn) {
      return mapFilterData(filteredData, mapFn);
    }

    return filteredData;
  });

  constructor() {
    super(initialFilterState);
  }

  private _getFilteredData(
    filter: string,
    filterFn: (node: FilterData, filter: string) => boolean,
    data: FilterData[]
  ) {
    return filter.trim().length
      ? this.flattenOptions().filter(o => filterFn(o, filter))
      : searchFilterData(data, item => filterFn(item, filter));
  }
}
