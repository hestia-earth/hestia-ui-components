import { SelectValue } from './select.model';

export type FilterData = FilterOption | FilterGroup;

export interface FilterElement {
  label: string;
  trackId?: string;
}

export interface FilterOption extends FilterElement {
  value: string;
  type: 'option';
}

// export interface FilterToggleOption extends FilterElement {
//   type: 'toggle';
//   value: string;
// }

export interface FilterGroup extends FilterElement {
  type: 'group';
  toggleButton?: boolean;
  options: FilterData[];
}

export type FilterFn = (node: FilterData, filter: string) => boolean;

export interface FilterState {
  limit: number;
  filter: string;
  data: FilterData[];
  filterFn?: FilterFn;
  mapFn: (node: FilterData) => FilterData;
  value: SelectValue<any>;
}

export const initialFilterState: FilterState = {
  limit: 100,
  filter: '',
  data: [],
  mapFn: null,
  filterFn: (item: FilterData, filter: string) =>
    item.type === 'group' ||
    (item.type === 'option' && item.label.toLowerCase().includes(filter)) ||
    item.value.toLowerCase().includes(filter),
  value: null
};
