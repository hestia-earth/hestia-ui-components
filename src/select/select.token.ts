import { EventEmitter, InjectionToken } from '@angular/core';
import { ActiveDescendantKeyManager } from '@angular/cdk/a11y';

import { SelectValue } from './select.model';
import { SelectOptionToken } from './select-option/select-option.token';
import { NodeElementToken } from './node-element.token';

export const SELECT_TOKEN = new InjectionToken<SelectToken<any>>('SELECT_TOKEN');

export interface SelectToken<T> {
  placeholder: string;
  searchable: boolean;
  disabled: boolean;
  displayWith: ((value: T) => string | number) | null;
  compareWith: (v1: T | null, v2: T | null) => boolean;
  selectionChanged: EventEmitter<SelectValue<T>>;
  closed: EventEmitter<void>;
  searchChanged: EventEmitter<string>;
  value: SelectValue<T>;

  emitChangeEvent(): void;

  markAsTouched(): void;

  open(): void;

  close(): void;

  writeValue(value: SelectValue<T>): void;

  expandAll(): void;

  setDisabledState?(isDisabled: boolean): void;

  clearSelection(e?: Event): void;

  handleSelection(option: SelectOptionToken<T>): void;

  isMultiple(): boolean;

  getListKeyManager(): ActiveDescendantKeyManager<NodeElementToken<any>>;
}
