import { Component, EventEmitter, forwardRef, inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { startWith } from 'rxjs/operators';

import { ControlValueAccessor } from '../common/control.value.accessor';
import { FilterData, FilterFn } from './filter.model';
import { FilterStore } from './filter-store';
import { SelectComponent } from './select.component';
import { SelectToken } from './select.token';
import { SelectValue } from './select.model';
import { FilterHasOptionsPipe } from './filter-has-options.pipe';
import { ToggleOptionDirective } from './toggle-option.directive';
import { SelectOptionGroupComponent } from './select-option-group/select-option-group.component';
import { SelectOptionComponent } from './select-option/select-option.component';
import { NodeElementDirective } from './node-element.directive';
import { NgIf, NgFor, SlicePipe } from '@angular/common';

@Component({
  selector: 'he-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FilterComponent),
      multi: true
    },
    FilterStore
  ],
  imports: [
    SelectComponent,
    FormsModule,
    ReactiveFormsModule,
    NgIf,
    NgFor,
    NodeElementDirective,
    SelectOptionComponent,
    SelectOptionGroupComponent,
    ToggleOptionDirective,
    SlicePipe,
    FilterHasOptionsPipe
  ]
})
export class FilterComponent<T> extends ControlValueAccessor<SelectValue<T>> implements OnInit {
  filterStore = inject(FilterStore);

  @Input()
  multiple = true;

  @Input()
  expandOnFilter = false;

  @Input()
  placeholder = 'Select filter...';

  @Input()
  valuePlaceholder = 'Select filter';

  @Input()
  value: SelectValue<T>;

  @Input()
  showSelectedOnTop = false;

  @Output()
  readonly selectionChanged = new EventEmitter<SelectValue<T>>();

  @ViewChild(SelectComponent, { static: true })
  protected select!: SelectToken<FilterData>;

  protected filter = this.filterStore.selectSignal(({ filter }) => filter);

  protected limitValue = this.filterStore.selectSignal(({ limit }) => limit);

  protected selectControl = new FormControl([] as T);

  @Input()
  set data(data: FilterData[]) {
    this.filterStore.setData(data);
  }

  private _disabled = false;

  @Input()
  set disabled(value: boolean) {
    this._disabled = value;
    value ? this.selectControl.disable() : this.selectControl.enable();
  }

  public get disabled() {
    return this._disabled;
  }

  @Input()
  set limit(limit: number) {
    this.filterStore.patchState({ limit });
  }

  @Input()
  set filterFn(filterFn: FilterFn) {
    this.filterStore.patchState({ filterFn });
  }

  @Input()
  set mapFn(mapFn: (node: FilterData) => FilterData) {
    this.filterStore.patchState({ mapFn });
  }

  writeValue(value: T): void {
    this.selectControl.setValue(value);
  }

  ngOnInit(): void {
    this.filterStore.setValue(this.selectControl.valueChanges.pipe(startWith(this.selectControl.value)));
  }

  public open() {
    this.select.open();
  }

  protected onClose() {
    this.onTouched();
  }

  protected displayWith = (item: string) => this.filterStore.flattenOptionsDictionary()[item]?.label;

  protected onSearchFilter(search: string) {
    this.filterStore.patchState(state => ({ ...state, filter: search }));
  }

  protected onSelectionChange($event: SelectValue<T>) {
    this.onChange($event);
    this.selectionChanged.emit($event);
    this._focusNeighbourItem();
  }

  protected trackByFn = (index: number, item: FilterData) => item.trackId;

  private _focusNeighbourItem() {
    const keyManager = this.select.getListKeyManager();
    if (keyManager.activeItemIndex !== -1) {
      keyManager.setNextItemActive();
      if (keyManager.activeItemIndex === 0) {
        keyManager.setPreviousItemActive();
        keyManager.setPreviousItemActive();
      }
    }
  }
}
