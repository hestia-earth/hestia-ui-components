import {
  ChangeDetectorRef,
  ContentChildren,
  Directive,
  ElementRef,
  HostBinding,
  inject,
  QueryList
} from '@angular/core';
import { Highlightable } from '@angular/cdk/a11y';

import { SelectOptionGroupComponent } from './select-option-group';
import { SelectOptionComponent } from './select-option';
import { NODE_ELEMENT_TOKEN, NodeElementToken } from './node-element.token';
import { ToggleOptionDirective } from './toggle-option.directive';

@Directive({
  selector: 'he-select-option, he-select-option-group',
  providers: [
    {
      provide: NODE_ELEMENT_TOKEN,
      useExisting: NodeElementDirective
    }
  ],
  standalone: true
})
export class NodeElementDirective implements Highlightable, NodeElementToken {
  @ContentChildren(NODE_ELEMENT_TOKEN, { descendants: true })
  descendants!: QueryList<NodeElementToken>;

  @HostBinding('class.active')
  isActive = false;

  private option = inject(SelectOptionComponent, {
    optional: true,
    self: true
  });
  private optionGroup = inject(SelectOptionGroupComponent, {
    optional: true,
    self: true
  });
  private toggleDirective = inject(ToggleOptionDirective, {
    optional: true,
    self: true
  });

  private _cd = inject(ChangeDetectorRef);
  private el = inject(ElementRef<HTMLElement>);
  private parentNode = inject(NodeElementDirective, {
    skipSelf: true,
    optional: true
  });

  get isVisible() {
    return !this.parentNode || this.parentNode.expanded();
  }

  get isOption() {
    return !!this.option;
  }

  get isOptionGroup() {
    return !!this.optionGroup;
  }

  get value() {
    if (this.isOptionGroup) {
      throw new Error('Option group does not have a value');
    }
    return this.option?.value;
  }

  setActiveStyles(): void {
    this.isActive = true;
    this._cd.markForCheck();
  }

  setInactiveStyles(): void {
    this.isActive = false;
    this._cd.markForCheck();
  }

  expanded() {
    return this.optionGroup?.isExpanded();
  }

  showChildren() {
    this.optionGroup?.showChildren();
  }

  hideChildren() {
    this.optionGroup?.hideChildren();
    this.descendants.forEach(node => node.hideChildren());
  }

  enterClick() {
    if (this.isOption) {
      this.option?.select();
      this.toggleDirective?.toggle();
    }

    if (this.isOptionGroup) {
      this.optionGroup?.toggleSelectAll();
    }
  }

  scrollIntoView(options?: ScrollIntoViewOptions) {
    this.el.nativeElement.scrollIntoView(options);
  }
}
