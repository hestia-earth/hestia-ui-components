import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  forwardRef,
  HostBinding,
  inject,
  Input,
  QueryList
} from '@angular/core';
import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';

import { OPTION_GROUP_TOKEN, SelectOptionGroupToken } from './select-option-group.token';
import { NODE_ELEMENT_TOKEN, NodeElementToken } from '../node-element.token';
import { SELECT_TOKEN, SelectToken } from '../select.token';

@Component({
  selector: 'he-select-option-group',
  templateUrl: './select-option-group.component.html',
  styleUrls: ['./select-option-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: OPTION_GROUP_TOKEN,
      useExisting: forwardRef(() => SelectOptionGroupComponent)
    }
  ],
  imports: [FaIconComponent]
})
export class SelectOptionGroupComponent<T> implements SelectOptionGroupToken {
  private cd = inject(ChangeDetectorRef);
  private _parent = inject<SelectToken<T>>(SELECT_TOKEN);

  static nextId = 0;

  @Input()
  label = '';

  @Input()
  @HostBinding('class.disabled')
  disabled = false;

  @HostBinding('attr.id')
  id = `option-group-${SelectOptionGroupComponent.nextId++}`;

  @HostBinding('class.expanded')
  protected expanded = false;

  // All options and group nodes.
  @ContentChildren(NODE_ELEMENT_TOKEN, { descendants: true })
  private _descendants!: QueryList<NodeElementToken>;

  get isAllSelected() {
    return this.allOptionNodes
      .map(o => o.value)
      .every(v => Array.isArray(this._parent.value) && this._parent.value?.includes(v));
  }

  protected get suffixIcon() {
    return this.expanded ? faMinus : faPlus;
  }

  protected get allOptionNodes() {
    return this._descendants.filter(o => o.isOption);
  }

  onGroupLabelClick($event: MouseEvent) {
    $event.stopPropagation();
    this.toggleMenu();
  }

  showChildren() {
    this.expanded = true;
    this.cd.markForCheck();
  }

  hideChildren() {
    this.expanded = false;
    this._descendants?.forEach(group => group.hideChildren());
    setTimeout(() => this.cd.markForCheck(), 10);
  }

  toggleMenu() {
    this.expanded = !this.expanded;
    this.cd.markForCheck();
  }

  isExpanded() {
    return this.expanded;
  }

  toggleSelectAll() {
    if (this.disabled) {
      return;
    }

    return this.isAllSelected ? this.unselectAll() : this.selectAll();
  }

  selectAll() {
    if (!this._parent.isMultiple()) {
      throw new Error('Cannot select all options in a single select');
    }

    const currentValues = Array.isArray(this._parent.value) ? (this._parent.value ?? []) : [];

    const values = this.allOptionNodes.map(o => o.value);
    this._parent.writeValue([...currentValues, ...values]);
    this._parent.emitChangeEvent();
  }

  unselectAll() {
    if (!this._parent.isMultiple()) {
      throw new Error('Cannot unselect all options in a single select');
    }

    const currentValues = Array.isArray(this._parent.value) ? (this._parent.value ?? []) : [];
    const values = this.allOptionNodes.map(o => o.value);
    this._parent.writeValue(currentValues.filter(v => !values.includes(v)));
    this._parent.emitChangeEvent();
  }
}
