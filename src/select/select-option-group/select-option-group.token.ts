import { InjectionToken } from '@angular/core';

export interface SelectOptionGroupToken {
  showChildren(): void;

  hideChildren(): void;

  toggleSelectAll(): void;

  selectAll(): void;

  unselectAll(): void;
}

export const OPTION_GROUP_TOKEN = new InjectionToken<SelectOptionGroupToken>('Option group token');
