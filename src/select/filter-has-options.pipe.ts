import { Pipe, PipeTransform } from '@angular/core';

import { FilterData, FilterGroup } from './filter.model';

export const hasLeafWithValue = (root?: FilterData) => root?.type === 'option' || root?.options?.some(hasLeafWithValue);

@Pipe({
  name: 'filterHasOptions',
  standalone: true
})
export class FilterHasOptionsPipe implements PipeTransform {
  transform(group: FilterGroup) {
    return hasLeafWithValue(group);
  }
}
