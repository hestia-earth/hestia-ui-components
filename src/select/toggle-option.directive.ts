import { Directive, HostListener, inject } from '@angular/core';

import { SELECT_OPTION_TOKEN } from './select-option/select-option.token';
import { OPTION_GROUP_TOKEN } from './select-option-group/select-option-group.token';

@Directive({
  selector: '[toggleGroup]',
  standalone: true
})
export class ToggleOptionDirective {
  private _parentOptionGroup = inject(OPTION_GROUP_TOKEN, {
    optional: true,
    skipSelf: true
  });
  private _option = inject(SELECT_OPTION_TOKEN, { self: true });

  public toggle() {
    if (!this._parentOptionGroup) {
      throw new Error(`[toggleGroup] directive must be used inside <he-select-option-group>`);
    }

    this._option.isSelected ? this._parentOptionGroup.selectAll() : this._parentOptionGroup.unselectAll();
  }

  @HostListener('click')
  protected click() {
    setTimeout(() => this.toggle());
  }
}
