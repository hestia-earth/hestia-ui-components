import { SelectionModel } from '@angular/cdk/collections';
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  DestroyRef,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  inject,
  Input,
  OnChanges,
  Output,
  QueryList,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ActiveDescendantKeyManager } from '@angular/cdk/a11y';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { ConnectedPosition, CdkOverlayOrigin, CdkConnectedOverlay } from '@angular/cdk/overlay';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgTemplateOutlet, NgSwitch, NgSwitchCase, NgClass } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { startWith, switchMap, tap } from 'rxjs/operators';
import { merge } from 'rxjs';

import { ClickOutsideDirective } from '../common/click-outside.directive';
import { ControlValueAccessor } from '../common/control.value.accessor';
import { SELECT_OPTION_TOKEN, SelectOptionToken } from './select-option/select-option.token';
import { SelectValue } from './select.model';
import { SELECT_TOKEN, SelectToken } from './select.token';
import { NODE_ELEMENT_TOKEN, NodeElementToken } from './node-element.token';

@Component({
  selector: 'he-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    },
    {
      provide: SELECT_TOKEN,
      useExisting: forwardRef(() => SelectComponent)
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CdkOverlayOrigin,
    NgTemplateOutlet,
    CdkConnectedOverlay,
    ClickOutsideDirective,
    NgSwitch,
    NgSwitchCase,
    FaIconComponent,
    NgClass
  ]
})
export class SelectComponent<T>
  extends ControlValueAccessor<SelectValue<T>>
  implements OnChanges, AfterContentInit, SelectToken<T>
{
  private _cd = inject(ChangeDetectorRef);
  private _hostEl = inject(ElementRef);
  private _destroyRef = inject(DestroyRef);

  @Input()
  placeholder = 'Nothing is selected...';

  @Input()
  searchPlaceholder = 'Start typing...';

  @Input()
  searchable = false;

  @Input()
  @HostBinding('class.is-disabled')
  disabled = false;

  @HostBinding('attr.tabindex') tabindex = '0';

  @Input()
  displayWith: ((value: T) => string | number) | null = null;

  @Input()
  valueTemplate: TemplateRef<any>;

  @Input()
  positions: ConnectedPosition[];

  @Output()
  readonly opened = new EventEmitter<void>();

  @Output()
  readonly selectionChanged = new EventEmitter<SelectValue<T>>();

  @Output()
  readonly closed = new EventEmitter<void>();

  @Output()
  readonly searchChanged = new EventEmitter<string>();

  @ContentChildren(NODE_ELEMENT_TOKEN, { descendants: true })
  nodes!: QueryList<NodeElementToken>;

  @ContentChildren(SELECT_OPTION_TOKEN, { descendants: true })
  options!: QueryList<SelectOptionToken<T>>;

  @ViewChild('input')
  searchInputEl!: ElementRef<HTMLInputElement>;

  @HostBinding('class.select-panel-open')
  isOpen = false;

  private _selectionModel: SelectionModel<T> = new SelectionModel<T>(coerceBooleanProperty(this.multiple));

  private _optionMap = new Map<T | null, SelectOptionToken<T>>();

  private _listKeyManager!: ActiveDescendantKeyManager<NodeElementToken>;

  private _keyboardDictionary = {
    ArrowDown: this._onArrowDown.bind(this),
    ArrowUp: this._onArrowUp.bind(this),
    ArrowLeft: this._onArrowLeft.bind(this),
    ArrowRight: this._onArrowRight.bind(this),
    Enter: this._onEnter.bind(this),
    Esc: this._onEsc.bind(this)
  };

  private _multiple = false;

  @Input()
  public set multiple(value: string | boolean) {
    this._multiple = coerceBooleanProperty(value);
    this._selectionModel = new SelectionModel<T>(coerceBooleanProperty(this.isMultiple()));
  }

  @Input() selectTemplate: TemplateRef<any>;

  @Input() overlayOptions: {
    offsetX?: number;
    offsetY?: number;
  };

  get value() {
    return this._selectionModel.isEmpty()
      ? null
      : this._selectionModel.isMultipleSelection()
        ? this._selectionModel.selected
        : this._selectionModel.selected[0];
  }

  @Input()
  set value(value: SelectValue<T>) {
    this._setupValue(value);
    this.onChange(this.value);
    this._highlightSelectedOptions();
  }

  protected get displayValue() {
    if (this.displayWith && this.value) {
      if (Array.isArray(this.value)) {
        return this.value.map(this.displayWith);
      }
      return this.displayWith(this.value);
    }
    return this.value;
  }

  protected get icon() {
    return this.isOpen ? faAngleUp : faAngleDown;
  }

  protected get labelState() {
    return this.isOpen && this.searchable ? 'search' : 'value';
  }

  protected get width() {
    return this.menuWidth ?? Math.max(this._hostEl.nativeElement.offsetWidth, 100) + 'px';
  }

  @Input() compareWith: (v1: T | null, v2: T | null) => boolean = (v1, v2) => v1 === v2;

  @Input() menuWidth: string;

  @HostListener('blur') markAsTouched() {
    if (!this.disabled && !this.isOpen) {
      this.onTouched();
      this._cd.markForCheck();
    }
  }

  @HostListener('click') open() {
    if (this.disabled) {
      return;
    }
    this.isOpen = true;
    if (this.searchable) {
      setTimeout(() => {
        this.searchInputEl?.nativeElement?.focus();
      });
    }
    this._cd.markForCheck();
  }

  close() {
    this.isOpen = false;
    this.onTouched();
    this._hostEl.nativeElement.focus();
    this.searchChanged.emit('');
    this._cd.markForCheck();
    this.closed.emit();
  }

  writeValue(value: SelectValue<T>): void {
    this._setupValue(value);
    this._highlightSelectedOptions();
    this._cd.markForCheck();
  }

  expandAll() {
    this.nodes.forEach(node => node.showChildren());
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this._cd.markForCheck();
  }

  isMultiple() {
    return this._multiple;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.compareWith) {
      this._selectionModel.compareWith = changes.compareWith.currentValue;
      this._highlightSelectedOptions();
    }
  }

  ngAfterContentInit(): void {
    this._listKeyManager = new ActiveDescendantKeyManager(this.nodes).withWrap();

    this._listKeyManager.change.pipe(takeUntilDestroyed(this._destroyRef)).subscribe(itemIndex => {
      this.nodes.get(itemIndex)?.scrollIntoView({
        behavior: 'smooth',
        block: 'center'
      });
    });

    this._selectionModel.changed.pipe(takeUntilDestroyed(this._destroyRef)).subscribe(values => {
      values.removed.forEach(rv => this._optionMap.get(rv)?.deselect());
      values.added.forEach(av => this._optionMap.get(av)?.highlightAsSelected());
    });
    this.options.changes
      .pipe(
        startWith<QueryList<SelectOptionToken<T>>>(this.options),
        tap(() => this._refreshOptionsMap()),
        tap(() => queueMicrotask(() => this._highlightSelectedOptions())),
        switchMap(options => merge(...options.map(o => o.selected))),
        takeUntilDestroyed(this._destroyRef)
      )
      .subscribe(selectedOption => this.handleSelection(selectedOption as any));
  }

  clearSelection(e?: Event) {
    e?.stopPropagation();
    if (this.disabled) {
      return;
    }
    this._selectionModel.clear();
    this.selectionChanged.emit(this.value);
    this.onChange(this.value);
    this._cd.markForCheck();
  }

  handleSelection(option: SelectOptionToken<T>) {
    if (this.disabled) {
      return;
    }
    if (option.value) {
      this._selectionModel.toggle(option.value);
      this.selectionChanged.emit(this.value);
      this.onChange(this.value);
    }
    if (!this._selectionModel.isMultipleSelection()) {
      this.close();
    }
  }

  getListKeyManager() {
    return this._listKeyManager;
  }

  emitChangeEvent() {
    this.onChange(this.value);
    this.selectionChanged.emit(this.value);
  }

  @HostListener('keydown', ['$event'])
  protected onKeyDown(e: KeyboardEvent) {
    if (e.key === 'ArrowDown' && !this.isOpen) {
      this.open();
      this.searchChanged.emit('');
    } else {
      this._keyboardDictionary[e.key]?.(e);
    }
  }

  protected onHandleInput(e: Event) {
    this.searchChanged.emit((e.target as HTMLInputElement).value);
    const renderTime = 50;
    setTimeout(() => {
      if (this._listKeyManager.activeItemIndex !== -1) {
        this._listKeyManager.setFirstItemActive();
      }
    }, renderTime);
  }

  private _onEsc(_e: KeyboardEvent) {
    this.searchInputEl.nativeElement.value = '';
    this.searchChanged.emit('');
  }

  private _onEnter(_e: KeyboardEvent) {
    this._listKeyManager.activeItem?.enterClick();
  }

  private _onArrowLeft(_e: KeyboardEvent) {
    this._listKeyManager.activeItem?.hideChildren();
  }

  private _onArrowRight(_e: KeyboardEvent) {
    this._listKeyManager.activeItem?.showChildren();
  }

  private _onArrowDown(e: KeyboardEvent) {
    this._horizontalNavigationHandler(e);
  }

  private _onArrowUp(e: KeyboardEvent) {
    this._horizontalNavigationHandler(e);
  }

  private _horizontalNavigationHandler(e: KeyboardEvent) {
    let max_backup = 10000;
    let visibleNode = false;

    while (!visibleNode && max_backup > 0) {
      this._listKeyManager.onKeydown(e);
      max_backup--;
      visibleNode = this._listKeyManager.activeItem?.isVisible;
    }
  }

  private _setupValue(value: SelectValue<T>) {
    this._selectionModel.clear();
    if (value) {
      const values = Array.isArray(value) ? value : [value];
      this._selectionModel.select(...values);
    }
  }

  private _refreshOptionsMap() {
    this._optionMap.clear();
    this.options.forEach(o => this._optionMap.set(o.value, o));
  }

  private _highlightSelectedOptions() {
    const valuesWithUpdatedReferences = this._selectionModel.selected.map(
      value => this._findOptionsByValue(value)?.value || value
    );
    this._selectionModel.clear();
    this._selectionModel.select(...valuesWithUpdatedReferences);
  }

  private _findOptionsByValue(value: T | null) {
    return this._optionMap.get(value) || this.options?.find(o => this.compareWith(o.value, value));
  }
}
