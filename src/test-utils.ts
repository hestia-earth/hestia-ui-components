export const readFixture = async (url: string) => {
  const content = await import(`./fixtures/${url}`);
  return url.endsWith('.txt') ? content.default : content;
};
