/* eslint-disable complexity */
import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from, of, ReplaySubject } from 'rxjs';
import { catchError, map, mergeMap, reduce, take } from 'rxjs/operators';
import { isEmpty, isUndefined, isEqual } from '@hestia-earth/utils';
import { SchemaType, NodeType, sortConfig, sortKeysByType } from '@hestia-earth/schema';
import {
  definitions,
  definition,
  IDefinition,
  IDefinitionObject,
  IDefinitionArray,
  IDefinitionProperties,
  recommendedProperties
} from '@hestia-earth/json-schema';
import { headersFromCsv } from '@hestia-earth/schema-convert';

import { schemaDataBaseUrl } from '../common/utils';

export const linkTypeEnabled = (type: SchemaType | NodeType) =>
  [NodeType.Cycle, NodeType.ImpactAssessment, NodeType.Site, NodeType.Source, NodeType.Term].includes(type as NodeType);

export const refToSchemaType = (ref = '') =>
  ref.startsWith('http') ? ref : (ref.substring(2).replace('-deep', '').replace('.json#', '') as SchemaType);

export const definitionToSchemaType = ({ title, $ref }: IDefinitionObject) =>
  (title || refToSchemaType($ref)) as SchemaType;

const schemaTypeProperties = (properties: IDefinitionProperties, schemaType?: SchemaType) =>
  schemaType ? ['type', ...('id' in properties && schemaType !== SchemaType.Term ? ['id'] : [])] : [];

const definitionToSchema = ({ required, properties }: IDefinitionObject) => ({
  required: required || [],
  properties: properties || {}
});

export const schemaRequiredProperties = (schemas: definitions, def: IDefinitionObject) => {
  const schemaType = definitionToSchemaType(def);
  const schema = schemas[schemaType] as IDefinitionObject;
  const { required, properties } = schema || definitionToSchema(def);
  const props = [...(required || []), ...recommendedProperties(schema || def)].filter(key => !properties[key].internal); // remove internal properties
  return {
    schemaType,
    properties: [...props, ...schemaTypeProperties(properties, schemaType)].map(key => ({
      key,
      value: properties[key]
    }))
  };
};

export const valueTypeToDefault = {
  number: '',
  integer: '',
  boolean: false,
  string: ''
};

export const nestingTypeEnabled = (schemaType: SchemaType) =>
  ![
    SchemaType.Cycle,
    SchemaType.ImpactAssessment,
    SchemaType.Organisation,
    SchemaType.Site,
    SchemaType.Source
  ].includes(schemaType);

export const nestingEnabled = (schemaType: SchemaType, key: string) =>
  nestingTypeEnabled(schemaType) || ['type', 'id'].includes(key);

export const schemaTypeToDefaultValue = (schemas: definitions, prop: definition, nested = false): any => {
  const mappings: {
    [key: string]: () => any;
  } = {
    object: () => {
      const { schemaType, properties } = schemaRequiredProperties(schemas, prop as IDefinitionObject);
      return properties
        .filter(({ key }) => !nested || nestingEnabled(schemaType, key))
        .map(({ key, value }) => ({ key, value: schemaTypeToDefaultValue(schemas, value, true) }))
        .reduce((prev, curr) => ({ ...prev, [curr.key]: curr.value }), {});
    },
    array: () => {
      const { items } = prop as IDefinitionArray;
      return [schemaTypeToDefaultValue(schemas, items, true)];
    },
    default: () => prop.default || (valueTypeToDefault as any)[prop.type as string]
  };
  const mapped = !prop.type
    ? mappings.object
    : (prop.type as string) in mappings
      ? mappings[prop.type as string]
      : mappings.default;
  return mapped();
};

const typePropertiesKeys: {
  [type in SchemaType]?: string[];
} = {
  [SchemaType.Term]: ['type', 'id', '@id', 'name']
};

export const availableProperties = (
  schema: definition,
  schemaType: SchemaType,
  value: any,
  isTopLevel = true,
  skipExistingNode = true
) => {
  const { properties } = 'properties' in schema ? schema : { properties: {} };
  const keys = schemaType in typePropertiesKeys ? typePropertiesKeys[schemaType] : Object.keys(properties || {});
  const internalProps = keys?.filter(prop => properties[prop].internal);
  const existing = [
    ...(isEmpty(value) ? [] : Object.keys(value).filter(v => !isEmpty(value[v]) || value[v] === '')),
    ...internalProps!,
    ...(isTopLevel ? [] : ['@id']),
    '@type'
  ];
  const available = keys
    ?.filter(prop => !existing.includes(prop))
    .reduce((prev: any, prop) => {
      prev[prop] = properties[prop];
      return prev;
    }, {});
  // avoid editing an existing node
  const isExistingNode =
    !isUndefined(value) && !isUndefined(value['@type'] || value.type) && !isUndefined(value['@id']);
  return isExistingNode && skipExistingNode ? {} : available;
};

export const isSchemaIri = (schema?: IDefinitionObject) =>
  isEqual(Object.keys(schema?.properties || {}), ['@id']) && isEqual(schema?.required || [], ['@id']);

@Injectable({
  providedIn: 'root'
})
export class HeSchemaService {
  protected http = inject(HttpClient);

  private schemasLoading = false;
  private schemasLoaded = false;
  private _schemas = new ReplaySubject<definitions>(1);

  private sortConfigLoading = false;
  private sortConfigLoaded = false;
  private _sortConfig = new ReplaySubject<sortConfig>(1);

  public getSchema$(type: SchemaType, version?: string) {
    return this.http.get<IDefinition>(`${schemaDataBaseUrl(version)}/json-schema/${type}.json`);
  }

  public getSchema(type: SchemaType, version?: string) {
    return this.getSchema$(type, version).toPromise();
  }

  private loadSchemas(version?: string) {
    this.schemasLoading = true;
    return from(Object.values(SchemaType)).pipe(
      mergeMap(type =>
        this.getSchema$(type, version).pipe(
          map(schema => ({ type, schema })),
          catchError(() => of({ type, schema: { properties: [] } }))
        )
      ),
      reduce((prev, { type, schema }) => ({ ...prev, [type]: schema }), {} as definitions),
      map(data => {
        this._schemas.next(data);
        this.schemasLoading = false;
        this.schemasLoaded = true;
        return data;
      })
    );
  }

  public get schemas$() {
    return this.schemasLoading || this.schemasLoaded ? this._schemas.asObservable() : this.loadSchemas();
  }

  public schemas() {
    return this.schemas$.pipe(take(1)).toPromise();
  }

  private loadSortConfig(version?: string) {
    this.sortConfigLoading = true;
    return this.http.get<sortConfig>(`${schemaDataBaseUrl(version)}/sort-config/config.json`).pipe(
      catchError(() => of(undefined as any as sortConfig)),
      map(data => {
        this._sortConfig.next(data);
        this.sortConfigLoading = false;
        this.sortConfigLoaded = true;
        return data;
      })
    );
  }

  public get sortConfig$() {
    return this.sortConfigLoading || this.sortConfigLoaded ? this._sortConfig.asObservable() : this.loadSortConfig();
  }

  public sortConfig() {
    return this.sortConfig$.pipe(take(1)).toPromise();
  }

  /**
   * Parse and sort (if possible) headers from CSV content according to the schema order.
   * This will load the sort configuration according to the current schema version.
   *
   * @param csvContent The content as a string
   * @returns List of strings as Observable when parsing / sorting is done.
   */
  public parseHeaders$(csvContent: string) {
    const headers = headersFromCsv(csvContent);
    return this.sortConfig$.pipe(
      take(1),
      map(config => sortKeysByType(headers, 'asc', config)),
      catchError(() => of(headers))
    );
  }
}
