export type validationErrorLevel = 'error' | 'warning';
export type validationErrorParam =
  | 'missingProperty'
  | 'type'
  | 'term'
  | 'termType'
  | 'termIds'
  | 'model'
  | 'product'
  | 'range'
  | 'node'
  | 'allowedValues'
  | 'allowedValue'
  | 'additionalProperty'
  | 'expected'
  | 'default'
  | 'current'
  | 'percentage'
  | 'keys'
  | 'threshold'
  | 'outliers'
  | 'country'
  | 'min'
  | 'max'
  | 'defaultSource'
  | 'source'
  | 'group'
  | 'message'
  | 'distance'
  | 'siteType'
  | 'products'
  | 'units'
  | 'duplicatedIndexes'
  | 'invalidCoordinates'
  | 'ids';
export type validationErrorKeyword = 'required' | 'type' | 'if' | 'then' | 'not';

export interface ICustomValidationRules {
  const?: any;
  required?: string[];
  enum?: string[];
  contains?: ICustomValidationRules;
  properties?: {
    [property: string]: ICustomValidationRules;
  };
  not?: ICustomValidationRules;
  items?: ICustomValidationRules;
  allOf?: ICustomValidationRules[];
  anyOf?: ICustomValidationRules[];
  oneOf?: ICustomValidationRules[];
}

export interface IValidationError {
  index?: number;
  nodeIndex?: number;
  level?: validationErrorLevel;
  dataPath?: string;
  schemaPath?: string;
  keyword?: validationErrorKeyword;
  message: string;
  params?: { [key in validationErrorParam]?: any };
  schema?: ICustomValidationRules;
}

export const hasValidationError = (errors: IValidationError[][] = [], level?: 'error' | 'warning') => {
  const allErrors = errors.flat(2);
  return level
    ? allErrors.some(
        err =>
          // schema errors have no level
          !err.level || err.level === level
      )
    : allErrors.length > 0;
};
