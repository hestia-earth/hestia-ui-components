import { Component, computed, inject, input } from '@angular/core';
import { SchemaType } from '@hestia-earth/schema';
import { toSignal } from '@angular/core/rxjs-interop';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

import { HeSchemaService } from '../schema.service';
import { RemoveMarkdownPipe } from '../../common/remove-markdown.pipe';

@Component({
  selector: 'he-schema-info',
  templateUrl: './schema-info.component.html',
  styleUrls: ['./schema-info.component.scss'],
  imports: [NgbTooltip, FaIconComponent, RemoveMarkdownPipe]
})
export class SchemaInfoComponent {
  private schemaService = inject(HeSchemaService);

  protected readonly faQuestionCircle = faQuestionCircle;

  private schemas = toSignal(this.schemaService.schemas$);
  private schema = computed(() => this.schemas()?.[this.type()]);

  protected type = input<SchemaType>();
  protected field = input<string>();
  protected content = input<string>();
  protected placement = input<'top' | 'bottom' | 'right' | 'left'>('bottom');
  protected triggers = input<'hover' | 'click'>('hover');
  protected container = input<'-' | 'body'>('body');

  protected contentString = computed(
    () => this.content() || this.schema()?.properties?.[this.field()]?.description || this.schema()?.description
  );
  protected autoClose = computed(() => (this.triggers() === 'click' ? 'outside' : true));
}
