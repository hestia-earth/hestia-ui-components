/* eslint-disable complexity */
/* eslint-disable max-len */
/* eslint-disable no-useless-escape */
import { CycleFunctionalUnit, NodeType, SchemaType, Term, TermTermType, isTypeValid } from '@hestia-earth/schema';
import { toPrecision, keyToLabel, unique } from '@hestia-earth/utils';

import { termTypeLabel } from '../terms/terms.model';
import { pathToApiDocsPath } from '../engine/engine.service';
import {
  Repository,
  Template,
  baseUrl,
  code,
  contactUsLink,
  externalLink,
  glossaryBaseUrl,
  glossaryLink,
  nodeLink,
  reportIssueLink,
  schemaLink
} from '../common/utils';
import { pluralize } from '../common/pluralize';
import { IValidationError } from '../schema/schema-validation.model';

export interface IValidationErrorWithIndex {
  error: IValidationError[];
  index: number;
  changed?: boolean;
}

type handleErrorFunc = (error: IValidationError, errorCount?: number, allErrors?: IValidationError[]) => string;

const mapErrorMessage = 'does not contain latitude and longitude';

interface IDataPath {
  path: string;
  /**
   * Human readable version of the path.
   */
  label: string;
}

export const parseDataPath = (dataPath = ''): IDataPath[] => {
  const [_, ...paths] = dataPath
    .replace(/[\[\]\'\'\"\"]/g, '')
    .replace(/(\w)(\@)/g, '$1.@')
    .split('.');
  return paths.map(path => {
    const label = path.replace(/[\d\@]+/g, '');
    const type = pluralize(keyToLabel(label), 1) as SchemaType;
    const isSchemaType = isTypeValid({ type });
    return {
      path: path.replace(/(\d+)/g, '[$1]'),
      label: isSchemaType ? type : `<code>${pluralize(label, 1)}</code>`
    };
  });
};

const allowedDataPathsLabels = Object.values(SchemaType);

export const dataPathLabel = (dataPath = '') =>
  parseDataPath(dataPath)
    .filter(({ label }) => allowedDataPathsLabels.includes(label as SchemaType))
    .pop()?.label;

const glossaryTypeLink = (type: TermTermType, text = termTypeLabel(type)) =>
  externalLink(`${glossaryBaseUrl()}?termType=${type}`, text);
const termLink = ({ id, name }: Partial<Term>) => externalLink(`${baseUrl()}/term/${id}`, name || id);
const dateFormatMessage = `should follow the ISO 8601 date format, e.g. ${code(2000)}, or ${code('2000-12')}, or ${code(
  '2000-12-30'
)}`;
const modelLink = (term: Term, model: Term) => `
  <a href="${pathToApiDocsPath(model['@id'], term['@id'])}" target="_blank">
    <span class="pr-1">${model.name}</span>
    <span>(View Docs)</span>
  </a>
`;
const threshold = (value: number) => code(`${value * 100}%`);

const noTillage: Partial<Term> = { id: 'noTillage', name: 'No tillage' };
const pastureGrass: Partial<Term> = { id: 'pastureGrass', name: 'Pasture grass' };

const idNotFoundDefaultMessage = `
  If you are using the ${code('name')} of the Term, make sure to use ${code('term.name')} column;
  if you are using the ${code('@id')} of the Term, use the ${code('term.@id')} column instead.
`.trim();

export const isMigrationError = ({ params }: IValidationError) => params?.current && 'expected' in params;

export const migrationErrorMessage = ({ params }: IValidationError) =>
  params?.expected
    ? `The Term ${code(params.current)} was updated, please use this Term instead: ${code(params.expected)}.`
    : `The Term ${code(params.current)} was deleted. Please search the ${glossaryLink('Glossary')} for a new Term.`;

const idNotFoundError: handleErrorFunc = (_error, _errorCount, allErrors) => {
  const migrationErrors = allErrors.filter(isMigrationError);
  const noMigrationErrors = allErrors.filter(error => !isMigrationError(error));
  return `
    <p>Does not exist in HESTIA.</p>
    ${noMigrationErrors.length ? `<p>${idNotFoundDefaultMessage}</p>` : ''}
    ${unique(migrationErrors.map(migrationErrorMessage)).join('</br>')}
  `;
};

const methodModelMappingError: handleErrorFunc = ({ params }, errorCount) =>
  `${
    errorCount === 1
      ? `${
          params?.allowedValues.length
            ? `can only be used with one of these methods: ${params?.allowedValues.map(code).join(', ')}`
            : `does not currently have any method allowed. Please ${contactUsLink()} to change this.`
        }.`
      : `Some Terms are not allowed with the method you selected.`
  }
  You can find the list of allowed methods for each Term by clicking on them.`;

// set message as empty to not display it
const customErrorMessage: {
  [key: string]: handleErrorFunc;
} = {
  'should not be empty': ({ params }) =>
    [
      `Empty ${params.type} are not allowed in HESTIA.`,
      params?.type === NodeType.ImpactAssessment
        ? `Please, either link your ImpactAssessment to an existing Cycle using the ${code('cycle.id')},
        or add one or more ${schemaLink('ImpactAssessment#emissionsResourceUse', 'emissionsResourceUse')},
        ${schemaLink('ImpactAssessment#impacts', 'impacts')},
        or ${schemaLink('ImpactAssessment#endpoints', 'endpoints')}.`
        : ''
    ].join('\n'),
  "should have required property '@id'": idNotFoundError,
  "should have required property '@type'": () => '',
  "should have required property 'id'": idNotFoundError,
  'should match exactly one schema in oneOf': () => '',
  'should match some schema in anyOf': () => '',
  'should match "then" schema': () => '',
  'should NOT have additional properties': ({ params }, errorCount) =>
    errorCount === 1
      ? `The following field does not exist: ${params?.additionalProperty} or is not allowed in this case.`
      : `Should not have additional properties.`,
  'should NOT be valid': ({ schema }) =>
    schema?.required?.includes('areaPercent')
      ? `If the term units is already in ${code('% area')}, just use ${code('value')}. Dont use ${code('areaPercent')}.`
      : schema?.required?.includes('transformation')
        ? 'A transformation can not be added here.'
        : schema?.required?.includes('fromCycle')
          ? `This field can only be added to the ${schemaLink('Transformation#inputs', 'inputs of a Transformation')}.`
          : schema?.required?.includes('otherSites')
            ? `Please only specify a single ${schemaLink('Cycle#site', 'site')} when the functional unit is ${code(
                CycleFunctionalUnit['1 ha']
              )}.`
            : // termType restriction on the item
              schema?.properties?.term?.properties?.termType?.enum?.length
              ? `This ${code('termType')} is not allowed here.`
              : 'Should not be used in this case.',
  'should match pattern "^\\d{13}$"': () => `should be composed of 13 numbers`,
  'should match pattern "^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$"': () => dateFormatMessage,
  'should match pattern "^([0-9]{4}|-)(-[0-9]{2})?(-[0-9]{2})?$"': () => dateFormatMessage,
  'should match pattern "^([0-9]{4}|-)(-[0-9]{2})?(-[0-9]{2})?([T][0-2][0-9]:[0-5][0-9]:[0-5][0-9]((+|-)[0-1][0-9]:[0-5][0-9])?)?$"':
    () => dateFormatMessage,
  'should match pattern "^10.(d)+/(S)+$"': () =>
    `The DOI should not include the domain name, e.g. ${code('10.1000/182')} instead of ${code(
      'https://doi.org/10.1000/182'
    )}`,
  'should have the same length as endDate': () =>
    `The startDate must be in the same date format as ${code('endDate')}: YYYY-MM-DD, YYYY-MM, or YYYY.`,
  'node not found': (error, _errorCount, allErrors) =>
    `${
      allErrors?.length
        ? `The following Nodes do not exist on the platform:
          <ul class="is-pl-3 is-list-style-disc is-nowrap">
            ${unique(
              allErrors.map(
                ({ params }) =>
                  `<li class="is-pre-wrap">${params?.node?.['@type']} with ${code(`@id=${params?.node?.['@id']}`)}</li>`
              )
            ).join('\n')}
          </ul>`
        : `The ${error.params?.node?.['@type']} with ${code(`@id=${error.params?.node?.['@id']}`)} does not exist on the platform.`
    } If you are trying to link to nodes included in this upload, you must use ${code(
      'id'
    )} field instead. Otherwise, please check the ids are correct and try again.`,
  'should be linked to an existing node': (error, _errorCount, allErrors) =>
    allErrors?.length
      ? `Your upload does not contain the following Nodes:
      <ul class="is-pl-3 is-list-style-disc is-nowrap">
      ${unique(
        allErrors.map(
          ({ params }) => `<li class="is-pre-wrap">${params?.node?.type} with ${code(`id=${params?.node?.id}`)}</li>`
        )
      ).join('\n')}
      </ul>
      If you are trying to link to an existing Node on the HESTIA platform, you must use ${code('@id')} field instead.
      Otherwise you must include a full ${unique(
        allErrors.map(({ params }) => `${params?.node?.type} with id ${code(params?.node?.id)}.`)
      ).join(', ')}`
      : `Your upload does not contain a ${error.params?.node?.type} with ${code(`id=${error.params?.node?.id}`)}.
      If you are trying to link to an existing Node on the HESTIA platform, you must use ${code('@id')} field instead.
      Otherwise you must include a full ${error.params?.node?.type} with id ${code(error.params?.node?.id)}.`,
  'must be within the country': () => `The country provided does not contain the region provided. Please check
  the country for errors or the region for errors, and reference the ${glossaryLink('Glossary of Terms')}.`,
  [mapErrorMessage]: ({ dataPath, params }, errorCount) =>
    errorCount === 1
      ? `The longitude and latitude provided are${
          params?.distance ? ` ${params?.distance}kms` : ''
        } outside of the ${dataPath.substring(1)} provided.
      Please check the longitude and latitude for errors or check if the wrong ${dataPath.substring(
        1
      )} from the ${glossaryTypeLink(TermTermType.region, 'Glossary')} was used.
      ${
        params?.expected
          ? `Hint: the coordinates appear to be located in the following ${dataPath.substring(1)}: ${termLink({
              id: 'params.expected'
            })}`
          : ''
      }`
      : `The region provided does not contain the longitude and latitude provided.
      Please check the longitude and latitude for errors or the region for error, and reference the ${glossaryTypeLink(
        TermTermType.region,
        'Glossary of Terms'
      )}.`,
  'sum not equal to 100% for sandContent, siltContent, clayContent': () =>
    `The sum of Sand, Silt, and Clay content should equal 100% for each soil depth interval.`,
  'is outside the allowed range': ({ params }, errorCount) =>
    `${
      errorCount === 1
        ? params?.term
          ? `The value for ${code(
              params?.term['@id'] || params?.term.name
            )} is outside the range established for the soil texture you provided.
            For this soil texture, the maximum ${code(params?.term['@id'] || params?.term.name)} is ${
              params?.range?.max
            } and the minimum is ${params?.range?.min}.`
          : `Does not match the possible ranges of sand, silt and clay content.`
        : `Each soil texture has a maximum and minimum value for sand, silt, and clay content.
        At least one measurement of sand, silt, or clay content does not match the possible range for the specified soil texture.`
    }
    ${
      errorCount === 1
        ? `Please check the measurement, its depth intervals, and the dates of the measurement or update the soil texture if needed.`
        : `Please check your measurements, their depth intervals, and the dates of the measurements or update the soil textures if needed.`
    }`,
  'should be equal to one of the allowed values': ({ params, dataPath }) => {
    const paths = parseDataPath(dataPath);
    const values =
      paths.length >= 2 ? pluralize(paths[paths.length - 1].path, params?.allowedValues?.length || 0) : 'values';
    return dataPath.endsWith('term.termType')
      ? `For ${pluralize(
          paths[paths.length - 3].label,
          0
        )}, we only allow terms from the following glossaries: ${params?.allowedValues
          .map(code)
          .join(
            ', '
          )}. Please either change the Term you are using and ensure it is from one of the glossaries listed above, or follow the schema to identify where this Term can be added.`
      : `Only the following ${values} are permitted: ${params?.allowedValues.map(code).join(', ')}`;
  },
  'should be equal to constant': ({ params }) => `Must be: ${code(params?.allowedValue)}`,
  'is not allowed for this emission': methodModelMappingError,
  'is not allowed for this characterisedIndicator': methodModelMappingError,
  'is not allowed for this resourceUse': methodModelMappingError,
  'should contain a valid item': ({ dataPath }) => {
    const paths = parseDataPath(dataPath!);
    return paths.length >= 2
      ? `The ${paths[paths.length - 1].path} related to this ${pluralize(paths[paths.length - 2].label, 1)} are invalid`
      : `The ${paths[0].path} are invalid`;
  },
  'may be between 0 and 100': () => 'Percentages should be between 0 and 100, not 0 and 1. This may be an error.',
  'may not all be set to false': () =>
    `Every value in the data completeness assessment is ${code(false)}. You may have forgotten to fill it in.
  For information on how to fill it in, please see the ${schemaLink('Cycle#completeness', 'schema')}.`,
  'may not be empty': () => 'if this value signifies no data, HESTIA only accepts "-" or empty for no data',
  'may not be 0': ({ dataPath }, errorCount) =>
    `Adding a value ${errorCount === 1 ? ` to every ${dataPathLabel(dataPath)} ` : ' '} is highly recommended.
      If the amount produced is zero, we recommend setting value to 0.`,
  'longFallowDuration must be lower than 5 years': () =>
    `Your long fallow duration is greater than five years.
    Fallow is defined by FAOSTAT as land left uncultivated for between 1 and 5 years,
    and this definition is used for HESTIA also.`,
  'should be more than the cropping duration': ({ params }) =>
    `The ${schemaLink(
      'Cycle#cycleDuration',
      'cycleDuration'
    )} is shorter than the country's minimum cropping duration for rice (${code(params?.expected)}).
    This might be an error and result in the underestimation of the ${code('CH4, to air, flooded rice')} emissions.
    Please, double-check that the ${schemaLink('Cycle#cycleDuration', 'cycleDuration')} is correctly set.
`,
  'croppingDuration must be between min and max': ({ params }) =>
    `The ${code('croppingDuration')} should be within the country's minimum (${params.min}) and maximum (${params.max}) cropping duration for rice.
  This might be an error and result in the overestimation of ${code('CH4, to air, flooded rice')} emissions.
  Please, double-check that the ${code('croppingDuration')} is correctly set.`,
  'should be within percentage of default value': ({ params }, errorCount) =>
    errorCount === 1
      ? `This value is ±${threshold(params?.threshold)} different to our default value which is ${params?.default}.
      Please check your uploaded data as this may be an error.`
      : `This value is substantially different than our default value.
      Please check your uploaded data as this may be an error.`,
  'the value provided is not consistent with the model result': ({ params }, errorCount) =>
    `The expected result ${
      errorCount === 1
        ? `for ${code(params?.term.name)} ${
            params?.model?.name ? `using ${modelLink(params?.term, params?.model)} ` : ''
          }`
        : ''
    }
    from the data provided in the upload is ${
      errorCount === 1
        ? `${code(toPrecision(params?.expected, 3))}, but the value in the upload is ${code(
            toPrecision(params?.current, 3)
          )}.
        ${params?.threshold ? `Our threshold is ±${threshold(params?.threshold)}.` : ''}
        Please either:
        <ul class="is-pl-3 is-list-style-decimal is-nowrap">
        <li class="is-pre-wrap">Check the ${params?.term.termType} value you provided;</li>
        <li class="is-pre-wrap">Check the data you provided which is the input into this model;</li>
        <li class="is-pre-wrap">Carefully read the documentation to understand if we used a default property as part of the calculations.</li>
        </ul>`
        : 'not consistent with the model result.'
    }`,
  'the measurement provided might be in error': ({ params }, errorCount) =>
    `The expected value for ${code(params?.term.name)}
    ${params?.model?.name ? `using ${modelLink(params?.term, params?.model)} ` : ''}
    from the data provided in the upload is ${
      errorCount === 1
        ? `${code(toPrecision(params?.expected, 3))}, but the value in the upload is ${code(
            toPrecision(params?.current, 3)
          )}.
        ${params?.threshold ? `Our threshold is ±${threshold(params?.threshold)}.` : ''}`
        : `not consistent with the model result.`
    }`,
  'must be equal to previous product multiplied by the share': () =>
    `Products from a transformation which are an Input in to the next transformation must follow the following rule:
    ${code('previous.product.value * current.transformedShare / 100 == current.input.value')}`,
  'at least one Input must be a Product of the Cycle': () =>
    `A Transformation converts a Product from a Cycle into another Product.
    Therefore, at least one Input into the Transformation must be a Product of the Cycle.`,
  'must have only one entry with the same term.termType = excretaManagement': () =>
    `There can only be one Practice of type excretaManagement in a Cycle.
    To represent multiple excreta management systems either use multiple Cycles and link them together,
    or use Transformations within a Cycle and link those together.`,
  'must be included as an Input': ({ params }, errorCount) =>
    `The excreta Products are not of the same type as the excreta Inputs.
    ${
      errorCount === 1
        ? `E.g., you cannot get ${code(params?.term?.name)} from ${(params?.expected || [])
            .map(v => code(keyToLabel(v)))
            .join(' or ')}.`
        : ''
    }
    Please check the Inputs and Products of the Transformation.`,
  'must add the linked inputs to the cycle': ({ params }, errorCount) =>
    `${
      errorCount === 1
        ? `You have stated that ${code(params?.term?.name)} was created by the following Inputs ${params?.expected
            ?.map(e => code(e.name))
            .join(' , ')}`
        : 'You have stated some Emissions were created by some Inputs'
    }.
    However, these Inputs do not exist in the Cycle.
    Please add these Inputs, even if you do not have a value for them.`,
  'must add the linked transformations to the cycle': ({ level, params }, errorCount) =>
    `${
      errorCount === 1
        ? `You have specified that${code(
            params?.term?.name
          )} was created during a Transformation with the Term name ${code(params?.expected?.name)}`
        : 'You have stated some Emissions were created by some Transformations'
    }.
    However, we cannot find a Transformation within the Cycle with this Term name.
    ${
      level === 'error'
        ? 'Please check the Terms used in the Emissions and/or the Transformations.'
        : 'You may want to add this Transformation.'
    }`,
  'every node should be unique': ({ params }) =>
    `This Node is duplicated. Every Node should be unique.
    Uniqueness is determined by the following fields: ${(params?.keys || []).map(code).join(', ')}`,
  'every item in the list should be unique': ({ params }) =>
    `This blank node is duplicated${
      params?.duplicatedIndexes?.length
        ? ` with the blank ${pluralize(
            'node',
            params.duplicatedIndexes.length
          )} with numbered: ${params.duplicatedIndexes.join(', ')}`
        : ''
    }. Every blank node should be unique.
    Uniqueness is determined by the following fields: ${(params?.keys || []).map(code).join(', ')}`,
  'must contain as many items as values': ({ params, dataPath }) =>
    `The number of ${code(dataPath.split('.').pop())} must match the number of ${code('value')}.
    Currently there are ${params?.current} ${code(dataPath.split('.').pop())} but ${params?.expected} ${code(
      'value'
    )}.`,
  'is too generic': ({ params }) =>
    `You have the following Product ${code(params?.product?.name)} however,
    you have used a generic ${glossaryTypeLink(params?.term?.termType)} term ${code(params?.term?.name)}.
    Use a more specific term for the ${glossaryTypeLink(
      params?.term?.termType
    )} which reflects the specific ${glossaryTypeLink(params?.product?.termType)}.`,
  'should not use identical terms with different units': ({ params }) =>
    `You have added the same ${code(params?.term?.termType)} in ${params?.units
      ?.map(unit => code(unit))
      .join(' and ')} units.
    If these terms refer to the same ${code(params?.term?.termType)}, this will lead to double counting.${
      params?.units?.includes('kg N')
        ? ` Instead use the ${termLink({ id: 'nitrogenContent', name: 'Nitrogen Content' })} Property on the ${code(
            'kg'
          )} term to define the nitrogen content of the ${code(params?.term?.termType)}.`
        : ''
    }`,
  'is missing required bibliographic information': () =>
    `The automatic bibliography search failed for this Bibliography. Either:
    <ul class="is-pl-3 is-list-style-decimal is-nowrap">
    <li class="is-pre-wrap">Manually fill-in the <b>required</b> bibliographic information as per ${schemaLink(
      SchemaType.Bibliography,
      'our schema'
    )}.</li>
    <li class="is-pre-wrap">Provide the ${code('documentDOI')} as well as the ${code('title')}.</li>
    <li class="is-pre-wrap">Check the ${code('documentDOI')} and ${code('title')} for typos against the ${externalLink(
      'https://www.mendeley.com',
      'Mendeley catalogue'
    )}.</li>
    </ul>`,
  'should be lower than max size': ({ params }) =>
    `The boundary or region is >${params?.expected}km2 and is too large to reliably gap fill Measurements.
    If you are able to use a more specific region or smaller boundary, please do.`,
  'an excreta input is required when using an excretaManagement practice': ({ dataPath }) => {
    const paths = parseDataPath(dataPath!);
    return `Excreta management is the conversion of excreta to any type of product (e.g., another type of excreta or organic fertiliser).
      You have added an ${code('excretaManagement')} Practice to this ${paths[0].label} but there is no excreta Input.
      To represent excreta management, use a ${paths[0].label} with excreta as an Input and ${code(
        'excretaManagement'
      )} as a Practice.`;
  },
  'only 1 primary product allowed': () => 'There can only be one primary product in each Cycle.',
  'is not allowed in combination with noTillage': () =>
    `This operation involves tillage, yet you have specified the Practice ${termLink(noTillage)}.
    Either change this operation or the Practice.`,
  'should contain a tillage practice': () =>
    `We recommend specifying the type of tillage used for this Cycle.
    Please see the ${glossaryTypeLink(TermTermType.tillage)} glossary.`,
  'must set value for every tillage practice': () =>
    `Either specify a single ${glossaryTypeLink(TermTermType.tillage)} practice or add a value to all practices.`,
  'sum not equal to 100% for tillage practices': () =>
    `The sum of ${glossaryTypeLink(TermTermType.tillage)} practices must equal 100%.`,
  'can only have 1 tillage practice without a value': ({ params }) =>
    `It is not possible for a Cycle to have the following tillage Practices:
    ${(params?.current ?? []).map(({ name }) => name).join(' and ')} at the same time.
    If multiple tillage practices did occur, please specify the percentage of area they occurred on.`,
  'cannot use no tillage if depth or number of tillages is not 0': () =>
    `This Practice cannot have a value of ${code(100)} if the Tillage depth is higher than ${code(
      0
    )} or the Number of tillages is higher than ${code(0)}. Please resolve this conflict.`,
  'cannot use full tillage if depth or number of tillages is 0': () =>
    `This Practice cannot have a value of ${code(100)} if the Tillage depth is equal to ${code(
      0
    )} or the Number of tillages is equal to ${code(0)}. Please resolve this conflict.`,
  'must use no tillage when number of tillages is 0': () =>
    `You cannot use a tillage term other than No tillage with a value of ${code(
      100
    )} if the Number of tillages has been set to ${code(0)}. Please resolve this conflict.`,
  'can not be linked to the same Cycle': () => 'You can not link an Input to the Impact Assessment of the same Cycle.',
  // deprecated, remove when message is not being used anymore
  'must be 0 for product value 0': ({ dataPath, params }, errorCount) =>
    `If the amount produced is zero, the ${code(dataPath?.split('.').pop())} of ${
      errorCount === 1 ? code(params?.term.name) : 'that product'
    } must also be zero.`,
  'economicValueShare must be 0 for product value 0': ({ params }, errorCount) =>
    `If the amount produced is zero, the economicValueShare of ${
      errorCount === 1 ? code(params?.term.name) : 'that product'
    } must also be zero.`,
  'revenue must be 0 for product value 0': ({ params }, errorCount) =>
    `If the amount produced is zero, the revenue of ${
      errorCount === 1 ? code(params?.term.name) : 'that product'
    } must also be zero.`,
  'should add a source': ({ params }) =>
    `We recommend adding a Source to all data items.
    This can be done by adding the ${code(params?.current)} field.
    Sources can also be specified for each data item (i.e., each Input, Emission, Product, Practice, Measurement, or Infrastructure).`,
  'cover crop cycle contains non cover crop product': () =>
    `You have specified that this crop is a cover crop using a Practice.
    A cover crop is defined as "a crop which is left in the field without any fraction harvested by the end of the Cycle".
    The Products of this Cycle are incompatible with this crop being a cover crop.
    Please check the Products or the Practices.`,
  'should be included in the cycle products': ({ params }, errorCount) =>
    `${
      errorCount === 1
        ? `The Product ${code(params.product.name)} cannot be found in the Cycle with id ${code(params.node.id)}`
        : 'The Product in the Impact Assessment cannot be found in the linked Cycle'
    }.
    This could be simply because the product does not exist in the Cycle: in this case, add it to the Cycle or remove the Impact Assessment.
    It could also be because you have multiple Products in the Cycle using the same Term, but having different Properties or different fields:
    in this case, ensure all of the Properties and fields which make the Product unique are copied into the Impact Assessment.`,
  'multiple ImpactAssessment are associated with the same Product of the same Cycle': () =>
    `Only one Impact Assessment can be related to this Product.
    Please make sure the product is correct or the cycle.id you are referring to is correct.`,
  'multiple ImpactAssessment are associated with this Product': () =>
    `Only one Impact Assessment can be related to this Product.
    Please make sure the product of the related ImpactAssessments are correct or the ${code('cycle.id')} is correct.`,
  'no ImpactAssessment are associated with this Product': () =>
    `In cases where Impact Assessment data are being uploaded, the number of Impact Assessments must match the number of Products.
    This ensures the Cycle data matches the Impact Assessment data.
    To fix this, add one Impact Assessment for every Product in the Cycle, following at least the minimum required fields for Impact Assessments.`,
  'must be less than or equal to land occupation': () =>
    `Land transformation should be less than or equal to land occupation
    (land transformation is the amount of land converted between some date in the past and the current year,
    divided by an amortization period, so it should always be less than land occupation by definition).`,
  'should be linked to an emission in the Cycle': ({ params }, errorCount) =>
    `${
      errorCount === 1 ? `${code(params?.term.name)} exists` : 'Some Emissions exist'
    } in both the Cycle and the Transformation but ${errorCount === 1 ? 'it is' : 'they are'} not linked using ${code(
      'cycle.emissions.X.transformation.term'
    )}. This may be an error.`,
  'should add an animal production system': () =>
    `For animal production cycles, we recommend specifying the animal production ${glossaryTypeLink(
      TermTermType.system
    )} e.g., ${code('Confined pasture system')} or ${code('Feedlot system')}, as a Practice.
    This is particularly important for ruminants, as the system influences animals' activity level and is used in the estimation of grass intake.
    It also ensures a more accurate gap-filling of beef cattle excreta.
    You can find a list of systems in the ${glossaryTypeLink(TermTermType.system, 'Glossary')}.`,
  'must be set to false when specifying fuel use': ({ params }) =>
    `If fuel for machinery (${params?.allowedValues.map(code).join(', ')}) is used during a Cycle,
    but there is no data on the amount of ${glossaryTypeLink(TermTermType.material)} used for agricultural equipment,
    then ${code('completeness.material')} must be ${code('false')}.
    Here, we mean the quantity of physical material used to construct agricultural machinery,
    which is then depreciated over each Cycle.`,
  'must be below or equal to 1 for unit in ha': () =>
    `The maximum value for a product with units ${code('ha')} on a Cycle with a ${schemaLink(
      'Cycle#functionalUnit',
      'functional unit'
    )} of ${code('1 ha')} is ${code('1')}.
    If you are trying to add a product with the units ${code('kg')}, use a different term.`,
  "pastureGrass key termType must be 'landCover'": ({ params }) =>
    `The Term ${code(params?.term.name)} can't be used as a key for ${termLink(pastureGrass)}.
    Please, use a term with termType ${glossaryTypeLink(TermTermType.landCover)} instead.
    This will help us gap-fill the amount of grass consumed by the herd.`,
  'the sum of all pastureGrass values must be 100': ({ params }) =>
    `The sum of all ${termLink(pastureGrass)} values must be equal to ${params.expected}%.`,
  'is outside confidence interval': (error, errorCount, allErrors) =>
    `${unique(
      (allErrors.length ? allErrors : [error]).flatMap(({ dataPath, params }) =>
        params?.outliers?.map(
          value =>
            `The ${dataPath.includes('product') ? 'product' : 'input'} value ${
              errorCount === 1 ? `${code(value)} ` : ''
            }is ${value > params?.min ? 'above' : 'below'} ${
              params?.threshold ? `${code(`${(params.threshold + (1 - params.threshold) / 2) * 100}%`)} of` : ''
            } all ${params?.group ? code(params.group) : code(params?.term?.name)} data in ${code(
              params?.country.name
            )} on the HESTIA platform.`
        )
      )
    ).join('</br>')}
    </br>
    Please double-check these values before submitting.`,
  'should be equal to boundary': ({ params }) =>
    `The calculated ${schemaLink('Site#area', 'area')} from ${code('boundary')} is ${code(params?.expected)} ha.
    This may be an error if the boundary represents the area under cultivation.`,
  'must specify the fate of cropResidue': ({ params }) =>
    `For sites of type ${params.siteType.map(code).join(' or ')},
    if data completeness is marked ${code(
      true
    )} for crop residue, the fates and quantities of the residue must be specified.
    <br/>
    The quantity must include both above and below ground crop residue, where the sum of above ground crop residue and below ground crop residue must be greater than zero.
    This can be added using the terms:
    <ul class="is-pl-3 is-list-style-disc is-nowrap">
    <li class="is-pre-wrap">${termLink({ id: 'belowGroundCropResidue', name: 'Below ground crop residue' })}; and</li>
    <li class="is-pre-wrap">${termLink({
      id: 'aboveGroundCropResidueTotal',
      name: 'Above ground crop residue total'
    })}.</li>
    </ul>
    <br/>
    The fate can be added using:
    <ul class="is-pl-3 is-list-style-disc is-nowrap">
    <li class="is-pre-wrap">the ${glossaryTypeLink(TermTermType.cropResidueManagement)} practices; and</li>
    <li class="is-pre-wrap">the ${glossaryTypeLink(TermTermType.cropResidue)} products</li>
    </ul>
    `,
  'should specify the fate of cropResidue': () =>
    `You have not specified the fate of the crop residue.
    Adding this information will improve emissions and soil carbon stock calculations.
    The fate can be added using:
    <ul class="is-pl-3 is-list-style-disc is-nowrap">
    <li class="is-pre-wrap">the ${glossaryTypeLink(TermTermType.cropResidueManagement)} practices; and/or</li>
    <li class="is-pre-wrap">the ${glossaryTypeLink(TermTermType.cropResidue)} products;</li>
    </ul>
    Remember to set ${code('completeness.cropResidue')} to ${code(
      true
    )} if both the above and below ground crop residue quantities and fates are specified.`,
  'should have the same privacy as the related source': ({ params }) =>
    `To link to a ${(params?.defaultSource || params?.source)?.dataPrivate ? 'private' : 'public'} Source,
    this Node needs to be ${(params?.defaultSource || params?.source)?.dataPrivate ? 'private' : 'public'} as well.
    Please set ${code('dataPrivate')}=${code(`${(params?.defaultSource || params?.source)?.dataPrivate}`)}.`,
  'should not use not relevant model': () =>
    `All emissions should generally have a method tier, which states how the data were modelled or how the data were collected.
    Please see the ${schemaLink(
      'Emission#methodTier',
      'schema'
    )} for the options available and definitions of the options.`,
  'should not use not relevant methodTier': () =>
    `All emissions should generally have a method tier, which states how the data were modelled or how the data were collected.
    Please see the ${schemaLink(
      'Emission#methodTier',
      'schema'
    )} for the options available and definitions of the options.`,
  'ingredients should be complete': ({ params }) =>
    `Data completeness for ${code('ingredient')} must be true for site of type ${params.siteType}`,
  'must have inputs to represent ingredients': () =>
    `Data completeness for ${code('ingredient')} cannot be true if there are no Inputs.`,
  'must have inputs representing the forage when set to true': ({ params }) =>
    `This Cycle includes animals ${params?.siteType ? `on ${params.siteType}` : ''} and completeness is set to ${code(
      true
    )} for ${code('freshForage')}.
    However, there are no Inputs representing the forage intake by animals in the Cycle (Inputs with ${code(
      'termType'
    )} ${code('freshForage')}" and a ${code('value')}).
    Either, set completeness to ${code(false)} for ${code(
      'freshForage'
    )}, make sure all fresh forage Inputs have a ${code(
      'value'
    )}, or add one or more Inputs representing the forage intake.`,
  'must specify is it an animal feed': () =>
    `This Cycle is an animal production Cycle and this Input could be an animal feed or could be for another use.
    Please specify the ${schemaLink('Input#isAnimalFeed', 'isAnimalFeed')} field on this Input.
    Knowing the fate is essential to distinguish Inputs between those which are fed to animals and those which are used for other purposes.`,
  'should specify a treatment when experimentDesign is specified': () =>
    `You have specified that this Source uses an experimental design, but have not specified the treatment for this Cycle.
    Adding the treatment provides important meta-data and will enable greater re-use of these data.
    Add the ${schemaLink('Cycle#treatment', 'treatment')} field to the Cycle.
    Note that the field ${schemaLink(
      'Cycle#commercialPracticeTreatment',
      'commercialPracticeTreatment'
    )} becomes required if treatment is specified.`,
  'should not specify both liveAnimal and animalProduct': () =>
    `You have added both a ${glossaryTypeLink(TermTermType.liveAnimal)} and ${glossaryTypeLink(
      TermTermType.animalProduct
    )} term as Products.
    If these terms refer to the same animals, this will lead to double counting:
    instead use a Property on the ${glossaryTypeLink(
      TermTermType.liveAnimal
    )} term to define the quantity of liveweight per head, carcass weight per head, etc.`,
  'should add an excreta product': () =>
    `Animal production Cycles create excreta.
    There should be at least one ${code(TermTermType.excreta)} product in this Cycle.
    Please add an ${code(TermTermType.excreta)} Product to the Cycle.
    You do not need to set a ${code('value')} for the Product, but adding a ${code('value')} is desirable.`,
  'is not an allowed excreta product': ({ params }) =>
    `This excreta product is not allowed for the animal products added to this Cycle.
    Instead, please add one of the following excreta: ${params.expected?.map(code).join(', ')}`,
  'value should sum to 100 across all values': ({ params }) =>
    `The total value of the following terms should sum up to a value of 100%:
    <ul class="is-pl-3 is-list-style-disc is-nowrap">
      ${(params.termIds || []).map(id => `<li class="is-pre-wrap">${code(id)}</li>`).join('')}
    </ul>
    Make sure to add any missing term if needed and/or adjust the values of the existing terms you already recorded.`,
  'value should sum to maximum 100 across all values': ({ params }) =>
    `The total value of the following terms should sum up to a maximum value of 100%:
    <ul class="is-pl-3 is-list-style-disc is-nowrap">
      ${(params.termIds || []).map(id => `<li class="is-pre-wrap">${code(id)}</li>`).join('')}
    </ul>
    Make sure to add any missing term if needed and/or adjust the values of the existing terms you already recorded.`,
  'rice products not allowed for this water regime practice': ({ params }) =>
    `This practice is not allowed with the products ${params.products?.map(p => code(p.name || p['@id']))}.
    Recording the right water regime for rice ensures methane emissions from flooded fields are correctly recalculated.
    To fix this error, either change the practice or the products.`,
  'must not be equal to 1 ha': ({ params }) =>
    `${code(CycleFunctionalUnit['1 ha'])} cannot be used as ${schemaLink(
      'Cycle#functionalUnit',
      'functionalUnit'
    )} for Cycles linked to Sites with ${schemaLink('Site#siteType', 'siteType')} = ${code(params?.siteType)}.
    Either change the ${schemaLink('Cycle#functionalUnit', 'functionalUnit')} to ${code(
      CycleFunctionalUnit.relative
    )} or change the ${schemaLink('Site#siteType', 'siteType')}.`,
  'should specify a value when HESTIA has a default one': ({ params }) =>
    `As you have not specified a ${code('value')}, we will assume the value is ${code(params?.expected)}.
    If this is incorrect, pelase add a value.`,
  'should contain at least one management node': ({ params }) =>
    `${
      params?.termType
        ? `Does not contain information about ${code(params.termType)}.
        Please consider adding a history of ${code(
          params.termType
        )} for as many years as possible in the Management blank nodes.`
        : `There is no Management node present in this upload.
        We recommend adding this to the Site, including information about current and historical tillage.
        `
    }
    We use this information to model CO<sub>2</sub> emissions from soil organic carbon change.`,
  'can not be used on this termType': ({ params }) =>
    `This Property can only be used on blank nodes with the following ${code('term.termType')}: ${params.expected
      .map(code)
      .join(', ')}.`,
  'not a valid GeoJSON': ({ params }) => `
  ${
    params?.invalidCoordinates === 'true'
      ? `GeoJSON data should follow the WGS84 datum.
  We have found coordinates outside of the -180 to +180 range which is not possible under WGS84.
  Please check the datum or check for other possible errors.`
      : 'This GeoJSON appears to be invalid.'
  }
  You can validate your GeoJSON using an online tool like <a href="https://geojson.io/">geojson.io</a>.
  If you think this is a mistake, please report the error ${reportIssueLink(Repository.schema, Template.bug)}.`,
  'should add the term stockingDensityPermanentPastureAverage': ({ params }) =>
    `You did not specify ${nodeLink({ '@type': NodeType.Term, '@id': params.expected })}.
    Adding this Practice is essential for Cycles with relative functional unit which happen entirely or partially on permanent pasture.
    Without this information HESTIA cannot calculate animal related emissions.`,
  'should specify the herd composition': () =>
    `We recommend adding information on the herd composition using the Animal node.
    Without this, we cannot re-model many animal related Inputs, Products, and Emissions.`,
  'should specify the pregnancy rate': ({ params }) =>
    `For female mammals in their reproductive age, we recommend adding ${nodeLink({
      '@type': NodeType.Term,
      '@id': params.expected
    })} as a ${schemaLink('Animal#properties', 'Property')}.
    This is particularly important for ruminants, as the pregnancy rate influences animals' energy requirements and is used in the estimation of grass intake.`,
  'should set both depthUpper and depthLower': () => `
    We recommend setting both ${schemaLink('Measurement#depthUpper', 'depthUpper')} and ${schemaLink(
      'Measurement#depthLower',
      'depthLower'
    )} on this Measurement.`,
  'must set both depthUpper and depthLower': () => `
    This soil measurement can vary substantially with depth.
    We require that you specify both ${schemaLink('Measurement#depthUpper', 'depthUpper')} and ${schemaLink(
      'Measurement#depthLower',
      'depthLower'
    )}.
    If you do not have these data, please delete the Measurement.`,
  'multiple cycles on the same site cannot overlap': (error, _, allErrors) =>
    `Cycles taking place on the same Site cannot overlap:
    <ul class="is-pl-3 is-list-style-disc is-nowrap">${(allErrors.length ? allErrors : [error]).map(
      ({ params }) => `<li class="is-pre-wrap">${params.ids?.map(code).join(', ')}</li>`
    )}.
    </ul>
    Please consider any of the following suggestions to resolve this conflict:
    <ul class="is-pl-3 is-list-style-disc is-nowrap">
    <li class="is-pre-wrap">You can amend the Cycles' ${schemaLink('Cycle#startDate', 'startDate')} and ${schemaLink(
      'Cycle#endDate',
      'endDate'
    )} to ensure that they do not overlap anymore.
    <li class="is-pre-wrap">You can break down your Cycles into shorter Cycles to ensure that they do not overlap anymore.
    <li class="is-pre-wrap">You can merge the overlapping Cycles into one overall Cycle if the Products of each Cycle are grown at the same time on the same Site.
    This is relevant if intercropping is used, for instance.</li>
    <li class="is-pre-wrap">You can create a different Site for each overlapping Cycle.
    This is appropriate if a field is divided into different plots, each dedicated to growing a different Product; each plot would be considered a unique Site on the platform.</li>
    </ul>
  `,
  'cycles linked together cannot be added to the same site': ({ params }) =>
    `Cycles linked together by an Impact Assessment cannot take place on the same Site.
    Please create different Sites for the following Cycles: ${params.ids?.map(code).join(', ')}.`,
  'is not an allowed animalProduct': () =>
    `The ${glossaryTypeLink(TermTermType.liveAnimal)} producing this Product is not recorded in the ${schemaLink(
      'Cycle#animals',
      'Animal'
    )} blank nodes. Either change this product or add the missing ${glossaryTypeLink(
      TermTermType.liveAnimal
    )} to the ${schemaLink('Cycle#animals', 'Animal')} blank nodes.`,
  'duration must be in specified interval': ({ params: { term } = {} }) =>
    ['shortFallow', 'shortBareFallow'].includes(term['@id'])
      ? `${code('Short fallow')} is defined as land left fallow for less than 365 days.
      Please check the ${schemaLink('Management#startDate', 'startDate')} and ${schemaLink(
        'Management#endDate',
        'endDate'
      )} on this term and ensure the difference between the two dates is less than 365 days.
      If it is more than 365 days, consider using the terms ${code('Long fallow')} or ${code('Set aside')} instead.`
      : ['longFallow', 'longBareFallow'].includes(term['@id'])
        ? `${code('Long fallow')} is defined as land left fallow for more than a year but less than five years.
        Please check the ${schemaLink('Management#startDate', 'startDate')} and ${schemaLink(
          'Management#endDate',
          'endDate'
        )} on this term and ensure the difference between the two dates falls between 365 and 1826 days.
        If it is less than 365 days, consider using the term ${code('Short fallow')} instead.
        If it is more than 1826 days, consider using the term ${code('Set aside')} instead.`
        : `${code(
            'Set aside'
          )} is defined as the temporary set aside of land for more than five years but less than twenty years.
        Please check the ${schemaLink('Management#startDate', 'startDate')} and ${schemaLink(
          'Management#endDate',
          'endDate'
        )} on this term and ensure the difference between the two dates falls between 1826 and 7305 days.
        If it is less than 1826 days, consider using the terms ${code('Short fallow')}or ${code('Long fallow')} instead.
        If it is more than 7305 days, consider using the term ${code('Other natural vegetation')} instead.`,
  'must not add the feed input to the Cycle as well': ({ params: { term } = {} }) =>
    `You have added the animal feed Input ${code(term['@id'])} to the Cycle inputs as well, which might lead to double counting.
    If you do not know the amount of ${code(term['@id'])} given to each Animal, please add the Input to the Cycle only.`,
  'must specify water type for ponds': () =>
    `For Sites with ${schemaLink('Site#siteType', 'siteType')} = ${code('pond')}, you must specify the ${nodeLink({
      '@type': NodeType.Term,
      '@id': 'waterSalinity',
      name: 'Water salinity'
    })} or the water type (${nodeLink({
      '@type': NodeType.Term,
      '@id': 'salineWater',
      name: 'Saline water'
    })} / ${nodeLink({ '@type': NodeType.Term, '@id': 'freshWater', name: 'Fresh water' })} / ${nodeLink({
      '@type': NodeType.Term,
      '@id': 'brackishWater',
      name: 'Brackish water'
    })}).
    This will allow us to calculate methane emissions from constructed waterbodies.`,
  'invalid water salinity': ({ params }) =>
    `The water type ${params?.current} is not consistent with the Water salinity you specified.
    Make sure the Water salinity value is correct and either fix it or update the water type accordingly.`,
  'must add substrate inputs': () =>
    'The substrate must be specified when a substrate-based protected cropping system has been added.',
  'should not be equal to cycleDuration for crop': ({ params }) =>
    `For crop production Cycles, siteDuration must represent the period from harvest of the previous crop to harvest of the current crop.
    Here, you have stated that ${schemaLink('Cycle#cycleDuration', 'cycleDuration')} represents the period from
    ${code(params?.current)} to harvest of the current crop, and set ${schemaLink(
      'Cycle#cycleDuration',
      'cycleDuration'
    )} as equal to ${schemaLink('Cycle#siteDuration', 'siteDuration')}.
    Please check the affected fields for errors.`,
  'must be linked via a defaultSource': () =>
    `You can only have a ${code('source.id')} if this is used as a ${code('defaultSource')} for either a Cycle or Site.
    If there is sufficient data from this source, then create a new Cycle or Site with ${code('the source.id')} as the ${code('defaultSource')}.
    Otherwise, remove this Source and, on the relevant data, change the method classification, e.g. to inconsistent external sources, with a description of the source used.`,
  'is required when using other model': () =>
    `If you are using the term ${code('Other model')}, please provide a short description of the model in ${code(
      'methodModelDescription'
    )}, including any references to model documentation.`,
  'should add the term productivePhasePermanentCrops': () =>
    `You need to specify whether the permanent crop is in its productive phase when the value of the primary product is ${code('0')}.
    To do this, add the ${termLink({
      id: 'productivePhasePermanentCrops',
      name: 'Productive phase (permanent crops)'
    })} practice and set a value of ${code('true')} or ${code('false')}.`,
  'must have a single inputs of termType waste': () =>
    `Can only be linked to a single ${glossaryTypeLink(TermTermType.waste)} input.`,
  'must be below maximum cycleDuration': ({ params }) =>
    `The maximum duration for this crop production cycle is ${(params as any).limit} days,
    but the calculated duration from the end date and start date provided is ${params.current}.`,
  'should add the term pastureGrass': () =>
    `For Cycles with ${code('siteType=permanent pasture')} we recommend specifying the type(s) of ${code('Pasture grass')} grown.
    This is particularly important for ruminants and will allow for a more accurate estimate of the amount of grass grazed.`
};

const requiredPropertyError = (message: string, error?: IValidationError) => {
  const field = message.split("'")[1].replace("'", '');
  const nodeType = dataPathLabel(error?.dataPath);
  return `You are missing the following required field: ${code(field)}. ${
    nodeType && nodeType.toLowerCase() !== 'completeness'
      ? `Either: add ${code(field)}, or if you if you did not intend to add this ${nodeType}, please remove all fields on this ${nodeType}.`
      : ''
  }`.trim();
};

const parseDefaultMessage = (message?: string, error?: IValidationError) =>
  `<span class="is-capitalized-first-letter">${
    message?.startsWith('should have required property') ? requiredPropertyError(message, error) : message
  }</span>`;

export const formatCustomErrorMessage = (
  message?: string,
  error?: IValidationError,
  allErrors: IValidationError[] = []
) => {
  const formattedMessage = customErrorMessage?.[message]
    ? customErrorMessage[message](error!, allErrors.length || 1, allErrors)
    : parseDefaultMessage(message, error);
  return formattedMessage ? `<p class="is-normal">${formattedMessage}</p>` : '';
};

export const formatError = (error?: IValidationError, allErrors: IValidationError[] = []): IValidationError =>
  error
    ? {
        level: 'error',
        ...error,
        message: formatCustomErrorMessage(error.message, error, allErrors)
      }
    : undefined;

export const errorHasError = (error?: IValidationError) => error && (error.level === 'error' || !error.level);

export const errorHasWarning = (error?: IValidationError) => error && error.level === 'warning';

export const isMissingPropertyError = ({ params }: IValidationError) => !!params && 'missingProperty' in params;

export const isMissingOneOfError = ({ keyword, schemaPath }: IValidationError) =>
  keyword === 'required' && (schemaPath || '').includes('oneOf');

const isFailingKeywordError = ({ params }: IValidationError) => !!params && 'failingKeyword' in params;

export const filterError = (error: IValidationError) => [isFailingKeywordError].every(func => !func(error));

export const missingNodeErrors = (errors: IValidationError[]) =>
  errors.filter(
    ({ keyword, params }) =>
      keyword === 'required' && (params?.missingProperty === '@type' || params?.missingProperty === '@id')
  );
