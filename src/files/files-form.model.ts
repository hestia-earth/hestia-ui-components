/* eslint-disable complexity */
import get from 'lodash.get';
import moment from 'moment';
import 'moment/locale/en-gb';
import { v4 as uuidv4 } from 'uuid';
import { diffInDays, isNumber } from '@hestia-earth/utils';
import {
  JSON as HestiaJson,
  SchemaType,
  NodeType,
  isTypeNode,
  isTypeValid,
  isExpandable,
  Site,
  Organisation,
  typeToSchemaType
} from '@hestia-earth/schema';
import { definitions, definition, IDefinitionObject, IDefinitionArray, genericType } from '@hestia-earth/json-schema';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faMap } from '@fortawesome/free-solid-svg-icons';

import { IValidationError, validationErrorLevel } from '../schema/schema-validation.model';
import {
  formatCustomErrorMessage,
  formatError,
  isMissingPropertyError,
  errorHasError,
  errorHasWarning,
  missingNodeErrors,
  isMissingOneOfError,
  isMigrationError,
  migrationErrorMessage
} from './files-error.model';
import {
  schemaRequiredProperties,
  valueTypeToDefault,
  definitionToSchemaType,
  availableProperties,
  nestingTypeEnabled,
  nestingEnabled,
  linkTypeEnabled,
  isSchemaIri
} from '../schema/schema.service';
import { matchCountry } from '../search/search.model';
import {
  code,
  glossaryBaseUrl,
  glossaryLink,
  isExternal,
  safeJSONParse,
  safeJSONStringify,
  schemaLink
} from '../common/utils';
import { mapsUrl } from '../common/maps-utils';

const missingNodeErrorMessage = ({ '@type': _t, type, name }: any, missingIdError?: IValidationError) =>
  (_t || type) === 'Term'
    ? // migrated from a value to another
      isMigrationError(missingIdError)
      ? migrationErrorMessage(missingIdError)
      : `${name ? `The term "${name}"` : 'This term'} doesn't match a term in the ${glossaryLink('Glossary of Terms')}.
      Please check the ${glossaryLink('Glossary')} for the correct ${code('@id')} or ${code('name')} and spelling.`
    : `This ${schemaLink(_t || type)} does not exist in Hestia. If you are trying to link to an existing
    ${schemaLink(_t || type)} in Hestia, please check the ${code('name')} or ${code('@id')} is correct.
    If you are trying to create a new ${schemaLink(_t || type)}, please identify it with an ${code('id')} field.`;

export const ARRAY_DELIMITER = ';';
const ignoreKeys = ['filepath', '_cache'];

export type suggestionType = NodeType | 'select' | 'default';
export const defaultSuggestionType: suggestionType = 'default';

interface INodePropertySuggestion {
  type?: suggestionType;
  queries?: any[];
  values?: string[];
  parentLevel?: number;
  isLinkNode?: boolean;
  isUniqueKey?: string;
}

export interface INewProperty {
  name?: string;
  prop?: definition;
}

export interface IErrorProperty {
  level?: validationErrorLevel;
  message?: string;
}

export interface INodeProperty {
  /**
   * Whether or not this property was added by the user
   */
  id: string;
  isAdded?: boolean;
  schemaType: SchemaType;
  key: string;
  fullKey: string;
  value: any;
  addPropertyEnabled: boolean;
  suggestions?: INodePropertySuggestion;
  schema?: definition;
  error?: IValidationError;
  hasError?: boolean;
  hasWarning?: boolean;
  editable?: boolean;
  closedVisible?: boolean;
  newProperty?: INewProperty;
  newError?: IErrorProperty;
  isRequired?: boolean;
  isHidden?: boolean;
  isArray?: boolean;
  isExpandable?: boolean;
  isOpen?: boolean;
  properties: INodeProperty[];
  placeholder?: string;
  externalUrl?: {
    url?: string;
    title: string;
    // default is external-link-alt
    icon?: IconDefinition;
    urlParamValue?: boolean;
  };
  // making changes on the property
  loading?: boolean;
  // user made changes to the original value
  changed?: boolean;
  formatter: (value: any) => string;
}

const stringValue = (value: any) => (typeof value === 'undefined' || value === null ? '' : value).toString();
const nonExpandableArrayDataPath = (key: string) => key.replace(/\[\d+\]$/, '');
export const keyToDataPath = (key: string) => (key?.length ? `.${key}` : '');
export const dataPathToKey = (dataPath = '', trimArray = false) =>
  trimArray ? nonExpandableArrayDataPath(dataPath.substring(1)) : dataPath.substring(1);
export const parentKey = (key: string) => {
  const keys = (key.startsWith('.') ? key.substring(1) : key).split('.');
  keys.pop();
  return keys.join('.');
};

/**
 * Sort properties having errors, type, id, ...properties (simple), and finally the groups
 *
 * @param a {INodeProperty}
 * @param b {INodeProperty}
 */
export const sortProperties = (a: INodeProperty, b: INodeProperty) => {
  if (isNumber(a.key) && isNumber(b.key)) {
    return +a.key - +b.key;
  }
  if (a.isExpandable) {
    return b.isExpandable ? a.key.localeCompare(b.key) : 1;
  }
  if (b.isExpandable) {
    return -1;
  }
  if (!a.key) {
    return -1;
  }
  const aKey = a.key.replace('@', '');
  const bKey = b.key.replace('@', '');
  if (aKey === 'type') {
    return -1;
  }
  if (bKey === 'type') {
    return 1;
  }
  if (aKey === 'id' && bKey !== 'type') {
    return -1;
  }
  if (a.hasError || a.hasWarning) {
    return -1;
  }
  return a.key.localeCompare(b.key);
};

const defaultPropertyError: {
  [key: string]: (error: any, property: INodeProperty) => string | undefined;
} = {
  max: ({ max }) => `should be <= ${max}`,
  min: ({ min }) => `should be >= ${min}`,
  pattern: ({ requiredPattern }) => `should match pattern "${requiredPattern}"`,
  required: (_e, { schemaType }) => (schemaType ? `should be set or linked to another ${schemaType}` : undefined)
};

export const formatPropertyError = (errors: any, property: INodeProperty) => {
  const key = Object.keys(errors)[0];
  return key in defaultPropertyError ? formatCustomErrorMessage(defaultPropertyError[key](errors[key], property)) : '';
};

export const formatter = (key: string) => (value: any) =>
  typeof value === 'object' ? value.bibliography?.title || value[key] || value.name : value;

export const hasError = value => (Array.isArray(value) ? value.some(hasError) : value.hasError);

export const hasWarning = value => (Array.isArray(value) ? value.some(hasWarning) : value.hasWarning);

const compileSuggestionQueries = (key: string, allOf: any[]) => {
  const conditions = (allOf || [])
    .filter(v => (v.if?.required || []).includes(key))
    .map(v => v.then?.properties?.[key])
    .map(v => v?.items?.properties || v?.properties)
    .filter(Boolean);
  return conditions.length
    ? [
        {
          bool: {
            should: conditions.flatMap(cond =>
              Object.keys(cond).flatMap(k =>
                (cond[k].enum || []).map(v => ({
                  match: { [k]: v }
                }))
              )
            ),
            minimum_should_match: 1
          }
        }
      ]
    : [];
};

// default suggest for linking nodes together using the `id` field
const suggestLinkNodes = (type: NodeType, fullKey: string): INodePropertySuggestion | undefined => {
  const keys = fullKey.split('.');
  const key = keys.pop();
  return key === 'id' && keys.length >= 1 ? { type, isLinkNode: true } : undefined;
};

const suggestExistingNode = (type: NodeType, fullKey: string): INodePropertySuggestion | undefined => {
  const keys = fullKey.split('.');
  const key = keys.pop()!;
  return keys.length >= 1 && ['@id', 'openLCAId'].includes(key) ? { type, isUniqueKey: '@id' } : undefined;
};

const defaultNodeTypeSuggestion = (type: NodeType) => (fullKey: string) =>
  suggestLinkNodes(type, fullKey) || suggestExistingNode(type, fullKey);

const termSuggestions = (parent: string, { allOf }: IDefinitionObject) => ({
  type: NodeType.Term,
  queries: [
    ...compileSuggestionQueries(nonExpandableArrayDataPath(parent), allOf!),
    ...(['country'].includes(parent) ? [matchCountry] : [])
  ]
});

const typeToSuggestion: {
  [type in SchemaType | NodeType | genericType | 'integer']?: (
    fullKey: string,
    parentSchema?: definition,
    schema?: definition
  ) => INodePropertySuggestion | any;
} = {
  [NodeType.Actor]: defaultNodeTypeSuggestion(NodeType.Actor),
  [NodeType.Cycle]: defaultNodeTypeSuggestion(NodeType.Cycle),
  [NodeType.ImpactAssessment]: defaultNodeTypeSuggestion(NodeType.ImpactAssessment),
  [NodeType.Organisation]: defaultNodeTypeSuggestion(NodeType.Organisation),
  [NodeType.Site]: defaultNodeTypeSuggestion(NodeType.Site),
  [NodeType.Source]: defaultNodeTypeSuggestion(NodeType.Source),
  [NodeType.Term]: (fullKey, parentSchema) => {
    const keys = fullKey.split('.');
    const key = keys.pop()!;
    return ['name'].includes(key) && keys.length >= 1
      ? termSuggestions(keys.pop()!, parentSchema as IDefinitionObject)
      : defaultNodeTypeSuggestion(NodeType.Term)(fullKey);
  },
  [SchemaType.Bibliography]: fullKey => {
    const keys = fullKey.split('.');
    const key = keys.pop()!;
    const parent = keys.pop()!;
    // prevent replacing a top-level source
    return ['title', 'documentDOI', 'scopus'].includes(key) && parent === 'bibliography' && keys.length >= 1
      ? {
          type: NodeType.Source,
          parentLevel: 1
        }
      : undefined;
  },
  number: (_a, _b, schema) => {
    const values = (schema as any).const ? [(schema as any).const] : [];
    return {
      type: values.length > 0 ? 'select' : defaultSuggestionType,
      // exclude suggesting SchemaType
      values
    };
  },
  integer: (_a, _b, schema) => {
    const values = (schema as any).const ? [(schema as any).const] : [];
    return {
      type: values.length > 0 ? 'select' : defaultSuggestionType,
      // exclude suggesting SchemaType
      values
    };
  },
  string: (fullKey, _b, schema) => {
    // exclude suggesting SchemaType
    const values = (schema as any).const
      ? [(schema as any).const]
      : ((schema as any).enum || []).filter(val => !Object.values(SchemaType).includes(val));
    const key = fullKey.split('.').pop();
    return {
      type: values.length > 0 && key !== 'type' ? 'select' : defaultSuggestionType,
      values
    };
  },
  boolean: _a => ({
    type: 'select',
    values: ['true', 'false']
  })
};

const enableSuggestions = (
  parentSchema: definition,
  type: SchemaType | NodeType | genericType,
  fullKey: string,
  schema?: definition
) => {
  const suggestion = type in typeToSuggestion ? typeToSuggestion[type]!(fullKey, parentSchema) : undefined;
  return (
    suggestion ||
    (schema && (schema.type as genericType) in typeToSuggestion
      ? typeToSuggestion[schema.type as genericType]!(fullKey, parentSchema, schema)
      : undefined)
  );
};

export const nodeAvailableProperties = (
  node: HestiaJson<SchemaType>,
  { fullKey, schema, schemaType }: Partial<INodeProperty>
) => availableProperties(schema!, schemaType!, fullKey ? get(node, fullKey, {}) : node, fullKey!.length > 0);

export const isAddPropertyEnabled = (
  node: HestiaJson<SchemaType>,
  schemaType: SchemaType,
  schema: definition,
  fullKey = ''
) =>
  !!schema &&
  !!Object.keys(nodeAvailableProperties(node, { fullKey, schema })).length &&
  (!fullKey || nestingTypeEnabled(schemaType));

const editableKey = (key: string) => !['type', '@type', 'termType'].includes(key);

export const propertyId = () => uuidv4();

const isGeojson = (schema: any) => schema.geojson;

const propertyUrl = (node: HestiaJson<SchemaType>, key: string) =>
  ['@id', 'id', 'name'].includes(key) && linkTypeEnabled(node['@type'] || node.type)
    ? node['@id'] ||
      // handle links on Community Edition
      (isExternal() && node.id)
      ? {
          url: `/${(node['@type'] || node.type).toLowerCase()}/${node['@id'] || node.id}`,
          title: 'Open'
        }
      : node['@type'] === SchemaType.Term
        ? {
            url: `${glossaryBaseUrl()}?query=`,
            urlParamValue: true,
            title: 'Search in Glossary'
          }
        : undefined
    : undefined;

const propertyMapsUrl = (node: Site | Organisation, key: string, value) =>
  ['type'].includes(key) &&
  [SchemaType.Site.toString(), SchemaType.Organisation.toString()].includes(stringValue(value) || node.type)
    ? {
        url: mapsUrl({ lat: node.latitude, lng: node.longitude }),
        title: 'View on maps',
        icon: faMap
      }
    : undefined;

const propertyExternalUrl = (node: HestiaJson<SchemaType>, key: string, value: any) =>
  propertyUrl(node, key) || propertyMapsUrl(node as any, key, value);

const arrayPlaceholder = (valueType: genericType) => `Type a ${valueType} then press Enter`;

const defaultPlaceholder = ({ type, examples, geojson }: any, valueType: genericType) =>
  geojson
    ? 'Must be a valid GeoJSON'
    : examples?.length
      ? Array.isArray(examples[0])
        ? arrayPlaceholder(valueType)
        : examples[0]
      : `Must be of type ${type || valueType}`;

const schemaPlacholder = (schema: any, fullKey: string) => {
  const [parent, ...keys] = fullKey.split('.');
  const properties = schema.properties || {};
  const { examples } = parent in properties ? properties[parent] : { examples: [] };
  return examples?.length && typeof examples[0] === 'object' ? get(examples[0], keys.join('.'), null) : null;
};

const propertyPlaceholder = (schema: any, valueType: genericType, fullKey = '', parentSchema?: any) =>
  schema.type === 'array'
    ? arrayPlaceholder(valueType)
    : (parentSchema && fullKey?.length ? schemaPlacholder(parentSchema, fullKey) : null) ||
      defaultPlaceholder(schema, valueType);

const propertyTypeValue: {
  [type: string]: (value: any, schema: definition) => any;
} = {
  object: safeJSONStringify,
  array: (val, schema) => {
    const { items } = schema as IDefinitionArray;
    return '$ref' in items
      ? val
      : Array.isArray(val)
        ? val.map(value => propertyValue(items, value)).join(ARRAY_DELIMITER)
        : val;
  },
  json: val => val || '',
  string: val => (`${val}` || '').trim(),
  number: val => +(`${val}` || '0'),
  integer: val => +(`${val}` || '0'),
  boolean: val => (`${val}` || '').toLowerCase() === 'true'
};

const propertyValue = (schema: definition, value: any) => {
  const valueType = isGeojson(schema)
    ? 'json'
    : schema.type
      ? Array.isArray(schema.type) && (typeof value) in propertyTypeValue
        ? typeof value
        : (schema.type as string)
      : 'object';
  return propertyTypeValue[valueType in propertyTypeValue ? valueType : 'string'](value, schema);
};

const propertyValueType = (schema: definition, value?: any) =>
  (isGeojson(schema)
    ? 'json'
    : schema.type
      ? Array.isArray(schema.type)
        ? schema.type[0]
        : schema.type === 'array'
          ? propertyValueType({ ...schema, type: [schema.items.type as genericType] })
          : schema.type
      : typeof value) as genericType;

const propertyTypeParseValue: {
  [type: string]: (value: any, schema: definition) => any;
} = {
  undefined: () => undefined,
  object: val => safeJSONParse(val),
  array: (val, schema) => {
    const { items } = schema as IDefinitionArray;
    return '$ref' in items
      ? val
      : !val || Array.isArray(val)
        ? val
        : val.split(ARRAY_DELIMITER).map(value => parseNewValue(items, value));
  },
  json: val => val || '',
  string: val => (`${val}` || '').trim(),
  number: val => +(`${val}` || '0'),
  integer: val => +(`${val}` || '0'),
  boolean: val => (`${val}` || '').toLowerCase() === 'true'
};

/**
 * Convert value to the final type in the schema.
 *
 * @param schema Definition of the value.
 * @param value
 * @returns Value in the correct format.
 */
export const parseNewValue = (schema: definition, value: any) =>
  schema?.type
    ? Array.isArray(schema?.type)
      ? propertyTypeParseValue[typeof value](value, schema)
      : propertyTypeParseValue[isGeojson(schema) ? 'json' : (schema.type as string)](value, schema)
    : value;

const isOpenDefault = (schemaType: SchemaType) => [SchemaType.Term].includes(schemaType);

// can the property be seen when the group is closed
const isKeyClosedVisible = (key: string) => ['id', '@id', 'originalId'].includes(key);

const isKeyHidden = (key: string) => ['type', '@type'].includes(key);

const isKeyRequired = (key: string, def?: IDefinitionObject) =>
  ['type', 'id'].includes(key) || (def?.required || []).includes(key);

export const propertyError =
  (errors: IValidationError[]) =>
  (dataPath: string, nonExpandableArray = false) =>
    errors.find(
      error =>
        (error.dataPath === dataPath ||
          (nonExpandableArray &&
            nonExpandableArrayDataPath(error.dataPath!) === nonExpandableArrayDataPath(dataPath))) &&
        (dataPath.length > 0 || !isMissingPropertyError(error))
    );

const missingPropertyErrors = (
  node: HestiaJson<SchemaType>,
  errors: IValidationError[],
  parent = ''
): INodeProperty[] => {
  const allErrors = errors.filter(error => error.dataPath === keyToDataPath(parent) && isMissingPropertyError(error));
  // check for node not found errors
  const missingNode = missingNodeErrors(allErrors);
  return missingNode.length === 2
    ? [
        {
          id: propertyId(),
          schemaType: node.type,
          suggestions: { type: defaultSuggestionType },
          key: '',
          fullKey: parent,
          value: '',
          error: {
            level: 'error',
            message: missingNodeErrorMessage(
              node,
              missingNode.find(({ params }) => params?.missingProperty === '@id')
            )
          },
          hasError: true,
          properties: [],
          addPropertyEnabled: false,
          formatter: formatter('')
        }
      ]
    : allErrors.map(error => {
        const { missingProperty } = error.params!;
        const fullKey = parent ? `${parent}.${missingProperty}` : missingProperty;
        const missingOneOf = isMissingOneOfError(error);
        return {
          id: propertyId(),
          schemaType: node.type,
          suggestions: { type: defaultSuggestionType },
          key: missingProperty,
          fullKey,
          value: '',
          editable: false,
          error: formatError(error),
          hasError: missingOneOf ? false : errorHasError(error),
          hasWarning: missingOneOf || errorHasWarning(error),
          properties: [],
          addPropertyEnabled: true,
          formatter: formatter(missingProperty)
        };
      });
};

const propertyFromNode =
  (
    schemas: definitions,
    errors: IValidationError[],
    nodeSchema: definition,
    deepEditable: boolean,
    parent = '',
    node: any
  ) =>
  (key: string): INodeProperty => {
    const value = node[key];
    const expandable = isExpandable(value);
    const isArray = Array.isArray(value);
    const nonExpandableArray = isArray && !expandable;
    const fullKey = Array.isArray(node) ? `${parent}[${key}]` : [parent, key].filter(Boolean).join('.');
    const nodeType = node.type || node['@type'];
    const schemaType = expandable
      ? isArray && value.length
        ? value[0].type || value[0]['@type']
        : value.type || value['@type']
      : nodeType;
    const schema =
      (schemas && isTypeValid({ type: schemaType })
        ? expandable
          ? schemas[schemaType]
          : schemas[schemaType].properties[key]
        : null) || {};
    const isIri = isSchemaIri(schema);
    const error = propertyError(errors)(keyToDataPath(fullKey), nonExpandableArray);
    const parentSchema = expandable
      ? // TODO: for non-array, should be schema unless last recursion
        isArray
        ? schema
        : nodeSchema
      : nodeSchema;
    const properties =
      expandable || isIri ? recursiveProperties(schemas, errors, parentSchema, deepEditable, fullKey)(value) : [];
    const inError = errorHasError(error) || hasError(properties);
    const inWarning = errorHasWarning(error) || hasWarning(properties);
    const editable =
      isTypeNode(node['@type']) || (expandable && isTypeNode(value['@type']))
        ? false
        : // prevent editing/adding new properties on existing sub-nodes
          !deepEditable &&
            isTypeNode(schemaType) &&
            (expandable ? value.type !== NodeType.Term : parent && node.type !== NodeType.Term)
          ? false
          : // handle IRI
            (!schemaType && key === '@id') || editableKey(key);
    const valueType = propertyValueType(schema, value);
    return {
      id: propertyId(),
      schemaType: isIri ? null : schemaType,
      schema,
      key,
      fullKey,
      value: propertyValue(schema, value),
      editable: inError || editable,
      suggestions: enableSuggestions(nodeSchema, node.type, fullKey, schema),
      addPropertyEnabled: isAddPropertyEnabled(node, schemaType, schema, fullKey),
      newProperty: {},
      newError: { level: 'warning' },
      error: formatError(error),
      isHidden: parent && isKeyHidden(key),
      closedVisible: isKeyClosedVisible(key),
      hasError: inError,
      hasWarning: inWarning,
      isRequired: isKeyRequired(key, schemas[nodeType]),
      isArray,
      isExpandable: expandable,
      isOpen: isOpenDefault(schemaType),
      properties,
      placeholder: propertyPlaceholder(schema, valueType, fullKey, parentSchema),
      // only set external url on nested properties
      externalUrl: parent ? propertyExternalUrl(node, key, value) : null,
      formatter: formatter(key)
    };
  };

export const singleProperty = (schemas: definitions, errors: IValidationError[], node: any, fullKey: string) => {
  const [_t, ...keys] = fullKey.split('.');
  const key = keys.pop()!;
  const parent = keys.join('.');
  const parentNode = parent.length ? get(node, parent) : node;
  const schemaType = typeToSchemaType(parentNode.type || parentNode['@type']);
  const parentSchema = schemas[schemaType];
  return propertyFromNode(schemas, errors, parentSchema, true, parent, parentNode)(key);
};

export const recursiveProperties =
  (schemas: definitions, errors: IValidationError[], nodeSchema: definition, deepEditable: boolean, parent = '') =>
  (node: any): INodeProperty[] =>
    Object.keys(node)
      .filter(key => !ignoreKeys.includes(key))
      .map(propertyFromNode(schemas, errors, nodeSchema, deepEditable, parent, node))
      .concat(...missingPropertyErrors(node, errors, parent))
      .sort(sortProperties);

const fullKeyParts = (fullKey: string) =>
  fullKey.split('.').flatMap(val => {
    if (val.endsWith(']')) {
      const [key, index] = val.split('[');
      return [key, index.replace(']', '')];
    }
    return val;
  });

export const updateProperties = (
  { properties }: Partial<INodeProperty>,
  updater: (prop: INodeProperty) => INodeProperty
) => (properties || []).map(prop => updateProperties(updater(prop), updater));

export const typeToNewProperty = (
  schemas: definitions,
  schema: definition,
  nodeSchema: definition,
  node: any,
  fullKey: string
): INodeProperty => {
  const key = fullKeyParts(fullKey).pop()!;
  const mappings: {
    [key: string]: () => INodeProperty;
  } = {
    object: () => {
      const { schemaType, properties } = schemaRequiredProperties(schemas, schema as IDefinitionObject);
      node[key] = node[key] || {};
      node[key].type = schemaType;
      // TODO: handle nodeSchema should be schema unless last recursion
      const props = properties
        .filter(prop => nestingEnabled(schemaType, prop.key) && !!prop.value)
        .map(v => ({
          ...typeToNewProperty(schemas, v.value, nodeSchema, node[key], `${fullKey}.${v.key}`),
          isRequired: true
        }));
      const value = props.reduce((prev, curr) => ({ ...prev, [curr.key]: curr.value }), {});
      return {
        id: propertyId(),
        isAdded: true,
        schema: schemas[schemaType],
        schemaType,
        key,
        fullKey,
        value,
        editable: editableKey(key),
        isHidden: isKeyHidden(key),
        closedVisible: isKeyClosedVisible(key),
        suggestions: enableSuggestions(nodeSchema, schemaType, fullKey),
        properties: props.sort(sortProperties),
        addPropertyEnabled: isAddPropertyEnabled(node, schemaType, schemas[schemaType], key),
        newProperty: {},
        newError: { level: 'warning' },
        isRequired: isKeyRequired(key, schema as any),
        isArray: false,
        isExpandable: true,
        isOpen: isOpenDefault(schemaType),
        formatter: formatter(key)
      };
    },
    array: () => {
      const { items } = schema as IDefinitionArray;
      // array of string, number and boolean
      if (items.type) {
        return mappings.default();
      }
      const schemaType = definitionToSchemaType(items as IDefinitionObject);
      node[key] = node[key] || [];
      node[key].push({});
      const property = typeToNewProperty(schemas, items, schemas[schemaType], node[key][0], `${fullKey}[0]`);
      return {
        id: propertyId(),
        isAdded: true,
        schema,
        schemaType,
        key,
        fullKey,
        value: [property.value],
        editable: editableKey(key),
        isHidden: isKeyHidden(key),
        closedVisible: isKeyClosedVisible(key),
        suggestions: undefined,
        properties: [property],
        newProperty: {},
        newError: { level: 'warning' },
        isArray: true,
        isExpandable: true,
        addPropertyEnabled: false,
        formatter: formatter(key)
      };
    },
    geojson: () => typeToNewProperty(schemas, { type: 'json' } as any, nodeSchema, node, fullKey),
    default: () => {
      const valueType = propertyValueType(schema);
      const value = schema.default || (schema as any).const || valueTypeToDefault[valueType];
      node[key] = value;
      return {
        id: propertyId(),
        isAdded: true,
        schema,
        schemaType: node.type,
        key,
        fullKey,
        value: propertyValue(schema, value),
        editable: editableKey(key),
        isHidden: isKeyHidden(key),
        closedVisible: isKeyClosedVisible(key),
        suggestions: enableSuggestions(nodeSchema, node.type, fullKey, schema),
        placeholder: propertyPlaceholder(schema, valueType, fullKey, nodeSchema),
        externalUrl: propertyExternalUrl(node, key, value),
        properties: [],
        addPropertyEnabled: false,
        formatter: formatter(key)
      };
    }
  };
  const mapped = isGeojson(schema)
    ? mappings.geojson
    : !schema.type
      ? mappings.object
      : (schema.type as string) in mappings
        ? mappings[schema.type as string]
        : mappings.default;
  return mapped();
};

export const findProperty = (properties: INodeProperty[], fullKey: string) => {
  for (const p of properties) {
    if (p.fullKey === fullKey) {
      return p;
    }
    const childP = findProperty(p.properties, fullKey);
    if (childP) {
      return childP;
    }
  }
  return null;
};

export const parentProperty = (properties: INodeProperty[], property: INodeProperty) => {
  const allKeys = property.fullKey.split('.');
  [...new Array(property.suggestions?.parentLevel || 0)].map(() => allKeys.pop());
  const parentKeys = fullKeyParts(allKeys.join('.'));
  parentKeys.pop();
  const parent = parentKeys.reduce((prev, curr, currIndex): any => {
    const prop = prev.find(v => v.key === curr);
    return currIndex === parentKeys.length - 1 ? prop || ({} as INodeProperty) : prop!.properties || [];
  }, properties) as any as INodeProperty;
  return Array.isArray(parent) ? property : parent;
};

export const nestedProperty = ({ properties }: INodeProperty, nestedKey: string) =>
  properties.find(({ key }) => key === nestedKey);

export const siblingProperty = (properties: INodeProperty[], property: INodeProperty, siblingKey: string) => {
  const parent = parentProperty(properties, property);
  const parentProperties = parent === property ? properties : parent.properties;
  return parentProperties.find(prop => prop.key === siblingKey);
};

export const groupChanged = (properties: INodeProperty[], key: string, value?: any) => {
  try {
    const parentKeys = fullKeyParts(key);
    parentKeys.reduce((prev, curr, currIndex) => {
      if (currIndex === parentKeys.length - 1 && value === null) {
        return prev.splice(
          prev.findIndex(v => v.key === curr),
          1
        );
      } else {
        const prop = prev.find(v => v.key === curr)!;
        prop.changed = true;
        return prop.properties;
      }
    }, properties);
  } catch (_err) {
    // ignore error
  }
};

export const refreshPropertyKeys = (property: INodeProperty) =>
  property.properties.map((prop, i) => {
    prop.fullKey = `${property.fullKey}${property.isArray ? `[${i}]` : `.${prop.key}`}`;
    prop.key = property.isArray ? `${i}` : prop.key;
    return refreshPropertyKeys(prop);
  });

export const calculateCycleDurationEnabled = (properties: INodeProperty[], property: INodeProperty) => {
  const startDate = siblingProperty(properties, property, 'startDate');
  const endDate = siblingProperty(properties, property, 'endDate');
  return !!startDate?.value && !!endDate?.value;
};

export const calculateCycleDuration = (properties: INodeProperty[], property: INodeProperty) => {
  const startDate = siblingProperty(properties, property, 'startDate')!;
  const endDate = siblingProperty(properties, property, 'endDate')!;
  return diffInDays(startDate.value, endDate.value);
};

export const calculateCycleStartDateEnabled = (properties: INodeProperty[], property: INodeProperty) => {
  const cycleDuration = siblingProperty(properties, property, 'cycleDuration');
  const endDate = siblingProperty(properties, property, 'endDate');
  return !!cycleDuration?.value && !!endDate?.value;
};

export const calculateCycleStartDate = (properties: INodeProperty[], property: INodeProperty) => {
  const cycleDuration = siblingProperty(properties, property, 'cycleDuration')!;
  const endDate = siblingProperty(properties, property, 'endDate')!;
  return moment(endDate.value).locale('en-gb').subtract(cycleDuration.value, 'days').format('YYYY-MM-DD');
};
