import { ChangeDetectionStrategy, Component, computed, effect, input, signal } from '@angular/core';
import { NgTemplateOutlet, DecimalPipe, JsonPipe } from '@angular/common';
import { parse } from 'papaparse';
import { SchemaType, NodeType, JSON as HestiaJson, isExpandable, typeToSchemaType } from '@hestia-earth/schema';
import { File, fileExt, maxFileSizeMb, pipelineStatus, status, SupportedExtensions } from '@hestia-earth/api';
import { toCsv, ErrorKeys as SchemaErrorKeys } from '@hestia-earth/schema-convert';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import get from 'lodash.get';

import { baseUrl, reportIssueUrl, Repository, schemaBaseUrl } from '../../common';
import { PluralizePipe } from '../../common/pluralize.pipe';
import { EllipsisPipe } from '../../common/ellipsis.pipe';

const nodeTypeToString = (type: NodeType) => `${type.charAt(0).toLowerCase()}${type.substring(1)}`;

const acceptedTypes = Object.values(NodeType)
  .filter(val => val !== NodeType.Term)
  .map(nodeTypeToString);

const issueLink = (fileStatus: status | pipelineStatus, error: any) =>
  `${reportIssueUrl(Repository.frontend)}?issue[title]=${encodeURIComponent(
    'Issue uploading a file on the platform'
  )}&issue[description]=${encodeURIComponent(
    `
  # 🐞 Bug Report

  ## Error

  \`\`\`
  ${JSON.stringify(error)}
  \`\`\`

  ## Details

  * Path: \`${window.location.href}\`
  * Stage: \`${fileStatus?.replace('Done', '')?.replace('Error', '')}\`

  /label ~bug
  /label ~"priority::MEDIUM"
      `.trim()
  )}`;

export enum ErrorKeys {
  PrivacyNotAllowed = 'privacy-not-allowed',
  NoData = 'no-data',
  NoHeaders = 'no-headers',
  InvalidJSON = 'invalid-json',
  InvalidSheetName = 'invalid-sheet-name',
  InvalidFirstColumn = 'invalid-first-column',
  InvalidExcelFile = 'invalid-excel-file',
  DuplicatedHeaders = 'duplicated-headers',
  DuplicatedIds = 'duplicated-ids',
  PropertyRequired = 'property-required',
  PropertyInternal = 'property-internal',
  UploadsLimit = 'upload-limit',
  NestedHeaders = 'nested-headers',
  ReferenceExistingHeaders = 'reference-existing-headers',
  MaxSize = 'max-size',
  MaxRows = 'max-rows',
  Mendeley = "'content-type'",
  Timeout = 'TimeoutError: Timeout has occurred'
}

interface ICSVHeader {
  name: string;
  index: number;
}

interface ICSVColumn extends ICSVHeader {
  column: string;
}

export interface IError {
  message: SchemaErrorKeys | ErrorKeys;
  schema?: SchemaType;
  schemaKey?: string;
  key?: string;
  property?: string;
  value?: any;
  min?: any;
  max?: any;
  error?: string;
  node?: HestiaJson<SchemaType>;
  index?: number;
  headers?: ICSVHeader[];
  suggestions?: string[];
}

const valueToNodes = (value: any, type: SchemaType) =>
  Array.isArray(value)
    ? isExpandable(value)
      ? value.map(val => ({
          type,
          ...val
        }))
      : null
    : typeof value === 'object'
      ? [
          {
            type,
            ...value
          }
        ]
      : null;

const nodesFromError = (error?: IError, type?: SchemaType) =>
  error ? valueToNodes(error.node, type) || valueToNodes(error.value, error.schema) || [] : [];

const errorCsv = nodes => {
  try {
    return toCsv(nodes, { includeExising: true });
  } catch (_err) {
    return '';
  }
};

const numberToCol = (num: number) => {
  let total = num + 1; // starts at 0
  let str = '';
  while (total > 0) {
    const q = (total - 1) / 26;
    const r = (total - 1) % 26;
    total = Math.floor(q);
    str = `${String.fromCharCode(65 + r)}${str}`;
  }
  return str;
};

@Component({
  selector: 'he-files-upload-errors',
  templateUrl: './files-upload-errors.component.html',
  styleUrls: ['./files-upload-errors.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NgTemplateOutlet, FaIconComponent, DecimalPipe, JsonPipe, EllipsisPipe, PluralizePipe]
})
export class FilesUploadErrorsComponent {
  protected readonly faExclamationTriangle = faExclamationTriangle;

  protected readonly error = input.required<IError>();
  protected readonly file = input<Partial<File>>();
  protected readonly debug = input(false);

  protected readonly fileExt = computed(() => fileExt(this.file().filename));
  protected readonly message = computed(() => this.error()?.message);

  private readonly isJSONFile = computed(() => this.file()?.filename?.endsWith(SupportedExtensions.json));
  protected readonly hasNumberWithCommasError = computed(
    () => this.error()?.error === 'failed to parse number' && (`${this.error()?.value}` || '').includes(',')
  );
  protected readonly hasGeoJSONError = computed(() => this.error()?.error?.includes('Unable to parse GeoJSON value'));
  protected readonly schemaUrl = computed(() =>
    [schemaBaseUrl(this.file()?.schemaVersion), this.error()?.schema].filter(Boolean).join('/')
  );
  protected readonly schemaKeyUrl = computed(() =>
    [this.schemaUrl(), this.error()?.schemaKey || this.error()?.key].filter(Boolean).join('#')
  );
  protected readonly isSchemaError = computed(() =>
    [SchemaErrorKeys.PropertyNotFound, SchemaErrorKeys.SchemaNotFound].includes(
      this.error()?.message as SchemaErrorKeys
    )
  );

  protected readonly showJsonPreview = computed(() => !!this.error()?.node && this.isJSONFile());
  protected readonly showCsvPreview = computed(
    () => this.headers().length && this.rows().length && !this.isSchemaError() && !this.isJSONFile()
  );
  protected readonly reportErrorUrl = computed(() =>
    issueLink(this.file()?.pipelineStatus || this.file()?.status, this.message())
  );

  protected readonly baseUrl = baseUrl();
  protected readonly nodeTypes = acceptedTypes;
  protected readonly SchemaErrorKeys = SchemaErrorKeys;
  protected readonly ErrorKeys = ErrorKeys;
  protected readonly maxFileSizeMb = maxFileSizeMb;
  protected readonly columns = signal<ICSVColumn[]>([]);
  protected readonly headers = signal<string[]>([]);
  protected readonly rows = signal<string[][]>([]);

  constructor() {
    effect(() => {
      this.columns.set(
        (this.error()?.headers || []).map(header => ({
          ...header,
          column: numberToCol(header.index)
        }))
      );

      const schemaType = this.error()?.key ? typeToSchemaType(this.error().key.split('.')[0]) : null;
      const nodeType = Object.values(SchemaType).includes(schemaType) ? schemaType : this.error()?.schema;
      const nodes = nodesFromError(this.error(), nodeType);
      const csv = errorCsv(nodes).trim();
      const {
        data: [headers, ...rows]
      } = csv ? parse(csv) : { data: [[]] };
      this.headers.set(headers);
      this.rows.set(rows);
    });
  }

  protected stringify(value) {
    return JSON.stringify(value, null, 2);
  }

  protected hasDuplicatedError(value: string, colIndex: number) {
    const firstVal = get(this.rows(), `0.${colIndex}`, '');
    return value !== firstVal;
  }
}
