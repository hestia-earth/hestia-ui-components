import {
  Component,
  ElementRef,
  output,
  inject,
  input,
  computed,
  signal,
  ChangeDetectionStrategy,
  model
} from '@angular/core';
import { NgTemplateOutlet, NgClass } from '@angular/common';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { JSON as HestiaJson, SchemaType, NodeType, Site } from '@hestia-earth/schema';
import { definition } from '@hestia-earth/json-schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import {
  faCheck,
  faComments,
  faEdit,
  faExternalLinkAlt,
  faPlusCircle,
  faMinus,
  faPlus,
  faClone as farClone
} from '@fortawesome/free-solid-svg-icons';
import { faCircleXmark } from '@fortawesome/free-regular-svg-icons';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { isUndefined } from '@hestia-earth/utils';
import { SvgIconComponent } from 'angular-svg-icon';
import { MarkdownComponent } from 'ngx-markdown';

import {
  INodeProperty,
  formatPropertyError,
  propertyError,
  hasError,
  hasWarning,
  recursiveProperties,
  propertyId,
  groupChanged,
  formatter
} from '../files-form.model';
import { errorHasError, errorHasWarning, filterError, formatError } from '../files-error.model';
import { IValidationError } from '../../schema/schema-validation.model';
import { HeSchemaService, linkTypeEnabled } from '../../schema/schema.service';
import { NodeIconComponent } from '../../node/node-icon/node-icon.component';
import { SitesMapsComponent } from '../../sites/sites-maps/sites-maps.component';
import { ClipboardComponent, isExternal } from '../../common';

const stringify = (value: any) => JSON.stringify(value);

@Component({
  selector: 'he-files-form',
  templateUrl: './files-form.component.html',
  styleUrls: ['./files-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormsModule,
    NgTemplateOutlet,
    NgClass,
    FaIconComponent,
    MarkdownComponent,
    NgbTooltip,
    NodeIconComponent,
    SitesMapsComponent,
    SvgIconComponent,
    ClipboardComponent
  ]
})
export class FilesFormComponent {
  /**
   * Query the content.
   */
  public readonly ref = inject(ElementRef);
  private readonly schemaService = inject(HeSchemaService);

  protected readonly faMinus = faMinus;
  protected readonly faPlus = faPlus;
  protected readonly faPlusCircle = faPlusCircle;
  protected readonly farCircleXmark = faCircleXmark;
  protected readonly faComments = faComments;
  protected readonly faExternalLinkAlt = faExternalLinkAlt;
  protected readonly faEdit = faEdit;
  protected readonly faCheck = faCheck;
  protected readonly farClone = farClone;

  protected readonly isOpen = model(true);
  protected readonly node = input.required<HestiaJson<SchemaType>>();
  protected readonly errors = input<IValidationError[]>([]);

  private readonly schemas = toSignal(this.schemaService.schemas$);
  protected readonly schemaType = computed(() => this.node()?.type || this.node()?.['@type']);
  private readonly schema = computed(() => this.schemas()?.[this.schemaType()] ?? ({} as definition));

  protected readonly nodeErorrResolved = output<number>();

  protected readonly NodeType = NodeType;
  protected readonly SchemaType = SchemaType;
  protected readonly formatPropertyError = formatPropertyError;
  protected readonly stringify = stringify;

  protected readonly mapVisible = signal(false);

  protected readonly showMap = computed(
    () =>
      [SchemaType.Site, SchemaType.Organisation].includes(this.schemaType()) &&
      [
        (this.node() as Site).latitude && (this.node() as Site).longitude,
        (this.node() as Site).region?.name,
        (this.node() as Site).country?.name
      ].some(Boolean)
  );
  private readonly nodeId = computed(() => this.node()?.id || this.node()?.['@id']);
  protected readonly nodeUrl = computed(() =>
    [
      linkTypeEnabled(this.schemaType()),
      // handle links on Community Edition
      this.node()?.['@id'] || (isExternal() && this.nodeId())
    ].every(Boolean)
      ? `/${this.schemaType().toLowerCase()}/${this.nodeId()}`
      : ''
  );

  private readonly filteredErrors = computed(() => this.errors().filter(filterError));
  private readonly nodeError = computed(() => propertyError(this.filteredErrors())(''));

  protected readonly nodeProperty = computed(() =>
    this.schema()
      ? {
          id: propertyId(),
          schema: this.schema(),
          schemaType: this.schemaType(),
          editable: false,
          fullKey: '',
          key: '',
          value: {},
          error: formatError(this.nodeError()),
          hasError: errorHasError(this.nodeError()),
          hasWarning: errorHasWarning(this.nodeError()),
          suggestions: {},
          newProperty: {},
          newError: { level: 'warning' },
          properties: [],
          addPropertyEnabled: false,
          formatter: formatter('')
        }
      : undefined
  );

  protected readonly properties = computed(() =>
    [!isUndefined(this.schemas()), !isUndefined(this.node())].every(Boolean)
      ? recursiveProperties(this.schemas(), this.filteredErrors(), this.schema(), false)(this.node())
      : []
  );

  protected readonly hasError = computed(() => hasError(this.properties()));
  protected readonly hasWarning = computed(() => hasWarning(this.properties()));

  protected trackByProperty(_index: number, { id, fullKey }: INodeProperty) {
    return [fullKey, id].join('_');
  }

  // --- Errors

  protected resolveError(property: INodeProperty) {
    const index = property.error?.index;
    property.error = undefined;
    property.hasError = hasError(property.properties);
    property.hasWarning = hasWarning(property.properties);
    this.nodeErorrResolved.emit(index);
    return groupChanged(this.properties(), property.fullKey);
  }
}
