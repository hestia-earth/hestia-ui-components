import { NodeType, TermTermType } from '@hestia-earth/schema';
import { validationErrorKeyword } from '../schema/schema-validation.model';
import { parseDataPath, dataPathLabel, formatCustomErrorMessage } from './files-error.model';

describe('files > files-error.model', () => {
  describe('parseDataPath', () => {
    it('should handle blank nodes', () => {
      expect(parseDataPath('.products[10]')).toEqual([
        {
          label: 'Product',
          path: 'products[10]'
        }
      ]);

      expect(parseDataPath('.inputs[0].term.@id')).toEqual([
        {
          label: 'Input',
          path: 'inputs[0]'
        },
        {
          label: 'Term',
          path: 'term'
        },
        {
          label: '<code>id</code>',
          path: '@id'
        }
      ]);

      expect(parseDataPath('.practices[3].term.termType')).toEqual([
        {
          label: 'Practice',
          path: 'practices[3]'
        },
        {
          label: 'Term',
          path: 'term'
        },
        {
          label: '<code>termType</code>',
          path: 'termType'
        }
      ]);

      expect(parseDataPath('.practices[3].properties[0].value')).toEqual([
        {
          label: 'Practice',
          path: 'practices[3]'
        },
        {
          label: 'Property',
          path: 'properties[0]'
        },
        {
          label: '<code>value</code>',
          path: 'value'
        }
      ]);
    });

    it('should handle nested Node', () => {
      expect(parseDataPath(".cycle['@type']")).toEqual([
        {
          label: 'Cycle',
          path: 'cycle'
        },
        {
          label: '<code>type</code>',
          path: '@type'
        }
      ]);

      expect(parseDataPath('.cycle.products[0].term')).toEqual([
        {
          label: 'Cycle',
          path: 'cycle'
        },
        {
          label: 'Product',
          path: 'products[0]'
        },
        {
          label: 'Term',
          path: 'term'
        }
      ]);
    });
  });

  describe('dataPathLabel', () => {
    it('should select the most precise schema type', () => {
      expect(dataPathLabel('.products[10]')).toBe('Product');
      expect(dataPathLabel('.inputs[0].term.@id')).toBe('Term');
      expect(dataPathLabel('.practices[3].term.termType')).toBe('Term');
      expect(dataPathLabel('.practices[3].properties[0].value')).toBe('Property');
      expect(dataPathLabel(".cycle['@type']")).toBe('Cycle');
      expect(dataPathLabel('.cycle.products[0].term')).toBe('Term');
      expect(dataPathLabel('.cycle.animals[0].inputs[0]')).toBe('Input');
    });
  });

  describe('formatCustomErrorMessage', () => {
    describe('with required property', () => {
      const message = "should have required property 'endDate'";

      describe('default error', () => {
        it('should format in code blocks', () => {
          expect(formatCustomErrorMessage(message)).toEqual(
            '<p class="is-normal"><span class="is-capitalized-first-letter">You are missing the following required field: <code>endDate</code>.</span></p>'
          );
        });
      });

      describe('with error and dataPath', () => {
        const error = {
          keyword: 'required' as validationErrorKeyword,
          dataPath: '.cycle.practices[3]',
          schemaPath: '#/allOf/2/then/required',
          params: { missingProperty: 'endDate' },
          message,
          schema: { required: ['endDate'] },
          parentSchema: { required: ['endDate'] }
        };

        it('should format the message', () => {
          expect(formatCustomErrorMessage(message, error)).toEqual(
            '<p class="is-normal"><span class="is-capitalized-first-letter">You are missing the following required field: <code>endDate</code>. Either: add <code>endDate</code>, or if you if you did not intend to add this Practice, please remove all fields on this Practice.</span></p>'
          );
        });
      });
    });

    describe('should NOT be valid', () => {
      const message = 'should NOT be valid';

      it('should handle Practice with areaPercent error', () => {
        const error = {
          keyword: 'not' as validationErrorKeyword,
          dataPath: '',
          schemaPath: '#/allOf/2/then/not',
          params: {},
          message,
          schema: { required: ['areaPercent'] },
          parentSchema: { not: { required: ['areaPercent'] } }
        };
        expect(formatCustomErrorMessage(error.message, error)).toEqual(
          '<p class="is-normal">If the term units is already in <code>% area</code>, just use <code>value</code>. Dont use <code>areaPercent</code>.</p>'
        );
      });
    });

    describe('should be equal to one of the allowed values', () => {
      const message = 'should be equal to one of the allowed values';

      it('should handle simple properties', () => {
        const error = {
          keyword: 'enum' as validationErrorKeyword,
          dataPath: '.functionalUnit',
          schemaPath: '#/properties/functionalUnit/enum',
          params: {
            allowedValues: ['1 ha', 'relative']
          },
          message: 'should be equal to one of the allowed values'
        };
        expect(formatCustomErrorMessage(error.message, error)).toEqual(
          '<p class="is-normal">Only the following values are permitted: <code>1 ha</code>, <code>relative</code></p>'
        );
      });

      it('should handle term.termType error', () => {
        const error = {
          keyword: 'enum' as validationErrorKeyword,
          dataPath: '.cycle.animals[0].inputs[0].term.termType',
          schemaPath: '#/properties/term/enum',
          params: { allowedValues: [TermTermType.crop, TermTermType.forage] },
          message
        };
        expect(formatCustomErrorMessage(error.message, error)).toEqual(
          '<p class="is-normal">For Inputs, we only allow terms from the following glossaries: <code>crop</code>, <code>forage</code>. Please either change the Term you are using and ensure it is from one of the glossaries listed above, or follow the schema to identify where this Term can be added.</p>'
        );
      });
    });

    it('should handle node not found error', () => {
      const error = {
        keyword: 'required' as validationErrorKeyword,
        dataPath: '.site',
        params: {
          node: {
            '@type': NodeType.Site,
            '@id': '1'
          }
        },
        message: 'node not found'
      };
      expect(formatCustomErrorMessage(error.message, error)).toEqual(
        '<p class="is-normal">The Site with <code>@id=1</code> does not exist on the platform. If you are trying to link to nodes included in this upload, you must use <code>id</code> field instead. Otherwise, please check the ids are correct and try again.</p>'
      );
    });
  });
});
