import {
  Component,
  ElementRef,
  output,
  inject,
  input,
  signal,
  model,
  computed,
  ChangeDetectionStrategy
} from '@angular/core';
import { NgTemplateOutlet, NgClass } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { toSignal } from '@angular/core/rxjs-interop';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import get from 'lodash.get';
import { JSON as HestiaJson, SchemaType, NodeType, Bibliography, Source, Property } from '@hestia-earth/schema';
import { definition } from '@hestia-earth/json-schema';
import { isEmpty, isUndefined } from '@hestia-earth/utils';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faIdBadge as farIdBadge } from '@fortawesome/free-regular-svg-icons';
import {
  faAngleDown,
  faAngleRight,
  faCalculator,
  faCheck,
  faClone,
  faComments,
  faEdit,
  faExclamationTriangle,
  faExternalLinkAlt,
  faLongArrowAltDown,
  faLongArrowAltUp,
  faMapMarkedAlt,
  faPlus,
  faPlusCircle,
  faSearch,
  faSpinner,
  faTimes
} from '@fortawesome/free-solid-svg-icons';
import { NgbTooltip, NgbTypeahead, NgbHighlight } from '@ng-bootstrap/ng-bootstrap';

import {
  INodeProperty,
  formatPropertyError,
  propertyError,
  suggestionType,
  defaultSuggestionType,
  recursiveProperties,
  typeToNewProperty,
  propertyId,
  isAddPropertyEnabled,
  nodeAvailableProperties,
  sortProperties,
  groupChanged,
  parentProperty,
  updateProperties,
  refreshPropertyKeys,
  findProperty,
  calculateCycleDurationEnabled,
  calculateCycleDuration,
  calculateCycleStartDateEnabled,
  calculateCycleStartDate,
  parseNewValue,
  formatter,
  INewProperty,
  hasError,
  hasWarning
} from '../files-form.model';
import { errorHasError, errorHasWarning, filterError, formatError } from '../files-error.model';
import { IValidationError } from '../../schema/schema-validation.model';
import { HeSearchService } from '../../search/search.service';
import { matchType, matchPhraseQuery, matchPhrasePrefixQuery, matchBoolPrefixQuery } from '../../search/search.model';
import { safeJSONParse, typeaheadFocus } from '../../common/utils';
import { PluralizePipe } from '../../common/pluralize.pipe';
import { ApplyPurePipe } from '../../common/apply-pure.pipe';
import { TagsInputDirective } from '../../common/tags-input.directive';
import { NodeRecommendationsComponent } from '../../node/node-recommendations/node-recommendations.component';
import { PopoverConfirmComponent } from '../../common/popover-confirm/popover-confirm.component';
import { SchemaVersionLinkComponent } from '../../common/schema-version-link/schema-version-link.component';
import { SchemaInfoComponent } from '../../schema/schema-info/schema-info.component';
import { HeSchemaService } from '../../schema/schema.service';
import { BibliographiesSearchConfirmComponent } from '../../bibliographies/bibliographies-search-confirm/bibliographies-search-confirm.component';
import { NodeIconComponent } from '../../node/node-icon/node-icon.component';

const MIN_TYPEAHEAD_LENGTH = 2;
const populateTermFields = ['termType', 'units'];
const populateTermDefaultProperties = [
  'defaultProperties.term.name',
  'defaultProperties.term.units',
  'defaultProperties.value'
];

const formatProperty = (prop: Property) => ({
  '@type': SchemaType.Property,
  ...prop,
  term: {
    '@type': NodeType.Term,
    ...prop.term
  }
});

const formatSuggestion = ({ defaultProperties, ...node }: any) => ({
  ...node,
  ...(defaultProperties?.length ? { defaultProperties: defaultProperties.map(formatProperty) } : {})
});

const formatLinkNodesPrefix = 'Link with';

// patch error when link is not working correctly
const cleanNewValue = value =>
  typeof value === 'object' && value.id
    ? { ...value, id: cleanNewValue(value.id) }
    : typeof value === 'string'
      ? value.startsWith(formatLinkNodesPrefix)
        ? value.split(':')[1].trim()
        : value
      : value;

const formatLinkNodesSuggestions = (nodeMap: { [type in NodeType]?: string[] }, type: NodeType) =>
  (nodeMap[type] || []).map(id => ({ type, id, name: `${formatLinkNodesPrefix} ${type}: ${id}` }));

type suggestFunc = (term: string, property: INodeProperty) => Observable<any[]>;

const insertProperty = (properties: INodeProperty[], key: string, property: INodeProperty) => {
  const existingPropertyIndex = properties.findIndex(v => v.fullKey === key);
  existingPropertyIndex >= 0 ? (properties[existingPropertyIndex] = property) : properties.push(property);
  properties.sort(sortProperties);
  return properties;
};

@Component({
  selector: 'he-files-form-editable',
  templateUrl: './files-form-editable.component.html',
  styleUrls: ['./files-form-editable.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgClass,
    NgTemplateOutlet,
    FormsModule,
    FaIconComponent,
    NgbTooltip,
    NgbTypeahead,
    NgbHighlight,
    ApplyPurePipe,
    PluralizePipe,
    NodeIconComponent,
    BibliographiesSearchConfirmComponent,
    SchemaInfoComponent,
    SchemaVersionLinkComponent,
    PopoverConfirmComponent,
    NodeRecommendationsComponent,
    TagsInputDirective
  ]
})
export class FilesFormEditableComponent {
  public readonly ref = inject(ElementRef);
  private readonly searchService = inject(HeSearchService);
  private readonly schemaService = inject(HeSchemaService);

  protected readonly faExternalLinkAlt = faExternalLinkAlt;
  protected readonly faTimes = faTimes;
  protected readonly faAngleDown = faAngleDown;
  protected readonly faAngleRight = faAngleRight;
  protected readonly faClone = faClone;
  protected readonly faLongArrowAltUp = faLongArrowAltUp;
  protected readonly faLongArrowAltDown = faLongArrowAltDown;
  protected readonly faPlusCircle = faPlusCircle;
  protected readonly faExclamationTriangle = faExclamationTriangle;
  protected readonly faPlus = faPlus;
  protected readonly faEdit = faEdit;
  protected readonly faCheck = faCheck;
  protected readonly faSpinner = faSpinner;
  protected readonly faComments = faComments;
  protected readonly faMapMarkedAlt = faMapMarkedAlt;
  protected readonly faSearch = faSearch;
  protected readonly faCalculator = faCalculator;
  protected readonly farIdBadge = farIdBadge;

  private readonly schemas = toSignal(this.schemaService.schemas$);
  protected readonly schemaType = computed(() => this.node()?.type || this.node()?.['@type']);
  private readonly schema = computed(() => this.schemas()?.[this.schemaType()] ?? ({} as definition));

  protected readonly isOpen = model(true);
  protected readonly node = input.required<HestiaJson<SchemaType>>();
  protected readonly errors = input<IValidationError[]>([]);
  protected readonly nodeMap = input<{ [type in NodeType]?: string[] }>({});
  /**
   * When selecting terms, will show default properties or not.
   * Should be `false` when using schema validation, as only the `name` would be added.
   */
  protected readonly showSuggestedDefaultProperties = input(true);

  protected readonly nodeChanged = output<{ key: string; value: any }>();

  protected readonly NodeType = NodeType;
  protected readonly SchemaType = SchemaType;
  protected readonly formatPropertyError = formatPropertyError;
  protected readonly typeaheadFocus = typeaheadFocus;

  private readonly filteredErrors = computed(() => this.errors().filter(filterError));
  private readonly nodeError = computed(() => propertyError(this.filteredErrors())(''));

  protected readonly nodeProperty = computed(() =>
    this.schema()
      ? ({
          id: propertyId(),
          schema: this.schema(),
          schemaType: this.schemaType(),
          editable: !!this.schemaType(),
          fullKey: '',
          key: '',
          value: {},
          error: formatError(this.nodeError()),
          hasError: errorHasError(this.nodeError()),
          hasWarning: errorHasWarning(this.nodeError()),
          suggestions: {},
          newProperty: {},
          newError: { level: 'warning' },
          properties: [],
          addPropertyEnabled: isAddPropertyEnabled(this.node(), this.schemaType(), this.schema()),
          formatter: formatter('')
        } as INodeProperty)
      : undefined
  );

  protected readonly properties = computed(() =>
    [!isUndefined(this.schemas()), !isUndefined(this.node())].every(Boolean)
      ? recursiveProperties(this.schemas(), this.filteredErrors(), this.schema(), true)(this.node())
      : []
  );

  protected readonly hasError = computed(() => hasError(this.properties()));
  protected readonly hasWarning = computed(() => hasWarning(this.properties()));

  protected readonly bibliographiesSearchProperty = signal(undefined as INodeProperty);

  protected readonly suggestNewProperty =
    ({ fullKey }: INodeProperty) =>
    (text$: Observable<string>) =>
      text$.pipe(
        distinctUntilChanged(),
        switchMap(term => of(this.newProperties(findProperty(this.properties(), fullKey) || this.nodeProperty(), term)))
      );
  protected readonly formatNewProperty = ({ name }: INewProperty) => name;

  private readonly suggestExistingNode = (type: NodeType, term: string, uniqueKey = '@id') =>
    term.length < MIN_TYPEAHEAD_LENGTH
      ? of([])
      : this.searchService.suggest$(
          term,
          type,
          [],
          {
            bool: {
              must: [matchType(type)],
              should: [
                matchPhraseQuery(term, 100, uniqueKey),
                matchPhrasePrefixQuery(term, 20, uniqueKey),
                matchBoolPrefixQuery(term, 10, uniqueKey)
              ],
              minimum_should_match: 1
            }
          },
          10,
          this.suggestTermFields
        );

  private readonly suggestNode =
    (type: NodeType, suggestDefault?: suggestFunc) => (term: string, property: INodeProperty) =>
      (property.suggestions?.isUniqueKey
        ? this.suggestExistingNode(type, term, property.suggestions.isUniqueKey)
        : property.suggestions?.isLinkNode
          ? of(formatLinkNodesSuggestions(this.nodeMap(), type))
          : suggestDefault
            ? suggestDefault(term, property)
            : of([])
      ).pipe(map((res: any) => res.map(formatSuggestion)));

  private readonly suggester: {
    [type in suggestionType]?: suggestFunc;
  } = {
    default: (term, property) =>
      of(
        (property?.suggestions?.values || [])
          .map(v => `${v}`)
          .filter(v => v.toLowerCase().includes(term.toLowerCase()))
          .map(name => ({ name }))
      ),
    [NodeType.Actor]: this.suggestNode(NodeType.Actor),
    [NodeType.Cycle]: this.suggestNode(NodeType.Cycle),
    [NodeType.ImpactAssessment]: this.suggestNode(NodeType.ImpactAssessment),
    [NodeType.Organisation]: this.suggestNode(NodeType.Organisation),
    [NodeType.Site]: this.suggestNode(NodeType.Site),
    [NodeType.Source]: this.suggestNode(NodeType.Source, (term, { key }) =>
      term.length < MIN_TYPEAHEAD_LENGTH
        ? of([])
        : this.searchService.suggestSource$(term, 5, [`bibliography.${key}`], [`bibliography.${key}`])
    ),
    [NodeType.Term]: this.suggestNode(NodeType.Term, (term, { suggestions }) =>
      term.length < MIN_TYPEAHEAD_LENGTH
        ? of([])
        : this.searchService.suggest$(term, NodeType.Term, suggestions?.queries || [], null, 5, this.suggestTermFields)
    )
  };
  protected readonly propertySuggest =
    ({ fullKey, suggestions }: INodeProperty) =>
    (text$: Observable<string>) =>
      text$.pipe(
        debounceTime(suggestions?.type === defaultSuggestionType ? 0 : 300),
        distinctUntilChanged(),
        mergeMap(term => {
          const property = findProperty(this.properties(), fullKey);
          return property?.editable && suggestions?.type in this.suggester
            ? of(term).pipe(
                tap(() => (property.loading = true)),
                switchMap(() => this.suggester[suggestions?.type]!(term, property)),
                tap(() => (property.loading = false))
              )
            : of([]);
        })
      );

  protected trackByIndex(index: number, _value: any) {
    return index;
  }

  protected trackByProperty(_index: number, { id, fullKey }: INodeProperty) {
    return [fullKey, id].join('_');
  }

  /* eslint-disable-next-line complexity */
  protected hasAddons(property: INodeProperty) {
    return (
      property &&
      (property.editable || this.addPropertyEnabled(property) || this.isRequired(property) || property.externalUrl?.url)
    );
  }

  protected isRequired({ value, isRequired }: INodeProperty) {
    return isRequired && isEmpty(value);
  }

  private get suggestTermFields() {
    return [...populateTermFields, ...(this.showSuggestedDefaultProperties() ? populateTermDefaultProperties : [])];
  }

  protected addPropertyEnabled(property: INodeProperty) {
    return property.addPropertyEnabled;
  }

  private newProperties(property: INodeProperty, field: string) {
    const properties = nodeAvailableProperties(this.node(), property);
    return Object.keys(properties)
      .filter(prop => prop.toLowerCase().includes(field.toLowerCase()))
      .map(name => ({ name, prop: properties[name] }));
  }

  private replacePropertyData(property: INodeProperty, data: any, editable?: boolean, replaceParent = true) {
    const parent = replaceParent ? parentProperty(this.properties(), property) : property;
    const properties = recursiveProperties(this.schemas(), [], this.schema(), true, parent.fullKey)(data);
    updateProperties({ properties }, prop => {
      prop.editable = typeof editable === 'undefined' ? prop.editable : editable;
      return prop;
    });
    parent.editable = typeof editable === 'undefined' ? parent.editable : editable;
    parent.properties = properties;
    parent.value = data;
    return this.propertyChanged(data, parent);
  }

  protected suggestionSelected({ '@id': _id, name, type, id, ...data }, property: INodeProperty) {
    return _id
      ? // suggestions from existing nodes
        this.replacePropertyData(property, { '@id': _id, '@type': property.suggestions?.type, name, ...data }, false)
      : // suggestion from linked nodes
        type && id
        ? this.replacePropertyData(property, { type, id, ...data }, true)
        : // suggestion by name => enum values
          this.propertyChanged(name, property);
  }

  protected addArrayGroup(array: INodeProperty) {
    const prop = this.schemas()?.[array.schemaType];
    const value = get(this.node(), array.fullKey);
    const newProperty = typeToNewProperty(
      this.schemas(),
      prop,
      prop,
      value,
      `${array.fullKey}[${array.properties.length}]`
    );
    array.properties.push(newProperty);
    this.nodeChanged.emit({ key: array.fullKey, value });
    return groupChanged(this.properties(), array.fullKey);
  }

  protected duplicateArrayGroup(array: INodeProperty, property: INodeProperty) {
    const index = +property.key;
    const newProperty: INodeProperty = JSON.parse(JSON.stringify(property));
    newProperty.id = propertyId();
    const value = [...get(this.node(), array.fullKey)];
    value.splice(index, 0, safeJSONParse(newProperty.value));
    array.properties.splice(index, 0, newProperty);
    // need to change key on every properties recursively
    refreshPropertyKeys(array);
    this.nodeChanged.emit({ key: array.fullKey, value });
    return groupChanged(this.properties(), array.fullKey);
  }

  protected removeArrayGroup(array: INodeProperty, { key }: INodeProperty) {
    const index = +key;
    const value = [...get(this.node(), array.fullKey)];
    value.splice(index, 1);
    array.properties.splice(index, 1);
    // need to change key on every properties recursively
    refreshPropertyKeys(array);
    this.nodeChanged.emit({ key: array.fullKey, value });
    // if no more values, remove parent group entirely
    return array.properties.length ? null : this.propertyChanged(null, array);
  }

  private moveArrayGroupToPosition(array: INodeProperty, currentIndex: number, newIndex: number) {
    const value = [...get(this.node(), array.fullKey)];
    moveItemInArray(value, currentIndex, newIndex);
    moveItemInArray(array.properties, currentIndex, newIndex);
    // need to change key on every properties recursively
    refreshPropertyKeys(array);
    this.nodeChanged.emit({ key: array.fullKey, value });
    return groupChanged(this.properties(), array.fullKey);
  }

  protected moveArrayGroupUp(array: INodeProperty, { key }: INodeProperty) {
    const index = +key;
    return this.moveArrayGroupToPosition(array, index, index - 1);
  }

  protected moveArrayGroupDown(array: INodeProperty, { key }: INodeProperty) {
    const index = +key;
    return this.moveArrayGroupToPosition(array, index, index + 1);
  }

  protected propertyChanged(value: any, property: INodeProperty) {
    groupChanged(this.properties(), property.fullKey, value);
    property.addPropertyEnabled = isAddPropertyEnabled(
      this.node(),
      property.schemaType,
      property.schema!,
      property.fullKey
    );

    if (value === null) {
      value = undefined;
    } else {
      property.value = value;
      property.changed = true;
    }

    const newValue = cleanNewValue(parseNewValue(property.schema!, value));
    // trigger for listeners
    this.nodeChanged.emit({ key: property.fullKey, value: newValue });
  }

  private addDefaultProperty(property: INodeProperty, key: string, def: definition) {
    const node = property.key ? get(this.node(), property.fullKey) : this.node();
    const fullKey = property.key ? `${property.fullKey}.${key}` : key;
    const properties = property.key ? property.properties : this.properties();
    const newProperty = typeToNewProperty(this.schemas(), def, this.schemas()?.[property.schemaType], node, fullKey);
    insertProperty(properties, fullKey, newProperty);
    return this.propertyChanged(newProperty.value, newProperty);
  }

  protected addMissingProperty(property: INodeProperty) {
    const parent = parentProperty(this.properties(), property);
    const newProperty = parent === property ? this.nodeProperty() : parent;
    const newProperties = this.newProperties(newProperty, property.key);
    newProperty.newProperty = newProperties[0];
    return newProperty.newProperty ? this.addProperty(newProperty) : null;
  }

  protected addProperty(property: INodeProperty) {
    const { name, prop } = property.newProperty!;
    property.newProperty = {};
    return name === '@id'
      ? this.replacePropertyData(property, { '@id': '', type: property.schemaType }, undefined, false)
      : this.addDefaultProperty(property, name!, prop!);
  }

  // Bibliography Search

  protected bibliographiesSearchKey({ key, schemaType }: INodeProperty) {
    const validSchema = [SchemaType.Bibliography].includes(schemaType);
    const valieKey = ['title', 'documentDOI', 'scopus'].includes(key);
    return validSchema && valieKey ? key : null;
  }

  private updateBibliography(value: Partial<Bibliography>) {
    const parent = parentProperty(this.properties(), this.bibliographiesSearchProperty());
    const newValue = this.bibliographiesSearchSources
      ? {
          ...parent.value,
          bibliography: value
        }
      : value;
    this.replacePropertyData(this.bibliographiesSearchProperty(), newValue, true);
    return undefined;
  }

  private updateSource(value: Partial<Source>) {
    // cannot replace a top-level source
    this.bibliographiesSearchSources &&
      this.replacePropertyData(
        this.bibliographiesSearchProperty(),
        {
          '@type': NodeType.Source,
          ...value
        },
        !('@id' in value)
      );
    return undefined;
  }

  protected get bibliographiesSearchSources() {
    return this.bibliographiesSearchProperty()?.fullKey?.split('.')?.length > 2;
  }

  protected onBibliographiesSearchClosed(value?: Partial<Source> | Partial<Bibliography>) {
    return this.bibliographiesSearchProperty() === value
      ? value.type === SchemaType.Bibliography
        ? this.updateBibliography(value)
        : this.updateSource(value as Partial<Source>)
      : undefined;
  }

  // Cycle.cycleDuration

  protected calculateCycleDurationEnabled(property: INodeProperty) {
    return calculateCycleDurationEnabled(this.properties(), property);
  }

  protected calculateCycleDuration(property: INodeProperty) {
    const duration = calculateCycleDuration(this.properties(), property);
    return this.propertyChanged(duration, property);
  }

  // Cycle.startDate

  protected calculateCycleStartDateEnabled(property: INodeProperty) {
    return calculateCycleStartDateEnabled(this.properties(), property);
  }

  protected calculateCycleStartDate(property: INodeProperty) {
    const startDate = calculateCycleStartDate(this.properties(), property);
    return this.propertyChanged(startDate, property);
  }

  // Recommendations

  protected onSelectRecommendation(array: INodeProperty, id: string) {
    const prop = this.schemas()?.[array.schemaType];
    const value = get(this.node(), array.fullKey);

    const newProperty = typeToNewProperty(
      this.schemas(),
      prop,
      prop,
      value,
      `${array.fullKey}[${array.properties.length}]`
    );
    newProperty.value.term = { type: NodeType.Term, '@id': id };
    value[array.properties.length].term = { type: NodeType.Term, '@id': id };

    // insert Term with @id
    const termKey = `${newProperty.fullKey}.term`;
    const termProperty =
      newProperty.properties.find(p => p.key === 'term') ||
      typeToNewProperty(this.schemas(), prop.properties.term, prop, newProperty.value, termKey);
    termProperty.value = { type: NodeType.Term, '@id': id };
    termProperty.properties = recursiveProperties(
      this.schemas(),
      [],
      this.schema(),
      true,
      termKey
    )({ type: NodeType.Term, '@id': id });
    insertProperty(newProperty.properties, termKey, termProperty);

    // term cannot be edited, need to remove first
    updateProperties(termProperty, p => {
      p.editable = false;
      return p;
    });
    termProperty.editable = false;

    array.properties.push(newProperty);
    return this.propertyChanged(value, array);
  }
}
