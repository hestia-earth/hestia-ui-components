import { from } from 'rxjs';
import { filter, map, mergeMap, reduce, toArray } from 'rxjs/operators';
import get from 'lodash.get';
import set from 'lodash.set';
import { JSON as HestiaJson, SchemaType } from '@hestia-earth/schema';
import { unique } from '@hestia-earth/utils';

import { code, nodeLink } from '../../common/utils';
import { IValidationError, validationErrorLevel } from '../../schema/schema-validation.model';
import { IValidationErrorWithIndex, filterError, formatError, dataPathLabel } from '../files-error.model';
import { dataPathToKey } from '../files-form.model';

export interface ISummaryError {
  node: HestiaJson<SchemaType>;
  error: IValidationError;
  errorIndex?: number;
  term?: any;
}

interface ISummaryGrouped {
  [message: string]: {
    [level: string]: {
      [label: string]: ISummaryError[];
    };
  };
}

export interface ISummary {
  /**
   * Index of the summary in the list.
   */
  index: number;
  level: validationErrorLevel;
  path: string;
  message: string;
  formattedMessage: string;
  count: number;
  showErrors: boolean;
  errors: ISummaryError[];
  nodeIndexes: number[];
  terms?: string;
}

const nodeTerm = (node: HestiaJson<SchemaType>, key: string) => {
  const termType = get(node, `${key}.type`, get(node, `${key}.@type`, null));
  return termType === SchemaType.Term ? get(node, key, null) : null;
};

const findNodeTerm = (node: HestiaJson<SchemaType>, { dataPath }: IValidationError) =>
  nodeTerm(node, dataPathToKey(dataPath)) ||
  nodeTerm(node, `${dataPathToKey(dataPath)}.term`) ||
  nodeTerm(node, dataPathToKey(dataPath, true)) ||
  nodeTerm(node, `${dataPathToKey(dataPath, true)}.term`);

const uniqueTerms = (errors: ISummaryError[]) =>
  unique(
    errors
      .map(({ term }) => (term ? nodeLink(term) || code(term.name || term['@id'] || term.id) : null))
      .filter(Boolean)
  );

const summaryShowErrors = (summaryData: ISummary[], { path, level, message }: Partial<ISummary>) =>
  summaryData.find(summary => summary.path === path && summary.level === level && summary.message === message)
    ?.showErrors || true;

const defaultNodeLabel = (node: any) => node?.type || node?.['@type'] || '';

/**
 * Build the summary objects.
 *
 * @param nodes The list of nodes.
 * @param errors The list of errors per node.
 * @param summaryData The previously computed summary objects.
 * @returns
 */
export const buildSummary = (
  nodes: HestiaJson<SchemaType>[] = [],
  errors: IValidationErrorWithIndex[] = [],
  summaryData: ISummary[] = []
) =>
  from(errors).pipe(
    mergeMap(error => error.error.map((v, index) => ({ ...v, nodeIndex: error.index, index }))),
    filter(filterError),
    reduce((prev, error) => {
      const { level, message, nodeIndex } = error as IValidationError;
      const node = get(nodes, nodeIndex!, null)!;
      const label = dataPathLabel(error.dataPath) || defaultNodeLabel(node);
      const term = error.params?.term || findNodeTerm(node, error);
      prev[message] = prev[message] || {};
      const key = [level, label].join('.');
      set(prev[message], key, get(prev[message], key, []));
      prev[message][level!][label].push({ node, error, term });
      return prev;
    }, {} as ISummaryGrouped),
    mergeMap(data =>
      from(Object.keys(data)).pipe(
        filter(message => !!message),
        mergeMap(message =>
          Object.entries(data[message]).flatMap(([level, paths]) =>
            Object.entries(paths).map(
              ([path, summaries]) =>
                ({
                  level,
                  path,
                  message,
                  formattedMessage: formatError(
                    summaries[0].error,
                    summaries.map(({ error }) => error)
                  )?.message,
                  terms: uniqueTerms(summaries).join(' ; '),
                  errors: summaries,
                  nodeIndexes: unique(summaries.map(summary => summary.error.nodeIndex)),
                  count: summaries.length,
                  showErrors: summaryShowErrors(summaryData, { level: level as validationErrorLevel, path, message })
                }) as ISummary
            )
          )
        ),
        filter(({ formattedMessage }) => !!formattedMessage)
      )
    ),
    toArray(),
    map(values => values.map((value, index) => ({ ...value, index })) as ISummary[])
  );
