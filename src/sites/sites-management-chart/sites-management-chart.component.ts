import { Component, computed, effect, input, Signal, signal } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ISiteJSONLD, Management, TermTermType } from '@hestia-earth/schema';
import { isNumber } from '@hestia-earth/utils';
import { ChartConfiguration, ChartData } from 'chart.js';

import { ChartComponent } from '../../chart';
import { KeyToLabelPipe } from '../../common/key-to-label.pipe';
import { getColor } from '../../common/color';
import { formatDate, uniqueDatesBetween, isDateBetween } from '../../common/node-utils';

interface IColorMapping {
  [term: string]: string;
}

type groupedManagement = {
  [type in TermTermType]: Management[];
};

interface IChartValue {
  [date: string]: number;
}

interface IChartValues {
  [term: string]: IChartValue;
}

const sortedPoints = (values: IChartValue) =>
  Object.entries(values)
    .map(([x, y]) => ({ x, y }))
    .sort((a, b) => new Date(a.x).getTime() - new Date(b.x).getTime());

@Component({
  selector: 'he-sites-management-chart',
  imports: [FormsModule, ChartComponent, KeyToLabelPipe],
  templateUrl: './sites-management-chart.component.html',
  styleUrl: './sites-management-chart.component.scss'
})
export class SitesManagementChartComponent {
  protected readonly site = input.required<ISiteJSONLD>();

  private readonly management = computed(() => this.site().management ?? []);

  private readonly managementPerTermType = computed(() =>
    this.management()
      .filter(({ value }) => typeof value === 'number' && isNumber(value))
      .reduce(
        (prev, curr) => ({
          ...prev,
          [curr.term.termType]: [...(prev[curr.term.termType] || []), curr]
        }),
        {} as groupedManagement
      )
  );
  protected readonly termTypes = computed(() => Object.keys(this.managementPerTermType()).sort() as TermTermType[]);
  protected readonly selectedTermType = signal<TermTermType>(undefined);
  private readonly selectedManagement = computed(() => this.managementPerTermType()?.[this.selectedTermType()] ?? []);
  private readonly dates = computed(() =>
    this.selectedManagement()
      .flatMap(({ startDate, endDate }) => [formatDate(startDate, true), formatDate(endDate, false)])
      .filter(Boolean)
  );

  private readonly chartColors = computed(() =>
    this.selectedManagement().reduce(
      (prev, { term }, index) => ({ ...prev, [term.name]: getColor(index) }),
      {} as IColorMapping
    )
  );
  // compute list of all dates used on the chart, sorted ascending.
  // need to add dates even when there are no data, otherwise the chart will stack wrongly
  // @ee https://github.com/chartjs/Chart.js/issues/5405
  private readonly chartDates = computed(() => uniqueDatesBetween(this.dates()));
  private readonly chartValues = computed(() =>
    this.selectedManagement().reduce((prev, { term, startDate, endDate, value }) => {
      prev[term.name] = this.chartDates().reduce((group, date) => {
        const key = date.toISOString();
        group[key] = (group[key] || 0) + (isDateBetween(date, { start: startDate, end: endDate }) ? +value : 0);
        return group;
      }, prev[term.name] || {});
      return prev;
    }, {} as IChartValues)
  );
  protected readonly chartData: Signal<ChartData> = computed(() => ({
    datasets: Object.entries(this.chartValues()).map(([label, data]) => ({
      label,
      data: sortedPoints(data),
      backgroundColor: this.chartColors()[label],
      borderColor: this.chartColors()[label],
      barPercentage: 3,
      barThickness: 'flex'
    }))
  }));
  protected readonly chartConfig: Partial<ChartConfiguration> = {
    type: 'bar',
    options: {
      legend: {
        display: true
      },
      scales: {
        xAxes: [
          {
            type: 'time',
            time: {
              unit: 'month'
            },
            stacked: true
          }
        ],
        yAxes: [
          {
            position: 'left',
            ticks: {
              min: 0
            },
            stacked: true
          }
        ]
      }
    }
  };

  constructor() {
    effect(() => {
      // make sure selected term exists
      const termTypes = this.termTypes();
      const selectedTermType = this.selectedTermType();
      if (!selectedTermType || !termTypes.includes(selectedTermType)) {
        this.selectedTermType.set(termTypes[0]);
      }
    });
  }
}
