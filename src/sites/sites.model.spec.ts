import { measurementValue } from './sites.model';

describe('sites > sites.model', () => {
  describe('measurementValue', () => {
    it('should handle multiple 0 values', () => {
      const values = {
        depthLower: [30],
        depthUpper: [0],
        value: [0, 0],
        nodes: [
          {
            value: 0,
            depthLower: 30,
            depthUpper: 0
          },
          { value: 0 }
        ]
      };
      const termId = '';
      expect(measurementValue(values as any, termId)).toEqual(0);
    });
  });
});
