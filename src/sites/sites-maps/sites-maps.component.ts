import {
  Component,
  ViewChild,
  ChangeDetectionStrategy,
  signal,
  computed,
  effect,
  OnDestroy,
  input,
  inject
} from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { toSignal } from '@angular/core/rxjs-interop';
import { GoogleMap, MapMarker, MapPolygon } from '@angular/google-maps';
import { NodeType, ISiteJSONLD, Site, IOrganisationJSONLD, Organisation } from '@hestia-earth/schema';
import { unique } from '@hestia-earth/utils';
import { DataState } from '@hestia-earth/api';
import { Observable, from } from 'rxjs';
import { map, mergeMap, shareReplay, toArray } from 'rxjs/operators';

import { HeNodeService, HeNodeStoreService } from '../../node';
import { termLocation, termLocationName } from '../../terms/terms.model';
import {
  HE_MAP_LOADED,
  createMarker,
  polygonsFromFeature,
  polygonBounds,
  strokeStyle,
  feature,
  polygonToMap,
  IMarker,
  IPolygonMap
} from '../../common/maps-utils';
import { baseUrl } from '../../common/utils';

interface ILoadingRequests {
  [id: string]: Observable<feature>;
}

type site = ISiteJSONLD | Site | IOrganisationJSONLD | Organisation;

const siteLocation = ({ latitude, longitude }: site) =>
  latitude && longitude
    ? {
        lat: latitude,
        lng: longitude
      }
    : undefined;

const siteDefaultLocation = ({ region, country }: site) => {
  const markers = [
    region ? createMarker(termLocation(region), termLocationName(region).name, undefined, 20) : undefined,
    country ? createMarker(termLocation(country), termLocationName(country).name, undefined, 40) : undefined
  ].filter(Boolean);
  return markers?.[0];
};

const siteMarker = (site: site) =>
  createMarker(site ? siteLocation(site) : undefined, site.name) || siteDefaultLocation(site);

const sitePolygon = ({ boundary }: site) =>
  (boundary ? polygonsFromFeature(boundary as any, '#4D8ECB') : undefined) as google.maps.Polygon[];

const regionIds = (sites: site[]): string[] =>
  unique(
    (sites || [])
      .map(({ country, region }) => (region ? region['@id'] : country ? country['@id'] : null))
      .filter(Boolean)
  );

const defaultCenter: google.maps.LatLngLiteral = { lat: 0, lng: 0 };

@Component({
  selector: 'he-sites-maps',
  templateUrl: './sites-maps.component.html',
  styleUrls: ['./sites-maps.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [GoogleMap, MapMarker, MapPolygon, AsyncPipe]
})
export class SitesMapsComponent implements OnDestroy {
  protected mapLoaded$ = inject(HE_MAP_LOADED);
  private nodeService = inject(HeNodeService);
  private nodeStoreService = inject(HeNodeStoreService);

  private _loadPolygonsRequests: ILoadingRequests = {};
  private _mapReady = signal(false);

  @ViewChild(GoogleMap)
  private map?: GoogleMap;

  protected loadPolygons = input(true);

  /**
   * Display Site directly without using the node store.
   */
  protected sites = input([] as site[]);

  private centerTimeout: any;

  protected zoom = input(2);
  protected showNotice = input(true);

  protected options: google.maps.MapOptions = {
    styles: []
  };

  private originalSites = toSignal(this.nodeStoreService.findByState$<ISiteJSONLD>(NodeType.Site, DataState.original));
  private nodes = computed(() => (this._mapReady() ? [...(this.sites() || []), ...(this.originalSites() || [])] : []));

  protected markers = computed(() => this.nodes().map(siteMarker).filter(Boolean));
  private _sitePolygons = computed(() => this.nodes().flatMap(sitePolygon).filter(Boolean));
  protected sitePolygons = computed(() => this._sitePolygons().map(polygon => polygonToMap(polygon)));
  private _termPolygons = signal([] as google.maps.Polygon[]);
  protected termPolygons = signal([] as IPolygonMap[]);

  protected showNoLocation = computed(
    () => this.markers().length === 0 && this.sitePolygons().length === 0 && this.termPolygons().length === 0
  );
  protected mapCenter = computed(() => this.markers()?.[0]?.position || defaultCenter);

  constructor() {
    effect(() => {
      const polygons = this._sitePolygons()?.length ? this._sitePolygons() : this._termPolygons();
      polygons?.length && this.centerPolygons(polygons);
    });

    effect(() => {
      if (this._mapReady() && this.loadPolygons()) {
        from(regionIds(this.nodes()))
          .pipe(
            mergeMap(id => this.loadTermId(id).pipe(map(feature => ({ id, polygons: polygonsFromFeature(feature) })))),
            toArray()
          )
          .subscribe(values => {
            this._termPolygons.set(values.flatMap(({ polygons }) => polygons));
            const polygons = values.flatMap(({ id, polygons }) =>
              polygons.map((polygon, index) => ({
                ...polygonToMap(polygon),
                id: [id, index].join('-')
              }))
            );
            this.termPolygons.set(polygons);
          });
      }
    });
  }

  ngOnDestroy() {
    this._loadPolygonsRequests = {};
  }

  private centerPolygons(polygons: google.maps.Polygon[]) {
    if (this.centerTimeout) {
      clearTimeout(this.centerTimeout);
      this.centerTimeout = null;
    }
    this.centerTimeout = setTimeout(() => {
      this.map!.googleMap!.fitBounds(polygonBounds(polygons));
    }, 1000);
  }

  private loadTermId(id: string) {
    this._loadPolygonsRequests[id] =
      this._loadPolygonsRequests[id] ||
      this.nodeService
        .downloadRaw$<feature>(`${baseUrl(false)}/gadm/${id}.geojson`)
        .pipe(shareReplay({ bufferSize: 1, refCount: true }));
    return this._loadPolygonsRequests[id];
  }

  protected trackByMarker(_index: number, { position, label: { text } }: IMarker) {
    return [text, position.lat, position.lng].join('-');
  }

  protected trackByPolygon(_index: number, { id }: IPolygonMap) {
    return id;
  }

  protected mapLoaded() {
    setTimeout(() => {
      this._mapReady.set(true);
      // loaded data as geojson
      this.map.googleMap.data.setStyle(() => ({ ...strokeStyle, strokeOpacity: 0.1 }));
    });
  }
}
