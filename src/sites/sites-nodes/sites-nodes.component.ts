import { Component, computed, effect, inject, input, signal } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { toSignal } from '@angular/core/rxjs-interop';
import { DataState } from '@hestia-earth/api';
import {
  BlankNodesKey,
  ISiteJSONLD,
  Management,
  managementTermTermType,
  Measurement,
  measurementTermTermType,
  NodeType,
  TermTermType
} from '@hestia-earth/schema';
import { propertyValue } from '@hestia-earth/utils/dist/term';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbPopover, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IconDefinition, faCalculator, faChartBar, faDownload, faList } from '@fortawesome/free-solid-svg-icons';
import orderBy from 'lodash.orderby';

import { filterBlankNode, groupNodesByTerm, IGroupedKeys, grouppedKeys } from '../../common/node-utils';
import { HeNodeStoreService } from '../../node/node-store.service';
import { measurementValue, maxAreaSize, siteTooBig } from '../sites.model';
import { defaultLabel } from '../../common/utils';
import { PrecisionPipe } from '../../common/precision.pipe';
import { EllipsisPipe } from '../../common/ellipsis.pipe';
import { DefaultPipe } from '../../common/default.pipe';
import { CompoundPipe } from '../../common/compound.pipe';
import { CapitalizePipe } from '../../common/capitalize.pipe';
import { NodeValueDetailsComponent } from '../../node/node-value-details/node-value-details.component';
import { NodeCsvExportConfirmComponent } from '../../node/node-csv-export-confirm/node-csv-export-confirm.component';
import { NodeLogsModelsComponent } from '../../node/node-logs-models/node-logs-models.component';
import { BlankNodeStateNoticeComponent } from '../../common/blank-node-state-notice/blank-node-state-notice.component';
import { BlankNodeStateComponent } from '../../common/blank-node-state/blank-node-state.component';
import { TermsUnitsDescriptionComponent } from '../../terms/terms-units-description/terms-units-description.component';
import { NodeLinkComponent } from '../../node/node-link/node-link.component';
import { DataTableComponent } from '../../common/data-table/data-table.component';
import { SearchExtendComponent } from '../../common/search-extend/search-extend.component';
import { SitesManagementChartComponent } from '../sites-management-chart/sites-management-chart.component';

enum View {
  table = 'Table view',
  chart = 'Chart view',
  logs = 'Recalculations logs'
}

const viewIcon: {
  [view in View]: IconDefinition;
} = {
  [View.chart]: faChartBar,
  [View.logs]: faCalculator,
  [View.table]: faList
};

const nodeKeyViews: {
  [type in BlankNodesKey]?: View[];
} = {
  [BlankNodesKey.management]: [View.table, View.chart, View.logs],
  [BlankNodesKey.measurements]: [View.table, View.logs]
};

const nodeKeyFilterTermTypes: {
  [type in BlankNodesKey]?: TermTermType[];
} = {
  [BlankNodesKey.measurements]: measurementTermTermType.term,
  [BlankNodesKey.management]: managementTermTermType.term
};

@Component({
  selector: 'he-sites-nodes',
  templateUrl: './sites-nodes.component.html',
  styleUrls: ['./sites-nodes.component.scss'],
  imports: [
    NgTemplateOutlet,
    FaIconComponent,
    SearchExtendComponent,
    DataTableComponent,
    NodeLinkComponent,
    TermsUnitsDescriptionComponent,
    NgbPopover,
    BlankNodeStateComponent,
    BlankNodeStateNoticeComponent,
    FormsModule,
    NodeLogsModelsComponent,
    NodeValueDetailsComponent,
    CompoundPipe,
    DefaultPipe,
    EllipsisPipe,
    PrecisionPipe,
    CapitalizePipe,
    SitesManagementChartComponent
  ]
})
export class SitesNodesComponent {
  private readonly modalService = inject(NgbModal);
  private readonly nodeStoreService = inject(HeNodeStoreService);

  protected readonly faDownload = faDownload;

  protected readonly dataState = input(DataState.original);
  protected readonly nodeKey = input(BlankNodesKey.measurements);
  protected readonly enableChart = input(true);

  protected readonly TermTermType = TermTermType;
  protected readonly BlankNodesKey = BlankNodesKey;
  protected readonly siteTooBig = siteTooBig;
  protected readonly defaultLabel = defaultLabel;
  protected readonly propertyValue = propertyValue;
  protected readonly measurementValue = measurementValue;
  protected readonly maxAreaSize = maxAreaSize;

  protected readonly headerKeys = computed(() => ['site.id', 'site.@id', `site.${this.nodeKey()}.`]);

  protected readonly View = View;
  protected readonly viewIcon = viewIcon;
  private readonly showView = computed(
    () =>
      ({
        [View.chart]: this.hasData() && this.enableChart(),
        [View.logs]: !this.isOriginal() && this.hasRecalculatedNodes(),
        [View.table]: true
      }) as { [view in View]: boolean }
  );
  protected readonly views = computed(() => nodeKeyViews[this.nodeKey()]?.filter(view => this.showView()[view]) ?? []);
  protected readonly selectedView = signal(View.table);

  private readonly originalSites = toSignal(
    this.nodeStoreService.findByState$<ISiteJSONLD>(NodeType.Site, DataState.original)
  );
  private readonly _allSites = toSignal(this.nodeStoreService.find$<ISiteJSONLD>(NodeType.Site));
  protected readonly sites = computed(() => this._allSites()?.map(data => data[this.dataState()]) || []);

  private readonly selectedIndex = signal(0);
  private readonly ogirinalSelectedSite = computed(() => this.originalSites()?.[this.selectedIndex()]);
  private readonly selectedSite = computed(() => this.sites()?.[this.selectedIndex()]);
  protected readonly selectedOriginalValues = computed(() => this.ogirinalSelectedSite()?.[this.nodeKey()] || []);
  protected readonly selectedRecalculatedValues = computed(() => this.selectedSite()?.[this.nodeKey()] || []);
  protected readonly selectedNode = computed(() =>
    this.selectedSite()
      ? {
          ...this.selectedSite(),
          dataState: DataState.recalculated
        }
      : null
  );

  protected readonly isOriginal = computed(() => this.dataState() === DataState.original);
  private readonly originalValues = computed(() => (this.isOriginal() ? null : this.originalSites()) || []);
  protected readonly isMeasurement = computed(() => this.nodeKey() === BlankNodesKey.measurements);

  protected readonly isNodeKeyAllowed = computed(() => nodeKeyViews[this.nodeKey()].includes(this.selectedView()));

  private readonly hasRecalculatedNodes = computed(() =>
    this.sites().some(({ aggregated }: { aggregated?: any }) => !aggregated)
  );
  protected readonly showSwitchToRecalculated = computed(() => this.isOriginal() && this.hasRecalculatedNodes());
  protected readonly showAreaTooBig = computed(
    () => this.isMeasurement() && !this.isOriginal() && (this.sites() || []).some(siteTooBig)
  );
  protected readonly hasData = computed(() => this.data().length > 0);

  protected readonly filterTermTypes = computed(() => nodeKeyFilterTermTypes[this.nodeKey()]);
  protected readonly filterTerm = signal('');

  protected readonly csvFilename = computed(() => `${this.nodeKey()}.csv`);

  protected readonly data = computed(() => {
    const dataPerSite = groupNodesByTerm<ISiteJSONLD, Management | Measurement>(
      this.sites(),
      this.nodeKey(),
      this.originalValues(),
      filterBlankNode(this.filterTerm())
    );
    return orderBy(grouppedKeys(dataPerSite), ['key'], ['asc']) as IGroupedKeys<Management | Measurement>[];
  });

  constructor() {
    effect(() => {
      // make sure logs does not remain displayed
      if ((this.isOriginal() && this.selectedView() === View.logs) || !this.views().includes(this.selectedView())) {
        this.selectedView.set(View.table);
      }
    });
  }

  protected trackById(_index: number, item: ISiteJSONLD) {
    return item['@id'];
  }

  protected selectIndex({ target: { value } }) {
    this.selectedIndex.set(+value);
  }

  protected showDownload() {
    const instance = this.modalService.open(NodeCsvExportConfirmComponent);
    const component = instance.componentInstance as NodeCsvExportConfirmComponent;
    component.nodes.set(this.sites());
    component.filename.set(this.csvFilename());
    component.isUpload.set(false);
    component.headerKeys.set(this.headerKeys());
  }
}
