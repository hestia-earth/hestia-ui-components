import { NodeType, ISiteJSONLD, Measurement } from '@hestia-earth/schema';
import { isEmpty } from '@hestia-earth/utils';
import { propertyValue } from '@hestia-earth/utils/dist/term';

import { IGroupedNode } from '../common/node-utils';

export interface ISiteJSONLDExtended extends ISiteJSONLD {
  related: {
    [type in NodeType]?: string[];
  };
}

export const maxAreaSize = 5000;

export const siteTooBig = ({ area }: ISiteJSONLD) => area && area / 100 > maxAreaSize;

const hasMultipleValues = (values: any[]) => (values || []).length > 1;

const weighedAverage = ({ value, depthLower, depthUpper }: Partial<IGroupedNode<Measurement>>) =>
  (value as number[]).reduce(
    (prev, curr, index) => prev + curr * (depthLower![index] || 0 - depthUpper![index] || 0),
    0
  ) / (value as number[]).reduce((prev, _curr, index) => prev + (depthLower![index] || 0 - depthUpper![index] || 0), 0);

export const measurementValue = (
  { value, depthLower, depthUpper, nodes }: Partial<IGroupedNode<Measurement>>,
  termId: string
) =>
  nodes?.length > 1
    ? // when dealing with multiple nodes, we start by computing the value for each then do weighted average
      measurementValue(
        {
          value: nodes.map(node => propertyValue(node.value, termId)).filter(v => !isEmpty(v)),
          depthLower: nodes.map(node => node.depthLower).filter(v => !isEmpty(v)),
          depthUpper: nodes.map(node => node.depthUpper).filter(v => !isEmpty(v))
        },
        termId
      )
    : hasMultipleValues(value) && hasMultipleValues(depthLower!) && hasMultipleValues(depthUpper!)
      ? weighedAverage({ value, depthLower, depthUpper })
      : propertyValue(value, termId);
