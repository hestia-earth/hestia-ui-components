import { ChangeDetectionStrategy, Component, computed, input, Signal } from '@angular/core';
import { ChartConfiguration, ChartData, ChartDataSets } from 'chart.js';
import merge from 'lodash.merge';

import { ChartComponent } from '../chart.component';

const defaultSettings: Partial<ChartConfiguration> = {
  type: 'line',
  options: {
    scales: {
      xAxes: [
        {
          type: 'time',
          time: {
            unit: 'day'
          }
        }
      ]
    }
  }
};

@Component({
  selector: 'he-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [ChartComponent]
})
export class LineChartComponent {
  protected readonly datasets = input<ChartDataSets[]>([]);
  protected readonly config = input<Partial<ChartConfiguration>>({});

  protected readonly dataConfig: Signal<ChartData> = computed(() => ({
    datasets: this.datasets()
  }));
  protected readonly configuration = computed(() => merge({}, defaultSettings, this.config()));
}
