import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { ChartConfiguration, ChartData } from 'chart.js';
import merge from 'lodash.merge';

import { ChartConfigurationDirective } from './chart-configuration.directive';

const defaultSettings: Partial<ChartConfiguration> = Object.freeze({
  options: {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
    }
  }
});

@Component({
  selector: 'he-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [ChartConfigurationDirective]
})
export class ChartComponent {
  protected readonly data = input<ChartData>({});
  protected readonly config = input<Partial<ChartConfiguration>>({});

  protected readonly configuration = computed(
    (): ChartConfiguration => ({
      ...merge({}, defaultSettings, this.config()),
      data: this.data()
    })
  );
}
