import { Directive, ElementRef, inject, Input, NgZone, OnDestroy } from '@angular/core';
import Chart, { ChartConfiguration } from 'chart.js';

@Directive({
  selector: '[chartConfiguration]',
  exportAs: 'chart',
  standalone: true
})
export class ChartConfigurationDirective implements OnDestroy {
  private readonly _zone = inject(NgZone);
  private readonly _elementRef = inject(ElementRef);
  private _chart: Chart;

  private readonly _observer = new ResizeObserver(() => this._zone.run(() => this.resize()));

  /**
   * The chart configuration.
   * This is used to initialize the chart.
   *
   * @param configuration The chart configuration
   */
  @Input() set chartConfiguration(configuration: ChartConfiguration) {
    if (!configuration) {
      this._chart?.destroy();
      this._chart = null;
      return;
    }

    this._zone.runOutsideAngular(() => {
      if (this._chart) {
        this._chart.destroy();
      }
      this._chart = new Chart(this._elementRef.nativeElement, configuration);
    });
  }

  /**
   * The container element of the chart.
   * This is used to observe the size of the container and resize the chart accordingly. (chart.js update charts only on the window resize event)
   * If not provided, the chart will not be resized.
   *
   * @param container The container element of the chart
   */
  @Input() set chartContainer(container: HTMLElement) {
    if (container) {
      this._observer?.observe(container);
    } else {
      this._observer?.disconnect();
    }
  }

  ngOnDestroy() {
    this._chart?.destroy();
    this._chart = null;
    this._observer?.disconnect();
  }

  public get chart() {
    return this._chart;
  }

  public resize() {
    this._chart?.resize();
  }
}
