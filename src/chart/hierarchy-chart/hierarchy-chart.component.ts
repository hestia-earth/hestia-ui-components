import {
  Component,
  computed,
  effect,
  ElementRef,
  inject,
  input,
  NgZone,
  output,
  signal,
  viewChild
} from '@angular/core';
import { linkHorizontal as d3LinkHorizontal } from 'd3-shape';
import { select as d3Select, Selection, BaseType, selectAll } from 'd3-selection';
import 'd3-transition';
import {
  hierarchy as d3Hierarchy,
  tree as d3Tree,
  HierarchyNode,
  HierarchyPointNode,
  HierarchyPointLink
} from 'd3-hierarchy';
import { easeElasticIn as d3EaseElasticIn, easeElasticOut as d3EaseElasticOut } from 'd3-ease';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { isEmpty, toPrecision, unique } from '@hestia-earth/utils';
import { TermTermType } from '@hestia-earth/schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

import { baseUrl, schemaBaseUrl } from '../../common/utils';
import { d3wrap } from '../../common/d3-utils';
import {
  ChartNodeType,
  getUnitsAndLabel,
  IChartData,
  ITermMapping,
  legend,
  nodeColours,
  NodeDatum
} from './hierarchy-chart.model';

interface ChartNode extends HierarchyPointNode<NodeDatum> {
  _children?: ChartNode[]; // children of collapsed nodes
  _groupChildren?: ChartNode[]; // all children when group closed; ungrouped children when group open
  _open?: boolean;
  _greyed?: boolean;
  _groupOpen?: boolean;
  _timeout?: NodeJS.Timeout;
}

interface HNode extends HierarchyNode<NodeDatum> {
  _children?: HNode[];
  _groupChildren?: HNode[];
  _groupOpen?: boolean;
  _open?: boolean;
  _greyed?: boolean;
}

interface Link extends HierarchyPointLink<NodeDatum> {
  source: ChartNode;
  target: ChartNode;
}

type NodeSelection = Selection<SVGGElement | BaseType, ChartNode, SVGGElement, unknown>;
type LinkSelection = Selection<SVGPathElement | BaseType, Link, SVGGElement, unknown>;

const typeMapping = {
  [TermTermType.endpointIndicator]: ChartNodeType.endpoint,
  [TermTermType.characterisedIndicator]: ChartNodeType.midpoint,
  [TermTermType.emission]: ChartNodeType.emission,
  [TermTermType.resourceUse]: ChartNodeType.emission,
  [TermTermType.operation]: ChartNodeType.operation
};

const width = 800;
const margin = { top: 10, right: 20, bottom: 10, left: 20 };
const fontSize = Math.floor(width / 100);
const nodePadding = fontSize / 2;
const lineHeight = fontSize * 1.2;
const barHeight = lineHeight;

const nodeHeight = nodePadding * 2 + 2 * lineHeight + barHeight;
const nodeWidth = nodeHeight * 2 * 1.618;
const nodeContentWidth = nodeWidth - nodePadding * 2;
const layerSpacing = (width - margin.left - margin.right - nodeWidth * 5) / 4;
const nodeMargin = nodePadding * 0.75;
const nodeSpacingFactor = (nodeHeight + nodeMargin * 2) / nodeHeight;
const nodeDuration = 600;
const linkDuration = 200;
const groupDelay = nodeDuration - 100;
const nonLCAIndicatorsId = 'nonLCAIndicators';
const greyedOpacity = 0.2;
const groupOverlap = lineHeight - nodePadding - nodeMargin;
const groupNodeSpace = nodeHeight - 2 * groupOverlap;

const collapseChildren = (node: ChartNode | HNode) => {
  node._children = node.children || node._children;
  node.children = null;
  return node;
};

const restoreChildren = (node: ChartNode | HNode) => {
  node.children = node._children;
  node._children = null;
  return node;
};

const wrap = (selection: Selection<SVGTextElement, ChartNode, any, any>, maxWidth = nodeContentWidth) => {
  selection.each(function (d) {
    const t = d3Select<SVGTextElement, ChartNode>(this);
    d3wrap<ChartNode>(t, {
      maxWidth,
      maxLines: d.data.showBar ? 2 : 3,
      vcentre: !d.data.showBar,
      words: [...d.data.label.split(/\s+/), d.data.units],
      ownLineWord: d.data.units,
      nHeight: nodeHeight,
      lHeight: lineHeight
    });
  });
};

const setNodeSize = selection => selection.attr('width', nodeWidth).attr('height', nodeHeight);

const formatBar = (selection: NodeSelection) =>
  selection
    .attr('height', barHeight)
    .attr('rx', '1')
    .attr('ry', '1')
    .attr('y', nodeHeight - nodePadding - barHeight)
    .attr('x', nodePadding);

const isTypeWithoutValue = nodeDatum => [ChartNodeType.input, ChartNodeType.operation].includes(nodeDatum.data.type);

const getWeightedValueText = nodeDatum =>
  `${toPrecision(nodeDatum.data.weightedValue, 3)}${
    nodeDatum.parent?.data.value === null
      ? ''
      : ` of ${toPrecision(nodeDatum.parent?.data.value || nodeDatum.parent?.data.weightedValue, 3)}`
  } ${nodeDatum.parent?.data.value ? nodeDatum.parent?.data.units : nodeDatum.parent?.parent?.data.units}`;

const generateTipData = nodeDatum => ({
  hasWeightedValue: nodeDatum.data.weightedValue,
  weightedValueText: getWeightedValueText(nodeDatum),
  valueKeyHref: `${schemaBaseUrl()}/Indicator#value`,
  modelDocsHref: nodeDatum.data.modelDocs,
  valueValueText: nodeDatum.data.value
    ? `${toPrecision(nodeDatum.data.value, 3)} ${nodeDatum.data.units} - `
    : isTypeWithoutValue(nodeDatum)
      ? 'no data (under development)'
      : 'no contribution',
  hasValue: nodeDatum.data.value,
  modelKeyHref: `${schemaBaseUrl()}/Indicator#methodModel`,
  modelValue: nodeDatum.data.modelName,
  modelValueHref: `${baseUrl()}/model/${nodeDatum.data.modelId}`,
  termKeyHref: `${schemaBaseUrl()}/Indicator#term`,
  termValue: nodeDatum.data.term,
  termValueText: nodeDatum.data.term,
  termValueHref: `${baseUrl()}/term/${nodeDatum.data.id}`
});

const addTooltip = (
  selection: NodeSelection,
  { tooltipOperator, zone }: { tooltipOperator: NgbPopover; zone: NgZone }
) => {
  let hoveringTip = false;
  let hoveringNode = false;
  let lastNodeId: string;

  const closeTip = () => {
    zone.run(() => tooltipOperator.close(false));
    selectAll('.tip-target').classed('tip-target', false);
    lastNodeId = null;
  };

  const startCloseTip = () => {
    setTimeout(() => {
      if (!hoveringTip && !hoveringNode) {
        closeTip();
      }
    }, 100);
  };

  selection
    .on('click', closeTip)
    .on('pointerenter', (event, nodeDatum) => {
      hoveringNode = true;
      if (lastNodeId === nodeDatum.data.id) {
        return;
      }
      closeTip();
      lastNodeId = nodeDatum.data.id;
      d3Select(event.target).classed('tip-target', true);
      zone.run(() => tooltipOperator.open({ tipData: generateTipData(nodeDatum) }));

      d3Select('.driver-chart-tooltip')
        .on('pointerenter', () => {
          hoveringTip = true;
        })
        .on('pointerleave', () => {
          hoveringTip = false;
          startCloseTip();
        });
    })
    .on('pointerleave', () => {
      hoveringNode = false;
      startCloseTip();
    });

  return selection;
};

const mergedNodes = (selection: NodeSelection, { tooltipOperator, zone }) => {
  selection
    .selectAll<SVGRectElement, ChartNode>('.node-box')
    .classed('node-openable', d => !!d.height)
    .classed('node-open', d => d._open)
    .call(setNodeSize)
    .attr('stroke', ({ data }) => nodeColours[data.type])
    .attr('fill', d => nodeColours[d.data.type])
    .style('user-select', window.innerWidth < 768 ? 'none' : null);

  selection.filter(d => !d.data.group && d.data.id !== nonLCAIndicatorsId).call(addTooltip, { tooltipOperator, zone });

  selection
    .selectAll<SVGTextElement, ChartNode>('.node-label')
    .attr('y', d => (d.data.showBar ? lineHeight / 2 + nodePadding / 2 : 0))
    .attr('x', nodeWidth / 2);

  const groups = selection.filter(d => d.data.group);

  groups
    .selectAll<SVGTextElement, ChartNode>('.group-border')
    .attr('width', nodeWidth + nodePadding * 2)
    .attr('x', -nodePadding)
    .attr('y', nodeHeight - (lineHeight + nodePadding * 2))
    .attr('opacity', d => (d._greyed && d._groupOpen ? greyedOpacity : d._groupOpen ? 1 : 0))
    .attr(
      'height',
      d =>
        Math.abs(d.parent._groupChildren.length - d.parent.children.length) * (nodeHeight * nodeSpacingFactor) +
        nodePadding +
        (lineHeight + nodePadding * 2) / 2
    );

  const groupLabels = groups
    .selectAll<SVGTextElement, ChartNode>('.group-label')
    .attr('y', nodeHeight / 2)
    .attr('x', nodePadding)
    .text(d => d.data.label)
    .attr('opacity', d => (d._groupOpen ? 0 : d._greyed ? greyedOpacity : 1));

  // unneeded with current dimensions
  // groupLabels.each(function() {
  //   const text = d3Select<SVGTextElement, ChartNode>(this);
  //   const textWidth = (text.node() as SVGTextElement).getComputedTextLength();
  //   const line = d3Select<SVGLineElement, ChartNode>(this.nextSibling as SVGLineElement);
  //   line
  //     .classed('group-line', true)
  //     .attr('x1', textWidth + nodePadding * 2)
  //     .attr('y1', nodeHeight / 2)
  //     .attr('x2', (nodeWidth * 5) / 7 - nodePadding)
  //     .attr('y2', nodeHeight / 2)
  //     .attr('opacity', (d) => (d._groupOpen ? 0 : d._greyed ? greyedOpacity : 1));
  // });

  const setGroupButtonXY = sel =>
    sel
      .attr('width', (nodeWidth * 2) / 7)
      .attr('height', d => (d._groupOpen ? lineHeight + nodePadding * 2 : nodePadding * 2 + lineHeight))
      .attr('x', d => (d._groupOpen ? (nodeWidth * 2.5) / 7 : (nodeWidth * 5) / 7 - nodePadding))
      .attr('y', d =>
        d._groupOpen
          ? groupOverlap + (groupNodeSpace - (lineHeight + nodePadding * 2)) / 2
          : nodePadding + lineHeight / 2
      );

  groups
    .selectAll<SVGRectElement, ChartNode>('.group-button')
    .call(setGroupButtonXY)
    .style('pointer-events', d => (d._greyed ? 'none' : 'auto'))
    .style('stroke-opacity', d => (d._greyed ? greyedOpacity : 1));

  groups
    .selectAll<SVGTextElement, ChartNode>('.group-button-text')
    .text(d => (d._groupOpen ? 'Hide' : 'Show'))
    .attr('x', d => (d._groupOpen ? nodeWidth / 2 : (nodeWidth * 6) / 7 - nodePadding))
    .attr('y', d => (d._groupOpen ? nodeMargin + lineHeight + lineHeight / 2 : nodeHeight / 2))
    .attr('font-size', () => `${fontSize * 1.3}px`);

  selection.selectAll('.sibling-mask').remove();

  selection
    .filter(d => d._greyed && !d.data.group)
    .append('rect')
    .classed('node-openable', d => !!d.height)
    .classed('sibling-mask mask-openable', true)
    .call(setNodeSize);

  selection
    .filter(d => d._greyed && d.data.group)
    .append('rect')
    .classed('sibling-mask', true)
    .call(setGroupButtonXY);

  return selection;
};

// more than 70%
const fractionCutoff = 0.7;

const fractionWidth = (data: NodeDatum) => Math.abs(data.fraction) * nodeContentWidth;

const enterNodes = (selection: NodeSelection) => {
  const nodes = selection.filter(d => d.data.type !== ChartNodeType.root && !d.data.group);

  nodes.append('rect').classed('node-box', true);

  nodes
    .append('text')
    .classed('node-label', true)
    .text(({ data }) => data.label + ' ' + data.units)
    .attr('opacity', 0);

  // Text elements must be rendered before we can calculate wrapping
  setTimeout(() => {
    const labels = nodes.selectAll<SVGTextElement, ChartNode>('.node-label').call(wrap);
    labels.attr('opacity', 1);
  });

  const groups = selection.filter(d => d.data.group);

  groups.append('rect').attr('class', 'group-border');
  groups.append('text').attr('class', 'group-label');
  // groups.append('line').attr('class', 'group-line');
  groups.append('rect').attr('class', 'group-button');
  groups.append('text').attr('class', 'group-button-text');

  const withBar = nodes.filter(d => d.data.showBar);

  withBar
    .filter(d => d.data.fraction > 0)
    .append('rect')
    .classed('bar-background', true)
    .attr('width', nodeContentWidth)
    .call(formatBar);

  withBar
    .append('rect')
    .classed('bar-foreground', true)
    .call(formatBar)
    .attr('width', d => fractionWidth(d.data))
    .attr('fill', d => nodeColours[d.data.type]);

  withBar
    .append('text')
    .classed('bar-label', true)
    .text(({ data }) =>
      data.fraction === 0
        ? 'no contribution'
        : `${Math.round(100 * data.fraction)}% ${Math.round(100 * data.fraction) === 0 ? ' (rounded)' : ''}`
    )
    .attr('text-anchor', d => (d.data.fraction === 0 ? 'middle' : d.data.fraction > fractionCutoff ? 'end' : 'start'))
    .attr('x', d =>
      d.data.fraction === 0
        ? nodeWidth / 2
        : fractionWidth(d.data) + nodePadding + (d.data.fraction > fractionCutoff ? -4 : 4)
    )
    .attr('y', nodeHeight - nodePadding - barHeight / 2);

  return selection;
};

const mergedLinks = (selection: LinkSelection, { togglingGroup, switchingSelection }) =>
  selection
    .style('stroke', d => {
      const colours = {
        [ChartNodeType.midpoint]: nodeColours[ChartNodeType.midpoint],
        [ChartNodeType.endpoint]: nodeColours[ChartNodeType.endpoint],
        [ChartNodeType.emission]: nodeColours[ChartNodeType.emission]
      };
      return (d.target._open && colours[d.source.data.type]) || null;
    })
    .attr('stroke-dasharray', d => (d.target.data.group ? '2,5' : null))
    .attr('stroke-width', d => (d.target._open ? 3 : d.target._groupOpen ? 0 : 2))
    .attr('stroke-opacity', d => (d.target._greyed ? 0.1 : 0.6))
    .transition()
    .duration(linkDuration)
    .delay(d => {
      const groupClosing = togglingGroup && !d.target.parent?.children?.find(n => n.data.group)?._groupOpen;
      return groupClosing
        ? groupDelay
        : (switchingSelection ? nodeDuration : 0) + d.source.children?.indexOf(d.target) * 10;
    })
    .attr('opacity', 1)
    .attr('d', d => {
      const targetDy = d.target._groupOpen ? nodePadding + lineHeight / 2 + nodeMargin * 2 : nodeHeight / 2;
      return d3LinkHorizontal()({
        source: [d.source.y + nodeWidth, d.source.x + nodeHeight / 2],
        target: [d.target.y, d.target.x + targetDy]
      });
    });

const groupByPercentage = node =>
  [ChartNodeType.endpoint, ChartNodeType.midpoint, ChartNodeType.emission].includes(node.data.type);

const groupPercentage = (node, percentage = 0.02) => {
  const grouped = node._children?.filter(child => child.data.fraction < percentage);
  if (node._children?.length > 6 && grouped?.length > 1) {
    const type = node._children[0].data.type;
    const groupNode = {
      ...d3Hierarchy({
        id: `${type}-group`,
        label: `${grouped.length} more under ${Math.round(percentage * 100)}%.`,
        weightedValue: null,
        value: null,
        type,
        group: true
      }),
      depth: node.depth + 1,
      height: 1,
      parent: node
    };
    const notGrouped = node._children?.filter(child => child.data.fraction >= percentage);
    node._children.splice(notGrouped.length, 0, groupNode);
    notGrouped.push(groupNode);
    node._groupChildren = node._children;
    node._children = notGrouped;
  }
};

interface IIndicatorsSum {
  [indicator: string]: number;
}

const sumIndicators = (data: IChartData[]) =>
  data.reduce((acc, log) => {
    // ignore negative values in the total
    acc[log.indicator] = (acc[log.indicator] || 0) + (log.weightedValue > 0 ? log.weightedValue : 0);
    return acc;
  }, {} as IIndicatorsSum);

const groupIndicators = (data: IChartData[], terms: ITermMapping = {}, totals: IIndicatorsSum = {}) =>
  data.reduce(
    (acc, log) => {
      const contributorType = typeMapping[terms[log.contributor]?.termType] || ChartNodeType.input;
      const indicatorType = typeMapping[terms[log.indicator]?.termType];
      const contributorNode = {
        id: log.contributor,
        ...getUnitsAndLabel(terms[log.contributor], log.contributor),
        type: contributorType,
        value: log.value,
        weightedValue: log.weightedValue,
        modelId: log.modelId,
        fraction: log.weightedValue / totals[log.indicator],
        children:
          contributorType === ChartNodeType.emission
            ? log.inputs.map(i => ({
                ...i,
                type: ChartNodeType.input,
                fraction: i.weightedValue / log.value,
                children: i.operations.map(o => ({
                  ...o,
                  type: ChartNodeType.operation,
                  fraction: o.weightedValue / i.weightedValue
                }))
              }))
            : []
      } as NodeDatum;

      acc[log.indicator]
        ? acc[log.indicator].children.push(contributorNode)
        : (acc[log.indicator] = {
            id: log.indicator,
            ...getUnitsAndLabel(terms[log.indicator], log.indicator),
            modelId: log.modelId,
            type: indicatorType,
            value: totals[log.indicator],
            children: [contributorNode]
          });
      return acc;
    },
    {} as { [id: string]: NodeDatum }
  );

const showBar = (node: HierarchyNode<NodeDatum>) =>
  [
    node.parent?.data.type !== ChartNodeType.root,
    node.parent?.data.id !== nonLCAIndicatorsId,
    !node.data.group,
    !isNaN(node.data.fraction)
  ].every(Boolean);

@Component({
  selector: 'he-hierarchy-chart',
  imports: [NgbPopover, FaIconComponent],
  templateUrl: './hierarchy-chart.component.html',
  styleUrl: './hierarchy-chart.component.scss'
})
export class HierarchyChartComponent {
  private readonly ngZone = inject(NgZone);

  protected readonly faExternalLinkAlt = faExternalLinkAlt;

  private readonly chartRef = viewChild.required<ElementRef>('chart');
  private readonly tooltipOperator = viewChild.required<NgbPopover>('t');

  protected readonly data = input<IChartData[]>([]);
  protected readonly terms = input({} as ITermMapping);

  protected readonly chartError = output<Error>();

  protected readonly nodeColours = nodeColours;

  private readonly includedTypes = signal<ChartNodeType[]>([]);
  protected readonly legend = computed(() => legend.filter(({ type }) => this.includedTypes().includes(type)));

  private readonly totalsPerIndicator = computed(() => sumIndicators(this.data()));
  private readonly indicators = computed(() => groupIndicators(this.data(), this.terms(), this.totalsPerIndicator()));
  private readonly indicatorArray = computed(() => Object.values(this.indicators()));
  private readonly linkedIndicators = computed(() =>
    this.indicatorArray()
      // link endpoints to midpoints and midpoints to any child midpoints
      .map(indicator => ({
        ...indicator,
        children: indicator.children.map(child => {
          const prefixRemoved = indicator.type === ChartNodeType.endpoint && child.label.split(', ')[1];
          return child.type === ChartNodeType.midpoint
            ? {
                ...child,
                label: prefixRemoved
                  ? '[...]' + prefixRemoved.charAt(0).toUpperCase() + prefixRemoved.slice(1)
                  : child.label,
                children: this.indicators()[child.id]?.children
              }
            : child;
        })
      }))
      // remove duplicated indicators
      .filter(
        indicator => !this.indicatorArray().some(parent => parent.children.some(child => child.id === indicator.id))
      )
  );
  private readonly nonLCAIndicators = computed(() =>
    this.linkedIndicators().filter(indicator => indicator.type === ChartNodeType.midpoint)
  );
  private readonly otherEndpoint = computed(() =>
    this.nonLCAIndicators().length
      ? {
          id: nonLCAIndicatorsId,
          label: 'Other indicators',
          term: 'n/a',
          modelId: 'n/a',
          units: '',
          value: null,
          weightedValue: null,
          type: ChartNodeType.endpoint,
          fraction: null,
          children: this.nonLCAIndicators()
        }
      : null
  );
  private readonly endpoints = computed(() =>
    this.linkedIndicators().filter(indicator => indicator.type === ChartNodeType.endpoint)
  );
  private readonly chartChildren = computed(() =>
    this.endpoints().length ? [...this.endpoints(), this.otherEndpoint()].filter(Boolean) : this.linkedIndicators()
  );

  private root?: HNode;
  private node: NodeSelection;
  private link: LinkSelection;
  private svg: Selection<SVGSVGElement, any, HTMLElement, unknown>;
  private yMin: number;
  private yTotal: number;

  constructor() {
    effect(() => {
      if ([!isEmpty(this.data()), !isEmpty(this.terms()), this.chartRef()?.nativeElement].every(Boolean)) {
        this.init();
      }
    });
  }

  private init() {
    try {
      this.root = null;
      this.node = null;
      this.link = null;
      this.initChart();
    } catch (error) {
      this.chartError.emit(error);
    }
  }

  protected initChart() {
    const root = d3Hierarchy<NodeDatum>({
      id: 'dummy-root',
      label: '',
      term: '',
      modelId: '',
      value: null,
      weightedValue: null,
      units: '',
      type: ChartNodeType.root,
      children: this.chartChildren()
    });

    this.includedTypes.set(unique(root.descendants().map(n => n.data.type)));

    this.root = root
      .sort((a, b) => b.data.fraction - a.data.fraction)
      .eachAfter(node => {
        if (node.data.fraction === 0) {
          node.children = null;
          (node.height as number) = 0;
        }
        node.data.type !== ChartNodeType.root && collapseChildren(node);
        groupByPercentage(node) && groupPercentage(node);
        node.data.showBar = showBar(node);
        node.data.modelName = this.terms()?.[node.data.modelId]?.name;
        node.data.modelDocs = this.terms()?.[node.data.modelId]?.docs;
      });

    this.renderChart();
  }

  private renderChart() {
    this.svg = d3Select(this.chartRef()?.nativeElement);
    this.svg.selectAll('*').remove();
    // root node is always positioned at ⟨0, 0⟩
    const innerHeight = (this.root.children.length - 1) * nodeHeight * nodeSpacingFactor + nodeHeight;
    this.yMin = (nodeHeight - innerHeight) / 2 - margin.top;
    this.yTotal = innerHeight + margin.top + margin.bottom;
    this.svg
      .attr('viewBox', [-margin.left, this.yMin, width, this.yTotal].join(' '))
      .attr('font-size', `${fontSize}px`);

    this.node = this.svg.append('g').selectAll('g');
    this.link = this.svg.append('g').selectAll('path');

    this.handleNodeClick(
      null,
      this.root.find(node => node.depth === 1)
    );
  }

  // flip x/y to go left-right instead of top-bottom
  private updateChart({ togglingGroup = false, switchingSelection = false } = {}) {
    const root = this.root;
    const treeLayout = d3Tree<NodeDatum>()
      .nodeSize([nodeHeight, nodeWidth])
      .separation((...neighbours: ChartNode[]) =>
        neighbours.some(n => n.data.group && n._groupOpen) ? 1 - groupOverlap / nodeHeight : nodeSpacingFactor
      );
    const tree: ChartNode = treeLayout(root);
    const links: Link[] = tree.links().filter(d => d.source.data.type !== ChartNodeType.root) || [];
    const nodes = tree.descendants().reverse();

    nodes.forEach(d => {
      d.y =
        (d.depth - 1 + (Object.keys(ChartNodeType).length - 1 - this.legend().length) / 2) * (nodeWidth + layerSpacing);
    });

    // By default the tree expands symmetrically upwards and downwards from the selected node.
    // We move children down to prevent expansion upwards or up to minimise expansion downwards
    const minimiseHeight = ({ previousTanslateY, previousYTotal }, depth: number) => {
      const maxYTotal = Math.max(previousYTotal, this.yTotal);
      const parent = tree.find(node => node.depth === depth - 1 && node._open);
      parent?.children?.forEach(node => (node.x += previousTanslateY));

      const { min: minX, max: maxX } = (parent?.children || []).reduce(
        ({ min, max }, n) => ({
          min: n.x < min ? n.x : min,
          max: n.x > max ? n.x : max
        }),
        { min: parent?.x, max: parent?.x }
      );

      const { yMin, yTotal } = parent?.children?.length
        ? { yMin: minX - margin.top, yTotal: maxX - minX + margin.bottom + margin.top + nodeHeight }
        : { yMin: null, yTotal: null };

      const overflowAbove = this.yMin - yMin; // positive when overflows above
      const overflowBelow = yTotal + yMin - (maxYTotal + this.yMin); // positive when overflows below
      // - translateY when overflows above: positive
      // - translateY when overflows below: negative by smallest of underflow above or overflow below
      // - translateY when no overflow: 0
      const translateY =
        overflowAbove > 0 ? overflowAbove : overflowBelow > 0 ? Math.max(-overflowBelow, overflowAbove) : 0;

      parent?.children?.forEach(node => (node.x += translateY));

      return {
        previousYTotal: Math.max(yTotal, previousYTotal),
        previousTanslateY: translateY + previousTanslateY
      };
    };

    const { previousYTotal: newHeight } = [2, 3, 4, 5].reduce(minimiseHeight, {
      previousTanslateY: 0,
      previousYTotal: this.yTotal
    });

    this.svg.transition().duration(300).attr('viewBox', [-margin.left, this.yMin, width, newHeight].join(' '));

    this.node = this.node
      .data(
        nodes,
        d => `${d.parent?.parent?.parent?.data.id}-${d.parent?.parent?.data.id}-${d.parent?.data.id}-${d.data.id}`
      )
      .join(
        enter =>
          enter
            .append('g')
            .call(enterNodes)
            .attr('opacity', 0)
            .attr('transform', d => 'translate(' + (togglingGroup ? d.y : d.y - nodeWidth / 2) + ',' + d.x + ')'),
        update => update,
        exit => {
          this.ngZone.run(() => this.tooltipOperator().close(false));
          return exit
            .style('pointer-events', 'none')
            .transition()
            .duration(nodeDuration / 2)
            .delay(d => {
              const children = d.parent?.children || d.parent?._children;
              return (children?.length - children?.indexOf(d) - 1) * 10;
            })
            .ease(d3EaseElasticIn.amplitude(0.5).period(1))
            .attr('transform', d => 'translate(' + (togglingGroup ? d.y : d.y - nodeWidth / 2) + ',' + d.x + ')')
            .attr('opacity', 0)
            .remove()
            .on('end', () => {
              // in case a tooltip was opened on a transitioning element
              this.ngZone.run(() => this.tooltipOperator().close(false));
            });
        }
      );

    this.node
      .call(mergedNodes, { tooltipOperator: this.tooltipOperator(), zone: this.ngZone })
      .transition()
      .delay(d => {
        const groupClosing = togglingGroup && !d.parent?.children?.find(n => n.data.group)?._groupOpen;
        return groupClosing ? groupDelay : (switchingSelection ? nodeDuration : 0) + d.parent?.children.indexOf(d) * 10;
      })
      .duration(nodeDuration)
      .ease(d3EaseElasticOut.amplitude(0.5).period(1))
      .attr('transform', d => `translate(${d.y},${d.x})`)
      .attr('opacity', 1);

    this.node.selectAll('.node-openable').on('click', this.handleNodeClick.bind(this));
    this.node.selectAll('.group-button').on('click', this.handleGroupButtonClick.bind(this));

    this.link = this.link
      .data(
        links,
        d => `${d.source.parent?.parent?.data.id}-${d.source.parent?.data.id}-${d.source.data.id}-${d.target.data.id}`
      )
      .join(
        enter => enter.append('path').classed('link-path', true).attr('opacity', 0),
        update => update,
        exit =>
          exit
            .transition()
            .duration(linkDuration / 2)
            .delay(d => {
              const children = d.source.parent?.children || d.source.parent?._children;
              return (children?.length - children?.indexOf(d.target) - 1) * 10;
            })
            .attr('opacity', 0)
            .remove()
      )
      .call(mergedLinks, { togglingGroup, switchingSelection });
  }

  private handleGroupButtonClick(_event: any, d: ChartNode) {
    if (d._greyed) {
      return;
    }
    d._groupOpen = !d._groupOpen;
    const children = d.parent._groupChildren;
    d.parent._groupChildren = d.parent.children;
    d.parent.children = children;
    this.updateChart({ togglingGroup: true });
  }

  private handleNodeClick(_event: any, d: ChartNode | HNode): void {
    d._open = !d._open;
    const switchingSelection = '_greyed' in d && d._greyed;
    d.parent.children.forEach(child => {
      child._greyed = false;
      if (child !== d) {
        child._open = false;
        collapseChildren(child);
        child._greyed = d._open;
      }
    });
    d.children ? collapseChildren(d) : restoreChildren(d);
    this.updateChart({ switchingSelection });
  }
}
