import { ITermJSONLD, Term } from '@hestia-earth/schema';

import { ISearchResultExtended } from '../../search';

export enum ChartNodeType {
  midpoint = 'midpoint',
  emission = 'emission',
  endpoint = 'endpoint',
  input = 'input',
  operation = 'operation',
  root = 'root'
}

export const nodeColours = {
  [ChartNodeType.endpoint]: 'blue',
  [ChartNodeType.midpoint]: 'red',
  [ChartNodeType.emission]: 'purple',
  [ChartNodeType.input]: 'green',
  [ChartNodeType.operation]: 'orange'
};

export const legend: {
  type: ChartNodeType;
  text: string;
  mobileText: string;
}[] = [
  { type: ChartNodeType.endpoint, mobileText: 'Endpoint', text: 'Endpoint Indicator' },
  { type: ChartNodeType.midpoint, text: 'Characterised Indicator', mobileText: 'Impact' },
  { type: ChartNodeType.emission, mobileText: 'Emission', text: 'Emission or Resource Use' },
  { type: ChartNodeType.input, text: 'Input', mobileText: 'Input' },
  { type: ChartNodeType.operation, text: 'Operation', mobileText: 'Operation' }
];

export interface NodeDatum {
  id?: string;
  term?: string;
  modelId?: string;
  modelName?: string;
  modelDocs?: string;
  label: string;
  type: ChartNodeType;
  value: number | null;
  weightedValue?: number | null;
  fraction?: number;
  children?: NodeDatum[];
  group?: boolean;
  showBar?: boolean;
  units: string;
}

export interface IChartInputLog {
  label: string;
  units: string;
  weightedValue: number;
  value: number;
  operations: {
    weightedValue: number;
    label: string;
    units: string;
    value: number;
  }[];
}

export interface IChartData {
  indicator: string;
  contributor: string;
  coefficient: number;
  value: number;
  weightedValue: number;
  modelId: string;
  inputs?: IChartInputLog[];
}

export type term = ITermJSONLD & ISearchResultExtended & { docs?: string };

export interface ITermMapping {
  [id: string]: term;
}

export const getUnitsAndLabel = (term: Partial<ITermJSONLD | Term>, fallbackLabel = 'unknown term') => {
  const label = term?.name || term?.['@id'] || fallbackLabel;
  const units = `(${term?.units || 'unknown units'})`;
  return { label: label.replace(units, '').trim(), term: label, units };
};
