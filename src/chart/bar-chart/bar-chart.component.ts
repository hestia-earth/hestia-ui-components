import { ChangeDetectionStrategy, Component, computed, input, Signal } from '@angular/core';
import { ChartConfiguration, ChartData, ChartDataSets } from 'chart.js';
import merge from 'lodash.merge';

import { ChartComponent } from '../chart.component';

export interface ChartDataItem {
  label: string;
  count?: number;
  color?: string;
  backgroundColor?: string;
  borderColor?: string;
}

const defaultSettings: Partial<ChartConfiguration> = Object.freeze({
  type: 'horizontalBar',
  options: {
    events: [],
    animation: {
      duration: 0
    },
    tooltips: {
      enabled: false
    },
    scales: {
      yAxes: [
        {
          display: true,
          position: 'left',
          gridLines: {
            display: false
          },
          ticks: {
            crossAlign: 'center'
          } as any
        }
      ]
    }
  }
});

const defaultDatasets: ChartDataSets = {
  // axis: 'y',
  barPercentage: 0.5,
  barThickness: 8,
  maxBarThickness: 12
};

@Component({
  selector: 'he-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [ChartComponent]
})
export class BarChartComponent {
  protected readonly data = input<ChartDataItem[]>([]);
  protected readonly max = input<number>();
  protected readonly datasets = input<Partial<ChartDataSets>[]>([]);
  protected readonly config = input<Partial<ChartConfiguration>>({});

  private readonly maxConfig = computed(() => (this.max() ? { max: this.max() } : {}));
  private readonly defaultConfig: Signal<Partial<ChartConfiguration>> = computed(() => ({
    options: {
      legend: {
        align: 'start'
      },
      scales: {
        xAxes: [
          {
            display: false,
            ticks: {
              min: 0,
              ...this.maxConfig()
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              fontColor: '#4A4A4A',
              fontFamily: 'Lato',
              fontSize: 10,
              fontStyle: '400'
            }
          }
        ]
      }
    }
  }));

  private readonly defaultDatasets: Signal<Partial<ChartDataSets>[]> = computed(() =>
    this.datasets()?.length
      ? this.datasets()
      : [
          {
            defaultDatasets,
            backgroundColor: this.data().map(({ backgroundColor, color }) => backgroundColor || color),
            borderColor: this.data().map(({ borderColor, color }) => borderColor || color),
            data: this.data().map(({ count }) => count)
          }
        ]
  );

  protected readonly dataConfig: Signal<ChartData> = computed(
    () =>
      ({
        datasets: this.defaultDatasets(),
        labels: this.data().map(({ label }) => label)
      }) as ChartData
  );
  protected readonly configuration = computed(() => merge({}, defaultSettings, this.defaultConfig(), this.config()));
}
