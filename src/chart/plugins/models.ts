import Chart from 'chart.js';

// Use not exported types from chart.js

export type ChartPlugin = Chart.PluginServiceGlobalRegistration & Partial<Chart.PluginServiceRegistrationOptions>;
export type DatasetModel = ReturnType<Chart['getDatasetMeta']>['data'][0]['_model'];
