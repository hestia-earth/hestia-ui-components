import { ChartPlugin } from './models';

export const chartAreaBorderPlugin = (styles?: { borderColor?: string; borderWidth?: string }): ChartPlugin => ({
  beforeDraw: (chart, args, options) => {
    const { ctx, chartArea } = chart;
    ctx.save();
    ctx.strokeStyle = styles.borderColor || options.borderColor;
    ctx.lineWidth = styles.borderWidth || options.borderWidth;
    ctx.setLineDash(options.borderDash || []);
    ctx.lineDashOffset = options.borderDashOffset;
    ctx.strokeRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
    ctx.restore();
  }
});
