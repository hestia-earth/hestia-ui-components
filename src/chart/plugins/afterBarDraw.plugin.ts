import { toPrecision } from '@hestia-earth/utils';

import { ChartPlugin } from './models';

export const defaultBarDrawSettings = {
  xPosFn: (x: number, index: number, width: number) => x + 10,
  yPosFn: (y: number, index: number) => y + 3,
  colorFn: (m: any, index: number) => 'black',
  textFn: (
    m: {
      label: string;
      data: number;
    },
    index: number
  ): string | [string, string] => `${m.label} - ${toPrecision(m.data)} kg`,
  font: '12px Lato',
  maxWidth: 90
};

export type BarDrawSettings = Partial<typeof defaultBarDrawSettings>;

export const afterBarDrawPlugin: (settings?: BarDrawSettings) => ChartPlugin = settings => ({
  afterDraw: chart => {
    if (!chart.data.datasets?.length) {
      return;
    }

    const { xPosFn, yPosFn, colorFn, textFn, maxWidth, font } = { ...defaultBarDrawSettings, ...(settings ?? {}) };

    const { ctx } = chart;
    ctx.save();

    const width = chart.chartArea.right - chart.chartArea.left;

    chart
      .getDatasetMeta(0)
      .data.filter(({ _view }) => !!_view)
      .forEach(({ _view, _model, _datasetIndex, _index }, index) => {
        const { x, y } = _view;
        const label = (_model as any).label;
        const data = chart.data.datasets[_datasetIndex].data[_index];

        //data should be a number
        if (typeof data !== 'number') {
          return;
        }

        const xPos = xPosFn(x, index, width);
        const yPos = yPosFn(y, index);
        const text = textFn({ label, data }, index);

        ctx.font = font;
        ctx.fillStyle = colorFn(_view, index);
        if (Array.isArray(text)) {
          ctx.fillText(text[0], xPos, yPos - 5, maxWidth);
          ctx.fillText(text[1], xPos, yPos + 5, maxWidth);
        } else {
          ctx.fillText(text, xPos, yPos, maxWidth);
        }
      });
  }
});
