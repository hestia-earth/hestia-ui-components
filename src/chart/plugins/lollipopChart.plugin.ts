import Chart from 'chart.js';
import { ChartPlugin, DatasetModel } from './models';

export const defaultLollipopSettings = {
  circleRadius: 4,
  colorFn: (m: DatasetModel, index: number) => m.backgroundColor,
  isMouseInsideLollipopFn: null as (fn: (x: number, y: number) => number) => void | null
};

export type LollipopPluginSettings = Partial<typeof defaultLollipopSettings>;

export const lollipopChartPlugin: (settings?: LollipopPluginSettings) => ChartPlugin = settings => ({
  afterDatasetsDraw: (chart: Chart) => {
    if (!chart.data.datasets?.length) {
      return;
    }

    const { circleRadius, colorFn, isMouseInsideLollipopFn } = { ...defaultLollipopSettings, ...(settings ?? {}) };

    const { ctx } = chart;
    ctx.save();
    const clickableDictionary = [];
    chart
      .getDatasetMeta(0)
      .data.filter(({ _view }) => !!_view)
      .forEach(({ _view }, index) => {
        const color = colorFn(_view, index);
        const { x, y } = _view;
        clickableDictionary.push((xClick, yClick) => {
          return isMouseInsideCircle({ clientX: xClick, clientY: yClick }, { x, y }, circleRadius);
        });
        circle(x, y, color);
      });

    isMouseInsideLollipopFn?.((x: number, y: number) => {
      return clickableDictionary.findIndex(fn => fn(x, y));
    });

    function circle(xPosition: number, yPosition: number, color: string) {
      const angle = Math.PI / 180;
      ctx.beginPath();

      ctx.fillStyle = color;
      ctx.arc(xPosition, yPosition, circleRadius ?? 7, 0, angle * 360, false);
      ctx.fill();
      ctx.closePath();
      ctx.restore();
    }
  }
});

function isMouseInsideCircle(
  event: Pick<MouseEvent, 'clientX' | 'clientY'>,
  circleCenter: {
    x: number;
    y: number;
  },
  circleRadius: number
): boolean {
  const { clientX, clientY } = event;
  const circleX = circleCenter.x;
  const circleY = circleCenter.y;

  // Calculate the distance between the click point and the circle center
  const distance = Math.sqrt((clientX - circleX) ** 2 + (clientY - circleY) ** 2);

  // Check if the distance is less than the circle radius
  return distance <= circleRadius;
}
