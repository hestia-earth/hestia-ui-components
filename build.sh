#!/bin/sh

set -e

rm -rf .angular
npm run build
cd dist
npm link
