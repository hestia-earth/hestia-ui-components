# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.31.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.31.4...v0.31.5) (2025-02-27)

### [0.31.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.31.3...v0.31.4) (2025-02-27)


### Features

* **files-upload-errors:** handle privacy not allowed error ([1329760](https://gitlab.com/hestia-earth/hestia-ui-components/commit/132976005301d0072d4186fdacd7ececb7654847))

### [0.31.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.31.2...v0.31.3) (2025-02-25)


### Bug Fixes

* **files-form:** put back links on nested existing nodes ([f258521](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f2585218ab07df62fec1382d108b18229f8dfe15))

### [0.31.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.31.1...v0.31.2) (2025-02-18)


### Features

* **chart:** add multiple plugins from frontend ([2372cc7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2372cc76ce78bba3699f187c8258cdef73ad3770))


### Bug Fixes

* **sutes-management-chart:** handle `endDate` with no day precision ([ea05596](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ea05596e3b65fe72e291525a15ade236913325dc))

### [0.31.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.31.0...v0.31.1) (2025-02-05)


### Features

* **mobile-shell:** handle disabled buttons ([a6252b1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a6252b19db55a9d4227309e77c0cc24a81db016c))


### Bug Fixes

* **drawer-container:** fix padding not applied to container ([746be1b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/746be1b7c96831426e48877bfc94e799a9a94c50))

## [0.31.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.30.6...v0.31.0) (2025-02-03)


### ⚠ BREAKING CHANGES

* **package:** Angular version supported is now `19`.

* **package:** update to angular 19 ([5381420](https://gitlab.com/hestia-earth/hestia-ui-components/commit/53814204d068239323c2bf8dc12a96f5f7d7dd3b))

### [0.30.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.30.5...v0.30.6) (2025-01-30)


### Bug Fixes

* **impact-assessments-graph:** fix infinite loading in original state ([55a4a3b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/55a4a3b569703df9f0cd8b45f814d69003a42ad6))
* **unit-converter:** fix issues selecting units and parsing values ([f1056ae](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f1056ae2fe4803bc37c7d081c71c42ddc2666605))

### [0.30.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.30.4...v0.30.5) (2025-01-29)


### Bug Fixes

* **styles:** fix screen padding on fullhd ([5e88802](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5e88802208e9424da453b875341488c9ed5452b8))

### [0.30.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.30.3...v0.30.4) (2025-01-29)


### Features

* **files-form:** move node link to top bar ([6cffc6f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6cffc6f9e455f158c3565cf9fca60f6321942b8b))

### [0.30.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.30.2...v0.30.3) (2025-01-28)


### Features

* **files-error:** improve error message on source ([94c69a5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/94c69a50ee9294714041546a5adaa1a2a73e0d1d)), closes [#353](https://gitlab.com/hestia-earth/hestia-ui-components/issues/353)
* **node-logs-models:** allow sorting on nested table logs ([8bb6509](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8bb650915a5570e8f8bc40c0e950e1391410663c)), closes [#367](https://gitlab.com/hestia-earth/hestia-ui-components/issues/367)
* **node-logs-models:** log failed inputs for background emissions ([6912845](https://gitlab.com/hestia-earth/hestia-ui-components/commit/69128458bef0707ab8d8cd763d4d18fce0a67d18))


### Bug Fixes

* **files-error:** handle completeness missing fields ([b06706f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b06706fa6f11d7ac2326e11b592165ade5b40f19)), closes [#380](https://gitlab.com/hestia-earth/hestia-ui-components/issues/380)

### [0.30.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.30.1...v0.30.2) (2025-01-27)


### Bug Fixes

* **svg-icons:** fix colors on glossary and schema ([2530442](https://gitlab.com/hestia-earth/hestia-ui-components/commit/25304425b2e0eb1462ae5b83dbbd67c728da031e))

### [0.30.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.30.0...v0.30.1) (2025-01-27)


### Bug Fixes

* **heirarchy-chart:** handle negative fractions ([ca4e733](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ca4e7330ed1d1c38ce3b8b05096fe167a54fcd9b))
* **select:** set correct color ([005ba44](https://gitlab.com/hestia-earth/hestia-ui-components/commit/005ba44473e8cfabe274b24df7e55ab7f91768c4))

## [0.30.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.29.7...v0.30.0) (2025-01-22)


### ⚠ BREAKING CHANGES

* **package:** Library no longer requires `engine-config`.

* **package:** remove requirements on engine-config ([247e98c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/247e98ce275f1c5b5b2b73b83e9a1e82766f4f2a))

### [0.29.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.29.6...v0.29.7) (2025-01-21)


### Bug Fixes

* **files-upload-errors:** handle no file status ([c548862](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c548862f760a85c54fca269660547c08c718a51d))
* **sites:** fix measurement value ignoring zeros ([1f94d01](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1f94d01da6e1fa576aa2cff83c3c15e5674f802d))
* **svg-icons:** update glossary and schema icon ([411ced0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/411ced0c9cb3188b58bf031b16996005393fea3a))

### [0.29.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.29.5...v0.29.6) (2025-01-17)


### Features

* **drawer-container:** use drag icon when resizable ([ada979c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ada979cded80886880d1c8c579af2294bb921d96))

### [0.29.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.29.4...v0.29.5) (2025-01-15)


### Bug Fixes

* **sites-management-chart:** handle multiple values of the same term ([7bbb0e4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7bbb0e44d203629003ae8188d9f1a8b7ea9d6e96)), closes [#373](https://gitlab.com/hestia-earth/hestia-ui-components/issues/373)

### [0.29.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.29.3...v0.29.4) (2025-01-15)


### Features

* **node-logs-models:** handle model for hestia/seed_emissions ([3aef011](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3aef0114e7280ca90951fed45661f756c4c480c7))
* **sites-nodes:** add component input to enable the chart ([a48347f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a48347fdda48e9eb130d06fd8a02c3852a03d75e))


### Bug Fixes

* add more overflow to try and fix double popup not visible ([2623c89](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2623c89dae702fc68b7bedee658660e86add2406)), closes [#337](https://gitlab.com/hestia-earth/hestia-ui-components/issues/337)
* **engine-models-stage:** remove duplicated nodes ([b92d4e7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b92d4e73f047a4211897ce0222b5b60c0388fc57))
* **impact-assessments-products:** show recalculation tabs even when no data ([ce73d90](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ce73d901dd821d04472812d32f3534f55a940bb3)), closes [#372](https://gitlab.com/hestia-earth/hestia-ui-components/issues/372)
* **node-logs-models:** filter all terms to list in logs ([642a347](https://gitlab.com/hestia-earth/hestia-ui-components/commit/642a34725cd08c994628ecd269af28218d7a24d7)), closes [#374](https://gitlab.com/hestia-earth/hestia-ui-components/issues/374)
* **node-logs-models:** put typeahead in body to avoid cutoff ([5d9057b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5d9057bbf6525fc4c095dde09515b91229439333)), closes [#365](https://gitlab.com/hestia-earth/hestia-ui-components/issues/365)
* **semver:** handle no version ([73d38d5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/73d38d50d684ee7f3ad273117c6831ac72569e88))
* **stes-management-chart:** handle Management with `endDate` only ([5de0a63](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5de0a63bcb2df8c077633d3b54b6bd3b1b8214e7)), closes [#373](https://gitlab.com/hestia-earth/hestia-ui-components/issues/373)

### [0.29.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.29.2...v0.29.3) (2025-01-07)


### Bug Fixes

* **node-value-details:** add `startDate` and `endDate` as default fields in table ([d4d2adc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d4d2adc1a8cf71c40843c38fadbfe3de0222cb36)), closes [#362](https://gitlab.com/hestia-earth/hestia-ui-components/issues/362)
* **semver-utils:** fix wrong value for `minor` version ([2d4c331](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2d4c331bb445e8b3e8a0fc9333eaceb856868cad))

### [0.29.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.29.1...v0.29.2) (2025-01-03)


### Bug Fixes

* **node-logs-models:** fix missing subvalue keys ([f142f5c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f142f5c45cfce2ab3ff432eff9db0914afbe2a48))
* **site-nodes:** fix wrong label system boundary filter ([a3c9954](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a3c9954470e9fad8c2d70a8c1882f8dc19aa834d))

### [0.29.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.29.0...v0.29.1) (2025-01-02)

## [0.29.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.9...v0.29.0) (2025-01-02)


### ⚠ BREAKING CHANGES

* **engine:** Now requires `@hestia-earth/config`.

### Features

* **files-form:** add external link icon on external website ([94519b6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/94519b65e816c160e0c38e0e78f047dfe75df2b4))


* **engine:** use configuration from `@hestia-earth/config` ([e4d0600](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e4d06003483fbf1cbb70108c6b5d2c0518d0e0ae))

### [0.28.9](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.8...v0.28.9) (2024-12-31)


### Bug Fixes

* **node-logs-models:** skip logging `input` subValue under `Input` logs ([980621a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/980621a638dc7027579d603e6922aa58d039273e))

### [0.28.8](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.7...v0.28.8) (2024-12-24)


### Features

* **common:** add `localStorageSignal` function ([a88f246](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a88f246c62cbe8b50dad40a6785908081841673f))
* **files-upload-errors:** handle mendeley and timeout errors ([ba6f444](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ba6f444f7113e9eabee9f5e2654e27caf9453bba))


### Bug Fixes

* **drawer-container:** remove animation scheduler to set width immediate ([184dc63](https://gitlab.com/hestia-earth/hestia-ui-components/commit/184dc63f5a0b430abe42d411c11fedf896ab1504)), closes [#360](https://gitlab.com/hestia-earth/hestia-ui-components/issues/360)
* **svg-icons:** fix incorrect size unit ([ea3aa22](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ea3aa22615429d2b804833ed7d61cf169418d103))

### [0.28.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.6...v0.28.7) (2024-12-20)


### Features

* **svg-icons:** add svg-icons/styles.sass to ng-package ([314dfa9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/314dfa9674de53e4625695cb4811cf32774e4341))
* **svg-icons:** load icon ([e50a790](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e50a79037de7f21bdc54b7a9a88979c7c4f50986))

### [0.28.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.5...v0.28.6) (2024-12-19)


### Bug Fixes

* **data-table:** fix content not resizing based on parent size ([d243abe](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d243abec7e02326c86cab6807c46c0dc9cfd0331))

### [0.28.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.4...v0.28.5) (2024-12-19)


### Bug Fixes

* **drawer-container:** fix alignment on content screen padding ([785475b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/785475bf97eff02f4467da91fd42d84f31065930))
* **drawer-container:** fix padding outside of scrollbar ([e0972a2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e0972a226d236e235c71f2bbd4c0bd5e41d40875)), closes [#330](https://gitlab.com/hestia-earth/hestia-ui-components/issues/330)

### [0.28.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.3...v0.28.4) (2024-12-18)


### Bug Fixes

* fix double content padding missing offset ([0b97754](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0b97754027f00dda2031a9d9330a123c9befa2dc))

### [0.28.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.2...v0.28.3) (2024-12-18)


### Bug Fixes

* **drawer-container:** handle paddings correctly with 2 drawers ([5898ffb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5898ffbab461da81491a1c56c8eca11862964463))

### [0.28.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.1...v0.28.2) (2024-12-18)


### Features

* **common:** add `he-collapsible-box` component ([5358680](https://gitlab.com/hestia-earth/hestia-ui-components/commit/535868015bdfb84e8fbd0e54b81663217a5c4de2))
* **files-error-summary:** add select specific error ([b2e0b46](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b2e0b465d02edcb4c78eee3c6c03edce1d4eba1a))
* **files-error-summary:** set variable max text length based on available width ([5ea0b8a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5ea0b8a3f1e75a686944f741aed0d459bb224170))
* **files-error-summary:** update table colors ([d190103](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d1901039efd9633f12fcbdbf6ecdd796ba98ad32))
* **files-error:** handle error and warning on `excreta` ([0e6781c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0e6781ca86c42687f2f498b27d0cae8f5ce0e028))

### [0.28.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.28.0...v0.28.1) (2024-12-13)


### Features

* **files-error:** handle error add `pastureGrass` ([25fcad8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/25fcad8afb14faa57fe013d0810966c5d534d8ab)), closes [#354](https://gitlab.com/hestia-earth/hestia-ui-components/issues/354)
* **files-error:** improve error message add animal production system ([f9a64e9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f9a64e95c99b436542645a28cb9de8a31834d6b4)), closes [#355](https://gitlab.com/hestia-earth/hestia-ui-components/issues/355)
* **files:** add `he-files-error-summary` component ([c790543](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c790543518c763cef71527cb0e8b01eb7996b9fa))
* **svg-icons:** add `far-magnifier-v2` icon ([ebe754f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ebe754fae410cee1137f4bdae653a8d676dacfc0))


### Bug Fixes

* **responsive-service:** update breakpoint for desktop ([f7dad55](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f7dad554bf27effcf2767fe5fe57016b5bccd24a))

## [0.28.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.69...v0.28.0) (2024-12-11)


### ⚠ BREAKING CHANGES

* **package:** Now supports Angular 18.

### Features

* **files-error:** handle error max `cycleDuration` ([8b329c9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8b329c96b4eafd2e624aef94ded1adc783d7adee))


* **package:** update to Angular 18 ([205da6f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/205da6fe6e13508ac868126ed519d87e3f57549a))

### [0.27.69](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.68...v0.27.69) (2024-12-08)


### Features

* **common:** add `capitalize` and `uncapitalize` pipes ([8b037c1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8b037c1deaf09423889425cfff04dc345b5e65f0))
* **svg-icons:** add new icons for tools ([d8f0b73](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d8f0b7347a8af412c265786086062e1f8e1bb4a3))

### [0.27.68](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.67...v0.27.68) (2024-12-06)


### Bug Fixes

* **files-error:** fix error reading node not found ([03fd636](https://gitlab.com/hestia-earth/hestia-ui-components/commit/03fd636d57f37e93bfedf3cc9401d41e63828d1b))

### [0.27.67](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.66...v0.27.67) (2024-12-06)


### Features

* **files-error:** handle node not found error ([ce0e53a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ce0e53ae925ab9cf5b077ed8edbe61bedcec7618))
* **files-error:** improve error message allowed termType ([8a61fb0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8a61fb07a56d58c093fa92e597faee0850845620))

### [0.27.66](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.65...v0.27.66) (2024-12-05)


### Features

* **files-error:** handle additional model-mapping errors ([c28f638](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c28f638beea4d330017d20b484adee5396896356))
* **files-error:** improve error message required property ([8109376](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8109376bbf72a7dd652c2afceab7bf623b10043d)), closes [#341](https://gitlab.com/hestia-earth/hestia-ui-components/issues/341)


### Bug Fixes

* **files-error:** fix wrong blank node shown in allowed values error ([1f494eb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1f494eb583779a5a755ca7f5f30b31859ff0cd40)), closes [#349](https://gitlab.com/hestia-earth/hestia-ui-components/issues/349)
* **sites-management-chart:** do not show data without both start and end ([aa327d2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/aa327d2b13599720d9eca70d001102da210e4bbe))
* **terms-property:** fix error name should not be visible from params ([ccc4a82](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ccc4a8267d38100c1a7c0d725baf4071be750e9a))

### [0.27.65](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.64...v0.27.65) (2024-12-04)


### Bug Fixes

* **filter:** set nested `he-select` as block ([e1dc977](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e1dc977552f9b2e8ce8b5eb5af540917d9867e07))
* **node-logs-file:** fix double-loading of logs ([16765c1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/16765c1b65b387f65ebc0e5c5b580ec7f3b58778))

### [0.27.64](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.63...v0.27.64) (2024-12-03)


### Features

* **engine-models-stage:** add popover with calculation details when complete ([58bf948](https://gitlab.com/hestia-earth/hestia-ui-components/commit/58bf9480cd24075f3c7adad072972333e8ee17ed))
* **sites-management-chart:** show termType as label ([9b38ee7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9b38ee71bc6bfbe3535eae32af850a1247a24d59)), closes [#346](https://gitlab.com/hestia-earth/hestia-ui-components/issues/346)

### [0.27.63](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.62...v0.27.63) (2024-11-29)


### Features

* **files-error:** handle error should specify pregnancy rate ([90ad953](https://gitlab.com/hestia-earth/hestia-ui-components/commit/90ad95391553e6b1da814d2db4d48fdfc87e2ced))
* **sites-nodes:** show logs for management nodes ([1a15d91](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1a15d919874d0ee7b71a68ce8373e0b6a6385752))

### [0.27.62](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.61...v0.27.62) (2024-11-27)


### Features

* **node-logs-models:** show subvalues when there is a model and no value ([29542fe](https://gitlab.com/hestia-earth/hestia-ui-components/commit/29542fea93bc81f7a5779e56c8de27e93f871369))

### [0.27.61](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.60...v0.27.61) (2024-11-27)


### Bug Fixes

* **engine:** fix recursion on nested nodes ([89a881d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/89a881db7d7090cf0042131f60864989d743d6e5))

### [0.27.60](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.59...v0.27.60) (2024-11-27)


### Features

* **files-error:** handle error ionisingCompounds waste input ([0f18444](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0f18444019e6c2c58f83123d42b6d39a48af6a0c))


### Bug Fixes

* **engine-models-stage-deep:** find relate IA in animals ([1945500](https://gitlab.com/hestia-earth/hestia-ui-components/commit/19455008e88efca5e32c5834dfe640e628bb2def))

### [0.27.59](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.58...v0.27.59) (2024-11-25)


### Features

* **files-form:** set border color on property error ([4df3988](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4df3988ad18cd3284c41d45f8a0eeeeed0046ab6))


### Bug Fixes

* **repsonsive-service:** fix tablet breakpoint is `768` ([56b4c50](https://gitlab.com/hestia-earth/hestia-ui-components/commit/56b4c506c1827155c77940606bbdcc2b2e81a24f))

### [0.27.58](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.57...v0.27.58) (2024-11-18)


### Bug Fixes

* **engine-models:** add fallback to original if recalculated in stored nodes not found ([3c92442](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3c924422b77da02aa5f4e9f9a15104670aaba4c6))

### [0.27.57](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.56...v0.27.57) (2024-11-18)


### Bug Fixes

* **engine-models:** handle recalculated data not found in stage deep ([c518051](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c5180514f6ff2c64e12ea331a17921459912f2c1))

### [0.27.56](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.55...v0.27.56) (2024-11-18)


### Features

* **engine:** add component to view the stage dependencies ([4fe8b20](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4fe8b20aef682909254923f6389582233d3b339b))

### [0.27.55](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.54...v0.27.55) (2024-11-17)


### Bug Fixes

* **node-store:** fix headers not fetched ([aead311](https://gitlab.com/hestia-earth/hestia-ui-components/commit/aead311f0de6b8824d6dda534cf8ce40b1c8fe25))

### [0.27.54](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.53...v0.27.54) (2024-11-17)


### Features

* **node-store:** fetch and returns `stage` and `maxstage` headers ([6daefc0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6daefc01b7026603b01998d90aa429e8a84595a3))

### [0.27.53](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.52...v0.27.53) (2024-11-16)


### Features

* **drawer-container:** show only when there is content to show ([54654df](https://gitlab.com/hestia-earth/hestia-ui-components/commit/54654df31d22649b5e958591058b7f0f2d40f2d9))


### Bug Fixes

* **svg-icon:** center icon in parent ([e68d9f6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e68d9f6ba9fffb82d62545f969e3837c87c5a452))

### [0.27.52](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.51...v0.27.52) (2024-11-13)


### Bug Fixes

* **files-form:** fix colors on error messages ([a4e5101](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a4e51017a3c8a8b3ca7badf452ae523416409411))

### [0.27.51](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.50...v0.27.51) (2024-11-13)


### Bug Fixes

* **chart:** fix overwrite of default settings ([535c48d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/535c48dec9112ef40e698553ec56ab6e5001a6f5))

### [0.27.50](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.49...v0.27.50) (2024-11-13)


### Bug Fixes

* **bar-chart:** fix data configuration ([a1ed0cb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a1ed0cb806edb4a1a95f0fa5ec4b2e33503386ab))
* **chart-configuration:** destroy chart on destroy ([79bcf13](https://gitlab.com/hestia-earth/hestia-ui-components/commit/79bcf135520570bac1d269038620fc99302d1a23))

### [0.27.49](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.48...v0.27.49) (2024-11-13)


### Features

* **file-form:** update form to new design and add node icons ([ae5af57](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ae5af57493599038cce679b23bea549021754393))

### [0.27.48](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.47...v0.27.48) (2024-11-12)


### Bug Fixes

* **clipboard:** export `clipboard` function ([92596f0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/92596f0511301b76c5b5c23bc6879fa0a2627480))

### [0.27.47](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.46...v0.27.47) (2024-11-12)


### Features

* **node-value-details:** force visible overflow on dropdown ([d83c282](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d83c2823dba9628c6fb13d6ba48fc5a9edc90282)), closes [#337](https://gitlab.com/hestia-earth/hestia-ui-components/issues/337)
* **svg-icons:** add `far-times-circle` icon ([fe34720](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fe347202db6cec5037f3bd713909ee091cae9e4b))


### Bug Fixes

* **files-form-editable:** remove maps feature ([9009ac8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9009ac8aac084356c683d9e0f221e5480bc2c47d))
* **node-link:** remove recalculated state on aggregated non-IA nodes ([7c9ca28](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7c9ca2823dd84f890f3960402a44de422a5c798a))
* **sites-maps:** fix incorrect url gadm data ([5536264](https://gitlab.com/hestia-earth/hestia-ui-components/commit/553626437a2d811b7a678b227276634c4f4a9f4a))

### [0.27.46](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.45...v0.27.46) (2024-11-07)


### Features

* **svg-icon:** add menu-selected icon ([fb1dd43](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fb1dd43397187a18f77bb055fdf4847d29fb59de))

### [0.27.45](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.44...v0.27.45) (2024-11-07)


### Features

* **files-error:** handle missing `productivePhasePermanentCrops` error ([78ad011](https://gitlab.com/hestia-earth/hestia-ui-components/commit/78ad01126c12c691fe13a9eac3d6600127a38a27))
* **navigation-menu:** auto-close non-collapsible top-level siblings ([650dab2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/650dab2f74f447d8df693966d1e35a3c0bc5f8ba))

### [0.27.44](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.43...v0.27.44) (2024-11-05)


### Bug Fixes

* **svg-icons:** fix typo in icon name ([c3b75a3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c3b75a35746843548217d92b32287d3bd147134a))

### [0.27.43](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.42...v0.27.43) (2024-11-05)


### Features

* **svg-icons:** add `clock-rotage-left` ([289ea02](https://gitlab.com/hestia-earth/hestia-ui-components/commit/289ea02a610ca867862d5bcd9ddf66dcb506686c))

### [0.27.42](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.41...v0.27.42) (2024-11-05)


### Bug Fixes

* **drawer-container:** close menu on link selection on mobile ([6765a2a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6765a2aa5bba7e053d752eb47403862a23fe799a))
* **files-error:** fix wrong text on links ([5b7c57e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5b7c57e61320a3a2e603ff6c9f136e5f8da93a54))

### [0.27.41](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.40...v0.27.41) (2024-11-01)


### Features

* **svg-icons:** add calculation-toolkit icon ([088b6e7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/088b6e7ae918e43177b675ad8bc738e1653c315d))
* **svg-icons:** add data-format icon ([6898649](https://gitlab.com/hestia-earth/hestia-ui-components/commit/68986491f6c613da66d3784e62581095a07112af))

### [0.27.40](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.39...v0.27.40) (2024-10-31)


### Features

* **icons:** new icons for data upload page ([99fbc46](https://gitlab.com/hestia-earth/hestia-ui-components/commit/99fbc46bf4a0b6e632232ecfec092011fa2f132a))
* **svg-icons:** add icons ([6621001](https://gitlab.com/hestia-earth/hestia-ui-components/commit/66210014aca258382a7130dedb6b457c2a9250eb))


### Bug Fixes

* **svg-icons:** remove override colors ([947d8d2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/947d8d2664574bc3ee543e564018a76a79d91502))

### [0.27.39](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.38...v0.27.39) (2024-10-29)


### Features

* **drawer-container:** store container size when resizable ([c5c26c0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c5c26c0ac19bb13b384bdac13b068aee97662673))

### [0.27.38](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.37...v0.27.38) (2024-10-28)


### Features

* **files-error:** handle additional validation errors ([a63640b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a63640b4291e6be7e83989a0f769cafd48207b33))


### Bug Fixes

* **drawer-container:** only disable transitions for modal open within the content ([3e43c8c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3e43c8cfb294042accfb2a685d919e880acadc84))
* **navigation-menu:** set font-size to 14px ([c711f45](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c711f4557ed965aecab3a9cecc936f36709deb67))

### [0.27.37](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.36...v0.27.37) (2024-10-25)


### Bug Fixes

* **glossary:** fix url download lookups and migrations on localhost ([3a8e1d4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3a8e1d45ab70c9f761c3bd6b98ac2bd45e3d121d))

### [0.27.36](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.35...v0.27.36) (2024-10-25)


### Bug Fixes

* **files-upload:** fix import ([39b8f1d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/39b8f1db5de2e9267c0ccf1578bad133fef18483))

### [0.27.35](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.34...v0.27.35) (2024-10-25)


### Bug Fixes

* **node-service:** fix get lookup data on localhost ([227e9cc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/227e9cc11e4ad2507dbd25cae35d05375e444008))

### [0.27.34](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.33...v0.27.34) (2024-10-25)


### Features

* **node-service:** add additional observable methods ([595066c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/595066c1c94263304086d488f1b636f2bd3bea9d))

### [0.27.33](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.32...v0.27.33) (2024-10-24)


### Bug Fixes

* **schema:** fix incorrect `schemaBaseUrl` and add `schemaDataBaseUrl` ([d914e18](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d914e1865a165eeae2a2d209288ee15b29b55b9a))

### [0.27.32](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.31...v0.27.32) (2024-10-24)


### Bug Fixes

* **styles:** make sure search form rounded is applied ([1abfbec](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1abfbeca167e9fff23e392de7d9006100273bbb4))

### [0.27.31](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.30...v0.27.31) (2024-10-24)


### Features

* **common:** add `he-maps-drawing` for inline map drawing ([3ce4d1c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3ce4d1c1874a3c31a4dea3955530556fff014f63))
* **styles:** add rounded `search-form` style ([2cf081b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2cf081b0fed64f7287bcb679ded34d263cf53899))


### Bug Fixes

* **common:** keep localhost url for base ([fad46b4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fad46b4afa1928a40b706037e3bd86701464fb18))
* **issue-confirm:** fix report bug url not showing ([75c81d2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/75c81d2b44a9375663bfc29fef25223d6f9f8d09))

### [0.27.30](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.29...v0.27.30) (2024-10-22)


### Features

* **svg-icons:** add more icons ([4393f17](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4393f17e62f08f028283287133447457fa1c8a97))

### [0.27.29](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.28...v0.27.29) (2024-10-21)


### Features

* **drawer-container:** add template to put above menu ([d6ac455](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d6ac4550a8441df21e63a0fc0169d23209873767))

### [0.27.28](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.27...v0.27.28) (2024-10-21)


### Features

* **navigation-menu:** expand sublinks on active ([27067d9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/27067d9ad67d04656aa79298d0e55f3c7dfbfea1)), closes [#331](https://gitlab.com/hestia-earth/hestia-ui-components/issues/331)
* **svg-icons:** add new icons for dashboard ([b8e3cc8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b8e3cc8963bc4abc583c8ff66eca62ef01de0c9a))

### [0.27.27](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.26...v0.27.27) (2024-10-21)


### Bug Fixes

* **common:** add missing `NavigationMenuComponent` ([8c803eb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8c803eb1e39c0b2957ef6b798f2769a3ef3bce15))

### [0.27.26](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.25...v0.27.26) (2024-10-21)


### Features

* **common:** add `fileSize` pipe ([ca3dc4c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ca3dc4c8eb2c1db4f208d93547106135c79e9e64))

### [0.27.25](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.24...v0.27.25) (2024-10-18)


### Features

* **navigation-menu:** handle sublinks without nested links ([7c5da16](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7c5da16d1f5fd83ed4b336cccee11c9cc3440aba))


### Bug Fixes

* **drawer-container:** content height should be scrollable ([26b19df](https://gitlab.com/hestia-earth/hestia-ui-components/commit/26b19df6f5e8a31bca41960a16521df3b41f9fe3))

### [0.27.24](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.23...v0.27.24) (2024-10-18)


### Bug Fixes

* **drawer-container:** export component ([0fbdf4c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0fbdf4c068e7d3e08217e2e93065ef592561fb60))

### [0.27.23](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.22...v0.27.23) (2024-10-18)


### Features

* **common:** add `he-drawer-container` component ([e849d51](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e849d517fe4757504a098d9959435e6a4c32bfc4))

### [0.27.22](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.21...v0.27.22) (2024-10-16)


### Bug Fixes

* expose inputs for modal components ([fdcdec8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fdcdec840875003687e0fabd1a4f0e91b9d9c8a9))

### [0.27.21](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.20...v0.27.21) (2024-10-16)


### Features

* **icons:** add new icons ([3dc78b0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3dc78b0bed5314f207069636906b92a27ed8ea5c))


### Bug Fixes

* **files-error:** fix typo in error requires fate of crop residue ([3c3ce75](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3c3ce75756e15838d0eb1f2acbe3e7b9a07da86b))

### [0.27.20](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.19...v0.27.20) (2024-10-14)


### Features

* **impact-assessments:** list method by name ([5bb4be9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5bb4be97ee1ea9872e3e0f872d8f69bf5b06c0d4)), closes [#329](https://gitlab.com/hestia-earth/hestia-ui-components/issues/329)
* **sites:** add management chart component ([52b5707](https://gitlab.com/hestia-earth/hestia-ui-components/commit/52b5707e227d62860de0044326462008049a0295))


### Bug Fixes

* **chart:** make chart whole parent height ([0753d5b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0753d5b4c448e6073822699ff4d3436965c8b477))

### [0.27.19](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.18...v0.27.19) (2024-10-09)


### Features

* **svg-icons:** add `sort-up` and `sort-down` icons ([d0e533f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d0e533f44bd764ec971f36cc0e47b111ea496e81))


### Bug Fixes

* **impact-assessments:** fix broken tooltip download csv ([b600438](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b600438e3af908a0fedfc4a6756dc3dfc90cb7ac))

### [0.27.18](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.17...v0.27.18) (2024-10-08)


### Features

* **chart:** add chart components ([8b24927](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8b249273d73790042553026a22a1290c7f95e877))
* **files-error:** add units to area error ([c0cf3a6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c0cf3a6f298e934e6e6c9a86bb8509f606f68ae3)), closes [#328](https://gitlab.com/hestia-earth/hestia-ui-components/issues/328)
* **files-upload-errors:** handle invalid excel file error ([84a2ef1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/84a2ef1426027fb2764cd0ab342850a2cd9b2fc5))
* **files-upload-errors:** handle schema not found error without `key` ([6f92ba4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6f92ba495bd1430208806ebc22ac1277ebd20323))
* **node-logs-models:** show subValues without a model ([cd1ca98](https://gitlab.com/hestia-earth/hestia-ui-components/commit/cd1ca988819ae092b922cc28b6075f24e96cda2b))
* **svg-icon:** add `sort` icon ([ad98825](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ad9882505230af1275229a987499604961d10ed9))

### [0.27.17](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.16...v0.27.17) (2024-10-01)


### Features

* **cycles-nodes:** enable logs on `animals` ([1d028c7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1d028c7244c1848cbfa5e5e9e62e4cefb9f04d98))

### [0.27.16](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.15...v0.27.16) (2024-09-17)


### Features

* add termType to compound ([2049df9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2049df96583b24ca53af3bb47c2068965f5fc7fb))
* **files-error:** handle empty ImpactAssessment error ([df386ca](https://gitlab.com/hestia-earth/hestia-ui-components/commit/df386ca66f819369b2ffdb0bb76f4fea24c5752a))
* **files-error:** handle error `croppingDuration` from rice ([30d7ee8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/30d7ee8f37402260b72f0b512bc979e3793630b4))


### Bug Fixes

* **cycles-nodes:** filter non-existing grouped nodes ([5846a86](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5846a869d25903d0de86f36d121c3048d7b033d8))

### [0.27.15](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.14...v0.27.15) (2024-09-05)


### Features

* **compound:** optionally use `TermType` to restrict conversion ([604e5fe](https://gitlab.com/hestia-earth/hestia-ui-components/commit/604e5fe4404ad36a0e120579ca590dd16600df6b))
* **impact-assessments:** group emissions by `methodTier` and order ([5608132](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5608132d5e68bbb1cdc4e4129dd29740a80f55a0))
* **node-logs-models:** improve no logs message ([434ec83](https://gitlab.com/hestia-earth/hestia-ui-components/commit/434ec83e5c7f747abf845183d83617e9a03efd03)), closes [#318](https://gitlab.com/hestia-earth/hestia-ui-components/issues/318)


### Bug Fixes

* **compound:** ignore CML values ([4307145](https://gitlab.com/hestia-earth/hestia-ui-components/commit/43071455eed84cf9ce31bd1cac3e03cb1961c685))

### [0.27.14](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.13...v0.27.14) (2024-08-26)


### Features

* **files-error:** handle error on `longFallowDuration` ([efc7363](https://gitlab.com/hestia-earth/hestia-ui-components/commit/efc7363f33c5e6b2b838c698b1485146f4d2a00e))
* **files-error:** handle error siteDuration on crops ([cdcbda5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/cdcbda537ce41e4842ad866d90708c52167f9ed7))

### [0.27.13](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.12...v0.27.13) (2024-08-15)


### Features

* **files-error:** improve list overlapping cycles ([4425c93](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4425c935a92915c30f6041257e290e21f2064baf)), closes [#298](https://gitlab.com/hestia-earth/hestia-ui-components/issues/298)
* **node-logs-models:** handle input background data ([22eb5ac](https://gitlab.com/hestia-earth/hestia-ui-components/commit/22eb5acb571532187cba793c840e22baabdcaa2b))
* **node-logs-models:** show original method when available ([a147734](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a14773483daa4c8fded3fec6fa8e8f4ab00029e5))
* **node-value-details:** customise table fields ([248defa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/248defafdf90c9fc2b0ed72b5cfd0dcd4009bd9a))


### Bug Fixes

* **blank node state:** remove special case for aggregation ([0e8cfb3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0e8cfb35e64e04d4aa90ba85387143cdb26e9551))
* **node logs models:** hide term links with no value ([b2306e9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b2306e903197e6c56969001a89f55c9ec2e1eebc))

### [0.27.12](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.11...v0.27.12) (2024-08-09)


### Features

* **cycles-nodes:** always show recalculation button in recalculated state ([6dd4647](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6dd46479d6fa36963c0421753f951e71a4d3765f))
* **files-error:** handle error for water salinity ([f8d41a5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f8d41a514a034f61300f6f5cf1f9c3fce0420b7a))
* **node-logs-models:** handle `animal` like `transformation` ([27cbcf8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/27cbcf8498209e4ee3d4e326b28d4496b641cfed))


### Bug Fixes

* **node-logs-models:** restrict extra terms from logs matching possible `termType` ([52b0ea2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/52b0ea24c231f2e750a6c334acf412b944165efc)), closes [#307](https://gitlab.com/hestia-earth/hestia-ui-components/issues/307)
* **shelf-dialog:** fix header padding ([31846da](https://gitlab.com/hestia-earth/hestia-ui-components/commit/31846da20f5b2ad4dd0b41d03e446cb83c9c31f2))

### [0.27.11](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.10...v0.27.11) (2024-07-31)


### Features

* **node-store:** add function to transform the data from API ([d78214d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d78214d832af6de5c3f4036997df8280c60f16a7))

### [0.27.10](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.9...v0.27.10) (2024-07-31)


### Features

* **files-error:** handle error must use substrate ([793c48c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/793c48ce518452a5cef580917b88d4a92e652973))
* **node:** add any extra params when fetching node data ([a7f463a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a7f463a346d0c8101f20da9a679b9314a7e08b2d))
* **node:** support using params when adding node to store ([f5f82af](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f5f82afef51b289d71c2e7e65e3a577fe9b3b5fb))

### [0.27.9](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.8...v0.27.9) (2024-07-23)


### Bug Fixes

* **engine models version:** fix incorrect link ([4f5947c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4f5947ccebd4b3f4dcab539c561f2e36aa077e33)), closes [#306](https://gitlab.com/hestia-earth/hestia-ui-components/issues/306)
* **node-logs-models:** include `units` when searching for terms ([6c04c73](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6c04c73c8d1cadffed106a48a6e410da794a61c8)), closes [#311](https://gitlab.com/hestia-earth/hestia-ui-components/issues/311)
* **node-logs:** fix unknown `bindOnce` directive ([26fdc2f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/26fdc2fccb733edc6fd4bb8761d218308ce76a61))

### [0.27.8](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.7...v0.27.8) (2024-06-19)


### Features

* **files-error:** handle error pond define water type ([066d49d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/066d49d1312d2bc05b678e6b8e48ff45744dbe81))
* **shelf-dialog:** use new icons ([c980fc1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c980fc17169ad34686c52d3c167c8bb8dcb76cd9))

### [0.27.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.6...v0.27.7) (2024-06-04)


### Bug Fixes

* **node-logs-file:** fix error in csv filename ([f79d519](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f79d51966bf1532130375dbca66a5d5604fae582))

### [0.27.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.5...v0.27.6) (2024-05-31)


### Features

* **files-error:** handle error duplicate feed inputs Animal and Cycle ([754578e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/754578e19d05541c404f7be6bcfd117a664de9e3))
* **files-error:** handle error management duration ([f127bfb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f127bfb79b37f279a25910b4efab38944a6d55f5))


### Bug Fixes

* **files-error:** simplify message measurement requires depths ([a5800e0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a5800e08bbc0bb5d501ee49fabe01c6941244d97)), closes [#303](https://gitlab.com/hestia-earth/hestia-ui-components/issues/303)

### [0.27.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.4...v0.27.5) (2024-05-29)


### Features

* **files-upload:** remove handling of limit error ([78e3c06](https://gitlab.com/hestia-earth/hestia-ui-components/commit/78e3c064643b8f830fa88029f7fe301d047508ed))
* **navigation-menu:** handle collapse of primary links with no icon and children ([0bb7b61](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0bb7b618e09fa4ff1f67d862a93a8ad8e1ea20cb))

### [0.27.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.3...v0.27.4) (2024-05-22)


### Features

* **navigation-menu:** reduce size to `14rem` ([6e2bf1a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6e2bf1a355ff71e341bae55b405fe25d4e9f1fb1))

### [0.27.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.2...v0.27.3) (2024-05-22)

### [0.27.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.1...v0.27.2) (2024-05-21)


### Bug Fixes

* **navigation-menu:** do not require `links` ([df5874f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/df5874f709f67d8f63dbeec59980a7d53e4d7e79))

### [0.27.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.27.0...v0.27.1) (2024-05-21)


### Bug Fixes

* **node csv export:** fix infinite loading when no keys provided ([f41366c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f41366cfa48aab8614a77099f7c455d5f55f72e9))

## [0.27.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.26.1...v0.27.0) (2024-05-19)


### ⚠ BREAKING CHANGES

* **files-form:** Use `he-files-form-editable` for non-readonly form.
* All modules have been removed. Please import components instead.

* convert all to standalone components ([433f31f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/433f31f289d6c619cb7e07c2e2d60aac8e9eed1a))
* **files-form:** make readonly and create `files-form-editable` ([18eae5f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/18eae5f2f651ea9da61591f34ca5b74fdc0b602e))

### [0.26.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.26.0...v0.26.1) (2024-05-17)


### Features

* **files-error:** handle error startDate same format as endDate ([ec2338d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ec2338defa2af4df13a86ee294014233ecf42679)), closes [#302](https://gitlab.com/hestia-earth/hestia-ui-components/issues/302)


### Bug Fixes

* **node value:** remove `term.units` in table display ([9a5984b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9a5984b2c84a5214b264b616084662b4861253db)), closes [#300](https://gitlab.com/hestia-earth/hestia-ui-components/issues/300)
* **sites-nodes:** handle measurement values ([5259fb0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5259fb00232d017a8ecfa8ad5142c7687a1994c2))

## [0.26.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.25.6...v0.26.0) (2024-05-15)


### ⚠ BREAKING CHANGES

* **package:** Supports Angular 17.

### Features

* **files-error:** handle error `otherSites` on `1 ha` ([1f0eae8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1f0eae8f61818ce460a1c1859833fbdefc33af28))


### Bug Fixes

* **node-logs-models:** fix fetch of additional terms ([19f9820](https://gitlab.com/hestia-earth/hestia-ui-components/commit/19f9820d2be841eea904daf29ffd44223339f255))


* **package:** update to Angular 17 ([62ddf76](https://gitlab.com/hestia-earth/hestia-ui-components/commit/62ddf7602950d7e6c87f41b39241395bec36d5bb))

### [0.25.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.25.5...v0.25.6) (2024-05-14)


### Bug Fixes

* **cycles-nodes:** fix issue with functionalUnit ([30d1e6c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/30d1e6c60eac7c0591e720c65f9dfd04778df600))

### [0.25.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.25.4...v0.25.5) (2024-05-09)


### Features

* **common:** add `he-shelf-dialog` component ([929e357](https://gitlab.com/hestia-earth/hestia-ui-components/commit/929e357f7f74aef6da1cc4c023d91a95994b77be))
* **navigation-menu:** add override active match options per link ([aa51249](https://gitlab.com/hestia-earth/hestia-ui-components/commit/aa51249a85722b029310ae1a0d6e7c5a38c4a457))


### Bug Fixes

* **node-logs-models:** handle multiple values array treatment ([558d5e5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/558d5e5020becdb905b74999588894765204eb27))

### [0.25.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.25.3...v0.25.4) (2024-05-02)


### Features

* **files-error:** handle error wrong animalProduct from liveAnimal ([13ccf55](https://gitlab.com/hestia-earth/hestia-ui-components/commit/13ccf556466702edebd3215ad1280f2705ac1814))
* **files-error:** handle errors tillage and LUM values ([9553a00](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9553a0043ddc9069f4726f54bcfcada2668edd37))


### Bug Fixes

* **glossary:** set `http` client as protected for extend ([ece640b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ece640b059d875df993602869b78c8f737bacd0e))

### [0.25.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.25.2...v0.25.3) (2024-04-29)


### Bug Fixes

* **files-error:** fix wrong error key ([ca8b595](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ca8b59567a7916c3b73e0c8e83aa5a87acdcc06a))

### [0.25.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.25.1...v0.25.2) (2024-04-29)


### Features

* **files-error:** handle error cycles linked by ia on same site ([37bebe3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/37bebe3d0da3cd9b0a095a24af1d28e2b0bf66dd))
* **files-error:** handle error overlapping cycles on site ([993fcfd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/993fcfd5762031edb3139078f9f8b7b6d58f3ad4))
* **files-error:** improve clarity on machinery error message ([2f880be](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2f880be48d857b71c0da43d9d23e1dcbbc393787))

### [0.25.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.25.0...v0.25.1) (2024-04-12)

## [0.25.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.3...v0.25.0) (2024-04-12)


### ⚠ BREAKING CHANGES

* **sites:** Use `he-sites-nodes` instead of `he-sites-measurements`.

### Features

* **files-error:** handle error restriction on `term.termType` ([869be16](https://gitlab.com/hestia-earth/hestia-ui-components/commit/869be16e15a54eb5fd9723436fb7e7ab7dc5479b))
* **node-logs-models:** show warning version outdated ([061d9e2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/061d9e26f3fd97e5b1890db3e4ca07a644ac867b))
* **sites:** handle management nodes ([debf541](https://gitlab.com/hestia-earth/hestia-ui-components/commit/debf541db8683695e7487fe0af45a4d0652c0028))

### [0.24.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.2...v0.24.3) (2024-04-11)


### Features

* **common:** add `changelogUrl` function ([ead2c4c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ead2c4ca7d95332e9ceb98d965aa4e6b9ca80506))


### Bug Fixes

* handle raw response from gitlab ([0a1f24f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0a1f24fb3aeca01abb8d6d627d11f0b82db50f36))

### [0.24.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.1...v0.24.2) (2024-04-11)


### Bug Fixes

* **impact-assessments-products:** fix filter by method/model ([406d596](https://gitlab.com/hestia-earth/hestia-ui-components/commit/406d5969cfada431c9e717418d32238654c81624))

### [0.24.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0...v0.24.1) (2024-04-05)


### Bug Fixes

* **sites-maps:** do not load sites data before map ready ([efebf63](https://gitlab.com/hestia-earth/hestia-ui-components/commit/efebf6319db854658825dd36057a8ea564f95661))

## [0.24.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0-8...v0.24.0) (2024-04-05)


### Bug Fixes

* **cycles-nodes:** hide tabs if only showing 1 ([9135350](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9135350372ad0faf4139c726d981127584d78be5))

## [0.24.0-8](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0-7...v0.24.0-8) (2024-04-05)


### Bug Fixes

* **node-store:** handle no original value on aggregated Cycle and Site ([75f2382](https://gitlab.com/hestia-earth/hestia-ui-components/commit/75f2382c1e4e80bf02ef69c36269b4c794b402c9))

## [0.24.0-7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0-6...v0.24.0-7) (2024-04-05)


### Bug Fixes

* **node-store:** handle error node state not found ([68c9e93](https://gitlab.com/hestia-earth/hestia-ui-components/commit/68c9e936fe449c78a7032217257fb489ef2c0c32))

## [0.24.0-6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0-5...v0.24.0-6) (2024-04-05)

## [0.24.0-5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0-4...v0.24.0-5) (2024-04-05)


### Bug Fixes

* **sites-maps:** avoid adding the same markers and polygons multiple times ([2de800e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2de800e23457a242a7a409ba9c2a31f893857608))

## [0.24.0-4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0-3...v0.24.0-4) (2024-04-04)


### Features

* **node-store:** add `sortedNodes$` to return sorted data ([7275bc7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7275bc7ba2fac7b0cfadba79cede6456d6436004))

## [0.24.0-3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0-2...v0.24.0-3) (2024-04-04)


### Features

* **commont:** export from node-utils ([7c3cba1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7c3cba145e9470f715bbcf683ca43048b1ecdfea))

## [0.24.0-2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0-1...v0.24.0-2) (2024-04-04)

## [0.24.0-1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.24.0-0...v0.24.0-1) (2024-04-03)

## [0.24.0-0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.23...v0.24.0-0) (2024-04-03)


### ⚠ BREAKING CHANGES

* Use `he-cycles-nodes` instead of `he-cycles-activity`.
* Use `he-cycles-nodes` instead of `he-cycles-animals`.
* Use `he-cycles-nodes` instead of `he-cycles-emissions`.
* Use `he-cycles-nodes` instead of `he-cycles-practices`.
* Use `[nodeKey]` instead of `[key]` in `he-impact-assessments-products`.
* Load nodes with `HeNodeStoreService` to share among components

### Features

* **cycles-nodes:** handle group by `transformations` or `animals` ([526b887](https://gitlab.com/hestia-earth/hestia-ui-components/commit/526b887245ca97098f4cf43726d0500aa9c3c630))
* **node-store:** automatically download relevant states per node Type ([6d31e7f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6d31e7f34bc0f8ddce00d487e5fe9308141edd5e))


* combine components into a single one with different keys ([0128771](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0128771c994419602f7a3e8cd2255861be74aada))
* use signals in components to avoid refresh ([6e96082](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6e960828fa9f5f409360f4e2db905501d81898a4))

### [0.23.23](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.22...v0.23.23) (2024-03-27)


### Features

* **navigation-menu:** reduce padding further on sub-items ([6a95b2d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6a95b2d77fac8971a8b32c065e99738f42cb7bf4))

### [0.23.22](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.21...v0.23.22) (2024-03-26)


### Features

* **files-error:** handle error no IA linked to Product ([ae2573d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ae2573df622a90456eb41ee07257e9a05121a388))
* **files-error:** handle sum terms 100% ([fecab39](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fecab3964eeeeb0362b2bd75b64f450e327ef4cd))
* **svg-icons:** add addiotional icons ([9889e36](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9889e369b939e508522af62ed6d07df828d304a3))


### Bug Fixes

* **maps drawing confirm:** wait for google maps loaded ([7ae0509](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7ae0509147f40ab5c348d04cd9c20a369b643fde))
* **navigation-menu:** fix auto-collapse elements on tablet only ([2d7c8fc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2d7c8fc323a6e17bf822e34f03e79413f3e16640)), closes [#287](https://gitlab.com/hestia-earth/hestia-ui-components/issues/287)
* **navigation-menu:** fix spacing on submenu even when hidden ([a057b1e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a057b1e9f2887d6febc01af47738cc1335117b0e))
* **navigation-menu:** update size and alignment of icons ([35ebb28](https://gitlab.com/hestia-earth/hestia-ui-components/commit/35ebb2871150e8669bff6d938fb5aa8e9fa08094))

### [0.23.21](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.20...v0.23.21) (2024-03-25)


### Bug Fixes

* **navigation-menu:** fix inconsistency in padding on labels ([cf91b3f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/cf91b3fde6ef5380414808c59110bf8514e2dc0d))
* **navigation-menu:** fix open submenu on tablet refresh page ([ee63b28](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ee63b289682ade32d42f240ec3a0f276a8adf635))
* **navigation-menu:** reduce menu width to `248px` ([1241ea2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1241ea2d8c51f0519eddd183cc5eab50fdbd0040))

### [0.23.20](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.19...v0.23.20) (2024-03-25)


### Features

* **files-error:** handle error measurement missing depths ([2bd61f4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2bd61f4b72818e717dfb50dbd380f7855c88751f))
* **navigation-menu:** change padding to give more space to subitems text ([8bca7d7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8bca7d76860eba9edee51436669da98721866d76))


### Bug Fixes

* **navigation-menu:** fix alignment of text and icons ([27a508e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/27a508e06fc49fbaaa031df7d84056aaba8f2028))

### [0.23.19](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.18...v0.23.19) (2024-03-21)


### Bug Fixes

* **navigation-menu:** enable `fragment` match link active ([46a8093](https://gitlab.com/hestia-earth/hestia-ui-components/commit/46a8093c42aa89770a229e5f391db8359c9c91fe))

### [0.23.18](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.17...v0.23.18) (2024-03-21)


### Features

* **files-error:** handle error sum practices 100 percent ([c75dc64](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c75dc641b57c849f018dd1fcaca065e652d46f00))
* **navigation-menu:** add `is-small` style ([08b67a4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/08b67a44711253a024c550fcbda150d4ed814cea))
* **navigation-menu:** handle link title as html ([24077e5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/24077e55c3a9a4c33c54048d5711b7804ed79b79))


### Bug Fixes

* **navigation-menu:** handle queryParams and fragment on level 1 links ([a9725ac](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a9725ac12a0bbea01fc8d87005b182ec9adea52b))
* **navigation-menu:** removing padding around card-header-icon button ([96e3e6a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/96e3e6a0393f5b2cb69e81f18bf2c1b718527b65))
* **navigation-menu:** set svg icon color to blue ([3a4a36f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3a4a36fe8e3e10d23f2f1526e8ffd233725a1286))
* **navigation-menu:** update card padding on tablet ([e2abb1f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e2abb1f0c6d9618549ec1f54664b699921717b65))

### [0.23.17](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.16...v0.23.17) (2024-03-19)


### Bug Fixes

* **navigation-menu:** fix selected link on tablet and reduce list padding ([bff2e98](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bff2e9800944af699ec4aba6b816866455abcaf9))
* **navigation-menu:** open parent group on secondary link active change ([3e62998](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3e62998a094416d4e9c8ea876c950177076682a0))

### [0.23.16](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.15...v0.23.16) (2024-03-14)


### Features

* **common:** add `ResponsiveService` ([3db6397](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3db63974dcf590ff860c470efc02267775bb454b))
* **files-error:** update freshForage error message ([a7db940](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a7db94017fbec3dc6f57eb8e27267ff188ce7751))
* **navigation-menu:** update style and auto-generate links ([bfedf9a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bfedf9a2a0823b2c81d59a9d38c9d861134ee088))
* **node logs:** handle completeness keys as links ([c0a81aa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c0a81aa651b4e9cf79dee5ac8b78d05e82039d5e)), closes [#274](https://gitlab.com/hestia-earth/hestia-ui-components/issues/274)


### Bug Fixes

* **node logs models:** remove match `termType` from schema on `emissionsResourceUse` ([e4b17fa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e4b17fa6af7e514e5b400ddc99fcbccf384d8ad8)), closes [#273](https://gitlab.com/hestia-earth/hestia-ui-components/issues/273)
* **svg-icons:** use relative path to preload from assets ([8fd114a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8fd114a4cbd6feaf5d178b89937d9f8459da97bc))

### [0.23.15](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.14...v0.23.15) (2024-03-10)


### Features

* **common:** add svg-icons module ([e80f061](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e80f06152ef097bce65691d8ca81f73702ef8b7a))
* **maps:** add global google maps loader ([7ebf6dc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7ebf6dc44e31161ddae8236ce319eb2e32d409b9))

### [0.23.14](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.13...v0.23.14) (2024-03-07)


### Features

* **node logs model:** display logs for `liveAnimal` model ([e6981ea](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e6981ea45f6fd9a720c1f1c3430108650c5ea3a0))
* **node logs models:** improve logs `not relevant` ([f66681a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f66681ad77061b75a43275f6eb655d0c3a75c560))
* **svg-icons:** copy from webapp ([01287a3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/01287a3ca19f896808baf7e8bc30f84daa9c2f81))

### [0.23.13](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.12...v0.23.13) (2024-03-04)


### Bug Fixes

* **select:** close panel on outside click ([32aa9cd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/32aa9cd32ba86a73d57cd07158005cc23a8afcc1))
* **select:** select panel width ([ca6eb3e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ca6eb3e8195c3ab220eeedafc479f02c7c01a428))

### [0.23.12](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.11...v0.23.12) (2024-02-29)


### Bug Fixes

* **engine models:** fix parse stage and maxstage as integers ([cc7576e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/cc7576e3103986ba4ff25c43fb644de24727dc43))

### [0.23.11](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.10...v0.23.11) (2024-02-29)


### Bug Fixes

* **engine models:** fix wrong max stage for Cycle ([7392796](https://gitlab.com/hestia-earth/hestia-ui-components/commit/739279692a5e62ffe3387267201725cb51ae165c))

### [0.23.10](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.9...v0.23.10) (2024-02-29)


### Features

* **engine:** add `he-engine-models-stage` component ([250374a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/250374af6ac87c34ff91dee380a52f8bd8f1bcba))

### [0.23.9](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.8...v0.23.9) (2024-02-28)


### Features

* **node logs model:** display custom no data message for `emissionsResouceUse` ([cb438c3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/cb438c38e1b5e1e23defca9b4a1361357b7d04dd)), closes [#223](https://gitlab.com/hestia-earth/hestia-ui-components/issues/223)


### Bug Fixes

* **select:** remove pointer event from backdrop ([7b2256a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7b2256a49a721b7b2afbbe061dbc4aad6590c858))

### [0.23.8](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.7...v0.23.8) (2024-02-27)


### Features

* **files-error:** improve error message linked product ([3025bdb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3025bdb4f52281e8fab65fad2af219945252eabf)), closes [#222](https://gitlab.com/hestia-earth/hestia-ui-components/issues/222)
* **node value details:** include `properties` in table view ([9c8dec8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9c8dec82f468621b717a753596e525778fbfb880)), closes [#228](https://gitlab.com/hestia-earth/hestia-ui-components/issues/228)

### [0.23.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.6...v0.23.7) (2024-02-20)


### Features

* **files-upload-errors:** improve error message duplicated ids ([a376c7c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a376c7c3429f64ce2afd28b62ef707a40cdc8324))


### Bug Fixes

* fix wrong getter to show "switch to recalculated" ([515bb8c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/515bb8c8f3c0048a2608a2e20b96d441bb4dab0a)), closes [#257](https://gitlab.com/hestia-earth/hestia-ui-components/issues/257)

### [0.23.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.5...v0.23.6) (2024-02-13)


### Features

* **select:** expand select component ([fb5f217](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fb5f217a67ce2e93c7c92cc75236a4075e309b7c))

### [0.23.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.4...v0.23.5) (2024-02-13)


### Features

* **cycle practices logs:** remove restriction system boundary ([0d3f201](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0d3f2010117b8e6d8e07ae4d220958a30c297ce7))
* **files-error:** improve error message for `documentDOI` ([6eac0ed](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6eac0edb8f422f07c7aa43504a0607d2ee7a1a7a))
* **node logs models:** improve log key parsing ([715a59f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/715a59ff59a966079b895d3b5fa479be02f5a254))


### Bug Fixes

* **node logs models:** handle `completeness` models ([fc185f1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fc185f10f2af66684c9443cc91ffe2a58b281d87))

### [0.23.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.3...v0.23.4) (2024-02-02)


### Features

* do not show "switch to recalculated" for aggregated Cycle and Site ([a00f289](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a00f28973d492a7d5289490b4b51eccca20f0566)), closes [#257](https://gitlab.com/hestia-earth/hestia-ui-components/issues/257)
* **files-error:** handle error `completeness.ingredient` ([0e9d41d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0e9d41d99bc261f2575e4a97654ebb35bf4ed436))
* **files-error:** handle error `fromCycle` used in `Cycle` ([d78b845](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d78b84521c3baac43f5e75c820e0520fb98757c3))
* **link key value:** show min max on blank node if present ([68dbc15](https://gitlab.com/hestia-earth/hestia-ui-components/commit/68dbc15c5e84e35200ed9b3896b89f3d6d3c9ccd)), closes [#232](https://gitlab.com/hestia-earth/hestia-ui-components/issues/232)
* **node details:** add `impactAssessment` to table display ([06fbced](https://gitlab.com/hestia-earth/hestia-ui-components/commit/06fbced33c8166fda700115133829a94843984f9)), closes [#239](https://gitlab.com/hestia-earth/hestia-ui-components/issues/239)
* **node value details:** show `fate` and `isAnimalFeed` ([558fe60](https://gitlab.com/hestia-earth/hestia-ui-components/commit/558fe60f321474b36af5c537994fbc779f94ceab)), closes [#258](https://gitlab.com/hestia-earth/hestia-ui-components/issues/258)


### Bug Fixes

* **node logs:** remove entries with no details ([71c692c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/71c692c5234585cceaa831c74c690950b8d56090)), closes [#234](https://gitlab.com/hestia-earth/hestia-ui-components/issues/234)

### [0.23.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.2...v0.23.3) (2024-01-23)


### Features

* **node logs:** improve parsing of completeness logs ([b5c7be4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b5c7be4f6255fe5626e48ae3f3c9012a6f8fc4ff))
* **node logs:** show completeness message for `seed` ([be4ca63](https://gitlab.com/hestia-earth/hestia-ui-components/commit/be4ca63096613ee4499914de0984c09929a60e91)), closes [#225](https://gitlab.com/hestia-earth/hestia-ui-components/issues/225)
* **node value details:** show `key` field ([9da177d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9da177dbbc021b0b21bf3f1f787880cac037e165)), closes [#224](https://gitlab.com/hestia-earth/hestia-ui-components/issues/224)


### Bug Fixes

* **files-error:** clarify definition for excreta transformation error ([7ed0c1e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7ed0c1e118958cf66ed19259131b799f73639fc6))
* **files-error:** update error message grazed forage inputs ([130839d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/130839d99f4f7b3b156071164672964f88b7dd52))

### [0.23.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.1...v0.23.2) (2024-01-09)


### Features

* **node logs models:** auto expand and select filtered row ([d8a9aec](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d8a9aec62606b5a9428f76241cc89e320df6f4cd)), closes [#220](https://gitlab.com/hestia-earth/hestia-ui-components/issues/220)


### Bug Fixes

* **files-error:** update error pastureGrass key ([199ee52](https://gitlab.com/hestia-earth/hestia-ui-components/commit/199ee5226da02767dd380cc10cdd5ff3b2e7cb05))
* make sure view switches back to table on state change ([93fff0e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/93fff0eabdbf9a4a9f632ba097b782415ba54e5d))

### [0.23.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.23.0...v0.23.1) (2023-12-21)


### Features

* **engine-models-version:** add `showDetails` parameter ([a478d16](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a478d16e2b9bab7cccabd68a5a0d4c707ee046bf))
* **files-error:** update error messages ([1694eb0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1694eb0cd6d405b6932fe59718c500cdbca0be34))

## [0.23.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.22.2...v0.23.0) (2023-12-18)


### ⚠ BREAKING CHANGES

* **package:** Minimum schema version is now `25.0.0.`

### Features

* **files-error:** add code block to default error messages ([0723d8d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0723d8dd5872f8732ea64a577d21e86aff069dd8)), closes [#215](https://gitlab.com/hestia-earth/hestia-ui-components/issues/215)
* **files-error:** improve error on GeoJSON invalid coordinates ([a1b1ad1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a1b1ad15361fa47736229a6c18b5c70868377ffb))
* **node:** display `model` in logs detail ([9232f87](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9232f87664a0bf1e2e297d540b7bfc395a64452b))


### Bug Fixes

* **files-error:** remove capitalization on path keys ([923bfcc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/923bfcc06946ddabb51e847664eeaeead5ff2210)), closes [#214](https://gitlab.com/hestia-earth/hestia-ui-components/issues/214)


* **package:** update schema to `25.0.0` ([483b11f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/483b11f4afe51d70cfcf1371f39b8ffd050c24ac))

### [0.22.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.22.1...v0.22.2) (2023-12-06)


### Features

* **schema:** update url to use `schema-data` path ([ede3f47](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ede3f47da64de95555ebceed9357840fa9c9767d))

### [0.22.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.22.0...v0.22.1) (2023-12-06)


### Bug Fixes

* **files form:** fix GeoJSON parsing ([268a277](https://gitlab.com/hestia-earth/hestia-ui-components/commit/268a277fb1854c6429c4834ce1bd04024f9542de)), closes [#219](https://gitlab.com/hestia-earth/hestia-ui-components/issues/219)
* **tags input:** handle incorrect container ([ee2af5a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ee2af5a6e82dbce082a29ea307c65d02602424f6))

## [0.22.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.21...v0.22.0) (2023-11-27)


### ⚠ BREAKING CHANGES

* **package:** Min schema version is `24.3.0`

### Features

* **files error:** improve error message duplicated nodes ([081c4ec](https://gitlab.com/hestia-earth/hestia-ui-components/commit/081c4ec2ad1df9b6ca056c76cfba026da211db39)), closes [#217](https://gitlab.com/hestia-earth/hestia-ui-components/issues/217)
* **files form:** ignore `_cache` key ([e7846b0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e7846b02aacf669c13606b7f8deb0bab760737d3))
* **files-error:** handle error stocking density on permanent pasture ([e77513b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e77513b5d951e5b7e83ab1fc5446878b1b34651b))
* **node logs models:** ignore commands and space filter by term ([4fe9080](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4fe908006404c68f1b9983bed9200de6ffc65b8e))


### Bug Fixes

* **node logs models:** sum up subvalues ([15b1321](https://gitlab.com/hestia-earth/hestia-ui-components/commit/15b1321a5d3f6460beb0bff7eea80f3a204497f2)), closes [#213](https://gitlab.com/hestia-earth/hestia-ui-components/issues/213)


* **package:** require schema min `24.3.0` ([1f2cccc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1f2cccc34e023791d7c5903f11c9442111cf84b1))

### [0.21.21](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.20...v0.21.21) (2023-11-21)


### Features

* **files error:** add duplicated blank nodes indexes ([bc51525](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bc51525d5419d9cb6aeae267671af28f8bb88546))
* **node logs models:** add indicator group open ([74082ce](https://gitlab.com/hestia-earth/hestia-ui-components/commit/74082ce338aac59cb848d21753cded0125c1c074)), closes [#211](https://gitlab.com/hestia-earth/hestia-ui-components/issues/211)


### Bug Fixes

* **node logs models:** add missing space in headers ([1da008a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1da008ad47ac10969cabdbfddacc9a1835beb5bc)), closes [#210](https://gitlab.com/hestia-earth/hestia-ui-components/issues/210)

### [0.21.20](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.19...v0.21.20) (2023-10-27)


### Features

* **terms unit description:** add option to override icon ([f4deb3f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f4deb3f7868d2e7f1e917cf43fdf8058a5f00d9a))


### Bug Fixes

* **common:** match shel behaviours ([39b7394](https://gitlab.com/hestia-earth/hestia-ui-components/commit/39b7394c745e7f747ca1dcc06e2213b1850c94d4))

### [0.21.19](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.18...v0.21.19) (2023-10-17)


### Features

* **files-error:** handle error invalid GeoJSON ([88fa989](https://gitlab.com/hestia-earth/hestia-ui-components/commit/88fa989c027bc9e3350eeb213804d3e2b325b185))


### Bug Fixes

* **common:** fix shell bug with active icon and filter ellipsis ([848f6e2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/848f6e2e80ed99346603e488fac0be5e4ceb3cd7))
* **files error:** fix typo in error message ([c80d698](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c80d69865de7d6f1b451a6e2d147ef9d9edcba90)), closes [#208](https://gitlab.com/hestia-earth/hestia-ui-components/issues/208)

### [0.21.18](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.17...v0.21.18) (2023-10-11)


### Features

* **select:** add line to separate selected options ([8610654](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8610654bfedf106b9a210b84239444389b93b9de))
* **shell:** add option to use custom icon ([7ed17a8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7ed17a858235726eaee0539d2a8e1ff9b9e4f661))

### [0.21.17](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.16...v0.21.17) (2023-10-10)


### Bug Fixes

* **common:** remove unused `bar-chart` ([fd014cd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fd014cde4193def31a9c94b0542a461fdf8546e8))

### [0.21.16](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.15...v0.21.16) (2023-10-10)


### Features

* **common:** add `he-dropdown` component ([073bcc5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/073bcc5e689addb448f10dd001fac6f5e1b2ff74))


### Bug Fixes

* **chart-configuration:** handle `chart` not set ([c256df2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c256df2d6976b5a6a1fd94360bc5bcbb2ccdd4c5))

### [0.21.15](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.14...v0.21.15) (2023-10-09)


### Features

* **common:** add `thousandSuff` pipe ([f26ef78](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f26ef7884f4e578b050c2982eb8134cca51e41e1))
* **common:** add bar chart configuration ([5e8227e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5e8227e2f52869597d11182bdd59189d150f11b9))
* **files-error:** handle error property on wrong `termType` ([37a61ce](https://gitlab.com/hestia-earth/hestia-ui-components/commit/37a61cedec029d5020cb4ee252c697d1dd56d2ee)), closes [#205](https://gitlab.com/hestia-earth/hestia-ui-components/issues/205)
* **files-error:** handle no `Management` node error ([1d0a7d8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1d0a7d872b2abc34aedd21b563a21c9002393a60))


### Bug Fixes

* **link-key-value:** fix display of multiple values in column ([b71e0ba](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b71e0bad678b65c0a3b51dd0b32d64bb746320eb)), closes [#204](https://gitlab.com/hestia-earth/hestia-ui-components/issues/204)
* **mobile-shell:** fix open menu ([fb8307c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fb8307cbfb3a9861839d40ade0c8cc383e946bdb))
* **node logs models:** fix search all blank node terms ([49601e3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/49601e35a4029934589d23b5bb54dbef145b4289))
* **select-option:** fix error in build ([f2c2c46](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f2c2c46d2a9852397a9b43098877d02a61357aa4)), closes [#201](https://gitlab.com/hestia-earth/hestia-ui-components/issues/201)

### [0.21.14](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.13...v0.21.14) (2023-09-29)


### Features

* **terms:** show units in `property-content` ([9be791f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9be791f8a9cfc16ad66ca1f719060e80b071c012)), closes [#199](https://gitlab.com/hestia-earth/hestia-ui-components/issues/199)


### Bug Fixes

* **node logs models:** handle system boundary for emission from lookup only ([3f367d4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3f367d411ae8a0170d2d365483a20850d3b06207))

### [0.21.13](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.12...v0.21.13) (2023-09-25)


### Features

* **files error:** handle areaPercent error on Practice ([f7240af](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f7240afedd22b7c2d6cbcc26d534aa679ded051a)), closes [#196](https://gitlab.com/hestia-earth/hestia-ui-components/issues/196)
* **files error:** improve parsing of error dataPath ([e4dce79](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e4dce79204aa03048fe4430eccbf2259616ed772)), closes [#197](https://gitlab.com/hestia-earth/hestia-ui-components/issues/197)


### Bug Fixes

* **link key value:** only format numbers with `precision` ([72c8dec](https://gitlab.com/hestia-earth/hestia-ui-components/commit/72c8dec102d2962dcca2b3bfbeccba08bab46c53)), closes [#198](https://gitlab.com/hestia-earth/hestia-ui-components/issues/198)

### [0.21.12](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.11...v0.21.12) (2023-09-21)


### Bug Fixes

* **select:** add missing exports ([22d7f37](https://gitlab.com/hestia-earth/hestia-ui-components/commit/22d7f37b0067527f63a8a7a86e48f5eff1d7b872))

### [0.21.11](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.10...v0.21.11) (2023-09-21)


### Bug Fixes

* **common:** remove exported filters ([eddb828](https://gitlab.com/hestia-earth/hestia-ui-components/commit/eddb8280a9a125b18baa9fee8fe7b1f1897d2d25))

### [0.21.10](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.9...v0.21.10) (2023-09-21)

### [0.21.9](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.8...v0.21.9) (2023-09-21)


### Bug Fixes

* **common:** add missing export ([9ca4cc2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9ca4cc28a44854acfb90eb088d9b06cfce7edae3))

### [0.21.8](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.7...v0.21.8) (2023-09-21)


### Features

* **common:** add bar-chart component ([96772d7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/96772d7e9c6f131187ee4faa7471b5a6734971d0))
* **common:** add resized directive ([b2ab196](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b2ab1967326c2ecd46f981716aa8cef42f10caab))
* **common:** add select with filters component ([187ded9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/187ded95ba64acff76d4d10ad7e1204451bb3a78))
* **common:** add shell and mobile-shell components ([5df516a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5df516aba3e8bd50b9250e8ae5d276d9102c8c2f))
* **common:** add sort-select component ([01a2a1d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/01a2a1d92467fea7ad3a555c31ffe02e3f088946))


### Bug Fixes

* hide search bar on non-table views ([9c16fb4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9c16fb418694c0f490ed382b1292611f13638885))

### [0.21.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.6...v0.21.7) (2023-09-12)


### Features

* **files error:** handle error unicity on nodes ([a2b174a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a2b174a0f6f3c51f3e180d4f6fa207952c4216c2))


### Bug Fixes

* **node logs models:** restrict extra terms by `termType` ([14cee0c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/14cee0c168ee045a1b726a65b901024f4f083bfe))

### [0.21.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.5...v0.21.6) (2023-09-04)


### Features

* **files upload errors:** handle error `duplicated-id-fields` ([8962183](https://gitlab.com/hestia-earth/hestia-ui-components/commit/896218306df682703f16126a6b9c01f6f2dfd6cc))
* improve message no data in `original` view ([16791fc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/16791fc30e9451326407129868c676bb3f3948ff)), closes [#192](https://gitlab.com/hestia-earth/hestia-ui-components/issues/192)


### Bug Fixes

* **maps drawing:** fix clear button not working ([8293c70](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8293c703cae552009d8d505858bc439195576a37)), closes [#193](https://gitlab.com/hestia-earth/hestia-ui-components/issues/193)

### [0.21.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.4...v0.21.5) (2023-08-18)


### Features

* add extendable search bar on tables ([c0d8dc3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c0d8dc3fb7110ca58d5bcc720f8679bde2d90df7))
* **files form:** use schema-info instead of tooltip ([2f902e0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2f902e0e1e37efa35ea6d101c8a7ee8b302a7419))


### Bug Fixes

* **compound:** fix `Oxygen` subscripting ([e0aaeb5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e0aaeb5554513d9d2c22dd29e263cb9ca52f0111))

### [0.21.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.3...v0.21.4) (2023-08-14)


### Features

* **node logs models:** hide emissions not in system boundary ([af4f426](https://gitlab.com/hestia-earth/hestia-ui-components/commit/af4f426445923c3d2b12a6904b82802a36f1e490)), closes [#190](https://gitlab.com/hestia-earth/hestia-ui-components/issues/190)


### Bug Fixes

* **files form:** handle schema type not in property types ([daa8c84](https://gitlab.com/hestia-earth/hestia-ui-components/commit/daa8c84178cdda17fd635b253d4739401469e6f5))

### [0.21.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.2...v0.21.3) (2023-08-09)


### Bug Fixes

* **common:** add missing component and fix import ([6726959](https://gitlab.com/hestia-earth/hestia-ui-components/commit/672695975e22924a642c03a843d37f1ce43989de))

### [0.21.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.1...v0.21.2) (2023-08-09)


### Features

* **files error:** handle error add management on `termType` ([ae49731](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ae49731003377080fee9b507e133fa99d30ad12c))
* **schema:** add `he-schema-info` component ([a97b58c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a97b58c866fdb61337b65dd099d00956781bc711))


### Bug Fixes

* **compound:** ignore compounds when text contains hyperlinks ([8ed3d26](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8ed3d26ef9e69e7cb839ca886ba4cc8b572e0688))

### [0.21.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.21.0...v0.21.1) (2023-08-04)


### Features

* add tooltip to describe Term `units` ([d1d29eb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d1d29eb115f097b898215ec745770ccd5a18c98b))
* **engine orchestrator:** add more filtering options ([4bd5a3f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4bd5a3fc71959dc9d37138a9f0a137a3b9352f35)), closes [#157](https://gitlab.com/hestia-earth/hestia-ui-components/issues/157)
* **engine orchestrator:** improve matching with terms ([71cf113](https://gitlab.com/hestia-earth/hestia-ui-components/commit/71cf1134390b64cbd14e09d69c86cbd190681397))

## [0.21.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.20.3...v0.21.0) (2023-08-03)


### ⚠ BREAKING CHANGES

* **package:** Compatible with version 16 of Angular

* **package:** update to angular 16 ([ce73dcb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ce73dcbda42873199de188167761def28e266240))

### [0.20.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.20.2...v0.20.3) (2023-08-02)


### Features

* **node logs models:** handle complete/incomplete log keys ([d9e1771](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d9e17716703d44d2f8709662194a5bd888cca336))


### Bug Fixes

* **files error:** handle single error in multiple errors message ([070852a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/070852a784dba709d572ef354a5e82dd29159e4d))

### [0.20.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.20.1...v0.20.2) (2023-07-31)

### [0.20.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.20.0...v0.20.1) (2023-07-31)

## [0.20.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.10...v0.20.0) (2023-07-31)


### ⚠ BREAKING CHANGES

* **package:** Compatible with version 15 of Angular

### Bug Fixes

* fix map search url ([c78ec1a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c78ec1a44fae7da60a041272bc1e1fcf38eb8b47)), closes [#183](https://gitlab.com/hestia-earth/hestia-ui-components/issues/183)
* improve support of mobile device ([3478cd1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3478cd120aa64fa218ac9910b058cf9bdbeee5f6))
* **node logs model:** revert display not in System Boundary when no value ([953048a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/953048ad83971a6064e771185242605b83405918)), closes [#155](https://gitlab.com/hestia-earth/hestia-ui-components/issues/155)


* **package:** update to angular 15 ([48d13d1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/48d13d13df34139f1f5d4dca53a8fe8d974582cb))

### [0.19.10](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.9...v0.19.10) (2023-07-18)


### Features

* **files error:** extend missing node error message to multiple errors ([bc9a43a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bc9a43a4edbc043d1c17e0d170efbf649983b264)), closes [#182](https://gitlab.com/hestia-earth/hestia-ui-components/issues/182)

### [0.19.9](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.8...v0.19.9) (2023-07-11)


### Features

* **files error:** handle warning no `value` has `default` ([6ff4e73](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6ff4e7330d7628dabfe1c420a90c76b7c7e5f2cf))


### Bug Fixes

* **compound:** ignore `AR` compounds ([f3b6bc8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f3b6bc84ee1f1da93395e3456b39e4e670372aec)), closes [#180](https://gitlab.com/hestia-earth/hestia-ui-components/issues/180)
* **node logs models:** fix display color in missing lookups ([9aded74](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9aded74a81446a94bcf76875d55e0bf1d9ac2745))
* **node logs models:** skip non-recalculated nodes in recalculated value ([c4aaf3b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c4aaf3bd5df42742d8720122c263f09702eeb930)), closes [#181](https://gitlab.com/hestia-earth/hestia-ui-components/issues/181)

### [0.19.8](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.7...v0.19.8) (2023-07-07)


### Features

* **files error:** handle error siteType and functionalUnit != `1 ha` ([78cfc49](https://gitlab.com/hestia-earth/hestia-ui-components/commit/78cfc49ae19bf1b9dc6678f949cc7104292bebe1))


### Bug Fixes

* **files form:** fix error google not defined ([1515d57](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1515d5766d7b9676717c7cb25ee7100de9028e5a))

### [0.19.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.6...v0.19.7) (2023-07-06)


### Features

* **node logs models:** include models not recalculated from orchestration ([f97a542](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f97a54268481bd7d70c70d5aea5797a986f6594f)), closes [#176](https://gitlab.com/hestia-earth/hestia-ui-components/issues/176)

### [0.19.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.5...v0.19.6) (2023-07-06)


### Features

* **glossary:** add service ([e4b2c39](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e4b2c39e7c1943e26525529ea0ea985df12f46a8))
* **node logs:** show details for `not relevant` model ([75ded36](https://gitlab.com/hestia-earth/hestia-ui-components/commit/75ded3690521a9e7a0f8aedb422f154f2d546734)), closes [#173](https://gitlab.com/hestia-earth/hestia-ui-components/issues/173)

### [0.19.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.4...v0.19.5) (2023-07-04)


### Features

* **terms:** add new components ([13f576d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/13f576d0f88bdde904b709ac099bd522ef57b33f))

### [0.19.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.3...v0.19.4) (2023-07-03)


### Features

* **data-table:** set min height of `200px` ([97c5151](https://gitlab.com/hestia-earth/hestia-ui-components/commit/97c5151cc561a20e9cb5f96948e11b803a18bf3e)), closes [#154](https://gitlab.com/hestia-earth/hestia-ui-components/issues/154)
* **files error:** handle error multiple IA linked to Cycle Product ([1cf048e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1cf048e1577aed02fed0ee871cd5ce93c19cf89c))
* **nodes logs models:** show not in system boundary ([542b5fa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/542b5fa1ed426b5bc6459bbe9368ee68929e7107)), closes [#155](https://gitlab.com/hestia-earth/hestia-ui-components/issues/155)
* **sites maps:** use different color for `site.boundary` ([89a1b13](https://gitlab.com/hestia-earth/hestia-ui-components/commit/89a1b13fac690815f692eb1d26ce7a2539e24295)), closes [#171](https://gitlab.com/hestia-earth/hestia-ui-components/issues/171)


### Bug Fixes

* **precision:** handle `boolean` values ([b9b0bfa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b9b0bfadb4e8e3f36ac339b6dba3bd2c892732f2)), closes [#172](https://gitlab.com/hestia-earth/hestia-ui-components/issues/172)

### [0.19.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.2...v0.19.3) (2023-06-30)


### Features

* **cycles:** add `he-cycles-animals` component ([582b243](https://gitlab.com/hestia-earth/hestia-ui-components/commit/582b2439b004c9988271d3d5eb7d1e6b0eb0cf86))
* **files error:** handle error `waterRegime` incorrect `rice` product ([8bfd268](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8bfd268bd7f78193b22482ddaeeb0d17961b47f9))
* **files error:** handle error duplicate terms different units ([1abd454](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1abd45438b08330986427ab9f5ed09d707e9a297))


### Bug Fixes

* **terms:** ignore property `updatedAt` ([cb12ac6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/cb12ac6f5cecc5a89c034746bfffc1bd83bd8800)), closes [#169](https://gitlab.com/hestia-earth/hestia-ui-components/issues/169)

### [0.19.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.1...v0.19.2) (2023-06-19)


### Features

* **files error:** handle error sum `waterRegime` is not `100%` ([693bd9e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/693bd9eaf7cfec5d69ca904d1b278f1ef9b44d38))
* **files error:** handle message liveAnimal requires excreta ([3640e9d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3640e9dd6cb15d4ea34af1cf2721372d70160aaf))
* **files upload errors:** handle error `object-array-invalid` ([5327fae](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5327faee7c9de100a900f37a3bf35297aba43502)), closes [#167](https://gitlab.com/hestia-earth/hestia-ui-components/issues/167)


### Bug Fixes

* **files error:** fix error message missing labels ([6f8497b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6f8497bfe04041fd6553c1eb626078986e06e775)), closes [#165](https://gitlab.com/hestia-earth/hestia-ui-components/issues/165)

### [0.19.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.19.0...v0.19.1) (2023-06-14)


### Features

* **files error:** improve error message completeness animalFeed ([a55e321](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a55e32111310e13e82995e66e5e90fdfbdff2abd))
* **files error:** improve error on enum ([8850ed3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8850ed3b1188417e7fbe94e210cb2ed2c41463d8)), closes [#153](https://gitlab.com/hestia-earth/hestia-ui-components/issues/153)
* **files upload errors:** add steps to simplify GeoJSON ([5f7cda5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5f7cda5f2bf90b42ca331a228b8bd6f06c230f80))
* **files upload errors:** set schema link to file schema version ([d50a206](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d50a20636faaec9eec2063fb9430dc185b8eb2f9))
* **node logs models:** show functional unit ([88130b8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/88130b8fcbbaec9e333d9a0300c912a80f42ed1d)), closes [#158](https://gitlab.com/hestia-earth/hestia-ui-components/issues/158)
* order emission not relevant last ([8ce0815](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8ce08151ce67dfbf4a8b671d692b76179ded7a3e)), closes [#152](https://gitlab.com/hestia-earth/hestia-ui-components/issues/152)


### Bug Fixes

* **impact assessment:** handle `product.term.name` ([95c040e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/95c040ead54ff4d6c1d16bbc1be4f70d5bb1e77f))
* **node logs:** handle no original value ([b904f45](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b904f45cefa4c24bdda6d30381df11d81e4e7425))

## [0.19.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.18.1...v0.19.0) (2023-05-31)


### ⚠ BREAKING CHANGES

* min schema version is `21.0.0`

### Features

* **files error:** improve error message allowed values ([f8495d1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f8495d144f267862149da71930487db1c4d64319))
* update schema to version 21 ([ff3cca7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ff3cca7cd5252383855a35d2319891c86d9ef9ff))

### [0.18.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.18.0...v0.18.1) (2023-05-22)


### Bug Fixes

* **files error:** handle `source` instead of `defaultSource` ([258197c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/258197cebbe2ac05a72a1cb62fbe4a2860a67f2c))

## [0.18.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.17.0...v0.18.0) (2023-05-03)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `20.0.0`

### Bug Fixes

* **compound:** add missing space on `m3` ([b9df342](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b9df342826464de90b1d7322db29f02db380e204)), closes [#146](https://gitlab.com/hestia-earth/hestia-ui-components/issues/146)
* **link key:** fix value as table not displaying ([914ef99](https://gitlab.com/hestia-earth/hestia-ui-components/commit/914ef998e12f786adc75c6715123e6dd3314b1b1)), closes [#147](https://gitlab.com/hestia-earth/hestia-ui-components/issues/147)


* **package:** update to schema `20.0.0` ([642cab3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/642cab34a0ba6369d448fb9c012ab41e53731ddb))

## [0.17.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.16.3...v0.17.0) (2023-04-19)


### ⚠ BREAKING CHANGES

* **package:** min api version is `0.19.0`

### Bug Fixes

* **files error:** handle data path on nested nodes ([f0a6993](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f0a6993c0e5519011a98433866b6c724891a8cdf))


* **package:** update api to `0.19.0` and schema to `19.0.0` ([87ad6b0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/87ad6b0fd61b6f146ecebc037c7963e574ce60a8))

### [0.16.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.16.2...v0.16.3) (2023-04-14)


### Bug Fixes

* **impact assessments products:** fix functional unit column content wrap ([1a3c640](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1a3c640e47ffc7c6ce63a75be1997faa3900f59f))
* **node logs models:** handle no id when searching for terms ([3843153](https://gitlab.com/hestia-earth/hestia-ui-components/commit/384315322e3de2d77cd1f4ad909311293a717adf))

### [0.16.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.16.1...v0.16.2) (2023-04-13)


### Features

* **data table:** handle multiline rows ([cd40332](https://gitlab.com/hestia-earth/hestia-ui-components/commit/cd40332e2f1dd8d3595356707f1de5344d4ca021))
* **files error:** improve error message coordinates in wrong region ([645224a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/645224a6fc2fd05c8990c8692f0131bce714c858))


### Bug Fixes

* **node logs models:** only show copy button on hover ([e5e4f46](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e5e4f4675cc09354312c10ecbe16d553be2819d2)), closes [#129](https://gitlab.com/hestia-earth/hestia-ui-components/issues/129)
* rename "Submission" into "Upload" ([78c16e8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/78c16e8631e0c64a2706638dfa46af4d1e87ccff))

### [0.16.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.16.0...v0.16.1) (2023-04-10)


### Features

* **files error:** handle error both liveAnimal and animalProduct ([e7152ef](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e7152efc72200ce3b7a7fbf00792420cc2bf0e54))
* **files error:** handle error treatment should be specified ([69a3214](https://gitlab.com/hestia-earth/hestia-ui-components/commit/69a3214fd30175b4babd988a25cd3e17906a1b9a))

## [0.16.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.15.3...v0.16.0) (2023-04-04)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `18`

### Features

* **cycle completeness:** add recalculation logs ([e540e01](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e540e01d28a4d1d16c747fd732f03f0dd214eda1))
* **cycles practices logs:** show all practice Terms ([f47fc56](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f47fc5622e96bb107f80787d5fef67866af19c65))


### Bug Fixes

* **common default:** handle undefined values ([1595fee](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1595fee3664b299da283c8ec47a913032c37c830))


* **package:** min schema version is `18` ([3bcf47a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3bcf47a3d2c973b88ce38a58991bbeb78c87cf43))

### [0.15.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.15.2...v0.15.3) (2023-03-23)


### Bug Fixes

* **node:** force refresh missing logs ([1ceed8a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1ceed8afe46dbf9c875186b4ac1544fd38ce303e))

### [0.15.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.15.1...v0.15.2) (2023-03-23)


### Bug Fixes

* **files form:** handle conditional schema other than `properties` ([406c8aa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/406c8aa84d99fd9582a7b64128ca9daeb4b0689a))

### [0.15.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.15.0...v0.15.1) (2023-03-17)


### Features

* **engine:** export function `nodeVersion` ([df3e7d8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/df3e7d88b0f02129a78031faa042addbd86d3dd2))

## [0.15.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.14.3...v0.15.0) (2023-03-15)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `17.3.0`

### Bug Fixes

* **files form:** use identical error message for missing terms as summary ([c42dde0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c42dde0cac98d9df2ea28b44960ce6de6c40c32a))


* **package:** use schema `17.3.0` ([301a242](https://gitlab.com/hestia-earth/hestia-ui-components/commit/301a242c426177fc107f11b842afe02ea1b5a568))

### [0.14.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.14.2...v0.14.3) (2023-03-13)


### Features

* **files error:** handle error `forage` needed for `animalFeed` ([7e2e04b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7e2e04b8143a4a8753f1ddc50ee6433631c034ab))
* **files error:** handle error `should be equal to constant` ([82f99c7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/82f99c77d89321e8db95736f5fc985521a997688))
* **files error:** handle error Input must have `fate` for animalFeed ([1a0e42c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1a0e42c908e5730a77e415a29b5b604fa7cb27bd))
* **files error:** handle error pasture grass values sum to 100% ([de888c1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/de888c140dba945454af5c0860ffb3aeb7b241a7))
* **files error:** handle error using `not relevant` methodTier ([8480d1e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8480d1e2a759414b5610a04194d25dda0622a159))
* **files error:** improve error message on `cropResidue` completeness ([3f1eee3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3f1eee34eae24dfc6a91c78b2c32bbdd94701bd6))


### Bug Fixes

* **files error:** fix migration errors message when no migrations ([bc9370b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bc9370b5c6c805efafe733d3f786d758864f81d2))

### [0.14.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.14.1...v0.14.2) (2023-03-03)


### Features

* **files error:** improve message Term not found ([fdd110a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fdd110abf06d865369bdb48d6ddc75495f0faf30)), closes [#143](https://gitlab.com/hestia-earth/hestia-ui-components/issues/143)
* **files form:** show migration suggestions ([e0fe3e6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e0fe3e6abdf5c3e6acef94d2f8239c84aa581313))


### Bug Fixes

* **data-table:** fix resize on menu collapse after width changes ([b2fe4f8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b2fe4f81ba72aca64eeddfc7cd5985c61fafa986)), closes [#144](https://gitlab.com/hestia-earth/hestia-ui-components/issues/144)
* **styles:** do not wrap highlight text ([076a65a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/076a65aa1cc7132a9078166a4c26fd398f9e629c))

### [0.14.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.14.0...v0.14.1) (2023-02-27)


### Features

* **files error:** display multiple errors on confidence interval ([72a8a80](https://gitlab.com/hestia-earth/hestia-ui-components/commit/72a8a805fb68626ab695797c03086853236b02ac))


### Bug Fixes

* **files form:** always show error if any ([ee708ed](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ee708ed5534884980059dc38eaad5ed1e215ac9a)), closes [#142](https://gitlab.com/hestia-earth/hestia-ui-components/issues/142)

## [0.14.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.13.2...v0.14.0) (2023-02-21)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `16`

### Features

* **data-table:** increase width first column non-small table ([0d6b8d1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0d6b8d1b46d0e5ec86e1914e4a74429d96961ed6))
* **files upload errors:** handle `invalid-json` error ([445892f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/445892f072215eef86d91673f6400f2d15698657))


* **package:** require schema min `16` ([72af451](https://gitlab.com/hestia-earth/hestia-ui-components/commit/72af45149ba439ead0889c64f7a6d7a38b263e27))

### [0.13.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.13.1...v0.13.2) (2023-02-16)


### Features

* **files error:** handle error emission not relevant ([30e58a2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/30e58a29573ec9137ce197dc3373dfce58536a40)), closes [#206](https://gitlab.com/hestia-earth/hestia-ui-components/issues/206)
* **files:** add `files-upload-errors` component ([aa327e5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/aa327e540e9bc6b77d6b5265d4c937e9c11d3ccb))


### Bug Fixes

* **files error:** fix error message outside distribution when using `group` ([75f1959](https://gitlab.com/hestia-earth/hestia-ui-components/commit/75f195925e3cd62b494ac09b4dfb899c9a1b0f3d))
* **files form:** fix overlap error message and group icon ([e60dfce](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e60dfce05adffe6c4a83f997cf0aafe51484de14)), closes [#141](https://gitlab.com/hestia-earth/hestia-ui-components/issues/141)
* **impact assessment products:** show all models by default in `original` view ([ce4fcdf](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ce4fcdfdecfb913f1bb07be42450caa1d24cb949)), closes [#140](https://gitlab.com/hestia-earth/hestia-ui-components/issues/140)

### [0.13.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.13.0...v0.13.1) (2023-02-09)


### Features

* **files error:** improve error message on node with source privacy error ([8b58c38](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8b58c38bc7929c5bf02cb290f9b7a282428de75d))


### Bug Fixes

* **files error summary:** fix error message with a `.` ([c4d5788](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c4d5788af4680135c2c0de4fe50214fdaa32d97e)), closes [#137](https://gitlab.com/hestia-earth/hestia-ui-components/issues/137)

## [0.13.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.12.5...v0.13.0) (2023-02-07)


### ⚠ BREAKING CHANGES

* **files form:** `nodeChange` event renamed `nodeChanged` on `he-files-form`

### Features

* **data table:** resize on navigation menu collapse ([f7b134e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f7b134ec7b463352ce6d79562cddbab93fe58260))
* **files error:** improve display of lists ([37c0109](https://gitlab.com/hestia-earth/hestia-ui-components/commit/37c0109b38b8c2395e316dc0e40ad9a59e94901f)), closes [#133](https://gitlab.com/hestia-earth/hestia-ui-components/issues/133)
* **files error:** improve error id not found ([1e9d588](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1e9d5885d10a1b69d671bc25b0caf21ece6f7ec0)), closes [#136](https://gitlab.com/hestia-earth/hestia-ui-components/issues/136)


### Bug Fixes

* **common:** handle no value in pipes ([2ef181d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2ef181d8b571e2cde5c4fabdf5c6dd09be7d8d0a))
* **files error summary:** group messages by `level` ([ce91263](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ce91263fc0d021c1c2bd12a5ddd3d29b13ff033f)), closes [#132](https://gitlab.com/hestia-earth/hestia-ui-components/issues/132)


* **files form:** render addons only once ([515af3f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/515af3f6dc247ea30667aa47b33119ec31c5b392))

### [0.12.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.12.4...v0.12.5) (2023-01-31)


### Features

* **common:** add `noExt` pipe ([8934652](https://gitlab.com/hestia-earth/hestia-ui-components/commit/893465209d0b8399eaaa428216fbe73329c6b2f4))


### Bug Fixes

* **clipboard:** remove click on whole component ([b9b7c26](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b9b7c266e3d2edc22315603bfc9dce16440d990f))
* **data table:** fix resize not working for all tables ([5d70d4b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5d70d4ba3b986b9aa0c9ae8c75452d062b54c709))

### [0.12.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.12.3...v0.12.4) (2023-01-30)


### Features

* **node logs models:** fix status details with merge args ([1c885ef](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1c885ef054de29605c3805b3a828e153c626bd97)), closes [#98](https://gitlab.com/hestia-earth/hestia-ui-components/issues/98) [#125](https://gitlab.com/hestia-earth/hestia-ui-components/issues/125)
* **node logs models:** group logs together if `value` and `coefficient` are logged ([6dedcdd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6dedcdd736332c25d54cc49d52f76d385a46b4e3)), closes [#128](https://gitlab.com/hestia-earth/hestia-ui-components/issues/128)
* **node logs models:** hide status details if skip hierarchy ([38f1c1e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/38f1c1ed7cf616b4a4d4697712d1dbdb4aeed27d)), closes [#131](https://gitlab.com/hestia-earth/hestia-ui-components/issues/131)


### Bug Fixes

* fix highlight inline style ([5c2e37c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5c2e37c9aa41209dc5d29d2da5b523c9d3b3f226))
* **impact assessments graph:** hide loading if not recalculated state ([260bf87](https://gitlab.com/hestia-earth/hestia-ui-components/commit/260bf874bbd3efc4088407b2f777b98ceb42ea41))
* **node logs models:** fix error grouping no order ([29d29c8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/29d29c8997db8cccdc71c8a9a7a4e7912e597eaf))
* **node logs models:** fix incorrect order of models ([a7c5a19](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a7c5a19b6f69a07460ff213a7220e242726a4209)), closes [#130](https://gitlab.com/hestia-earth/hestia-ui-components/issues/130)

### [0.12.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.12.2...v0.12.3) (2023-01-27)


### Features

* **node logs models:** show `not relevant` status ([4415d04](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4415d04c129cb8dd8f3c0628f920f708f5e870cd))


### Bug Fixes

* **node logs models:** remove dotted lines on rows ([2c2ffeb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2c2ffeb62c531a5ef6053b52da061f7bcacba4f6))

### [0.12.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.12.1...v0.12.2) (2023-01-27)


### Bug Fixes

* **impact assessments graph:** fix no-reload after state changes ([83d80be](https://gitlab.com/hestia-earth/hestia-ui-components/commit/83d80be823a38e509a7f2d4824d02f6cfca735d1))

### [0.12.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.12.0...v0.12.1) (2023-01-27)


### Bug Fixes

* **impact assessments graph:** prevent infinite reload on `impactAssessments` change ([ac50dbf](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ac50dbf4c40642fc88fa0f634a04777841137616))

## [0.12.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.11.6...v0.12.0) (2023-01-26)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `15`

* **package:** update schema to `15` ([9f7752a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9f7752a020efdc09cf389d8f523fafae9e2394b6))

### [0.11.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.11.5...v0.11.6) (2023-01-26)


### Features

* get Cycle name for transformation if available ([88af81c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/88af81cc486f78a6958a239071ba5368c8c1e65c))
* **node logs models:** display logs as table ([2ec8958](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2ec8958495239a9271a7aedaafbbf55123541ab2)), closes [#113](https://gitlab.com/hestia-earth/hestia-ui-components/issues/113) [#118](https://gitlab.com/hestia-earth/hestia-ui-components/issues/118)
* **node value details:** set dark mode on nested tables ([b8f94bf](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b8f94bf3b641a18712c0a97f4e9d0b2720fb044e)), closes [#109](https://gitlab.com/hestia-earth/hestia-ui-components/issues/109)


### Bug Fixes

* **files error:** improve error message fate of crop residue ([e965ca6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e965ca6c9398109b83d1054d6d9638793bded406))
* **files error:** update error message for allowed range ([35cb0b9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/35cb0b985f28dadcc1bd1bba6a3cb30e4ac21782)), closes [#127](https://gitlab.com/hestia-earth/hestia-ui-components/issues/127)
* **styles:** add missing `bulma-switch` import ([050fa7b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/050fa7b5017dfcb093bae3c8a6ea21378f4cda1c))

### [0.11.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.11.4...v0.11.5) (2023-01-19)


### Features

* **files error:** handle error fate of cropResidue error ([4620e08](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4620e083f0bd08e1a5c24916dc2d305023daae9c))
* **files error:** handle error fate of cropResidue missing ([7bcdd24](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7bcdd2487389ccbe8a51ca8f23e707a8e33fce68))
* **node details:** add `depthUpper` and `depthLower` to table keys ([29a67cb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/29a67cbee7a314582836092e2d568ad0fdb665b2)), closes [#126](https://gitlab.com/hestia-earth/hestia-ui-components/issues/126)


### Bug Fixes

* **bibliographies search confirm:** show loading icon ([3ed08a3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3ed08a3b3f0a89ed99668d077cad66d80185ff71))

### [0.11.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.11.3...v0.11.4) (2023-01-17)


### Features

* **engine:** get config by optional `id` ([0252265](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0252265e676f8a8d676c84707c9251dfae92f839))

### [0.11.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.11.2...v0.11.3) (2023-01-16)


### Features

* **demo:** add demo to test components ([39a4851](https://gitlab.com/hestia-earth/hestia-ui-components/commit/39a4851dfae6fd44da6d7a03be126450e325ca7a))

### [0.11.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.11.1...v0.11.2) (2023-01-12)


### Features

* **node logs models:** handle logs of value as array ([f0b6580](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f0b6580fd8ca8b8ecc76f0e0214fa1a5bd193143))
* **node logs models:** improve logging on Spatial model ([86a51f8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/86a51f85d340431522be6b493801f8c9d4ee5287)), closes [#124](https://gitlab.com/hestia-earth/hestia-ui-components/issues/124)
* **precision:** add option to format using commas ([4763134](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4763134316eb92a49aceeb2fa6479c90c9263827)), closes [#121](https://gitlab.com/hestia-earth/hestia-ui-components/issues/121)


### Bug Fixes

* **files form:** fix formatter of suggested existing Node ([18eca6b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/18eca6be2d5832e7e2fcf17a7668c854608ab71c))
* ignore state in original on tooltip ([782266b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/782266be8c550d43fd737543d4ce44f7bf2d168f)), closes [#123](https://gitlab.com/hestia-earth/hestia-ui-components/issues/123)
* **style:** move copy clipboard to not overflow scrollbar ([b6f1b89](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b6f1b89c09e12570340a9ae25b89d33bb80010ff)), closes [#122](https://gitlab.com/hestia-earth/hestia-ui-components/issues/122)

### [0.11.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.11.0...v0.11.1) (2023-01-02)


### Features

* **compound:** ignore string containing more than 1 dash ([24b10e8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/24b10e8fdf2747c60d0f3bcd3e4cbe9a0ba1fc98)), closes [#120](https://gitlab.com/hestia-earth/hestia-ui-components/issues/120)
* **node logs models:** add details on log status ([f5a9819](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f5a981906ae1912082c4e594a3f19f46c2d1a2f2))
* **node logs models:** add translations to pesticideAI keys ([33e8daa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/33e8daad88783dbc8570ab3184b9f0acd920795a))
* **node logs models:** display term name for subValues ([f02feb0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f02feb0a44d05ba49674b3b917b8194bf2c796fe)), closes [#117](https://gitlab.com/hestia-earth/hestia-ui-components/issues/117)
* **terms:** add mapping `Term` key to label ([9b7acf8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9b7acf85e9608c5a549f09bd7a84adb66013fcfb)), closes [#112](https://gitlab.com/hestia-earth/hestia-ui-components/issues/112)


### Bug Fixes

* **node logs models:** add missing narrow class on popover ([c6d0b17](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c6d0b1701ca3d43548ad6623e3e9f222212a2ff5)), closes [#115](https://gitlab.com/hestia-earth/hestia-ui-components/issues/115)
* **node logs models:** fix width first column on mobile ([fc94e38](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fc94e38cbd853e509b3fa90e9a6b105437eab270)), closes [#119](https://gitlab.com/hestia-earth/hestia-ui-components/issues/119)
* **node logs models:** handle model had unexpected exception while running ([e9db197](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e9db197b115453cd4c846f1601228aa213ef4ccf))
* **styles:** set tooltip on top of popover ([9189c45](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9189c45fb6b1f6986d6132e1dedfa20f79f0de80)), closes [#114](https://gitlab.com/hestia-earth/hestia-ui-components/issues/114)

## [0.11.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.10.5...v0.11.0) (2022-12-19)


### ⚠ BREAKING CHANGES

* **engine:** min api version is `0.17.0`

### Bug Fixes

* apply narrow style to popover from data tables ([57b80bc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/57b80bc8e4d6fd2839711853be46d552a1de59c9)), closes [#110](https://gitlab.com/hestia-earth/hestia-ui-components/issues/110)


* **engine:** get config from api instead of cdn ([d2484e6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d2484e679821509c6d27f3575948b68ffa8e1d46))

### [0.10.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.10.4...v0.10.5) (2022-12-15)


### Features

* **files error:** handle warning area is not equal to boundary size ([5e18c35](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5e18c35b59002e4e00b83b8a609fe3218c3ca864))


### Bug Fixes

* **data table:** fix wrong shadow on first th/td ([7c61415](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7c614159b69abc5d9480151b71c20f5f8a70e3da)), closes [#108](https://gitlab.com/hestia-earth/hestia-ui-components/issues/108)
* **files error:** improve error message outside confidence interval ([d975410](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d9754102bd4dc5f43863f806c9b00f81f19c9fbb))

### [0.10.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.10.3...v0.10.4) (2022-12-13)


### Bug Fixes

* **color:** handle out of bounds shades ([f582f1b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f582f1bc0cc8970b185c81348c976e482b0152cd))

### [0.10.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.10.2...v0.10.3) (2022-12-13)


### Bug Fixes

* **color:** fix broken import ([2449d54](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2449d547ea3f09bc09a5d816822604de92132ee8))

### [0.10.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.10.1...v0.10.2) (2022-12-13)


### Features

* **common:** replace random list colors with new style guide colors ([3e6e5bd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3e6e5bd395140a8eb192105ede1a31a411c10e92))
* **files error:** handle error message multiple IA linked to same product ([3acef85](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3acef85c9fb8390810981d558b299a924b647b8b))
* **files error:** handle warning yield value distribution ([16d37e9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/16d37e9cee34b99e1daa8bf5eb1954b579822a54))
* **node logs models:** add mapping of log keys ([35acc84](https://gitlab.com/hestia-earth/hestia-ui-components/commit/35acc842789039d32d51b7bfe5bf65189c0b4726)), closes [#107](https://gitlab.com/hestia-earth/hestia-ui-components/issues/107)


### Bug Fixes

* **node logs models:** set dark style on links ([7992c2c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7992c2c721f2b6c78b31febe477545e004291a64))
* **styles:** remove bootstrap visually hidden elements ([3514a74](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3514a743705f81fb199a34dac71115df689589a7))

### [0.10.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.10.0...v0.10.1) (2022-12-07)


### Features

* **files error:** handle error on `pastureGrass` key `units` ([76754ae](https://gitlab.com/hestia-earth/hestia-ui-components/commit/76754aeb91fb19158a3446e3163b66e50bf0d9fd))

## [0.10.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.8...v0.10.0) (2022-11-30)


### ⚠ BREAKING CHANGES

* **cycles:** min schema version `14`

### Features

* **cycles:** update `dataCompleteness` to `completeness` ([61b6980](https://gitlab.com/hestia-earth/hestia-ui-components/commit/61b69805ede88148511f23d63b0b3e6a1db18ae3))
* **link key value:** handle data state for list of blank nodes ([5905b6a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5905b6ad3293216e80963f282166459d8844ba26)), closes [#95](https://gitlab.com/hestia-earth/hestia-ui-components/issues/95)


### Bug Fixes

* **link key value:** use standard `is-dark` color for links ([5c4f74d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5c4f74d514c8614a19943ac76bd7b128f6ac7eaa))
* **node logs models:** handle skipped models after non-required model ([181ba78](https://gitlab.com/hestia-earth/hestia-ui-components/commit/181ba78d4e3ea3e258cecd37442bf38f1089c632))

### [0.9.8](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.7...v0.9.8) (2022-11-29)


### Features

* **data table:** change opacity on table inner borders ([efb6339](https://gitlab.com/hestia-earth/hestia-ui-components/commit/efb6339bae97a5f3302e3e2a150b0ade0f2ef04d))


### Bug Fixes

* **data table:** update border style on non-fixed columns ([47a840d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/47a840d5bd23dd8cd5c17b9156a7e5140578d281))
* **popover:** update style to match other components ([bafe77a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bafe77a7f4320d9f251581903b710a7a04f970b3))

### [0.9.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.6...v0.9.7) (2022-11-24)


### Features

* update ui-framwork to v3 ([8676f46](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8676f4672cec1a81b1d56018456c340e498f04bd))


### Bug Fixes

* **data table:** fix z-index on fixed table headers ([a08aa21](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a08aa21c0876bb5f08396ff4a530468283b455cb))
* **files form:** fix autocomplete not working ([1d4d2d1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1d4d2d1029b1e21641c7e6c8d7af1bdb0167038b)), closes [#102](https://gitlab.com/hestia-earth/hestia-ui-components/issues/102)

### [0.9.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.5...v0.9.6) (2022-11-18)


### Features

* **impact assessments:** move graph component from frontend ([df0aa5a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/df0aa5a01d0ddf079604cf8e1e09d59077faf9fd)), closes [#16](https://gitlab.com/hestia-earth/hestia-ui-components/issues/16)


### Bug Fixes

* **compound:** handle `GWP` and `m3` ([757d50e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/757d50e7b88baf4fe45e56e54dcd833c53919ebf)), closes [#101](https://gitlab.com/hestia-earth/hestia-ui-components/issues/101)

### [0.9.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.4...v0.9.5) (2022-11-15)

### [0.9.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.3...v0.9.4) (2022-11-14)


### Features

* **node logs models:** display units as compound ([2bf2b3a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2bf2b3af6a0d14ea1fca9700c4d2e73d99d7559f))


### Bug Fixes

* **compound:** handle value is not a string ([568c860](https://gitlab.com/hestia-earth/hestia-ui-components/commit/568c8606a960b6ca9b9ea64a01bc04da0ee9c3d4))

### [0.9.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.2...v0.9.3) (2022-11-14)


### Bug Fixes

* **compound:** handle sup `-` not in a compound ([6c1a78a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6c1a78a3ebe316dfc56480e9084d6a8575d14c22))

### [0.9.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.1...v0.9.2) (2022-11-14)


### Features

* **files error:** handle error product `ha` functional unit `1 ha` ([c6d0b3e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c6d0b3e7f9f9d548e2903582ded9902cd4792736))
* handle compounds sup/sub in html ([0ac9ebf](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0ac9ebfaae08abdedb710a7e28c8ef6a354deb8a))


### Bug Fixes

* **compound:** handle empty value ([713b572](https://gitlab.com/hestia-earth/hestia-ui-components/commit/713b572bd6b6aae3a66e15a25ea01b27ea4b5213))
* **engine:** fix path to `excreta-kg` model ([5c4750d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5c4750d6e6d24d369757a44db2aef9ddc5ea36df)), closes [#97](https://gitlab.com/hestia-earth/hestia-ui-components/issues/97)
* **impact assessments products:** show table when data empty on selected default model ([26a2fea](https://gitlab.com/hestia-earth/hestia-ui-components/commit/26a2feaedfcba740022c0805b38f54962fa5f638))

### [0.9.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.0...v0.9.1) (2022-11-09)


### Bug Fixes

* **node logs:** fix issue displaying log values ([c87d5d5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c87d5d50dc3b34191f4e7c0c359127cfaae41b73))

## [0.9.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.0-6...v0.9.0) (2022-11-07)

## [0.9.0-6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.0-5...v0.9.0-6) (2022-11-07)


### Bug Fixes

* remove use of ~ for relative import in scss ([26e20fa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/26e20fa2c02194a808fd22952dd784fff0ee7d05))

## [0.9.0-5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.0-4...v0.9.0-5) (2022-10-28)

## [0.9.0-4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.0-3...v0.9.0-4) (2022-10-28)


### ⚠ BREAKING CHANGES

* **styles:** import jsondiffpatch stylesheet manually

### Bug Fixes

* **styles:** remove import of css ([e6cf42c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e6cf42c9ee589567543d94dc88633bcf3e690c68))

## [0.9.0-3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.0-2...v0.9.0-3) (2022-10-28)

## [0.9.0-2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.0-1...v0.9.0-2) (2022-10-28)

## [0.9.0-1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.9.0-0...v0.9.0-1) (2022-10-28)

## [0.9.0-0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.8.2...v0.9.0-0) (2022-10-28)


### ⚠ BREAKING CHANGES

* **package:** angular `14` required

### Features

* **node value details:** show units if present ([54ea08f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/54ea08f984c39a948ad94084f24278ad2471401f)), closes [#94](https://gitlab.com/hestia-earth/hestia-ui-components/issues/94)


* **package:** update to angular 14 ([0958c9b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0958c9b6da2fbec6971f791f214c17a8396874e5))

### [0.8.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.8.1...v0.8.2) (2022-10-28)


### Features

* **node logs models:** link related impact assessment ([8e95dd7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8e95dd731391f2d2ac8234ec0adc8f495d4d9494)), closes [#76](https://gitlab.com/hestia-earth/hestia-ui-components/issues/76)
* **node logs models:** show `methodTier` from model if any ([4e7cfad](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4e7cfad8908ea2443f1c9606f2602b5261d91c4b))


### Bug Fixes

* **node logs models:** handle skip measured emissions ([0c62c47](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0c62c4718910bd890ab0be073149fcc27905dabe))

### [0.8.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.8.0...v0.8.1) (2022-10-26)


### Features

* **files error:** handle message transformation products != inputs ([2ec4c70](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2ec4c703790ad0fd811f448c6e45ee70f60726ca))
* **node logs models:** do not show `notRequired` models ([5b73242](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5b732423ffaf66e0ca3e46b4610a8912500e3802)), closes [#93](https://gitlab.com/hestia-earth/hestia-ui-components/issues/93)


### Bug Fixes

* **cycles activity:** fix missing border between Inputs and Products ([8a2bec6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8a2bec63e33898bbaa4c83196a7c92a19d28f3e6))

## [0.8.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.7.2...v0.8.0) (2022-10-19)


### ⚠ BREAKING CHANGES

* **terms:** min schema version is `13.0.0`

### Features

* **cycles activity:** swap order Inputs and Products ([44ee90d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/44ee90d76edfc7f7467c1e7f0b4d71eef0daeff6)), closes [#90](https://gitlab.com/hestia-earth/hestia-ui-components/issues/90)
* **terms:** remove `cropProtection` termType ([3d69370](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3d6937020ed07a4601d42c81a15a502cef913c06))


### Bug Fixes

* handle transformation header keys ([4066271](https://gitlab.com/hestia-earth/hestia-ui-components/commit/40662718dc92fc00bf9fa10fa3321bb5322c3adc))
* **impact assessments products:** fix unicity of models ([4b159a0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4b159a00e43b121695939b758bda2d49c11e7806)), closes [#91](https://gitlab.com/hestia-earth/hestia-ui-components/issues/91)

### [0.7.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.7.1...v0.7.2) (2022-10-18)


### Bug Fixes

* **common:** use dev url for git raw ([d94d5c1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d94d5c14899e9cc8ebb05b182ce3e162bae9a8f8))
* **engine models version:** hide if no version detected ([dfc09e4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/dfc09e4897571f522631e288ac8dbfe1ec69608b))
* **link key value:** fix style text white ([fdf77b9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fdf77b9213bbab34088072da350f3121a2597d35))
* **node service:** fix logs on recalculated/aggregated ([820eefa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/820eefa0643b97bc21e914ad6727e8eea6038e63))

### [0.7.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.7.0...v0.7.1) (2022-10-12)


### Bug Fixes

* fix chart view not working after grouping changed to `term.name` ([5c0c39b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5c0c39bb37b11a1802b3c32386bee72e8415260b))

## [0.7.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.6.2...v0.7.0) (2022-10-11)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `12.0.0`

* **package:** update schema to `12.0.0` ([8a2862d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8a2862db9ad8e4ac6c797a6b750dbdbef740b4e3))

### [0.6.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.6.1...v0.6.2) (2022-10-07)


### Features

* **data-table:** reduce first col width to `120px` on mobile ([356474d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/356474d49f5bd08ff8325f3a80ea3f0ad5be8f29))


### Bug Fixes

* **engine orchestrator:** fix filter non-gap-filled models ([c739da7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c739da7a156b16face21db90efe28135e5ceb7de))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.6.0...v0.6.1) (2022-10-06)


### Bug Fixes

* **semver:** build custom semver-regex to fix bug in Safari ([25fb773](https://gitlab.com/hestia-earth/hestia-ui-components/commit/25fb77390af5fe9d60b42040caa8e88746645789))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.6.0-4...v0.6.0) (2022-10-05)


### Features

* **cycle chart:** show labels and values and remove tooltips ([233818e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/233818e8e681ffc744c1ffba4250bf2781e17ed9))
* **impact chart:** show labels and values and remove tooltips ([f1ca48e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f1ca48e021715d73ca603882aadb912f0aeb5a5b)), closes [#86](https://gitlab.com/hestia-earth/hestia-ui-components/issues/86)
* **node logs models:** filter results on suggestion selected ([18dd739](https://gitlab.com/hestia-earth/hestia-ui-components/commit/18dd739cc5c9bed20c0d6f7a20e4332982c9427e)), closes [#87](https://gitlab.com/hestia-earth/hestia-ui-components/issues/87)
* **nodes:** add getDeepRelations service ([1066540](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1066540f519c850839720746280e744343a779e2))
* **schema:** include `recommended` properties when creating new node ([ecffb1d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ecffb1d68d8a75dffabd298dd671c172dd7cef67))


### Bug Fixes

* **node logs models:** remove subValues without any original/recalculated value ([56bdcfc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/56bdcfc10ce04100e4becca3994ac7b0a389c51d)), closes [#89](https://gitlab.com/hestia-earth/hestia-ui-components/issues/89)
* **nodes:** sort blank nodes by `term.name` instead of `term.[@id](https://gitlab.com/id)` ([5bd8b86](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5bd8b865de6b709472d896e37da6854dc1d40d8f)), closes [#88](https://gitlab.com/hestia-earth/hestia-ui-components/issues/88)

## [0.6.0-4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.6.0-3...v0.6.0-4) (2022-09-30)


### Features

* **files form:** change suggestion count to `10` ([eb440ae](https://gitlab.com/hestia-earth/hestia-ui-components/commit/eb440aed2b8e7fa36f3a62c1f9325dc9ca58bf9b))


### Bug Fixes

* **unit converter:** handle no conversion units ([597bc45](https://gitlab.com/hestia-earth/hestia-ui-components/commit/597bc459002b16f1388082b1ab8b51ab4ba6c7af))

## [0.6.0-3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.6.0-2...v0.6.0-3) (2022-09-29)


### Features

* **cycle emissions:** group by `methodTier` for display ([607b35f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/607b35fea79a8cbfeb8bdc2e5c0667d56f595c2e)), closes [#34](https://gitlab.com/hestia-earth/hestia-ui-components/issues/34)
* **terms:** add missing `biologicalControlAgent` termType in Inputs ([00ad444](https://gitlab.com/hestia-earth/hestia-ui-components/commit/00ad4444e27224229c2bea014d49382f5f788223))

## [0.6.0-2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.6.0-1...v0.6.0-2) (2022-09-21)


### Features

* **node logs models:** show units on subValues ([926ca07](https://gitlab.com/hestia-earth/hestia-ui-components/commit/926ca079ccad114de4cbb8cbc86e9dab2499f1a2)), closes [#85](https://gitlab.com/hestia-earth/hestia-ui-components/issues/85)


### Bug Fixes

* **engine models link:** use the highest version in node ([6b57d18](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6b57d180c2c3cd805a025cf3e5989ac0d52c9a19))

## [0.6.0-1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.6.0-0...v0.6.0-1) (2022-09-21)

## [0.6.0-0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.5.3...v0.6.0-0) (2022-09-21)


### ⚠ BREAKING CHANGES

* **package:** ui-framework min v2

### Features

* **search:** export query products ([4191273](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4191273e07847ed2f47044a3890b92d9cde59bd2))


* **package:** use ui-framework v2 pre-release ([951f441](https://gitlab.com/hestia-earth/hestia-ui-components/commit/951f4415d3088b916a4e12dba0d43a37a4c85154))

### [0.5.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.5.2...v0.5.3) (2022-09-20)


### Features

* **engine:** add version link component ([3e6e224](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3e6e22402905163b91b3f867705db1c58851faea))


### Bug Fixes

* **link key value:** round numbers to 3 figures ([735a800](https://gitlab.com/hestia-earth/hestia-ui-components/commit/735a800337a772a657a8829069d48a3420615f4e)), closes [#81](https://gitlab.com/hestia-earth/hestia-ui-components/issues/81) [#82](https://gitlab.com/hestia-earth/hestia-ui-components/issues/82)
* **node logs models:** remove duplicated model on `input`/`product` same term ([20703dd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/20703dd588eb9f53abc2c3e2d9cc304b0b9a37f2))
* **node logs models:** remove duplicated models ([16e7ffe](https://gitlab.com/hestia-earth/hestia-ui-components/commit/16e7ffe0cd5dfe90015abf50610c2eecae4d6528)), closes [#84](https://gitlab.com/hestia-earth/hestia-ui-components/issues/84)

### [0.5.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.5.1...v0.5.2) (2022-09-14)


### Bug Fixes

* **node logs models:** handle advanced model keys including full path ([5856bfa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5856bfaf5c370e2c732925c3835eae7d599f54cb))

### [0.5.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.5.0...v0.5.1) (2022-09-13)


### Bug Fixes

* **node logs models:** handle emission reqiured/not required per model ([ec4a47c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ec4a47c29483451de1fba68ffec61604c2ed9539))
* **node logs models:** handle logs display as table with single row ([e2add86](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e2add86039e3d04ebadf06b83da2a08e5f6006ba))

## [0.5.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.4.5...v0.5.0) (2022-09-09)


### ⚠ BREAKING CHANGES

* **package:** min schema supported version is `11.0.0`

### Features

* **impact assessments products:** remove restriction on `emissionsResourceUse` ([d8cdd19](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d8cdd19fcbe445efa6286d703116d216d1781a25))


### Bug Fixes

* **node logs models:** fix status success + fail ([825e654](https://gitlab.com/hestia-earth/hestia-ui-components/commit/825e654390be778cacc557e1b07a4bd4317e17b9))


* **package:** update schema to `11.0.0` ([5015692](https://gitlab.com/hestia-earth/hestia-ui-components/commit/501569271d4114dcd6d8c4c37b48b5d3dfd7ae56))

### [0.4.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.4.4...v0.4.5) (2022-09-08)


### Bug Fixes

* **node logs models:** fix missing model label ([a698efd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a698efd7014e505af4957056b28bb072ad70c7be))

### [0.4.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.4.3...v0.4.4) (2022-09-08)


### Features

* **node details:** show "from cycle" when no linked transformation ([3c069d8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3c069d81461ee1c1047ef207f59f4178c63ea233)), closes [#78](https://gitlab.com/hestia-earth/hestia-ui-components/issues/78)


### Bug Fixes

* **cycles activity:** show Chart in original view ([cc8d7c1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/cc8d7c1b765baa503914cc336193d88735c97962))

### [0.4.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.4.2...v0.4.3) (2022-09-07)


### Features

* **node logs models:** display logs as table when applicable ([47fb385](https://gitlab.com/hestia-earth/hestia-ui-components/commit/47fb3852a7b254b3f9fe81f5c79ad6a128cc85e4))
* **node logs model:** show transformation as subValues of `Emission` ([50706b1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/50706b1581d504df41d4e45561fccf2ca6bc6ea0)), closes [#74](https://gitlab.com/hestia-earth/hestia-ui-components/issues/74) [#75](https://gitlab.com/hestia-earth/hestia-ui-components/issues/75)
* **node logs models:** show Cycle emission same level as Transformation ([3e15242](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3e15242eecc02a59811f754434f6f5a5816cb864)), closes [#74](https://gitlab.com/hestia-earth/hestia-ui-components/issues/74) [#75](https://gitlab.com/hestia-earth/hestia-ui-components/issues/75)

### [0.4.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.4.1...v0.4.2) (2022-09-06)


### Features

* **cyckes activity:** add result component under chart view ([e417739](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e4177390af9cdac19e2234fa740bf5ddd362ce60)), closes [#80](https://gitlab.com/hestia-earth/hestia-ui-components/issues/80)
* **files error:** handle error `dataCompleteness.material` ([475a141](https://gitlab.com/hestia-earth/hestia-ui-components/commit/475a141a63f54d6428326c2ce99ffb4410eaaada))
* **issue confirm:** handle `aggregation engine` repo ([bd4266d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bd4266d4d5b91445cca99cb7c3d7f02c575c277a))
* **node logs:** show `units` for `input` sub-values ([1c6418f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1c6418faa982c7e1219cb7331cf7c6d96786e032)), closes [#77](https://gitlab.com/hestia-earth/hestia-ui-components/issues/77)
* **search:** export `matchPrimaryProductQuery` function ([0307232](https://gitlab.com/hestia-earth/hestia-ui-components/commit/030723294831f408a0244d0995770e279731ae62))


### Bug Fixes

* **engine:** filter out `ecoinvent` models ([80296db](https://gitlab.com/hestia-earth/hestia-ui-components/commit/80296db34b98f4ee8db2eff835f407d711f44faf)), closes [#72](https://gitlab.com/hestia-earth/hestia-ui-components/issues/72)
* force refresh of logs view when nodes change ([5a459ad](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5a459ad0f5606592f8a40eb65384fcd4aa853ad0)), closes [#79](https://gitlab.com/hestia-earth/hestia-ui-components/issues/79)
* **node logs:** rename `Missing lookups` into `Optional data missing` ([570e61b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/570e61b59dfb31022e2f9d975dbfaf8044fcdd62)), closes [#73](https://gitlab.com/hestia-earth/hestia-ui-components/issues/73)

### [0.4.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.4.0...v0.4.1) (2022-08-26)


### Features

* **node:** handle get `aggregated` logs ([f6554b4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f6554b42deb32cabd182499f670321be9abc40d8))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.3.7...v0.4.0) (2022-08-25)


### ⚠ BREAKING CHANGES

* **package:** schema version `10` and api version `0.13` required

### Bug Fixes

* **data table:** make sure we return `100%` by default ([e1f324c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e1f324c634c45a7c1f3e27a506cef2127bafcc77))


* **package:** use api version `0.13.0` ([936da44](https://gitlab.com/hestia-earth/hestia-ui-components/commit/936da443f6f0c6cb5100f34565e56774c2265ec0))

### [0.3.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.3.6...v0.3.7) (2022-08-24)


### Features

* **blank node state:** add state unchanged and improve notice ([d15ad99](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d15ad99a6a2d6c73101ebf2307597d2a79feba08))
* **engine orchestrator:** group models by key and filter gap-filled only ([e60e859](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e60e859f05571e490e16800adadef179782418fe))

### [0.3.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.3.5...v0.3.6) (2022-08-23)


### Features

* **impact assessments products:** ellipsize product units ([d15d2b6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d15d2b610e3ffb9391169ad5ba2b36cce6176788)), closes [#71](https://gitlab.com/hestia-earth/hestia-ui-components/issues/71)


### Bug Fixes

* **node link:** only default to recalculated for `aggregated` nodes ([210b038](https://gitlab.com/hestia-earth/hestia-ui-components/commit/210b038ab5bc38e6c74ae7a0c03fb72478b77136))

### [0.3.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.3.4...v0.3.5) (2022-08-16)


### Features

* **node logs models:** show model for `liveAnimal` ([540f349](https://gitlab.com/hestia-earth/hestia-ui-components/commit/540f349992b746143a2d7a2dd9f6f1f144a98e9e)), closes [#70](https://gitlab.com/hestia-earth/hestia-ui-components/issues/70)


### Bug Fixes

* **cycle practices:** handle no cycles ([c361435](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c3614358d54557c89c78e7b7b4cb40746b48656a))
* **impact assessments:** handle no indicators ([4b884eb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4b884eb72ce5af492f4f05b2d3685772dca5d6ed))

### [0.3.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.3.3...v0.3.4) (2022-08-12)


### Features

* **files error:** handle error liveAnimal with system ([075f7e9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/075f7e901f015d97db3e77a6c85f2ba43fcfb569))
* **link key value:** show arrays side-by-side ([9ff2d06](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9ff2d06ddc48b26def87474a9b02ed33265c16ac)), closes [#67](https://gitlab.com/hestia-earth/hestia-ui-components/issues/67)
* **node:** add method to get errors ([5a4d1a7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5a4d1a7a26d42d307de821b43c0544ce6f9c63cc))


### Bug Fixes

* **node link:** add `dataState` except for `Term` ([7271ddc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7271ddcf832c27759bb5754a1592d2eeb3783e5e))
* **node logs models:** fix wrong models for properties ([2bb79c5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2bb79c5f83f5097d9d89f8bebf256377248735af))

### [0.3.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.3.2...v0.3.3) (2022-08-10)


### Bug Fixes

* **node:** fix compiling issue ([6dd7ad4](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6dd7ad4925ed459ed5425cf36730d8dd9ed1df44))

### [0.3.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.3.1...v0.3.2) (2022-08-10)


### Bug Fixes

* **node:** export model to fix differential loading error ([061345b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/061345ba86cd4c781c84cb1deef1db26f9e6332b))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.3.0...v0.3.1) (2022-08-08)


### Features

* **node link:** always load `recalculated` state by default ([94e9066](https://gitlab.com/hestia-earth/hestia-ui-components/commit/94e906646585764151bb931a21ea8070b6744cd0))
* **node value details:** show `operation` in table ([79a02ef](https://gitlab.com/hestia-earth/hestia-ui-components/commit/79a02ef51d99c327581f9636aa58f7d72db98350))


### Bug Fixes

* **cycles:** fix emissions logs on transformations ([87fde7d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/87fde7d63584d10509bebfc73040c47d8d3fbf9e))
* **node logs:** fix log status ([7c78159](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7c7815932016a6a9d36ac3513eba638cff943f92))
* **node logs:** skip keys without running models ([a67c2f2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a67c2f22a0961889a43a8c2064a02e2948da7ade))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.12...v0.3.0) (2022-08-04)


### ⚠ BREAKING CHANGES

* `selected` param has been removed on all nodes components

### Features

* **node value details:** show `operation` ([f986ab8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f986ab8c76270506266a9d18dfd9bbe477924a8d)), closes [#64](https://gitlab.com/hestia-earth/hestia-ui-components/issues/64)


* remove selection on nodes in components ([2bbbd7d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2bbbd7dd4749c54457c1d7defd18023eeb8e9df7))

### [0.2.12](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.11...v0.2.12) (2022-08-04)


### Bug Fixes

* **node link:** fix wrong url for aggregated data ([3830f14](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3830f14b942688bc86d48c9a191c713bf00cbe59))

### [0.2.11](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.10...v0.2.11) (2022-08-03)


### Features

* add select node for logs ([5f01f87](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5f01f879f0ad1f77f1685ec45783e188b5dd6eea))
* **files error:** handle additional validation errors ([8b08f2d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8b08f2d982ff41dff2a5cf2a12d5f7e67c9b02c3))
* **node link:** send to recalculated for `aggregated` data ([982a1cd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/982a1cde800e84798aa6a0a932266ce7570db4c1))
* **node logs model:** handle displays of parallel values ([ce396c6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ce396c69ba1d9a1ff581cbe8b756dd327d506073)), closes [#54](https://gitlab.com/hestia-earth/hestia-ui-components/issues/54)
* **node logs model:** show documentation link when possible ([d094a28](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d094a28071ff79ab21817d97b21bc416372a74c9)), closes [#35](https://gitlab.com/hestia-earth/hestia-ui-components/issues/35)


### Bug Fixes

* **node logs models:** fix show all blank nodes without filtering by `termType` ([0587355](https://gitlab.com/hestia-earth/hestia-ui-components/commit/05873555ed87dd91e63b182484925e31da93d01c))
* **node value details:** fix hide terms in table ([53be257](https://gitlab.com/hestia-earth/hestia-ui-components/commit/53be257442e503bc742d0c1b50b7ab8d2ee02685)), closes [#58](https://gitlab.com/hestia-earth/hestia-ui-components/issues/58)
* **sites:** handle different measurements with multiple values and depths ([799c958](https://gitlab.com/hestia-earth/hestia-ui-components/commit/799c958b6d9523db5dfd9f3b61414e6e67603cec)), closes [#57](https://gitlab.com/hestia-earth/hestia-ui-components/issues/57)

### [0.2.10](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.9...v0.2.10) (2022-07-26)


### Bug Fixes

* **impact assessments products:** show all when filter model is disabled ([49a234d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/49a234d639eee2133d4d3443b5ccea9721a63562))

### [0.2.9](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.8...v0.2.9) (2022-07-26)


### Bug Fixes

* **node logs models:** fix requirements not displayed on parallel models ([dc2bc2b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/dc2bc2b618108c67d117c98856c40c435debf0a8))
* **node:** skip terms without `defaultModelId` mapping ([ab4b576](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ab4b5768ba6295b712a04cca04108cf790625dc3))
* **site maps:** fix wrong bounds on multi polygons ([a984d9a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a984d9a891208f404443723f52a9638b0cfa8dcf)), closes [#39](https://gitlab.com/hestia-earth/hestia-ui-components/issues/39)

### [0.2.8](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.7...v0.2.8) (2022-07-20)


### Bug Fixes

* **impact assessments products logs:** skip list all terms for emissions and ci ([c411dec](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c411dec403165a124d0b40bb9e705f65df63779c))
* **node logs models:** filter out only non-required logs ([41aab69](https://gitlab.com/hestia-earth/hestia-ui-components/commit/41aab69e1c372a6044e90c07a750e4c266b35b9a)), closes [#52](https://gitlab.com/hestia-earth/hestia-ui-components/issues/52)
* **terms:** rename `Production Practices` to `Practices` ([d38fcfd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d38fcfd3ee5b949a2a5d425385fbc19988ce245c))

### [0.2.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.6...v0.2.7) (2022-07-19)


### Features

* **impact assessment breakdown:** enable for `endpoints` ([ebb38b2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ebb38b2053de530a955e5d14fff4f426b837220d))
* **node logs models:** show all `properties` with a model ([175008e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/175008ecb04730779b8f97574948f9794169fed8)), closes [#51](https://gitlab.com/hestia-earth/hestia-ui-components/issues/51)
* **node value details:** show `properties` ([d04ddb9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d04ddb9ca012a0dfd364d66a4dd524bbdd4bcd0c)), closes [#46](https://gitlab.com/hestia-earth/hestia-ui-components/issues/46)


### Bug Fixes

* **files form:** handle schema type is `undefined` ([94e44bd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/94e44bdd78bbe9c0dbcc7de3eb6278db7f9d1b83))
* **node logs models:** handle not required no run orchestrator ([df208ba](https://gitlab.com/hestia-earth/hestia-ui-components/commit/df208baf2a11be65c424655c6c9ad8e36cb69b17)), closes [#50](https://gitlab.com/hestia-earth/hestia-ui-components/issues/50)

### [0.2.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.5...v0.2.6) (2022-07-14)


### Features

* **files error:** handle `model` used to calculate measurement ([153629c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/153629cef83afba2ce3a009ff1ac8b579526b3c5))
* **files error:** handle error impact linked to cycle missing products ([22e63e5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/22e63e5511c4c264b9c66e6857eadb3c83b1ef89))
* **node logs model:** allow partial match on filtered term ([c361c18](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c361c18a90c2e3449c703262da548be9dda61f75)), closes [#49](https://gitlab.com/hestia-earth/hestia-ui-components/issues/49)


### Bug Fixes

* **node logs model:** show all models when not filtering by type ([34db4b2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/34db4b2d9f6e9eb81774f2c5330135d0b3b23537)), closes [#47](https://gitlab.com/hestia-earth/hestia-ui-components/issues/47)

### [0.2.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.4...v0.2.5) (2022-07-13)


### Bug Fixes

* **files error:** fix message on count ([b20113a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b20113a36f2089d40b7e3a0369bd011edd7f44c0)), closes [#44](https://gitlab.com/hestia-earth/hestia-ui-components/issues/44)
* **node logs model:** fix `not required` when all models are not required ([8980cbc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8980cbcf7ff4c5a76546f1c173606ada37bb10b7))
* **node logs model:** fix order of models when order not found in config ([1642a81](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1642a811fe5d2be17ea87c6ece568d456a3c5751))
* **node logs model:** show info for model when filtered ([796cfbf](https://gitlab.com/hestia-earth/hestia-ui-components/commit/796cfbfc337110638e3fbf05db8290d35a3d0898)), closes [#43](https://gitlab.com/hestia-earth/hestia-ui-components/issues/43)

### [0.2.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.3...v0.2.4) (2022-07-12)


### Bug Fixes

* **cycles practices:** restrict timeline to `operation` only ([a419358](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a419358397ad7e9c378b994f02e37f59373ed42b))

### [0.2.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.2...v0.2.3) (2022-07-11)


### Features

* **maps:** handle display of `Polygon` and `MultiPolygon` ([7c59125](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7c59125f7e099f49fd43075409fcc3a705813e0a))
* **node logs models:** show units ([ce87f29](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ce87f2915594121b8ab8317250659f9a3c37aad7)), closes [#42](https://gitlab.com/hestia-earth/hestia-ui-components/issues/42)


### Bug Fixes

* **impact assessments logs:** restrict emissions model ([547ab74](https://gitlab.com/hestia-earth/hestia-ui-components/commit/547ab74373f341377d7d084efb116b2aa3db191c))
* **link key value:** restrict array height with scroll ([db5eea3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/db5eea34c4bd24465ac2b0bc86a480d14c4ff546)), closes [#41](https://gitlab.com/hestia-earth/hestia-ui-components/issues/41)
* **node logs model:** show as `skipped` when no log on `shouldRun` ([0f4c8b8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0f4c8b8939508b36d0bad6ce6a81a4a0ac56af56))
* **site maps:** wait for `google.maps` library to be available ([026fb74](https://gitlab.com/hestia-earth/hestia-ui-components/commit/026fb74fa60e7f03e5ff48169f8281f226ffd7bd))

### [0.2.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.1...v0.2.2) (2022-07-08)


### Features

* **impact assessments breakdown:** add `Method` and units to CSV download ([48c67a3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/48c67a346ae46c77a7c241236f56f1f92580fd9b)), closes [#40](https://gitlab.com/hestia-earth/hestia-ui-components/issues/40)


### Bug Fixes

* **cycles practices timeline:** show "no date" for `operation` only ([d0e22c9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d0e22c906eb1aa0c74fdb075ee1257bc2d832d31)), closes [#38](https://gitlab.com/hestia-earth/hestia-ui-components/issues/38)
* **sites maps:** force set center on load ([c0553bd](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c0553bd775ddf4553528f45af8a0f5be45277a73)), closes [#39](https://gitlab.com/hestia-earth/hestia-ui-components/issues/39)

### [0.2.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.2.0...v0.2.1) (2022-07-04)


### Features

* **cycles practices:** add timeline view ([ba64a07](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ba64a07440900f8822e43dd38d3b88ba1411e1c2)), closes [#38](https://gitlab.com/hestia-earth/hestia-ui-components/issues/38)
* **files error:** handle emission linked `inputs` missing ([9efdc63](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9efdc6327512488f35c0987cf196f1391c09020e))
* **files error:** handle emission linked `transformation` missing ([37443e5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/37443e58ddb41aafdd11248fd2ffa8357d6a29e9))
* **files error:** handle error for `coverCrop` ([5425c01](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5425c017cf9076b5804ee45d6c068bebe9d5f388))
* **files error:** handle properties not required ([bb3e86b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bb3e86b0a7675dd24e4fcd083b3c06ded26235eb))
* **node details:** show `transformation` in table if any ([f3f672c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f3f672cefe07dfc1aa9605b8137acd09ef8fb9ef))


### Bug Fixes

* **blank node state notice:** show except for `original` state ([6077809](https://gitlab.com/hestia-earth/hestia-ui-components/commit/607780910544fc095b958c723ba708be06ceeaad))
* **node logs:** remove colour coding on logs details ([14fa315](https://gitlab.com/hestia-earth/hestia-ui-components/commit/14fa3154753a8e156ff2eff6661a4e6bfb65a19a)), closes [#37](https://gitlab.com/hestia-earth/hestia-ui-components/issues/37)

## [0.2.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.1.6...v0.2.0) (2022-07-01)


### ⚠ BREAKING CHANGES

* **terms:** schema min version is `9.0.0`

### Features

* **terms:** add `grass` to products list ([0c1879a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0c1879abd164d20d5b1be28f2b4a355c6cd8fea2))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.1.5...v0.1.6) (2022-06-30)


### Features

* **files error:** handle error missing linked nodes ([c3eaa2e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c3eaa2e92b9b70983656eb96b9cd1df6c0aaa006))
* **files error:** handle error Transformation with input not in Cycle ([f75b5d6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/f75b5d6e5337bdeec560033e982db576017f4fac))
* **issue-confirm:** add link to `front-end` ([7f720ce](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7f720ceb4fab027402f3c2012142f65ffd003ab6))
* **issue-confirm:** skip community from input ([b78cf34](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b78cf34bf1ed037868cc7ec4d6a928eedc2cc9ed))


### Bug Fixes

* **terms:** remove `biodiversity` as no longer included ([6e8dc38](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6e8dc38f293ec45d48e01e119c5ab4c5d3fa3055)), closes [#36](https://gitlab.com/hestia-earth/hestia-ui-components/issues/36)

### [0.1.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.1.4...v0.1.5) (2022-06-28)


### Features

* add border when splitting data in table ([a7b0424](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a7b0424e959dfdbda50c52f1be2fe7eeb0e75930)), closes [#32](https://gitlab.com/hestia-earth/hestia-ui-components/issues/32)
* **node logs models:** show only relevant terms ([99bc78d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/99bc78d31661494ae728c82af5b69f150ffc0401)), closes [#30](https://gitlab.com/hestia-earth/hestia-ui-components/issues/30)


### Bug Fixes

* **cycles activity:** skip products/inputs header when no values ([0bf121a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0bf121af75d5f7488c58199eb7fbc0e9ecef77ac))
* **node log models:** fix input value not showing ([a3cb8fe](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a3cb8fe43b95624f2592857706fbcbeaf2a26d88)), closes [#31](https://gitlab.com/hestia-earth/hestia-ui-components/issues/31)

### [0.1.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.1.3...v0.1.4) (2022-06-27)


### Features

* **cycles activity:** add headers products/inputs ([297e697](https://gitlab.com/hestia-earth/hestia-ui-components/commit/297e697b22e8aa35d30b69c77f9cea5190c9457f)), closes [#28](https://gitlab.com/hestia-earth/hestia-ui-components/issues/28)
* **node logs models:** rename `transformation` model ([a45555e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a45555e74a4e0c57a27bce7af1cea0742fcb6843)), closes [#29](https://gitlab.com/hestia-earth/hestia-ui-components/issues/29)


### Bug Fixes

* **data-table:** account for scrollbar height when `nbRows` provided ([400ecfe](https://gitlab.com/hestia-earth/hestia-ui-components/commit/400ecfe41f92fa1068db26e6e625567ef1f04916)), closes [#27](https://gitlab.com/hestia-earth/hestia-ui-components/issues/27)
* **files form:** make sure "Link with" is removed when selecting value ([4d44fdc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4d44fdc68e1b2de2d5eb0403639649465cee8350))
* **node logs:** replace `not updated` by `-` ([55f8a76](https://gitlab.com/hestia-earth/hestia-ui-components/commit/55f8a76051a9c68fdc3b16d1139eeb3fb183ba5b))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.1.2...v0.1.3) (2022-06-23)


### Features

* **cycles:** handle using `Transformation` as a `Cycle` ([b63a100](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b63a1007f543779e1d70169ce50956f3d078d2c5))
* **node logs models:** change `not required to run` to `not relevant` ([b57152b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b57152b78921370c5f8dcd34cd81da8df5610696)), closes [#26](https://gitlab.com/hestia-earth/hestia-ui-components/issues/26)


### Bug Fixes

* **data-table:** fix aligment on first col ([3f72966](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3f729669755bcd66e8320097041751041939f6b2)), closes [#24](https://gitlab.com/hestia-earth/hestia-ui-components/issues/24)
* display empty data-table correctly ([c0f7c04](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c0f7c043bdd47d08ebca5fa69fa74192cb7a5203))
* **node logs model:** remove `transformation` ([b1ad70c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b1ad70c7146e12fc47b6047b5aa2c83a4a5a52f1)), closes [#25](https://gitlab.com/hestia-earth/hestia-ui-components/issues/25)
* **sites maps:** handle map not loaded yet ([15f3e09](https://gitlab.com/hestia-earth/hestia-ui-components/commit/15f3e0941521a5d554f6743ddfca1d336852ff33))
* **sites maps:** remove set zoom when using `fitBounds` ([7a75810](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7a75810d6a59d3b78298bf858a82c70a8b7ff64e)), closes [#23](https://gitlab.com/hestia-earth/hestia-ui-components/issues/23)

### [0.1.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.1.1...v0.1.2) (2022-06-21)


### Features

* **common:** add `issue-confirm` component ([5d5ded1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5d5ded1c890f7beaf3e78f439e2adbbc70db694c))
* transform into data-table with fixed rows and columns ([6a06067](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6a06067c4aba0bf8f1924b84b54af185e29d90d9))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.1.0...v0.1.1) (2022-06-16)


### Bug Fixes

* **files form:** remove change detection on push only ([ff5ee38](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ff5ee38af0c7d50a7df3c154bfc740b50dcea6a9))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.34...v0.1.0) (2022-06-14)


### ⚠ BREAKING CHANGES

* **package:** min schema version is `8.0.0`, api is `0.10.0`

* **package:** update schema to `8.0.0` ([a0b32a7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/a0b32a75522f3be1a9e2e5b5c7d416143af6743b))

### [0.0.34](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.33...v0.0.34) (2022-06-09)


### Bug Fixes

* **files form:** enable recommendations if editable only ([689c60d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/689c60d8d78c58ec085391203b66e9a9ee267502))

### [0.0.33](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.32...v0.0.33) (2022-06-09)


### Features

* **impact assessments products:** filter by models default ([4ad2ba9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4ad2ba9ab3a401c50f4c9eedfa527ba927317a11))
* **node recommendations:** add toggle show/hide ([19a493c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/19a493cfb0b9b39b84cd9b1d94605a5982b765be))


### Bug Fixes

* **node recommendations:** find first primary product with term `[@id](https://gitlab.com/id)` ([c4d06ae](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c4d06ae788555be241c90d31724e89e897705350))
* **node recommendations:** sort by `[@id](https://gitlab.com/id)` ([1c5cfdb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1c5cfdb187d212d039d2a03f38dc4d02733cc451))

### [0.0.32](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.31...v0.0.32) (2022-06-07)


### Features

* **files form:** add recommendations for arrays ([2de399e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2de399e53ca535ebc1df0cb93dd44f18998052d1))

### [0.0.31](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.30...v0.0.31) (2022-06-07)


### Features

* **engine:** add function to get models term ids ([d839f17](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d839f1784bbd01b03d1cba7328b620004c355c91))


### Bug Fixes

* fix table view showing with no other options ([7f47120](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7f471209565b7769a46a4e5f103910052405d884)), closes [#21](https://gitlab.com/hestia-earth/hestia-ui-components/issues/21)
* **node link:** remove vertical centering ([9c11455](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9c1145577cea06b8113673fe56b6f11a2e5c7569)), closes [#20](https://gitlab.com/hestia-earth/hestia-ui-components/issues/20)

### [0.0.30](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.29...v0.0.30) (2022-06-03)

### [0.0.29](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.28...v0.0.29) (2022-06-02)


### Bug Fixes

* **files form:** fix error selecting `Link with` node ([4c1c6d5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4c1c6d5164bc2270357bbc337caae0c8a8a04d71))
* **node logs model:** handle emission without `inputs` ([bb958a5](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bb958a5527f035f97841373f6e1a10247f9d7711))

### [0.0.28](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.27...v0.0.28) (2022-06-01)


### Features

* **engine requirements:** add form ([fa51aa8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/fa51aa8a741169b3cdf001d376eb086b13b4541a))
* **node logs:** display `input` sub value ([2adf914](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2adf914bd5122f34791515fbfcf30970539fc32c)), closes [#17](https://gitlab.com/hestia-earth/hestia-ui-components/issues/17)


### Bug Fixes

* **ia indicator:** remove logs without emission ([8a25756](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8a257561c798900ee43f7237ecc06dcd7074e678))
* **node logs models:** remove background data in Inputs ([47ed599](https://gitlab.com/hestia-earth/hestia-ui-components/commit/47ed5992c60fe9506f64fe46a03084866e82ad41))
* **node logs:** fill-in empty cells ([3c9c4d3](https://gitlab.com/hestia-earth/hestia-ui-components/commit/3c9c4d37b102ceece59e5010c636936e9ee6ecdd)), closes [#18](https://gitlab.com/hestia-earth/hestia-ui-components/issues/18)

### [0.0.27](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.26...v0.0.27) (2022-05-27)


### Features

* **impact assessment breakdown:** download inputs value in CSV ([1da2e7b](https://gitlab.com/hestia-earth/hestia-ui-components/commit/1da2e7b3ca01800a31c4466b94be93b4417c53b1))
* **node logs model:** handle models in parallel ([4817124](https://gitlab.com/hestia-earth/hestia-ui-components/commit/481712466edf9c5b4b4c192467b571d4818738c1))
* **node logs models:** handle transformations ([b7eb03e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b7eb03e4f4875b0e012d456f17d5e56d198e7a1a))


### Bug Fixes

* **blank node state:** handle multiple values with different state ([117bf10](https://gitlab.com/hestia-earth/hestia-ui-components/commit/117bf1078f1c41cab384cf881a743390bfc10f74))
* **impact assessment products:** fix incorrect functional unit ([75df9d2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/75df9d28c689fb9e72e0c859e2555ca8d028ede7))

### [0.0.26](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.25...v0.0.26) (2022-05-16)


### Features

* **link key value:** handle display of arrays ([668a823](https://gitlab.com/hestia-earth/hestia-ui-components/commit/668a823ece419e5bf93cd892761a733868fb4a31))


### Bug Fixes

* **engine orchestrator:** show each arg per line ([47cdbe8](https://gitlab.com/hestia-earth/hestia-ui-components/commit/47cdbe8aea5bb10698668f6bd50ac9f968bfc70d))

### [0.0.25](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.24...v0.0.25) (2022-05-13)


### Features

* **engine:** add orchestrator edit component ([9983916](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9983916f2b2c8df834f9e22ab377d4fa3ca73fee))

### [0.0.24](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.23...v0.0.24) (2022-04-23)


### Features

* **files form:** add `showSuggestedDefaultProperties` parameter ([2fc610e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2fc610eea48383fb6820877e5f3155829d9f946b))

### [0.0.23](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.22...v0.0.23) (2022-04-21)


### Features

* **files error:** improve warning message for data completeness ([ad867e0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/ad867e08ccdde4489e4f3db66ea32c2a86e768c2))


### Bug Fixes

* **cycles completeness:** remove `type` field ([38b7269](https://gitlab.com/hestia-earth/hestia-ui-components/commit/38b72699423dca4945b921ace346e1e04045631a))
* **node logs models:** fallback finding models using `model-links.json` file ([7f146a6](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7f146a67f762e36e7118a6b2538e0dbdf439bc15))
* **node logs models:** fix summing of null values ([7f2bebc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7f2bebcc3c358efe0c431320b8c8a0ec9d3bb0db))

### [0.0.22](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.21...v0.0.22) (2022-04-15)


### Features

* **node csv headers:** toggle `internal` fields ([122870f](https://gitlab.com/hestia-earth/hestia-ui-components/commit/122870f3ff61b4bf55b845505477238b3ba40d66))

### [0.0.21](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.20...v0.0.21) (2022-04-15)


### Features

* **cycles:** remove suggest add aggregated cycle ([6cd467e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6cd467e1d352f948863c3c326388231b95842731))
* **impact assessments:** handle selection of nodes and remove add aggregated ([b1898fa](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b1898fafe1c1074187bb49ed74f04fce40be64b2))
* **sites:** handle selection of nodes ([d5e9576](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d5e9576bdd31042d05dab58987284d2ef2a2375a))


### Bug Fixes

* **files error:** fix typo in error length of values ([9d13286](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9d13286ca317252a55e37e0748774471c229c57c))

### [0.0.20](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.19...v0.0.20) (2022-04-12)


### Bug Fixes

* **files error:** fix error number of values no match ([20fb83d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/20fb83d9f0fa5f0eb08ac5c347f27f1059bf2c2d))

### [0.0.19](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.18...v0.0.19) (2022-04-09)


### Features

* **node logs models:** handle status `not required` ([801bffc](https://gitlab.com/hestia-earth/hestia-ui-components/commit/801bffcfaf05ba92bd9d148d6a9ba2b84e6f5d97))

### [0.0.18](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.17...v0.0.18) (2022-04-07)


### Features

* **blank node state notice:** show "unchanged" status ([999ad4a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/999ad4a7f594dc59de2876bb013cbbb33b6e3bc6))
* **blank node state:** handle `unchanged` state ([194d408](https://gitlab.com/hestia-earth/hestia-ui-components/commit/194d408c3c92ba66cfed11e732da7853be0184f2))


### Bug Fixes

* **files error:** set correct path for check number of values ([83efb49](https://gitlab.com/hestia-earth/hestia-ui-components/commit/83efb49e17788bb3e7bae7823fdaf5bef891aad6))

### [0.0.17](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.16...v0.0.17) (2022-04-07)


### Features

* **files error:** handle pattern date and time ([5fe9ed9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/5fe9ed9748dbdb61c961de4872e838668aacc950))
* **utils:** use `arrayTreatment` in `propertyValue` to average values ([c1db01a](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c1db01aafe1b2a45b4dc9a3852c9b6a77b9371bb))


### Bug Fixes

* **cycles practices:** add missing table style ([bec07c7](https://gitlab.com/hestia-earth/hestia-ui-components/commit/bec07c70351b523224694439aee05757a49b1370))

### [0.0.16](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.15...v0.0.16) (2022-04-06)


### Features

* **files error:** handle error messages no product value ([7f502c9](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7f502c95ea417f8293de65b2ea815b152551755e))


### Bug Fixes

* **cycles:** fix inline of term id in table ([61f3b7d](https://gitlab.com/hestia-earth/hestia-ui-components/commit/61f3b7d08e0c6397d4d256f3ae4047d7ee5eed24))

### [0.0.15](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.14...v0.0.15) (2022-04-05)


### Features

* **node logs model:** add link to `property` and `input` ([d880843](https://gitlab.com/hestia-earth/hestia-ui-components/commit/d8808439dcccd04b5a100005977e86c56030beb1))


### Bug Fixes

* **fontawesome:** add missing `circle` icon ([479eac1](https://gitlab.com/hestia-earth/hestia-ui-components/commit/479eac1189609779ba141f9a435865c0c406178a))

### [0.0.14](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.13...v0.0.14) (2022-04-04)


### Features

* **cycle completeness:** hide aggregated notice ([6b13fed](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6b13fedfe606a1a9d8766117534b2048c06c396c))
* **node logs model:** handle model skipped and background data ([96e8ce0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/96e8ce0be269bc3a1020b374ef469f300c54c7b9)), closes [#3](https://gitlab.com/hestia-earth/hestia-ui-components/issues/3) [#4](https://gitlab.com/hestia-earth/hestia-ui-components/issues/4)
* **node logs models:** show different icon for non-updated value ([9c5acca](https://gitlab.com/hestia-earth/hestia-ui-components/commit/9c5acca2c7633f9a97eddf866438a20e679f4036))


### Bug Fixes

* **utils:** handle no `location.origin` ([2213455](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2213455c90e63f3302d246c3b6e30ebc747e21d1))

### [0.0.13](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.12...v0.0.13) (2022-03-31)


### Features

* **cycles:** add `he-cycles-practices` component ([160cf1c](https://gitlab.com/hestia-earth/hestia-ui-components/commit/160cf1c3b7a16f08a8b21a4138cabb63ac89da34))


### Bug Fixes

* **node csv headers:** toggle term headers on toggle all ([e6ead14](https://gitlab.com/hestia-earth/hestia-ui-components/commit/e6ead14f105a23d4540f1b2bd0e35423363da5ef))

### [0.0.12](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.11...v0.0.12) (2022-03-29)


### Bug Fixes

* **files form:** fix generic types suggestions not showing ([2445d7e](https://gitlab.com/hestia-earth/hestia-ui-components/commit/2445d7e65cf281c87d658baeee1ce14f6755298d))

### [0.0.11](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.10...v0.0.11) (2022-03-28)


### Features

* **files:** build error summary data ([6c1c3a2](https://gitlab.com/hestia-earth/hestia-ui-components/commit/6c1c3a25c7ca64536c2a6c88297a613e2ceadad7))

### [0.0.10](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.9...v0.0.10) (2022-03-24)


### Bug Fixes

* **tags input:** import component to avoid universal import error ([c12fc34](https://gitlab.com/hestia-earth/hestia-ui-components/commit/c12fc34aac75928f03f475ab379c7440a61a3a40))

### [0.0.9](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.8...v0.0.9) (2022-03-24)


### Features

* **common:** split generic component/pipes into a lighter module ([b376770](https://gitlab.com/hestia-earth/hestia-ui-components/commit/b376770b22f54badb288052adda0fa701e2cf10b))

### [0.0.8](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.7...v0.0.8) (2022-03-24)


### Bug Fixes

* **engine service:** fix injected token for config url ([8342d75](https://gitlab.com/hestia-earth/hestia-ui-components/commit/8342d75a78a9335f200b89d530f74c5e4eab6337))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.6...v0.0.7) (2022-03-24)


### Features

* **common:** export delta utils ([89a19df](https://gitlab.com/hestia-earth/hestia-ui-components/commit/89a19df0e58bbcaf2e718bdff1a002c3e716d41b))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.5...v0.0.6) (2022-03-24)


### Bug Fixes

* **engine:** export all functions ([7d364c0](https://gitlab.com/hestia-earth/hestia-ui-components/commit/7d364c07b590a61ae6f433ffe68157b0b34faf5f))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.4...v0.0.5) (2022-03-24)


### Bug Fixes

* **common:** rename `ToastService` in `HeToastService` ([28fb5bb](https://gitlab.com/hestia-earth/hestia-ui-components/commit/28fb5bb2fd890bb664b84286f0424f006a768c14))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.3...v0.0.4) (2022-03-23)


### Bug Fixes

* **styles:** add missing bulma-tagsinput styles ([0945379](https://gitlab.com/hestia-earth/hestia-ui-components/commit/0945379b343a0fa553c238e53c50509fd7818a61))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.2...v0.0.3) (2022-03-23)


### Features

* **common:** export functions for calculating delta between values ([4723e52](https://gitlab.com/hestia-earth/hestia-ui-components/commit/4723e52f0dd99ff2e7bda628221dba9105957390))


### Bug Fixes

* remove localized messages ([dc7da17](https://gitlab.com/hestia-earth/hestia-ui-components/commit/dc7da17641af78c80739ca115f8ecf80124a6534))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-ui-components/compare/v0.0.1...v0.0.2) (2022-03-23)

### 0.0.1 (2022-03-23)
